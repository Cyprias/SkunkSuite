/********************************************************************************************\
	File:           SkunkTimer-1.0.js
	Purpose:        Timer handler for Skunkworks.
	Creator:        Cyprias
	Date:           09/17/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkTimer-1.0";
var MINOR = 190723;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkTimer10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	
	core.Error = Error; // Default to built in error object. This allows other scripts to replace it if need be.

	var maxMasterDuration = 10 * 1000;

	var debugging = false;
		
	core.debug = function debug(/*args*/) {
		if (debugging != true) return;
		core.consoleLog.apply(core, arguments);
	};
	
	core.consoleLog = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
	
		for (var i = 0; i < args.length; i++) {
			if (typeof args[i] === "object" && args[i] !== null && typeof args[i].toString === "function" && args[i].toString() === "[object Object]") {
				args[i] = JSON.stringify(args[i]);
			}
		}
	
		skapi.OutputLine(core.MAJOR + " " + args.join('\t'), opmConsole);
	};
	
	function embedMixins(script) {
		var scriptTimers = [];
		script.setTimeout = function setTimeoutWrapper(/*method, miliseconds, args...*/) {
			var tid = setTimeout.apply(undefined, arguments);
			scriptTimers.push(tid);
			return tid;
		};
		script.setInterval = function setIntervalWrapper(/*method, miliseconds, args...*/) {
			var tid = core.setInterval.apply(undefined, arguments);
			scriptTimers.push(tid);
			return tid;
		};
		script.setImmediate = function setImmediateWrapper(/*method, args...*/) {
			var tid = core.setImmediate.apply(undefined, arguments);
			scriptTimers.push(tid);
			return tid;
		};
		
		script.cancelAllTimers = function cancelAllTimers() {
			var tid;
			for (var i = scriptTimers.length - 1; i >= 0; i--) {
				tid = scriptTimers[i];
				clearTimeout(tid);
			}
			scriptTimers = [];
		};
		
		script.clearTimeout = function clearTimeoutWrapper(tid) {
			for (var i = scriptTimers.length - 1; i >= 0; i--) {
				if (scriptTimers[i] == tid) {
					scriptTimers.splice(i, 1);
				}
			}
			clearTimeout(tid);
		};
		
		script.clearInterval = script.clearTimeout;
		script.clearImmediate = script.clearTimeout;
		
		if (typeof script.on === "function") {
			script.on("onDisable", function() {
				script.cancelAllTimers();
			});
		} else {
			var oldOnDisable = script.onDisable;
			script.onDisable = function onDisable() {
				script.cancelAllTimers();
				if (oldOnDisable) oldOnDisable();
			};
		}
		
		return script;
	}

	core.embeds = core.embeds || {};
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	if (typeof core.emitter !== "undefined") {
		core.emitter.on("onEmbedDisable", function(script) {
		//	core.consoleLog("[onEmbedDisable]");
			script.cancelAllTimers();
		});
	}

	var profiler;
	core.setProfiler = function setProfiler(value) {
		profiler = value;
	};
	
	(function() {
		//////////////////////////////////////
		// Global timer functions.
		//////////////////////////////////////
		
		core.globalTimers = core.globalTimers || [];

		function UUID() {
			//https://stackoverflow.com/a/8809472
			var d = new Date().getTime();
			var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = (d + Math.random() * 16) % 16 | 0;
				d = Math.floor(d / 16);
				return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16).toUpperCase();
			});
			return uuid;
		}

		// Setup our timer event.
		var handler = {};
		handler.OnTimer = function OnTimer(timer) {
			core.debug(core.MAJOR + " <SkunkTimer.OnTimer> " + timer.tag);
			if (timer.tag != masterTimer.tag) return;

			flushTimers();
			core.debug(core.MAJOR + " globalTimers: " + core.globalTimers.length);
			if (core.globalTimers.length > 0) {
				//core.debug(core.MAJOR + " Calling resetMasterTimer()...");
				resetMasterTimer();
			}
		};
		skapi.AddHandler(evidOnTimer, handler);
			
		// Create a main timer.
		var masterTimer = skapi.TimerNew();
		masterTimer.tag = MAJOR + "." + MINOR;
		
		//function compareTimers(a, b) {return b.when - a.when;};
		
		var lastResetWhen = 0;
		function insertTimer(timer) {
			core.debug(core.MAJOR + " <insertTimer> masterTimer: " + masterTimer.cmsec);
			core.globalTimers.splice(0, 0, timer);
			
			if (masterTimer.cmsec > maxMasterDuration) {
				// Our timer is supposed to fire every 10 seconds or so. If its last fire is past that, there's a problem. Likely our OnTimer handler was wiped by some other script calling skapi.RemoveHandler(evidNil, undefined).
				core.debug(core.MAJOR + " cmsec is above maxMasterDuration! " + masterTimer.cmsec);
				core.consoleLog(core.MAJOR + " cmsec is above maxMasterDuration! " + masterTimer.cmsec);
				
				// Reset our handler
				skapi.RemoveHandler(evidOnTimer, handler);
				skapi.AddHandler(evidOnTimer, handler);
				lastResetWhen = 0;
			}

			// Check if we need to set our SW timer to fire sooner than previously set.
			if (!lastResetWhen || timer.when < lastResetWhen) {
				lastResetWhen = timer.when;
				resetMasterTimer();
			}
		}
		
		function getEarliestTimer() {
			var timer = core.globalTimers[0];
			var t;
			for (var i = 0; i < core.globalTimers.length; i++) {
				t = core.globalTimers[i];
				if (t.when < timer.when) {
					timer = t;
				}
			}
			return timer;
		}

		function resetMasterTimer() {
			core.debug(core.MAJOR + " <resetMasterTimer> " + core.globalTimers.length);
			if (core.globalTimers.length == 0) return;
			var timer = getEarliestTimer();
			var diff = (new Date()).getTime() - timer.when;
			masterTimer.cmsec = Math.max(0 - maxMasterDuration, Math.min(diff, -1));
			core.debug(core.MAJOR + " diff: " + diff + ", cmsec: " + masterTimer.cmsec);
		}

		function flushTimers() {
			//var offtime = new Date().getTime() - fireTime; // how late we are in ms.
			core.debug(core.MAJOR + " <flushTimers>");

			lastResetWhen = 0;
			
			var t;
			var diff;
			var started;
			var elapsed;
			for (var i = core.globalTimers.length - 1; i >= 0; i--) {
				if (i >= core.globalTimers.length) {
					core.debug(core.MAJOR + " Timer " + i + " no longer exists.");
					continue;
				}
				t = core.globalTimers[i];

				// Skip timers which aren't supposed to fire yet.
				diff = t.when - (new Date()).getTime();
				if (diff > 0) continue;
				
				// Splice out the timer before executing it so if there's a error it won't repeat.
				if (t.repeat != true) {
					core.globalTimers.splice(i, 1);
				}
				
				//debug("Firing timer", t.miliseconds);
				core.debug(core.MAJOR + " Executing timer '" + t.methodName + "'...");
				started = new Date().getTime();
				t.method.apply(undefined, t.args);
				elapsed = new Date().getTime() - started;
				core.debug(core.MAJOR + " Execution of timer '" + t.methodName + "' complete... elapsed: " + elapsed);

				if (profiler) {
					profiler({method: t.method, args: t.args, elapsed: elapsed});
				}
				
				if (t.repeat == true) {
					t.when = (new Date()).getTime() + t.miliseconds;
					continue;
				}
				
				// Don't splice out the timer, it might have called clearTimeout itself which would cause our current index to not be accurate.
				//clearTimeout(t.id);
			}
			core.debug(core.MAJOR + " </flushTimers>");
		}
		
		core.getMethodName = function factory() {
			return function getMethodName(method) {
				var szMethod = "{anonymous}";// anon function
				if (method.toString().match(/function ([^\(]+)/)) {
					szMethod = RegExp.$1;
				} else if (method.name) {
					szMethod = method.name;
				}
				return szMethod;
			};
		}();
		
		var maxTimers = 0;
		
		// eslint-disable-next-line no-undef
		setTimeout = core.setTimeout = function setTimeout_() {
			core.debug(core.MAJOR + " <setTimeout>");
			
			var args = [].slice.call(arguments);
			var method = args.splice(0, 1)[0];
			var miliseconds = args.splice(0, 1)[0];

			if (typeof method !== "function") {
				skapi.OutputLine("<setTimeout> method is not a function!", opmConsole);
				throw new core.Error("method is not a function!");
			}

			var timer = {};
			timer.method = method;
			timer.methodName = debugging && core.getMethodName(method);
			timer.miliseconds = miliseconds;
			timer.args = args;
			timer.when = (new Date()).getTime() + miliseconds;
			timer.id = UUID();
			
			insertTimer(timer);
			
			if (core.globalTimers.length > maxTimers) {
				core.debug(core.MAJOR + " maxTimers: " + maxTimers + ", method: " + method);
				maxTimers = core.globalTimers.length;
			}
			
			return timer.id;
		};
		
		// eslint-disable-next-line no-undef
		setImmediate = core.setImmediate = function setImmediate_() {
			core.debug(core.MAJOR + " <setImmediate>");
			
			var args = [].slice.call(arguments);
			args.splice(1, 0, 1); // Insert a 1ms value at index 1;
			return setTimeout.apply(undefined, args);
		};

		// eslint-disable-next-line no-undef
		setInterval = core.setInterval = function setInterval_() {
			core.debug(core.MAJOR + " <setInterval>");
			
			var args = [].slice.call(arguments);
			var method = args.splice(0, 1)[0];
			var miliseconds = args.splice(0, 1)[0];

			if (typeof method !== "function") {
				skapi.OutputLine("<setInterval> method is not a function!", opmConsole);
				throw new core.Error("method is not a function!");
			}
			
			var timer = {};
			timer.method = method;
			timer.methodName = debugging && core.getMethodName(method);
			timer.miliseconds = miliseconds;
			timer.args = args;
			timer.when = (new Date()).getTime() + miliseconds;
			timer.id = UUID();
			timer.repeat = true;
			
			insertTimer(timer);

			if (core.globalTimers.length > maxTimers) {
				core.debug(core.MAJOR + " maxTimers: " + maxTimers + ", method: " + method);
				maxTimers = core.globalTimers.length;
			}
			
			return timer.id;
		};
		
		// eslint-disable-next-line no-undef
		clearTimeout = core.clearTimeout = function clearTimeout_(timerId) {
			core.debug(core.MAJOR + "<clearTimeout>");
			for (var i = core.globalTimers.length - 1; i >= 0; i--) {
				if (core.globalTimers[i].id == timerId) {
				//	debug("<clearTimeout", "Removing timer " + timerId, i, timers.length);
					core.globalTimers.splice(i, 1);
					break;
				}
			}
		};
		
		// eslint-disable-next-line no-undef
		clearInterval = core.clearInterval = clearTimeout;
		
		// eslint-disable-next-line no-undef
		clearImmediate = core.clearImmediate = clearTimeout;
	})();

	
	
	return core;
}));

/*
	var MyScript = LibStub("SkunkScript-1.0").newScript("MyScript", "SkunkTimer-1.0");

	MyScript.onInitialize = function onInitialize() {};
	MyScript.onEnable = function onEnable() {

		MyScript.setInterval(function(){ 
			skapi.OutputLine("Timer!", opmConsole);
		}, 1000);
		
	};
	MyScript.onDisable = function onDisable() {
		// Any timers registered via MyScript.setTimeout/setInterval/setImmediate will auto stop when MyScript.disable() is called.
	};

	function main() {
		MyScript.initialize();
		MyScript.enable();
		while (MyScript._enabledState == true) {
			skapi.WaitEvent(1000, wemFullTimeout);
		}
		console.StopScript(); // Prevent Skunkworks from sometimes crashing.
	}
*/