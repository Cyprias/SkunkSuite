/********************************************************************************************\
	File:           SkunkCache-1.0.js
	Purpose:        Key value storage for Skunkworks.
	Creator:        Cyprias
	Date:           10/17/2018
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkCache-1.0";
var MINOR = 210917;

(function (factory) {
	var deps = {};
	deps.EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(deps);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkCache10 = factory(deps);
	}
}(function (deps) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	
	var EventEmitter = deps.EventEmitter;
	
	/* eslint-disable */
	// OpenAsTextStream Modes
	var ForReading              = 1;  // Open a file for reading. You cannot write to this file
	var ForWriting              = 2;  // Open a file for writing
	var ForAppending            = 8;  // Open a file and write to the end of the file
	
	// OpenAsTextStream Formats
	var TristateFalse           = 0;  // Open the file as ASCII
	var Tristate                = -1; // Open the file as Unicode
	var TristateUseDefault      = -2; // Open the file using the system default 
	/* eslint-enable */
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	
	core.debugging = false;
	
	var fso;
	if (typeof jest === "undefined") {
		fso = new ActiveXObject("Scripting.FileSystemObject");
	}
	
	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(args.join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (core.debugging != true) return;
		skapi.OutputLine(core.MAJOR + " " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};
	
	core.Cache = function factory() {
		function Cache(params) {
			EventEmitter.call(this);
			this.data = params && params.data || {};
			this.changed = false;
			this.filePerKey = params && params.filePerKey || false;
			this.filesLastModified = {};
			this.keysChanged = {};
			this.path = params && params.path;
		}

		Cache.prototype = new EventEmitter();
		Cache.prototype.constructor      = Cache;

		Cache.prototype.save = function save(params) {
			var path = this.path = params && params.path || this.path;
			core.debug("<save>" + path);
			if (path) {
				if (this.filePerKey == true) {
					if (!fso.FolderExists(path)) {
						mkdirSync(path);
					}
					
					// Save each key as its own file.
					var json, filePath;
					for (var key in this.data) {
						if (!this.data.hasOwnProperty(key)) continue;
						core.debug("key: " + key, "changed: " + this.keysChanged[key]);
						if (this.keysChanged[key] != true) continue;
						filePath = path + "\\" + key + ".json";
						if (typeof this.data[key] === "undefined") {
							// Delete file.
							if (fso.FileExists(filePath)) {
								fso.DeleteFile(filePath);
							}
						} else {
							json = JSON.stringify(this.data[key], null, "\t");
							writeFileSync(filePath, json);
							this.filesLastModified[key] = getDateLastModified(filePath);
						}
						delete this.keysChanged[key];
					}
				
				} else {
					if (!existsSync(path)) {
						mkdirSync(fso.GetParentFolderName(path));
					}
					
					for (var key in this.data) {
						if (!this.data.hasOwnProperty(key)) continue;
						if (typeof this.data[key] === "undefined") {
							delete this.data[key];
						}
					}

					var json = JSON.stringify(this.data, null, "\t");
					writeFileSync(path, json);
					this.lastFileModified = getDateLastModified(path);
				}
				this.changed = false;
				this.emit("onSave");
			}
			return this;
		};

		Cache.prototype.load = function load(params) {
			var path = this.path = params && params.path || this.path;
			var defaults = params && params.defaults;
			
			if (this.filePerKey == true) {
				this.data = {};
				if (!fso.FolderExists(path)) {
					mkdirSync(path);
				}
				
				// Load each file as a key in our object.
				var f =   fso.GetFolder(path);
				var files = new Enumerator(f.files);
				var file, fileName, json;
				for (; !files.atEnd();   files.moveNext()) {
					file = files.item();
					fileName = fso.GetBaseName(files.item().Path);
					json = readFileSync(files.item().Path);
					if (json) {
						try {
							this.data[fileName] = JSON.parse(json);
						} catch (e) {
							if (defaults) {
								this.data[fileName] = defaults;
							} else {
								throw e;
							}
						}
						this.filesLastModified[fileName] = getDateLastModified(files.item().Path);
					}
					delete this.keysChanged[fileName];
				}
			} else {
				if (existsSync(path)) {
					var json = readFileSync(path);
					if (json) {
						try {
							this.data = JSON.parse(json);
						} catch (e) {
							if (defaults) {
								this.data = defaults;
							} else {
								core.console("Error parsing " + path);
								core.console("Error: " + e);
								throw e;
							}
						}
						this.lastFileModified = getDateLastModified(path);
						this.emit("onLoad");
					}
				}
			}
			this.changed = false;
			return this;
		};

		Cache.prototype.refresh = function refresh(params) {
			var path = this.path = params && params.path || this.path;
			var key = params && params.key;
			if (path && existsSync(path)) {
				if (this.filePerKey == true) {
					if (typeof key !== "undefined") {
						var filePath = path + "\\" + key + ".json";
						if (fso.FileExists(filePath)) {
							var file = fso.getFile(filePath);
							var fileLastModified = getDateLastModified(file.Path);
							if (!this.filesLastModified[key] || fileLastModified > this.filesLastModified[key]) {
								var json = readFileSync(file.Path);
								if (json) {
									try {
										this.data[key] = JSON.parse(json);
									} catch (e) {
										throw e;
									}
									this.filesLastModified[key] = getDateLastModified(file.Path);
								}
								delete this.keysChanged[key];
							}
						}
					} else {
						var f =   fso.GetFolder(path);
						var files = new Enumerator(f.files);
						var file, fileName, json, fileLastModified;
						for (; !files.atEnd();   files.moveNext()) {
							file = files.item();
							fileName = fso.GetBaseName(files.item().Path);
							fileLastModified = getDateLastModified(files.item().Path);
							if (!this.filesLastModified[fileName] || fileLastModified > this.filesLastModified[fileName]) {
								json = readFileSync(files.item().Path);
								try {
									this.data[fileName] = JSON.parse(json);
								} catch (e) {
									throw e;
								}
								this.filesLastModified[fileName] = getDateLastModified(files.item().Path);
								delete this.keysChanged[fileName];
							}
						}
					}
				} else {
					var fileLastModified = getDateLastModified(path);
					if (fileLastModified > this.lastFileModified) {
						this.load({
							path: path
						});
					}
				}
			}
			return this;
		};

		Cache.prototype.get = function get(key) {
			this.refresh({key: key});
			return this.data[key];
		};

		Cache.prototype.set = function set(key, value) {
			if (typeof key === "undefined") {
				throw new core.Error("KEY_UNDEFINED");
			}
			var prev = this.data[key];
			if (typeof prev === "object" && prev == value) {
				// Our internal object for this key was provided back to us. 
				// Likely they tried to modify the object and give it back, but then we can't see differences. Use the clone() function on the data before modifying it.
				throw new core.Error("SAME_OBJECT");
			}
			if (!this.equals(key, value)) {
				this.data[key] = value;
				this.emit("onSet", {
					key  : key,
					prev : prev,
					value: value
				});
				this.changed = true;
				this.keysChanged[key] = true;
			}
			return this;
		};

		Cache.prototype.equals = function equals(key, value, arrIgnore) {
			return core.equals(this.get(key), value, arrIgnore);
		};

		Cache.prototype.saveIfChanged = function saveIfChanged() {
			if (this.changed == true) {
				this.save();
			}
			return this;
		};
		
		// Return array of object keys.
		Cache.prototype.keys = function _keys() {
			if (this.filePerKey == true) {
				if (typeof this.path === "undefined") {
					throw new core.Error("No file path set.");
				}
				var keys = [];
				var f = fso.GetFolder(this.path);
				var files = new Enumerator(f.files);
				var file, fileName;
				for (; !files.atEnd();   files.moveNext()) {
					file = files.item();
					fileName = fso.GetBaseName(files.item().Path);
					keys.push(fileName);
				}
				return keys;
			}
			this.refresh();
			return getKeys(this.data);
		};
		
		Cache.prototype.clear = function clear() {
			this.data = {};
			if (this.filePerKey == true) {
				if (typeof this.path === "undefined") {
					throw new core.Error("No file path set.");
				}
				var f = fso.GetFolder(this.path);
				var files = new Enumerator(f.files);
				var file;
				for (; !files.atEnd();   files.moveNext()) {
					file = files.item();
					fso.DeleteFile(file.Path);
				}
			} else {
				this.save();
			}
			return this;
		};
		
		return Cache;
	}();
	
	function readFileSync(path, options) {
		if (!fso.FileExists(path)) {
			throw new core.Error(path + " does not exist.");
		}
		
		var mode = options && options.mode || ForReading;
		var format = options && options.format || TristateUseDefault;
		
		var file = fso.getFile(path);
		var hFile = file.OpenAsTextStream(mode, format);
		var dataString;
		if (!hFile.AtEndOfStream) {
			dataString = hFile.Read(file.Size); //http://www.4guysfromrolla.com/webtech/010401-1.shtml
		}
		hFile.close();
		return dataString;
	}
	
	function writeFileSync(path, data, options) {
		var mode = options && options.mode || ForWriting;
		var format = options && options.format || TristateUseDefault;
		var textStream = fso.OpenTextFile(path, mode, true, format);
		textStream.Write(data);
		textStream.Close();
	}
	
	function mkdirSync(path) {
		if (!fso.FolderExists(path)) {
			var parentFolder = fso.GetParentFolderName(path);
			if (parentFolder && !fso.FolderExists(parentFolder)) {
				mkdirSync(parentFolder);
			}
			fso.CreateFolder(path);
		}
	}
	
	function existsSync(path) {
		return fso.FileExists(path) || fso.FolderExists(path);
	}
	
	function getDateLastModified(path) {
		return new Date(fso.getFile(path).DateLastModified).getTime() / 1000;
	}
	
	function isArray(arr) {
		return Object.prototype.toString.call(arr) == '[object Array]';
	}

	function getKeys(obj) {
		var result = [];
		for (var key in obj) {
			if (!obj.hasOwnProperty(key)) continue;
			result.push(key);
		}
		return result;
	}

	function equalObjects(objA, objB, arrIgnore) {
		var aKeys = getKeys(objA);
		var bKeys = getKeys(objB);
		var len = aKeys.length;

		if (bKeys.length !== len) {
			return false;
		}

		for (var i = 0; i < len; i++) {
			var key = aKeys[i];
			if (arrIgnore && inArray(arrIgnore, key)) continue;
			if (!core.equals(objA[key], objB[key])) {
				return false;
			}
		}

		return true;
	}

	function equalArrays(arrA, arrB) {
		var len = arrA.length;

		if (arrB.length !== len) {
			return false;
		}

		for (var i = 0; i < len; i++) {
			if (!core.equals(arrA[i], arrB[i])) {
				return false;
			}
		}

		return true;
	}
	
	core.equals = function equals(a, b, arrIgnore) {
		if (a === b) {
			return true;
		}

		if (!a || !b) {
			return false;
		}

		if (isArray(a) && isArray(b)) {
			return equalArrays(a, b);
		} else if (typeof a === "object" && typeof b === "object") {
			return equalObjects(a, b, arrIgnore);
		}

		return a === b;
	};
	
	function embedMixins(script) {
		script.Cache = script.Cache || core.Cache;
		script.equals = script.equals || core.equals;
		script.clone = script.clone || core.clone;
		return script;
	}
	
	core.embeds = core.embeds || {};
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};
	
	function inArray(array, value) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == value) {
				return true;
			}
		}
		return false;
	}

	core.clone = function clone(obj) {
		// Clone a object.
		var copy;

		// Handle the 3 simple types, and null or undefined
		if (null == obj || "object" != typeof obj) return obj;

		// Handle Date
		if (obj instanceof Date) {
			copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}

		// Handle Array
		if (obj instanceof Array) {
			copy = [];
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0, len = obj.length; i < len; i++) {
				copy[i] = core.clone(obj[i]);
			}
			return copy;
		}

		// Handle Object
		if (obj instanceof Object) {
			copy = {};
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = core.clone(obj[attr]);
			}
			return copy;
		}

		throw new Error("Unable to copy obj! Its type isn't supported.");
	};

	return core;
}));