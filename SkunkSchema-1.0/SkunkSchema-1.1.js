/********************************************************************************************\
	File:           SkunkSchema-1.1.js
	Purpose:        Objected oriented GUI schema creation for Skunkworks.
	Creator:        Cyprias
	Date:           02/20/2018
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkSchema-1.1";
var MINOR = 210820;

(function (factory) {
	var dependencies = {};
	dependencies.EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof dependencies.EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
	}
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkSchema11 = factory(dependencies);
	}
}(function (dependencies) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	var EventEmitter = dependencies.EventEmitter;

	core.debugging = false;

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	core.setInterval = setInterval;
	core.setTimeout = setTimeout;
	core.setImmediate = setImmediate;
	core.clearTimeout = clearTimeout;
	
	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(core.MAJOR + " " + args.join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (core.debugging != true) return;
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(core.MAJOR + " " + args.join('\t'), opmConsole);
	};
	if (console.log) core.console = function noop() {};

	core.stringTemplate = function stringTemplate(string, data) {
		var proxyRegEx = /\{\{([^\}]+)?\}\}/g;
		return string.replace(proxyRegEx, function(_, key) {
			var keyParts = key.split('.');
			var value = data;
			keyParts.forEach(function(v) {
				value = value[v];
			});
			if (typeof value !== "undefined") {
				return value;
			}
			return '';
		});
	};

	core.stringTimesN = function stringTimesN(n, _char) {
		return Array(n + 1).join(_char);
	};
	
	var EOL = "\n";
	
	core.prettifyXml = function prettifyXml(xmlInput) {
		var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		var _options$indent = options.indent,
			indentOption = _options$indent === void 0 ? 2 : _options$indent,
			_options$newline = options.newline,
			newlineOption = _options$newline === void 0 ? EOL : _options$newline;
		var indentString = core.stringTimesN(indentOption, ' ');
		var formatted = '';
		var regex = /(>)(<)(\/*)/g;
		var xml = xmlInput.replace(regex, "$1".concat(newlineOption, "$2$3"));
		var pad = 0;
		xml.split(/\r?\n/).forEach(function (l) {
			var line = l.trim();
			var indent = 0;

			if (line.match(/.+<\/\w[^>]*>$/)) {
				indent = 0;
			} else if (line.match(/^<\/\w/)) {
				// Somehow istanbul doesn't see the else case as covered, although it is. Skip it.

				/* istanbul ignore else  */
				if (pad !== 0) {
					pad -= 1;
				}
			} else if (line.match(/^<\w([^>]*[^\/])?>.*$/)) {
				indent = 1;
			} else {
				indent = 0;
			}

			var padding = core.stringTimesN(pad, indentString);
			formatted += padding + line + newlineOption; // eslint-disable-line prefer-template

			pad += indent;
		});
		return formatted.trim();
	};

	var View = core.View = function View(params) {
		EventEmitter.apply(this);
		this.title = params.title;
		this.icon = params.icon;
		this.width = params.width;
		this.height = params.height;
		this.children = [];
		this.handler = {};
		
		this.scripts = [];// <script language='VBScript' src="Strings.vbs"/>
		this.cdata = [];
		
		//<script language='VBScript'>
		
	};
	View.prototype = new EventEmitter();
	View.prototype.constructor = View;
	View.prototype.toString = function () {
		return "[View " + this.title + "]";
	};
	View.prototype.toXML = function toXML() {
		var template = "<view title='{{title}}' icon='{{icon}}' width='{{width}}' height='{{height}}'>{{children}}</view>";
		if (this.children.length == 0) throw new core.Error("Invalid XML: view '" + this.title + "' needs at least one child.");
		
		var children = [];
		if (this.scripts.length > 0) {
			var self = this;
			this.scripts.forEach(function(data) {
				children.push("<script language='" + data.language + "' src='" + data.src + "'/>");
			});
		}
		
		if (this.cdata.length > 0) {
			this.cdata.forEach(function(data) {
				children.push("<script language='" + data.language + "'><![CDATA[" + data.str + "]]></script>");
			});
		}
		
		children = children.concat(this.children.map(function(child) {
			if (typeof child === "string") {
				return child;
			} else {
				return child.toXML();
			}
		}));
		
		return core.stringTemplate(template, {
			title   : this.title,
			icon    : this.icon,
			width   : this.width,
			height  : this.height,
			children: children.join("")
		});
	};
	View.prototype.getHeight = function getHeight() {
		return this.height - 32;
	};
	View.prototype.getWidth = function getWidth() {
		return this.width;
	};

	View.prototype.getControl = function factory() {
		function findByName(data, name) {
			var result;
			data.some(function iter(a) {
				if (a.name === name) {
					result = a;
					return true;
				}
				return Array.isArray(a.children) && a.children.some(iter);
			});
			return result;
		}
		return function getControl(params) {
			var name = (!params || params.name === undefined ? false : params.name);
			return findByName(this.children, name);
		};
	}();
	
	View.prototype.registerEvents = function registerEvents() {
		var handler = this.handler;
		var self = this;
		handler.OnControlEvent = handler.OnControlEvent || function OnControlEvent(szPanel, szControl, dictSzValue) {
			core.debug("<registerEvents|OnControlEvent>", szPanel, szControl);
			if (szPanel != self.title) return;
			var control = self.getControl({name: szControl});
			core.debug("control: " + control);
			if (control) {
				var value = dictSzValue(szControl);
				core.debug("value: " + value);
				
				
				if (control instanceof Slider && value == control.value) return;
				control.value = value;
				var current = control;
				do {
					if (current.events["onControlEvent"] && current.events["onControlEvent"].length > 0) {
						current.emit("onControlEvent", control, szPanel, szControl, value);
						break;
					}
					current = current.parent;
				} while (current);
				
				if (control instanceof Notebook) {
					var parent = control.parent;
					emitShown(control.parent);
				}
				return;
			}
			core.console("No control found for " + szControl, "value: " + value);
		};
		skapi.AddHandler(evidOnControlEvent,  handler);
		
		/*
		handler.OnControlProperty = handler.OnControlProperty || function OnControlProperty(szPanel, szControl, szProperty, vValue) {
			if (szPanel != self.title) return;
			core.console("<OnControlProperty>", szPanel, szControl, szProperty, vValue);
		};
		skapi.AddHandler(evidOnControlProperty,	handler);
		*/
	};
	
	function emitShown(control) {
		//core.debug("<emitShown>", control);
		if (typeof control.emit === "function") {
			control.emit("onShow");
		}
		if (control.children) {
			control.children.forEach(function(child, index) {
				if (child instanceof Page) {
					var value = control.value || 0;
					if (value == index) {
						emitShown(child);
					} else {
						emitHidden(child);
					}
				} else {
					emitShown(child);
				}
			});
		}
	}

	function emitHidden(control) {
		//core.debug("<emitHidden>", control);
		if (typeof control.emit === "function") {
			control.emit("onHide");
		}
		if (control.children) {
			control.children.forEach(function(child) {
				emitHidden(child);
			});
		}
	}
	
	View.prototype.unregisterEvents = function unregisterEvents() {
		skapi.AddHandler(evidNil,  this.handler);
	};
	
	View.prototype.showControls = function showControls(params) {
		var visable = (!params || params.visable === undefined ? false : params.visable);

		//var verbose = (!params || params.verbose === undefined ? core.debug : params.verbose);
		var schema = this.toXML();
		if (core.debugging == true) {
			var pretty = core.prettifyXml(schema);
			core.console("pretty: " + pretty);
		}
		if (typeof jest !== "undefined") {
			var pretty = core.prettifyXml(schema);
			skapi.ShowControls(pretty, visable);
		} else {
			skapi.ShowControls(schema, visable);
		}
		this.registerEvents();
		emitShown(this);
	};
	
	View.prototype.removeControls   = function removeControls() {
		this.unregisterEvents();
		skapi.RemoveControls(this.title);
		emitHidden(this);
	};

	var FixedLayout = core.FixedLayout = function FixedLayout(params) {
		EventEmitter.apply(this);
		this.children = [];
		this.parent = params.parent;
		if (params && params.parent && params.parent.children) {
			params.parent.children.push(this);
		}
	};
	FixedLayout.prototype = new EventEmitter();
	FixedLayout.prototype.constructor = FixedLayout;
	FixedLayout.prototype.toString = function () {
		return "[FixedLayout]";
	};
	FixedLayout.prototype.toXML = function toXML() {
		var template = "<control progid='DecalControls.FixedLayout'>{{children}}</control>";
		var children = this.children.map(function(child) {
			return child.toXML();
		});
		return core.stringTemplate(template, {
			children: children.join("")
		});
	};
	FixedLayout.prototype.getLeft = function getLeft() {
		return this.left || 0;
	};
	FixedLayout.prototype.getTop = function getTop() {
		return this.top || 0;
	};
	FixedLayout.prototype.getHeight = function getHeight() {
		return this.parent.getHeight() || 0;
	};
	FixedLayout.prototype.getWidth = function getWidth() {
		return this.parent.getWidth() || 0;
	};

	var Control = core.Control = function Control(params) {
		if (!(this instanceof Control)) {
			return new Control(params);
		}
		if (!params) return;
		EventEmitter.call(this);
		this.children = params.children || [];

		var self = this;
		Object.keys(params).forEach(function(key) {
			self[key] = params[key];
		});

		if (typeof params.width !== "undefined") {
			this.width = params.width;
		} else if (params.parent && params.parent instanceof FixedLayout) {
			this.width = params.parent.getWidth();
		}
		if (typeof params.height !== "undefined") {
			this.height = params.height;
		} else if (params.parent && params.parent instanceof FixedLayout) {
			this.height = params.parent.getHeight();
		}

		if (params.parent && params.parent.children) {
			params.parent.children.push(this);
		}
	};
	Control.prototype = new EventEmitter();
	Control.prototype.constructor      = Control;
	
	Control.prototype.toString = function () {
		return "[Control " + this.name + "]";
	};
	
	Control.prototype.toXML = function toXML() {
		if (this instanceof Page && this.children.length == 0) throw new core.Error("Invalid XML: page '" + this.label + "' needs at least one child.");
		if (this instanceof List && this.children.length == 0) throw new core.Error("Invalid XML: list '" + this.name + "' needs at least one column.");
		if (this instanceof Choice && this.children.length == 0 && typeof this.selected !== "undefined") throw new core.Error("Invalid XML: list '" + this.name + "' cannot set choice selected without children.");

		var children = [];
		if (this.children) {
			children = this.children.map(function(child) {
				return child.toXML();
			});
		}
		var rows = [];
		if (this.rows) {
			rows = this.rows.map(function(row) {
				return "<row>" + row.map(function(field) {
					return "<data>" + field + "</data>";
				}).join("") + "</row>"; 
			});
		}

		if (typeof this.preXML === "function") {
			this.preXML();
		}

		return core.stringTemplate(this.template, {
			name          : this.name,
			left          : this.getLeft(),
			top           : this.getTop(),
			width         : this.getWidth(),
			height        : this.getHeight(),
			textcolor     : this.textcolor,
			fontface      : this.fontface,
			fontsize      : this.fontsize,
			fontstyle     : this.fontstyle,
			text          : this.text,
			fixedwidth    : this.fixedwidth,
			align         : this.align,
			label         : this.label,
			selected      : this.selected,
			imageportalsrc: this.imageportalsrc,
			marginx       : this.marginx,
			marginy       : this.marginy,
			icon          : this.icon,
			pressedicon   : this.pressedicon,
			value         : this.value,
			fillcolor     : this.fillcolor,
			bordercolor   : this.bordercolor,
			border        : this.border,
			pretext       : this.pretext,
			posttext      : this.posttext,
			drawtext      : this.drawtext,
			facecolor     : this.facecolor,
			minimum       : this.minimum,
			vertical      : this.vertical,
			maximum       : this.maximum,
			rows          : rows.join(""),
			'default'     : this['default'],
			data          : this.data,
			children      : children.join("")
		});
	};
	
	Control.prototype.toString = function () {
		return "[" + core.getClassType(this) + " " + this.name + "]";
	};
		
	Control.prototype.setText = function setText(value) {
		this.text = value;
		this.setProperty("text", value);
	};
	
	Control.prototype.setValue = function setValue(value) {
		this.value = value;
		this.setProperty("value", value);
	};

	Control.prototype.getControlPropertyAsync = function getControlPropertyAsync(params, callback) {
		var view = this.getView();
		if (!view) throw new core.Error("CONTROL_HAS_NO_VIEW");
		return core.getControlPropertyAsync({
			panelName  : view.title,
			controlName: this.name,
			property   : params.property
		}, callback);
	};
	
	/*
	Control.prototype.getControlProperty = function getControlProperty(property) {
		var view = this.getView();
		if (!view) throw new core.Error("CONTROL_HAS_NO_VIEW");
		skapi.GetControlProperty(view.title, this.name, property);
	};
	*/

	Control.prototype.getView = function getView() {
		var current = this;
		do {
			if (current instanceof View) return current;
			current = current.parent;
		} while (current);
	};

	Control.prototype.getParents = function getParents() {
		var controls = [];
		var current = this.parent;
		do {
			controls.push(current);
			current = current.parent;
		} while (current);
		return controls;
	};

	Control.prototype.getHeight = function getHeight() {
		if (this instanceof Page) return this.parent.getHeight() - 16;
		if (this instanceof Notebook && typeof this.height === "undefined") return this.parent.getHeight();
		return this.height || 0;
	};
	Control.prototype.getWidth = function getWidth() {
		if (this instanceof Page) return this.parent.getWidth();
		if (this instanceof Notebook && typeof this.width === "undefined") return this.parent.getWidth();
		return this.width || 0;
	};

	Control.prototype.setAnchor = function setAnchor(relativeTo, relativePoint, point) {
		this.anchorRelativeTo = relativeTo;
		this.anchorRelativePoint = relativePoint;
		this.anchorPoint = point;
		return this;
	};

	Control.prototype.getLeft = function getLeft() {
		if (this.anchorRelativeTo) {
			var anchorRelativeTo = this.anchorRelativeTo;
			var anchorRelativePoint = this.anchorRelativePoint || "BOTTOMLEFT";
			var point = this.anchorPoint || "TOPLEFT";
			var relLeft = anchorRelativeTo.getLeft();
			if (anchorRelativePoint.match(/RIGHT/)) {
				relLeft = anchorRelativeTo.getWidth();
			}
			if (point.match(/RIGHT/)) {
				relLeft -= this.getWidth();
			}
			return relLeft;
		}
		return this.left || 0;
	};

	
	Control.prototype.getTop = function getTop() {
		if (this.anchorRelativeTo) {
			var anchorRelativeTo = this.anchorRelativeTo;
			var anchorRelativePoint = this.anchorRelativePoint || "BOTTOMLEFT";
			var point = this.anchorPoint || "TOPLEFT";
			var relTop = anchorRelativeTo.getTop();
			if (anchorRelativePoint.match(/BOTTOM/)) {
				var parentHeight = anchorRelativeTo.getHeight();
				relTop += parentHeight;
				if (point.match(/BOTTOM/)) {
					relTop -= this.getHeight();
				}
			}
			return relTop;
		}
		return this.top || 0;
	};


	/*
	Control.prototype.toJSON = function toJSON() {
		var copy2 = {};
		for (var key in this) {
			if (!this.hasOwnProperty(key)) continue;
			if (key == "parent") return;
			copy2[key] = this[key];
			//core.console("key: " + key);
		}
		return copy2;
	};
	*/


	var Notebook = core.Notebook = function Notebook(params) {
		if (!(this instanceof Notebook)) {
			return new Notebook(params);
		}
		Control.call(this, params);

		//this.children = [];
	};
	Notebook.prototype = new Control();
	Notebook.prototype.constructor = Notebook;
	
	Notebook.prototype.toString = function () {
		return "[Notebook " + this.name + "]";
	};
	
	Notebook.prototype.template = "<control progid='DecalControls.Notebook' name='{{name}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}'>{{children}}</control>"; //  left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}'

	var PushButton = core.PushButton = function PushButton(params) {
		if (!(this instanceof PushButton)) {
			return new PushButton(params);
		}
		Control.call(this, params);
	};
	PushButton.prototype = new Control();
	PushButton.prototype.constructor = PushButton;
	
	PushButton.prototype.toString = function () {
		return "[PushButton " + this.name + "]";
	};
	
	PushButton.prototype.template = "<control name='{{name}}' progid='DecalControls.PushButton' text='{{text}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}' />";

	var Page = core.Page = function Page(params) {
		if (!(this instanceof Page)) {
			return new Page(params);
		}
		Control.call(this, params);
	};
	Page.prototype = new Control();
	Page.prototype.constructor = Page;
	Page.prototype.toString = function () {
		return "[Page " + this.label + "]";
	};
	Page.prototype.template = "<page label='{{label}}'>{{children}}</page>";


	Control.prototype.setProperty = function setProperty(property, value) { // szProperty, vValue
		var view = this.getView();
		if (view && typeof skapi !== "undefined") {
			skapi.SetControlProperty(view.title, this.name, property, value);
		}
		return this;
	};

	var List = core.List = function List(params) {
		if (!(this instanceof List)) {
			return new List(params);
		}
		Control.call(this, params);
		this.rows = params.rows || [];
	};
	List.prototype = new Control();
	List.prototype.constructor = List;
	List.prototype.toString = function () {
		return "[List " + this.name + "]";
	};
	List.prototype.template = "<control progid='DecalControls.List' name='{{name}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}'>{{children}}{{rows}}</control>";
	List.prototype.addRow = function addRow() {
		var args = [].slice.call(arguments);
		this.rows.push(args);
		var view = this.getView();
		if (view && typeof skapi !== "undefined") {
			skapi.GetControlProperty(view.title, this.name, "AddRow()");

			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0; i < args.length; i++) {
				this.setListProperty(i, this.rows.length - 1, args[i]);
			}
		}
		return this;
	};
	List.prototype.getColumns = function getColumns() {
		return this.children.filter(function(child) {
			if (child instanceof TextColumn) return true;
			if (child instanceof IconColumn) return true;
			if (child instanceof CheckColumn) return true;
			return false;
		});
	};
	List.prototype.setListProperty = function setListProperty(intCol, intRow, vValue) {
		if (typeof vValue === "undefined" || vValue == null) {
			//core.debug(core.MAJOR, vValue + " is a invalid column value.");
			return;
		}

		var columns = this.getColumns();
		if (columns.length <= intCol) {
			core.console(core.MAJOR, this.name + " does not have a column #" + intCol + " for " + vValue);
			return;
		}

		var column = columns[intCol];
		var property = "Data(" + intCol + ", " + intRow + ")";
		if (column instanceof IconColumn) {
			property = "Data(" + intCol + ", " + intRow + ", 1)";
		}

		var value = vValue;
		var color;
		if (typeof vValue === "object") {
			value = vValue.value;
			color = vValue.color;
		}
		
		this.setProperty(property, value);
		if (this.rows[intRow]) {
			this.rows[intRow][intCol] = vValue;
		}
		
		if (color) {
			this.setProperty("Color(" + intCol + ", " + intRow + ")", color);
		}

		return this;
	};
	List.prototype.clear = function clear() {
		this.rows = [];
		var view = this.getView();
		skapi.GetControlProperty(view.title, this.name, "Clear()");
		return this;
	};
	List.prototype.getScrollPositionAsync = function getScrollPositionAsync(callback) {
		return this.getControlPropertyAsync({
			property: "ScrollPosition"
		}, callback);
	};
	List.prototype.setScrollPosition = function setScrollPosition(value) {
		this.setProperty("ScrollPosition", value);
	};
	List.prototype.setRows = function setRows(rows) {
		core.debug("rows: " + rows.length, "this.rows: " + this.rows.length);
		var view = this.getView();
		var row, column;
		var value, color;
		// eslint-disable-next-line no-restricted-syntax
		for (var ri = 0; ri < rows.length; ri++) {
			row = rows[ri];
			
			if (ri >= this.rows.length) {
				core.debug(core.MAJOR, " " + ri + " Adding row");
				skapi.GetControlProperty(view.title, this.name, "AddRow()");
			}
			
			// eslint-disable-next-line no-restricted-syntax
			for (var ci = 0; ci < row.length; ci++) {
				column = row[ci];
				
				if (typeof column === "object") {
					value = column.value;
					color = column.color;
				} else {
					value = column;
					color = undefined;
				}

				// If value is a function, call function to get a value.
				if (typeof value === "function") {
					value = value() || "";
				}

				// if color is a function, call function to get a color.
				if (typeof color === "function") {
					color = color();
				}
				
				if (this.rows[ri]) {
					
					// Check if row are objects, which have value and color properties.
					if (typeof this.rows[ri][ci] === "object" && typeof column === "object") {
						var oldValue = this.rows[ri][ci].value;
						var oldColor = this.rows[ri][ci].color;
						if (typeof oldValue === "function") {
							oldValue = oldValue();
						}

						if (typeof oldColor === "function") {
							oldColor = oldColor();
						}

						if (oldValue != value) {
							core.debug(core.MAJOR, " " + ri + " " + ci + " objects have different values.");
							this.setProperty("Data(" + ci + ", " + ri + ")", value);
						}
						
						if (oldColor != color) {
							core.debug(core.MAJOR, " " + ri + " " + ci + " objects have different colors.");
							this.setProperty("Color(" + ci + ", " + ri + ")", color);
						}
						continue;
					}
					
					// rows are strings.
					if (this.rows[ri][ci] == value) {
						core.debug(core.MAJOR, "Skipping " + ri + " " + ci + " (matching values)");
						continue;
					}
				}
				
				core.debug(core.MAJOR, "Setting " + ri + " " + ci + " to " + value);

				//this.setProperty("Data(" + ci + ", " + ri + ")", value);
				this.setListProperty(ci, ri, value);
				if (typeof color !== "undefined") {
					this.setProperty("Color(" + ci + ", " + ri + ")", color);
				}
				
			}
		}
		
		// eslint-disable-next-line no-restricted-syntax
		for (var ri = this.rows.length - 1; ri >= rows.length; ri--) {
			core.debug(core.MAJOR, "Removing " + ri);
			this.deleteRow(ri);
		}
			
		this.rows = rows;
		return this;
	};
	List.prototype.deleteRow = function fDeleteRow(i) {
		var view = this.getView();
		skapi.GetControlProperty(view.title, this.name, "DeleteRow(" + i + ")");
		this.rows.splice(i, 1);
		return this;
	};
	List.prototype.getListPropertyAsync = function getListPropertyAsync(intCol, intRow, callback, thisArg) {
		//return this.getPropertyAsync("data(" + intCol + "," + intRow + ")", callback, thisArg);
		return this.getControlPropertyAsync({
			property: "data(" + intCol + "," + intRow + ")"
		}, callback);
	};
	

	var TextColumn = core.TextColumn = function TextColumn(params) {
		if (!(this instanceof TextColumn)) {
			return new TextColumn(params);
		}
		Control.call(this, params);
	};
	TextColumn.prototype = new Control();
	TextColumn.prototype.constructor = TextColumn;
	TextColumn.prototype.toString = function () {
		return "[TextColumn]";
	};
	TextColumn.prototype.template = "<column progid='DecalControls.TextColumn' fixedwidth='{{fixedwidth}}' align='{{align}}'/>";

	var IconColumn = core.IconColumn = function IconColumn(params) {
		if (!(this instanceof IconColumn)) {
			return new IconColumn(params);
		}
		Control.call(this, params);
	};
	IconColumn.prototype = new Control();
	IconColumn.prototype.constructor = IconColumn;
	IconColumn.prototype.toString = function () {
		return "[IconColumn]";
	};
	IconColumn.prototype.template = "<column progid='DecalControls.IconColumn'/>";

	var CheckColumn = core.CheckColumn = function CheckColumn(params) {
		if (!(this instanceof CheckColumn)) {
			return new CheckColumn(params);
		}
		Control.call(this, params);
	};
	CheckColumn.prototype = new Control();
	CheckColumn.prototype.constructor = CheckColumn;
	CheckColumn.prototype.toString = function () {
		return "[CheckColumn]";
	};
	CheckColumn.prototype.template = "<column progid='DecalControls.CheckColumn'/>";

	/*
	var Row = core.Row = function Row(params) {
		if (!(this instanceof Row)) {
			return new Row(params);
		}
		Control.call(this, params);
	};
	Row.prototype = new Control();
	Row.prototype.constructor = Row;
	Row.prototype.template = "<row><data>A1</data><data>B1</data></row>";
*/

	var StaticText = core.StaticText = function StaticText(params) {
		if (!(this instanceof StaticText)) {
			return new StaticText(params);
		}
		Control.call(this, params);
		this.text = params.text || params.name;
	};
	StaticText.prototype = new Control();
	StaticText.prototype.constructor = StaticText;
	StaticText.prototype.toString = function () {
		return "[StaticText " + this.name + "]";
	};
	StaticText.prototype.template = "<control progid='DecalControls.StaticText' name='{{name}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}' text='{{text}}'/>";
	
	var Checkbox = core.Checkbox = function Checkbox(params) {
		if (!(this instanceof Checkbox)) {
			return new Checkbox(params);
		}
		Control.call(this, params);
		this.text = params.text || params.name;
	};
	Checkbox.prototype = new Control();
	Checkbox.prototype.constructor = Checkbox;
	Checkbox.prototype.toString = function () {
		return "[Checkbox " + this.name + "]";
	};
	Checkbox.prototype.template = "<control progid='DecalControls.Checkbox' name='{{name}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}' text='{{text}}'/>";
	
	var Choice = core.Choice = function Choice(params) {
		if (!(this instanceof Choice)) {
			return new Choice(params);
		}
		Control.call(this, params);
		if (typeof params.height === "undefined") this.height = 20;
	};
	Choice.prototype = new Control();
	Choice.prototype.constructor = Choice;
	Choice.prototype.toString = function () {
		return "[Choice " + this.name + "]";
	};
	Choice.prototype.template = "<control progid='DecalControls.Choice' selected='{{selected}}' name='{{name}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}'>{{children}}</control>";
	Choice.prototype.addChoice = function addChoice(text, data) {
		this.children.push(new Option({
			text: text,
			data: data
		}));
		var view = this.getView();
		if (view && typeof skapi !== "undefined") {
			data = data || text;
			skapi.GetControlProperty(view.title, this.name, "AddChoice('" + text + "','" + data + "')");
		}
		return this;
	};
	Choice.prototype.clear              = function clear() {
		this.children = [];
		var view = this.getView();
		skapi.GetControlProperty(view.title, this.name, "Clear()");
		return this;
	};
	Choice.prototype.preXML              = function preXML() {
		if (this.children.length > 0 && typeof this.selected === "undefined") {
			this.selected = 0;
		}
	};
		
		
	var Option = core.Option = function Option(params) {
		if (!(this instanceof Option)) {
			return new Option(params);
		}
		Control.call(this, params);
		this.data = params.data || params.text;
	};
	Option.prototype = new Control();
	Option.prototype.constructor = Option;
	Option.prototype.toString = function () {
		return "[Option " + this.text + "]";
	};
	Option.prototype.template = "<option text='{{text}}' data='{{data}}'/>";
	
	var Edit = core.Edit = function Edit(params) {
		if (!(this instanceof Edit)) {
			return new Choice(params);
		}
		Control.call(this, params);
		this.height         = params.height || 20;
		this.imageportalsrc = params.imageportalsrc || 4726;
		this.marginx        = params.marginx || 5;
		this.marginy        = params.marginy || 2;
	};
	Edit.prototype = new Control();
	Edit.prototype.constructor = Edit;
	Edit.prototype.toString = function () {
		return "[Edit " + this.name + "]";
	};
	Edit.prototype.template = "<control progid='DecalControls.Edit' name='{{name}}' text='{{text}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}' marginx='{{marginx}}' marginy='{{marginy}}' imageportalsrc='{{imageportalsrc}}' />";

	// textcolor='{{textcolor}}' fontface='{{fontface}}' fontsize='{{fontsize}}' fontstyle='{{fontstyle}}'

	var Button = core.Button = function Button(params) {
		if (!(this instanceof Button)) {
			return new Button(params);
		}
		Control.call(this, params);
	};
	Button.prototype = new Control();
	Button.prototype.constructor = Button;
	Button.prototype.toString = function () {
		return "[Button " + this.name + "]";
	};
	Button.prototype.template = "<control progid='DecalControls.Button' name='{{name}}' icon='{{icon}}' pressedicon='{{pressedicon}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}' />";

	var Progress = core.Progress = function Progress(params) {
		if (!(this instanceof Progress)) {
			return new Progress(params);
		}
		Control.call(this, params);
		this.value = params.value || 0;
	};
	Progress.prototype = new Control();
	Progress.prototype.constructor = Progress;
	Progress.prototype.toString = function () {
		return "[Progress " + this.name + "]";
	};
	Progress.prototype.template = "<control progid='DecalControls.Progress' name='{{name}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}' value='{{value}}' minimum='{{minimum}}' maximum='{{maximum}}' fillcolor='{{fillcolor}}' bordercolor='{{bordercolor}}' border='{{border}}' align='{{align}}' pretext='{{pretext}}' posttext='{{posttext}}' drawtext='{{drawtext}}' facecolor='{{facecolor}}' />";

	var Slider = core.Slider = function Slider(params) {
		if (!(this instanceof Slider)) {
			return new Slider(params);
		}
		Control.call(this, params);
	};
	Slider.prototype = new Control();
	Slider.prototype.constructor = Slider;
	Slider.prototype.toString = function () {
		return "[Slider " + this.name + "]";
	};
	Slider.prototype.template = "<control progid='DecalControls.Slider' name='{{name}}' left='{{left}}' top='{{top}}' width='{{width}}' height='{{height}}' vertical='{{vertical}}' value='{{value}}' minimum='{{minimum}}' maximum='{{maximum}}' default='{{default}}' />";
	Slider.prototype.setValue = function setValue(value) {
		this['default'] = value;
		this.setProperty("sliderposition", value);
		return this;
	};
		

	/*
	var BorderLayout = core.BorderLayout = function BorderLayout(params) {
		if (!(this instanceof BorderLayout)) {
			return new BorderLayout(params);
		}
		Control.call(this, params);
		this.text = params.text ||params.name;
	};
	BorderLayout.prototype = new Control();
	BorderLayout.prototype.constructor = Control;
	BorderLayout.prototype.toXML = function toXML() {
		var template = "<control progid='DecalControls.BorderLayout'>{{children}}</control>";
		var children = this.children.map(function(child) {
			return child.toXML();
		});
		return core.stringTemplate(template, {
			children: children.join("")
		});
	};
	*/

	core.getClassType = function getClassType(object) {
		if (object instanceof PushButton) return "PushButton";
		if (object instanceof Page) return "Page";
		if (object instanceof FixedLayout) return "FixedLayout";
		if (object instanceof Notebook) return "Notebook";
		if (object instanceof List) return "List";
		if (object instanceof TextColumn) return "List";
		if (object instanceof StaticText) return "StaticText";
		if (object instanceof Checkbox) return "Checkbox";
		if (object instanceof Choice) return "Choice";
		if (object instanceof Option) return "Option";
		if (object instanceof Edit) return "Edit";
		if (object instanceof Button) return "Button";
		if (object instanceof Progress) return "Progress";
		if (object instanceof Control) return "Control";
		return "Unknown";
	};

	core.timedOut = function timedOut(callback, name) {
		callback(new core.Error(name || "TIMED_OUT"));
	};

	core.applyMethod = function applyMethod() {
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};

	core.getControlPropertyAsync = function getControlPropertyAsync(params, callback) {
		var panelName = params.panelName;
		var controlName = params.controlName;
		var property = params.property;
		
		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
	
		var handler = {};
		handler.OnControlProperty = function OnControlProperty(szPanel2, szControl2, szProperty2, vValue) {
			if (szPanel2 == panelName && szControl2 == controlName && (szProperty2 == property || szProperty2 == "data")) {
				finish(undefined, vValue);
			}
		};
		skapi.AddHandler(evidOnControlProperty,	handler);
		skapi.GetControlProperty(panelName, controlName, property);

		var tid = core.setTimeout(core.timedOut, 1000, finish, "GET_PROPERTY_TIMED_OUT");
	};

	core.showDropdownMenuPrompt = function showDropdownMenuPrompt(params, callback) {
		core.debug("<showDropdownMenuPrompt>");
		var options = params.options || [];
		var title = (params.title === undefined ? "Dropdown Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
		var selected = (params.selected === undefined ? 0 : params.selected);
		function finish() {
			view.removeControls();
			view = undefined;
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
		}
		var view = new core.View({
			title : title, 
			width : width, 
			height: 72, 
			icon  : icon
		});
		var layout = new core.FixedLayout({parent: view});
		var choMenu = new core.Choice({
			parent  : layout,
			name    : "choMenu",
			text    : "Hello",
			children: [
				new core.Option({text: "Select..."})
			]
		});
		choMenu.selected = selected;
		options.forEach(function(option) {
			choMenu.children.push(new core.Option(option));
		});
		var btnCancel = new core.PushButton({
			parent: layout,
			name  : "btnCancel",
			text  : "Cancel",
			width : 100,
			height: 20
		})
			.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
			.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
				return finish(new core.Error("CANCELED"));
			});
		var btnConfirm = new core.PushButton({
			parent: layout,
			name  : "btnConfirm",
			text  : "Confirm",
			width : 100,
			height: 20
		})
			.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
			.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
				var value = choMenu.value;
				if (value == "Select...") {
					return finish(new SkunkSchema11.Error("NOTHING_SELECTED"));
				}
				return finish(undefined, options.find(function(option) {
					return option.data == value || option.text == value;
				}));
			});

		view.showControls({visable: true});
	};


	core.showEditPrompt = function showEditPrompt(params, callback) {
		var title = (params.title === undefined ? "Edit Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
		var value = (params.value === undefined ? "" : params.value);
		var imageportalsrc = (params.imageportalsrc === undefined ? 4726 : params.imageportalsrc);

		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			skapi.RemoveControls(title);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var schema = Element("view", {title: title, icon: icon, width: width, height: 72}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.Edit', name: "edText",  width: width, height: 20, top: 0, left: 0,             text: value, imageportalsrc: imageportalsrc}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnCancel",  width: width/2, height: 20, top: 20, left: 0,             text: "Cancel"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnConfirm",    width: width/2, height: 20, top: 20, left: width / 2,    text: "Confirm"})
			])
		]);
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			if (szControl == "btnCancel") {
				return finish(new core.Error("CANCELED"));
			} else if (szControl == "btnConfirm") {
				return core.getControlPropertyAsync({
					panelName: title,
					controlName: "edText",
					property: "text"
				}, finish);
			}
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		var string = schema.toXML();
		skapi.ShowControls(core.prettifyXml(string), true);
	};

	core.showConfirmationPrompt = function showConfirmationPrompt(params, callback) {
		core.debug("<showConfirmationPrompt>");
		var title = (params.title === undefined ? "Edit Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
		
		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			skapi.RemoveControls(title);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var schema = Element("view", {title: title, icon: icon, width: width, height: 52}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.PushButton', name: "btnCancel",  width: width/2, height: 20, top: 0, left: 0,             text: "Cancel"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnConfirm",    width: width/2, height: 20, top: 0, left: width/2,    text: "Confirm"})
			])
		]);
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			if (szControl == "btnCancel") {
				return finish(new core.Error("CANCELED"));
			} else if (szControl == "btnConfirm") {
				return finish(undefined, true);
			}
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		var string = schema.toXML();
		skapi.ShowControls(core.prettifyXml(string), true);
	};

	core.showBooleanPrompt = function showBooleanPrompt(params, callback) {
		core.debug("<showBooleanPrompt>");
		var title = (params.title === undefined ? "Boolean Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
	
		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			skapi.RemoveControls(title);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var schema = Element("view", {title: title, icon: icon, width: width, height: 52}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.PushButton', name: "btnCancel",  width: 50, height: 20, top: 0, left: 0,             text: "Cancel"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnFalse",   width: 50, height: 20, top: 0, left: width - 100,   text: "False"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnTrue",    width: 50, height: 20, top: 0, left: width - 50,    text: "True"})
			])
		]);
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			if (szControl == "btnCancel") {
				return finish(new core.Error("CANCELED"));
			} else if (szControl == "btnFalse") {
				return finish(undefined, false);
			} else if (szControl == "btnTrue") {
				return finish(undefined, true);
			}
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		var string = schema.toXML();
		skapi.ShowControls(string, true);
	};

	var Element = core.Element = function Element(parent, selector, attrs, children) {
		if (!(this instanceof Element)) {
			return new Element(parent, selector, attrs, children);
		}
		if (parent instanceof Element) {
			this.parent = parent;
		} else {
			children = attrs;
			attrs = selector;
			selector = parent;
		}

		if (typeof attrs === "string" || Array.isArray(attrs)) {
			children = attrs;
			attrs = undefined;
		}

		//core.console("children: " + typeof children);
		if (typeof children == "boolean" || typeof children == "number") {
			children = String(children);
		}
		
		this.selector = selector;
		this.attrs = attrs;
		this.children = children;
	};
	
	Element.prototype.getChildren = function getChildren() {
		if (typeof this.children === "function") {
			return this.children(this);
		} else if (Array.isArray(this.children)) {
			return this.children;
		}
	};
	
	Element.prototype.toString = function toString() {
		var parts = [this.selector];
		if (this.attrs.title) parts.push(this.attrs.title);
		if (this.attrs.progid) parts.push(this.attrs.progid);
		if (this.attrs.name) parts.push(this.attrs.name);
		return "[" + parts.join(" ") + "]";
	};
	
	Element.prototype.toXML = function toXML() {
		var selector = this.selector;
		var attrs = this.attrs || {};
		var children = this.children;
		
		var self = this;

		var strAttrs = [];
		if (typeof children === "undefined") {
			Object.keys(attrs).forEach(function(attr) {
				var value = attrs[attr];
				if (typeof value === "string" || typeof value === "number") {
					strAttrs.push(attr + "='" + attrs[attr] + "'");
				}
			});
			return core.stringTemplate("<{{selector}} {{attrs}} />", {selector: selector, attrs: strAttrs.join(" ")});
		}
		
		var template = "<{{selector}} {{attrs}}>{{children}}</{{selector}}>";
		
		if (Object.keys(attrs).length == 0) {
			template = "<{{selector}}>{{children}}</{{selector}}>";
		}
		
		var strChildren;
		if (typeof children === "string") {
			strChildren = children;
		} else if (typeof children === "function") {
			strChildren = children(this).map(function(child) {
				return child.toXML();
			})
				.join("");
		} else {
			strChildren = children.map(function(child) {
				return child.toXML();
			}).join("");
		}
		
		Object.keys(attrs).forEach(function(attr) {
			var value = attrs[attr];
			if (typeof value === "string" || typeof value === "number") {
				strAttrs.push(attr + "='" + attrs[attr] + "'");
			}
		});
		return core.stringTemplate(template, {selector: selector, attrs: strAttrs.join(" "), children: strChildren});
	};
	
	Element.prototype.getWidth = function getWidth() {
		return this.attrs.width || this.parent && this.parent.getWidth() || 0;
	};
	
	Element.prototype.getHeight = function getHeight() {
		if (this.attrs.height) {
			return this.attrs.height;
		}
		if (this.parent) {
			if (this.parent.selector == "view") {
				return this.parent.getHeight() - 32;
			} else if (this.parent.selector == "page") {
				return this.parent.getHeight() - 16;
			}
			return this.parent.getHeight();
		}
		return 0;
	};

	function registerChildren(registry, element, root) {
		root = root || element;
		if (element.attrs && element.attrs.name) {
			registry[element.attrs.name] = element;
		}
		if (element.children) {
			var children;
			if (Array.isArray(element.children)) {
				children = element.children;
			} else if (typeof element.children === "function") {
				children = element.children(element);
			}
			children.forEach(function(child) {
				child.parent = element;
				child.root = root;
				registerChildren(registry, child, root);
			});
		}
	}

	Element.prototype.showControls = function showControls(visible) {
		visible = (!visible === undefined ? false : visible);
		var string = this.toXML();
		skapi.ShowControls(string, visible);
		
		var registry = this.registry = {};
		var title = this.attrs.title;
		registerChildren(registry, this);
		
		var handler = this.handler = {};
		var self = this;
		handler.OnControlEvent = handler.OnControlEvent || function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			var element = registry[szControl];
			if (element && element.attrs && element.attrs.onControlEvent) {
				element.attrs.onControlEvent.apply(element, arguments);
				return;
			}
			core.debug("No controlEvent for " + szPanel + " - " + szControl);
		};
		skapi.AddHandler(evidOnControlEvent,  handler);
	};

	Element.prototype.removeControls   = function removeControls() {
		var title = this.attrs.title;
		skapi.RemoveControls(title);
		skapi.RemoveHandler(evidNil, this.handler);
	};

	Element.prototype.getElement = function getElement(name) {
		var root = this.root;
		return root.registry[name];
	};

	Element.prototype.getControlProperty = function getControlProperty(property, callback) {
		var root = this.root;
		var title = root.attrs.title;
		return core.getControlPropertyAsync({
			panelName  : title,
			controlName: this.attrs.name,
			property   : property
		}, callback);
	};

	core.embeds = core.embeds || {};
	
	/**
	 * createView() Returns a view object and will unload it when the parent script is disabled.
	 */
	function createView(params) {
		var script = this;
		var view = new core.View(params);
		if (typeof script.on === "function") {
			script.on("onDisable", function onDisable() {
				core.debug("Script disabling, unloading view...");
				view.removeControls();
			});
		}
		return view;
	}
	
	function embedMixins(script) {
		script.createView = createView;
		return script;
	}
	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	return core;
}));