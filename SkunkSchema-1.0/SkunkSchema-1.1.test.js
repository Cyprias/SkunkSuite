const events = require('events');
EventEmitter =  events.EventEmitter; // Global

const SkunkSchema = require("./SkunkSchema-1.1");

function SKAPI() {}
SKAPI.prototype.AddHandler = function AddHandler(evid, handler) {};
SKAPI.prototype.ShowControls = function ShowControls(schema, visable) {};
SKAPI.prototype.SetControlProperty = function SetControlProperty(schema, visable) {};
SKAPI.prototype.GetControlProperty = function GetControlProperty(schema, visable) {};

beforeEach(() => {
	skapi = new SKAPI();
});

// Globals
evidOnControlEvent = null;

describe('SkunkSchema-1.1', () => {  
	it("View without children throws error", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		expect(function() {
			view.toXML();
		}).toThrowError('Invalid XML: view \'viewTitle\' needs at least one child.');
	});
	
	it("View with FixedLayout", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
  </control>
</view>`);
	});

	it("View, FixedLayout and PushButton", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var button = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnButton",
			text  : "Button",
			width : 100,
			height: 20
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control name='btnButton' progid='DecalControls.PushButton' text='Button' left='0' top='0' width='100' height='20' />
  </control>
</view>`);
	});
	
	it("View > Layout > Buttons anchored to corners", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 300,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var btnTOPLEFT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnTOPLEFT",
			text  : "TOPLEFT",
			width : 100,
			height: 20
		}).setAnchor(layout, "TOPLEFT", "TOPLEFT");
		var btnTOPRIGHT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnTOPRIGHT",
			text  : "TOPRIGHT",
			width : 100,
			height: 20
		}).setAnchor(layout, "TOPRIGHT", "TOPRIGHT");
		var btnBOTTOMLEFT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnBOTTOMLEFT",
			text  : "BOTTOMLEFT",
			width : 100,
			height: 20
		}).setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT");
		var btnBOTTOMRIGHT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnBOTTOMRIGHT",
			text  : "BOTTOMRIGHT",
			width : 100,
			height: 20
		})
			.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT");
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='300' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control name='btnTOPLEFT' progid='DecalControls.PushButton' text='TOPLEFT' left='0' top='0' width='100' height='20' />
    <control name='btnTOPRIGHT' progid='DecalControls.PushButton' text='TOPRIGHT' left='200' top='0' width='100' height='20' />
    <control name='btnBOTTOMLEFT' progid='DecalControls.PushButton' text='BOTTOMLEFT' left='0' top='148' width='100' height='20' />
    <control name='btnBOTTOMRIGHT' progid='DecalControls.PushButton' text='BOTTOMRIGHT' left='200' top='148' width='100' height='20' />
  </control>
</view>`);
	});

	it("View with notebook (won't show in game)", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.Notebook' name='notebookName' left='0' top='0' width='200' height='168'>
  </control>
</view>`);
	});

	it("View, notebook and layout", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		var page = new SkunkSchema.Page({
			parent: notebook,
			label : "Page 1"
		});
		var layout = new SkunkSchema.FixedLayout({parent: page});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.Notebook' name='notebookName' left='0' top='0' width='200' height='168'>
    <page label='Page 1'>
      <control progid='DecalControls.FixedLayout'>
      </control>
    </page>
  </control>
</view>`);
	});

	it("View, notebook, page, layout and button", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		var page = new SkunkSchema.Page({
			parent: notebook,
			label : "Page 1"
		});
		var layout = new SkunkSchema.FixedLayout({parent: page});
		var button = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnButton",
			text  : "Button",
			width : 100,
			height: 20
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.Notebook' name='notebookName' left='0' top='0' width='200' height='168'>
    <page label='Page 1'>
      <control progid='DecalControls.FixedLayout'>
        <control name='btnButton' progid='DecalControls.PushButton' text='Button' left='0' top='0' width='100' height='20' />
      </control>
    </page>
  </control>
</view>`);
	});
	
	it("View, notebook, page, layout and buttons in corners", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		var page = new SkunkSchema.Page({
			parent: notebook,
			label : "Page 1"
		});
		var layout = new SkunkSchema.FixedLayout({parent: page});
		var btnTOPLEFT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnTOPLEFT",
			text  : "TOPLEFT",
			width : 100,
			height: 20
		}).setAnchor(layout, "TOPLEFT", "TOPLEFT");
		var btnTOPRIGHT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnTOPRIGHT",
			text  : "TOPRIGHT",
			width : 100,
			height: 20
		}).setAnchor(layout, "TOPRIGHT", "TOPRIGHT");
		var btnBOTTOMLEFT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnBOTTOMLEFT",
			text  : "BOTTOMLEFT",
			width : 100,
			height: 20
		}).setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT");
		var btnBOTTOMRIGHT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnBOTTOMRIGHT",
			text  : "BOTTOMRIGHT",
			width : 100,
			height: 20
		}).setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT");
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.Notebook' name='notebookName' left='0' top='0' width='200' height='168'>
    <page label='Page 1'>
      <control progid='DecalControls.FixedLayout'>
        <control name='btnTOPLEFT' progid='DecalControls.PushButton' text='TOPLEFT' left='0' top='0' width='100' height='20' />
        <control name='btnTOPRIGHT' progid='DecalControls.PushButton' text='TOPRIGHT' left='100' top='0' width='100' height='20' />
        <control name='btnBOTTOMLEFT' progid='DecalControls.PushButton' text='BOTTOMLEFT' left='0' top='132' width='100' height='20' />
        <control name='btnBOTTOMRIGHT' progid='DecalControls.PushButton' text='BOTTOMRIGHT' left='100' top='132' width='100' height='20' />
      </control>
    </page>
  </control>
</view>`);
	});

	it("View > notebook > page > notebook", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		var page = new SkunkSchema.Page({
			parent: notebook,
			label : "Page 1"
		});
		var notebook2 = new SkunkSchema.Notebook({
			parent: page,
			name  : "notebookName2"
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.Notebook' name='notebookName' left='0' top='0' width='200' height='168'>
    <page label='Page 1'>
      <control progid='DecalControls.Notebook' name='notebookName2' left='0' top='0' width='200' height='152'>
      </control>
    </page>
  </control>
</view>`);
	});
	
	it("View > notebook > page > notebook > page", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		var page = new SkunkSchema.Page({
			parent: notebook,
			label : "Page 1"
		});
		var notebook2 = new SkunkSchema.Notebook({
			parent: page,
			name  : "notebookName2"
		});
		var page2 = new SkunkSchema.Page({
			parent: notebook2,
			label : "Page 2"
		});
		var layout = new SkunkSchema.FixedLayout({parent: page2});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.Notebook' name='notebookName' left='0' top='0' width='200' height='168'>
    <page label='Page 1'>
      <control progid='DecalControls.Notebook' name='notebookName2' left='0' top='0' width='200' height='152'>
        <page label='Page 2'>
          <control progid='DecalControls.FixedLayout'>
          </control>
        </page>
      </control>
    </page>
  </control>
</view>`);
	});
	
	it("View > notebook > page > notebook > page > buttons in corners", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		var page = new SkunkSchema.Page({
			parent: notebook,
			label : "Page 1"
		});
		var notebook2 = new SkunkSchema.Notebook({
			parent: page,
			name  : "notebookName2"
		});
		var page2 = new SkunkSchema.Page({
			parent: notebook2,
			label : "Page 2"
		});
		var layout = new SkunkSchema.FixedLayout({parent: page2});
		var btnTOPLEFT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnTOPLEFT",
			text  : "TOPLEFT",
			width : 100,
			height: 20
		}).setAnchor(layout, "TOPLEFT", "TOPLEFT");
		var btnTOPRIGHT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnTOPRIGHT",
			text  : "TOPRIGHT",
			width : 100,
			height: 20
		}).setAnchor(layout, "TOPRIGHT", "TOPRIGHT");
		var btnBOTTOMLEFT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnBOTTOMLEFT",
			text  : "BOTTOMLEFT",
			width : 100,
			height: 20
		}).setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT");
		var btnBOTTOMRIGHT = new SkunkSchema.PushButton({
			parent: layout,
			name  : "btnBOTTOMRIGHT",
			text  : "BOTTOMRIGHT",
			width : 100,
			height: 20
		}).setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT");
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.Notebook' name='notebookName' left='0' top='0' width='200' height='168'>
    <page label='Page 1'>
      <control progid='DecalControls.Notebook' name='notebookName2' left='0' top='0' width='200' height='152'>
        <page label='Page 2'>
          <control progid='DecalControls.FixedLayout'>
            <control name='btnTOPLEFT' progid='DecalControls.PushButton' text='TOPLEFT' left='0' top='0' width='100' height='20' />
            <control name='btnTOPRIGHT' progid='DecalControls.PushButton' text='TOPRIGHT' left='100' top='0' width='100' height='20' />
            <control name='btnBOTTOMLEFT' progid='DecalControls.PushButton' text='BOTTOMLEFT' left='0' top='116' width='100' height='20' />
            <control name='btnBOTTOMRIGHT' progid='DecalControls.PushButton' text='BOTTOMRIGHT' left='100' top='116' width='100' height='20' />
          </control>
        </page>
      </control>
    </page>
  </control>
</view>`);
	});
	
	it("List without a column throws error", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var list = new SkunkSchema.List({
			parent: layout,
			name  : "listA"
		});
		expect(function() {
			view.toXML();
		}).toThrowError('Invalid XML: list \'listA\' needs at least one column.');
	});
	
	it("View > layout > list", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var list = new SkunkSchema.List({
			parent  : layout,
			name    : "list",
			children: [
				new SkunkSchema.IconColumn(),
				new SkunkSchema.TextColumn({fixedwidth: 80}),
				new SkunkSchema.CheckColumn()
			],
			rows: [
				[0x6000000 + 4600, "a", true],
				[0x6000000 + 4601, "b", false]
			]
		});
		list.addRow(0x6000000 + 4602, "addRow", false);
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.List' name='list' left='0' top='0' width='200' height='168'>
      <column progid='DecalControls.IconColumn'/>
      <column progid='DecalControls.TextColumn' fixedwidth='80' align=''/>
      <column progid='DecalControls.CheckColumn'/>
      <row>
        <data>100667896</data>
        <data>a</data>
        <data>true</data>
      </row>
      <row>
        <data>100667897</data>
        <data>b</data>
        <data>false</data>
      </row>
      <row>
        <data>100667898</data>
        <data>addRow</data>
        <data>false</data>
      </row>
    </control>
  </control>
</view>`);
	});
	
	it("Page without children throws error", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var notebook = new SkunkSchema.Notebook({
			parent: view,
			name  : "notebookName"
		});
		var page = new SkunkSchema.Page({
			parent: notebook,
			label : "Page 1"
		});
		expect(function() {
			view.toXML();
		}).toThrowError('Invalid XML: page \'Page 1\' needs at least one child.');
	});
	
	it("View > layout > text", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var stText = new SkunkSchema.StaticText({
			parent: layout,
			name  : "stText",
			text  : "Hello"
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.StaticText' name='stText' left='0' top='0' width='200' height='168' text='Hello'/>
  </control>
</view>`);
	});
	
	it("View > layout > checkbox", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var chkBox = new SkunkSchema.Checkbox({
			parent: layout,
			name  : "chkBox",
			text  : "Hello"
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Checkbox' name='chkBox' left='0' top='0' width='200' height='168' text='Hello'/>
  </control>
</view>`);
	});
	
	it("View > layout > choice with option selected", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var cho = new SkunkSchema.Choice({
			parent  : layout,
			name    : "cho",
			text    : "Hello",
			children: [
				new SkunkSchema.Option({text: "Hello"})
			]
		});
		cho.selected = 0;
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Choice' selected='0' name='cho' left='0' top='0' width='200' height='20'>
      <option text='Hello' data='Hello'/>
    </control>
  </control>
</view>`);
	});
	
	it("View > layout > editbox", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var ed = new SkunkSchema.Edit({
			parent: layout,
			name  : "ed",
			text  : "Hello"
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Edit' name='ed' text='Hello' left='0' top='0' width='200' height='20' marginx='5' marginy='2' imageportalsrc='4726' />
  </control>
</view>`);
	});
	
	it("View > layout > button", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var btnButton = new SkunkSchema.Button({
			parent     : layout,
			name       : "btnButton",
			icon       : 10667,
			pressedicon: 10668
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Button' name='btnButton' icon='10667' pressedicon='10668' left='0' top='0' width='200' height='168' />
  </control>
</view>`);
	});
	
	it("View > layout > progress", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var progress = new SkunkSchema.Progress({
			parent: layout,
			name  : "progress",
			value : 50
		});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Progress' name='progress' left='0' top='0' width='200' height='168' value='50' minimum='' maximum='' fillcolor='' bordercolor='' border='' align='' pretext='' posttext='' drawtext='' facecolor='' />
  </control>
</view>`);
	});
	
	it("View > layout > slider", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var slider = new SkunkSchema.Slider({
			parent: layout,
			name  : "slider"
		});
		slider.setValue(50);
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Slider' name='slider' left='0' top='0' width='200' height='168' vertical='' value='' minimum='' maximum='' default='50' />
  </control>
</view>`);
	});
	
	it("Add option to choice", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var cho = new SkunkSchema.Choice({
			parent  : layout,
			name    : "cho",
			text    : "Hello",
			children: [
				new SkunkSchema.Option({text: "Hello"})
			]
		});
		cho.addChoice("abc");
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='viewTitle' icon='0' width='200' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Choice' selected='0' name='cho' left='0' top='0' width='200' height='20'>
      <option text='Hello' data='Hello'/>
      <option text='abc' data='abc'/>
    </control>
  </control>
</view>`);
	});
	
	it("Notebook with 20px above and below", () => {
		var view = new SkunkSchema.View({
			title : "Test", 
			width : 300, 
			height: 200, 
			icon  : 7400
		});
		var fixedLayout = new SkunkSchema.FixedLayout({parent: view});
		var notebookView = new SkunkSchema.Notebook({
			parent: fixedLayout, 
			name  : "notebookView",
			left  : 0,
			top   : 20,
			width : fixedLayout.getWidth(),
			height: fixedLayout.getHeight() - 40
		});
		var pageStatus = new SkunkSchema.Page({
			parent: notebookView, 
			label : "Status"
		});
		var statusLayout = new SkunkSchema.FixedLayout({parent: pageStatus});
		var btnTOPLEFT = new SkunkSchema.PushButton({
			parent: statusLayout,
			name  : "btnTOPLEFT",
			text  : "TOPLEFT",
			width : 100,
			height: 20
		}).setAnchor(statusLayout, "TOPLEFT", "TOPLEFT");
		var btnTOPRIGHT = new SkunkSchema.PushButton({
			parent: statusLayout,
			name  : "btnTOPRIGHT",
			text  : "TOPRIGHT",
			width : 100,
			height: 20
		}).setAnchor(statusLayout, "TOPRIGHT", "TOPRIGHT");
		var btnBOTTOMLEFT = new SkunkSchema.PushButton({
			parent: statusLayout,
			name  : "btnBOTTOMLEFT",
			text  : "BOTTOMLEFT",
			width : 100,
			height: 20
		}).setAnchor(statusLayout, "BOTTOMLEFT", "BOTTOMLEFT");
		var btnBOTTOMRIGHT = new SkunkSchema.PushButton({
			parent: statusLayout,
			name  : "btnBOTTOMRIGHT",
			text  : "BOTTOMRIGHT",
			width : 100,
			height: 20
		}).setAnchor(statusLayout, "BOTTOMRIGHT", "BOTTOMRIGHT");
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='Test' icon='7400' width='300' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Notebook' name='notebookView' left='0' top='20' width='300' height='128'>
      <page label='Status'>
        <control progid='DecalControls.FixedLayout'>
          <control name='btnTOPLEFT' progid='DecalControls.PushButton' text='TOPLEFT' left='0' top='0' width='100' height='20' />
          <control name='btnTOPRIGHT' progid='DecalControls.PushButton' text='TOPRIGHT' left='200' top='0' width='100' height='20' />
          <control name='btnBOTTOMLEFT' progid='DecalControls.PushButton' text='BOTTOMLEFT' left='0' top='92' width='100' height='20' />
          <control name='btnBOTTOMRIGHT' progid='DecalControls.PushButton' text='BOTTOMRIGHT' left='200' top='92' width='100' height='20' />
        </control>
      </page>
    </control>
  </control>
</view>`);
	});
	
	it("Button anchored to left side of button", () => {
		
		var view = new SkunkSchema.View({
			title : "TITLE", 
			width : 300, 
			height: 300, 
			icon  : 7400
		});

		var fixedLayout = new SkunkSchema.FixedLayout({parent: view});
		
		var btnExit = new SkunkSchema.PushButton({
			parent: fixedLayout,
			name: "btnExit",
			text: "Exit",
			width: 50,
			height: 20
		})
		.setAnchor(fixedLayout, "BOTTOMRIGHT", "BOTTOMRIGHT");

		var btnTest = new SkunkSchema.PushButton({
			parent: fixedLayout,
			name: "btnTest",
			text: "Test",
			width: 50,
			height: 20
		})
		.setAnchor(btnExit, "TOPLEFT", "TOPRIGHT");
		
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
			`<view title='TITLE' icon='7400' width='300' height='300'>
  <control progid='DecalControls.FixedLayout'>
    <control name='btnExit' progid='DecalControls.PushButton' text='Exit' left='250' top='248' width='50' height='20' />
    <control name='btnTest' progid='DecalControls.PushButton' text='Test' left='200' top='248' width='50' height='20' />
  </control>
</view>`);
		
	});

	it("Choice with 'selected' set but no children should throw error.", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		var layout = new SkunkSchema.FixedLayout({parent: view});
		var cho = new SkunkSchema.Choice({
			parent  : layout,
			name    : "cho",
			text    : "Hello",
			children: []
		});
		cho.selected = 0;
		expect(function() {
			view.toXML();
		}).toThrowError('Invalid XML: list \'cho\' cannot set choice selected without children.');
	});
	
	it("showDropdownMenuPrompt()", (done) => {
		jest.spyOn(skapi, 'ShowControls').mockImplementationOnce(function(schema, visable) {
			expect(schema).toBe(
`<view title='Dropdown Prompt' icon='0' width='200' height='72'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Choice' selected='1' name='choMenu' left='0' top='0' width='200' height='20'>
      <option text='Select...' data='Select...'/>
      <option text='hello' data='hello'/>
    </control>
    <control name='btnCancel' progid='DecalControls.PushButton' text='Cancel' left='0' top='20' width='100' height='20' />
    <control name='btnConfirm' progid='DecalControls.PushButton' text='Confirm' left='100' top='20' width='100' height='20' />
  </control>
</view>`);
			done();
		});
		SkunkSchema.showDropdownMenuPrompt({options:[{text:"hello"}], selected: 1}, function(err, results) {});
	});
	
	it("showEditPrompt()", (done) => {
		jest.spyOn(skapi, 'ShowControls').mockImplementationOnce(function(schema, visable) {
			
			
			
			expect(schema).toBe(
`<view title='demo' icon='0' width='200' height='72'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Edit' name='edText' width='200' height='20' top='0' left='0' text='foobar' imageportalsrc='4726' />
    <control progid='DecalControls.PushButton' name='btnCancel' width='100' height='20' top='20' left='0' text='Cancel' />
    <control progid='DecalControls.PushButton' name='btnConfirm' width='100' height='20' top='20' left='100' text='Confirm' />
  </control>
</view>`);
			done();
		});
		SkunkSchema.showEditPrompt({title: "demo", value:"foobar"}, function(err, results) {})
	});
	
	it("showConfirmationPrompt()", (done) => {
		jest.spyOn(skapi, 'ShowControls').mockImplementationOnce(function(schema, visable) {
			//console.log("schema: " + schema);
			
			expect(schema).toBe(
`<view title='demo' icon='0' width='200' height='52'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.PushButton' name='btnCancel' width='100' height='20' top='0' left='0' text='Cancel' />
    <control progid='DecalControls.PushButton' name='btnConfirm' width='100' height='20' top='0' left='100' text='Confirm' />
  </control>
</view>`);
			done();
		});
		SkunkSchema.showConfirmationPrompt({title: "demo"}, function(err, results) {})
	});
	
	it("View with script file", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		view.scripts.push({language: "JScript", src: "./viewFunctions.js"});
		
		var layout = new SkunkSchema.FixedLayout({parent: view});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
`<view title='viewTitle' icon='0' width='200' height='200'>
  <script language='JScript' src='./viewFunctions.js'/>
  <control progid='DecalControls.FixedLayout'>
  </control>
</view>`);
	});
	
	it("View with script function", () => {
		var view = new SkunkSchema.View({
			title : "viewTitle",
			icon  : 0,
			width : 200,
			height: 200
		});
		view.cdata.push({language: "JScript", str: "function foo() {}"});
		
		var layout = new SkunkSchema.FixedLayout({parent: view});
		expect(SkunkSchema.prettifyXml(view.toXML())).toBe(
`<view title='viewTitle' icon='0' width='200' height='200'>
  <script language='JScript'>
    <![CDATA[function foo() {}]]>
  </script>
  <control progid='DecalControls.FixedLayout'>
  </control>
</view>`);
	});
	
	it("Element tag", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			SkunkSchema.Element("control", {progid:'DecalControls.FixedLayout'}, [
				SkunkSchema.Element("control", {progid:'DecalControls.PushButton', name: "btnButton", text:"Button", left:0, top:0, width:50, height:20})
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.PushButton' name='btnButton' text='Button' left='0' top='0' width='50' height='20' />
  </control>
</view>`);
	});
	
	it("Element tag with children using parent's size", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, function(parent) {
			return [
				SkunkSchema.Element(parent, "control", {progid:'DecalControls.FixedLayout'}, function(parent) {
					return [
						SkunkSchema.Element(parent, "control", {progid:'DecalControls.PushButton', name: "btnButton2", text:"Button2", left:0, top:0, width:parent.getWidth(), height:parent.getHeight()})
					];
				})
			];
		});
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.PushButton' name='btnButton2' text='Button2' left='0' top='0' width='400' height='168' />
  </control>
</view>`);
	});
	
	it("Element tag with notebook and child", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			SkunkSchema.Element("control", {progid:'DecalControls.FixedLayout'}, [
				SkunkSchema.Element("control", {progid:'DecalControls.Notebook', name: "notebookView", left:0, top:0, width: 400, height: 168}, [
					SkunkSchema.Element("page", {label:'Page 1'}, [
						SkunkSchema.Element("control", {progid:'DecalControls.FixedLayout'}, [
							SkunkSchema.Element("control", {progid:'DecalControls.PushButton', name: "btnButton2", text:"Button2", left:0, top:0, width:100, height:20})
						])
					])
				])
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Notebook' name='notebookView' left='0' top='0' width='400' height='168'>
      <page label='Page 1'>
        <control progid='DecalControls.FixedLayout'>
          <control progid='DecalControls.PushButton' name='btnButton2' text='Button2' left='0' top='0' width='100' height='20' />
        </control>
      </page>
    </control>
  </control>
</view>`);
	});
	
	it("Element tag with notebook and children using parent size", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, function(parent) {
			return [
				SkunkSchema.Element(parent, "control", {progid:'DecalControls.FixedLayout'}, function(parent) {
					return [
						SkunkSchema.Element(parent, "control", {progid:'DecalControls.Notebook', name: "notebookView", left:0, top:0, width: parent.getWidth(), height: parent.getHeight()}, function(parent) {
							return [
								SkunkSchema.Element(parent, "page", {label:'Page 1'}, function(parent) {
									return [
										SkunkSchema.Element(parent, "control", {progid:'DecalControls.FixedLayout'}, function(parent) {
											return [
												SkunkSchema.Element(parent, "control", {progid:'DecalControls.PushButton', name: "btnButton2", text:"Button2", left:0, top:0, width:parent.getWidth(), height:parent.getHeight()})
											];
										})
									];
								})
							];
						})
					];
				})
			];
		});
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Notebook' name='notebookView' left='0' top='0' width='400' height='168'>
      <page label='Page 1'>
        <control progid='DecalControls.FixedLayout'>
          <control progid='DecalControls.PushButton' name='btnButton2' text='Button2' left='0' top='0' width='400' height='152' />
        </control>
      </page>
    </control>
  </control>
</view>`);
	});

	it("Element, string as second argument", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			SkunkSchema.Element("control", {progid: 'DecalControls.list', name: "lstConfigProfileGeneral"}, [
				SkunkSchema.Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 400}),
				SkunkSchema.Element("row", "test")
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.list' name='lstConfigProfileGeneral'>
    <column progid='DecalControls.TextColumn' fixedwidth='400' />
    <row>test</row>
  </control>
</view>`);
	});
	
	it("Element with events example", () => {
		var Element = SkunkSchema.Element;
		var schema = Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			Element("control", {progid:'DecalControls.FixedLayout'}, [
				Element("control", {progid:'DecalControls.Notebook', name: "notebookView", left:0, top:0, width: 400, height: 168, onControlEvent: function(szPanel, szControl, dictSzValue) {
					core.info("Tab clicked!", szPanel, szControl);
				}}, [
					Element("page", {label:'Page 1'}, [
						Element("control", {progid:'DecalControls.FixedLayout'}, [
							Element("control", {progid: 'DecalControls.Edit',       name: "edCopperScarab", left: 0, top: 0,  width: 100, height: 20, text: 0,  imageportalsrc: 4726}),
							Element("control", {progid:'DecalControls.PushButton',  name: "btnButton2",	    left: 0, top: 20, width: 100, height: 20, text: "Button2", onControlEvent: function(szPanel, szControl, dictSzValue) {
								var edCopperScarab = this.getElement("edCopperScarab");
								edCopperScarab.getControlProperty("text", function(err, results) {
									if (err) return core.warn("Error getting property", err);
									core.info("edCopperScarab's value is " + results);
								});
							}}),
						])
					])
				])
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Notebook' name='notebookView' left='0' top='0' width='400' height='168'>
      <page label='Page 1'>
        <control progid='DecalControls.FixedLayout'>
          <control progid='DecalControls.Edit' name='edCopperScarab' left='0' top='0' width='100' height='20' text='0' imageportalsrc='4726' />
          <control progid='DecalControls.PushButton' name='btnButton2' left='0' top='20' width='100' height='20' text='Button2' />
        </control>
      </page>
    </control>
  </control>
</view>`);
	});
	
	
});