/********************************************************************************************\
	File:           SkunkSchema-1.0.js
	Purpose:        Objected oriented GUI schema creation for Skunkworks.
	Creator:        Cyprias
	Date:           02/20/2018
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkSchema-1.0";
var MINOR = 200422;

(function (factory) {
	
	var dependencies = {};
	
	dependencies.EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof dependencies.EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
	}
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkSchema10 = factory(dependencies);
	}
}(function (dependencies) {

	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	
	core.debugging = false;
	
	core.defaultWidth = 100;
	core.defaultHeight = 20;
	
	var EventEmitter = dependencies.EventEmitter;
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	core.setInterval = setInterval;
	core.setTimeout = setTimeout;
	core.setImmediate = setImmediate;
	
	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(core.MAJOR + " " + args.join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (core.debugging != true) return;
		skapi.OutputLine(core.MAJOR + " " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};
	
	if (typeof jstoxml === "undefined") {
		if (typeof require === "function") {
			core.console(core.MAJOR, "Loading jstoxml...");
			jstoxml = require("SkunkSuite\\SkunkSchema-1.0\\jstoxml.js");
		} else {
			core.console(core.MAJOR, "jstoxml is undefined! You need to load jstoxml.js prior to SkunkSchema in your swx file.");
			skapi.WaitEvent(1000, wemFullTimeout);
		}
	}

	function setLeft(value) {
		this.xml._attrs.left = value;
		return this;
	}
		
	function getPointOffset(point) {
		if (typeof point === "undefined") {
			point = "TOPLEFT";
		}
		
		var left;
		var top;
		
		if (point.match("LEFT")) {
			left = 0;
		} else if (point.match("RIGHT")) {
			left = this.getWidth();
		} else {
			left = (this.getWidth() / 2);
		}
		
		if (point.match("TOP")) {
			top = 0;
		} else if (point.match("BOTTOM")) {
			top = this.getHeight();
		} else {
			top = (this.getHeight() / 2);
		}
		return {left: left, top: top};
	}
	
	function updateLocations() {
		core.debug(core.MAJOR, "<updateLocations> " + this.xml);

		if (this.anchorRelativeTo) {
			var offsetX = (this.offsetX || 0);
			var offsetY = (this.offsetY || 0);
		
			// Get the offset coords for our current control's anchor point. Default is topleft, so 0,0. If say they wanted to anchor themselves to a frame to their right, they'd set their anchorPoint to TOPRIGHT and anchor to the other frame's TOPLEFT point.
			var myPointOffset = this.getPointOffset(this.anchorPoint);
			if (typeof JSON !== "undefined") {
				//core.debug(this.getName() + " myPointOffset: " + JSON.stringify(myPointOffset));
			}

			offsetX += myPointOffset.left;
			offsetY += myPointOffset.top;

			if (typeof JSON !== "undefined") {
				//core.debug("Getting point from " + JSON.stringify(this.anchorRelativeTo.xml));
			}
			if (this.anchorRelativeTo.getPoint) {
				var theirPoint = this.anchorRelativeTo.getPoint(this.anchorRelativePoint);
				if (theirPoint) {

					if (typeof JSON !== "undefined") {
						//core.debug("Updating " + this.getName() + "'s coords to anchor to " + this.anchorRelativeTo.getName() + ". " + JSON.stringify(theirPoint));
					}

					this.setLeft(theirPoint.left - offsetX);
					this.setTop(theirPoint.top - offsetY);
				}
			}
		}
	}
	
	function toXML() {
		updateLocations.apply(this);
		return jstoxml.toXML(this.xml, {header: false, indent: '\t'}); 
	}

	function getContent() {
		if (this.children) {
			var xml = "";
			var child;
			for (var i = 0; i < this.children.length; i++) {
				child = this.children[i];
				xml += child.toXML();
			}
			return xml;
		}
	}
	
	function addChild(child) {
		//	core.debug("Adding child " + child);
		this.children = this.children || [];
		this.children.push(child);
		var self = this;
		this.xml._content = this.xml._content || function _getContent() {
			return getContent.apply(self);
		};
		return this;
	}
	
	function removeChild(child) {
		if (this.children) {
			var c;
			for (var i = this.children.length - 1; i >= 0; i--) {
				c = this.children[i];
				if (c == child) {
					this.children.splice(i, 1);
				}
			}
		}
		return this;
	}
	
	function getTitle() {
		return this.xml._attrs.title;
	}
	
	function setTitle(value) {
		this.xml._attrs.title = value;
		return this;
	}
	
	function setWidth(value) {
		this.xml._attrs.width = value;
		return this;
	}
	
	function getWidth() {
		return this.xml._attrs.width;
	}
	
	function setHeight(value) {
		this.xml._attrs.height = value;
		return this;
	}
	
	function getHeight() {
		if (this.xml._name == "view") {
			return this.xml._attrs.height - 30; // The title/minimize button uses 30px.
		} else if (this.xml._attrs.progid == "DecalControls.Notebook") {
			return this.xml._attrs.height - 16; // tabs are 16 pixels.
		}

		return this.xml._attrs.height;
	}
	
	function setIcon(value) {
		this.xml._attrs.icon = value;
		return this;
	}
	
	function getIcon() {
		return this.xml._attrs.icon;
	}

	core.View = function factory() {
		var names = {};
		function View(params) { // title, width, height, icon
			this.isShown = false;
		
			this.xml = {};
			this.children = [];
			this.xml._name      = 'view';
			this.xml._attrs     = {};
			this.xml._attrs.title	        = params.title;
			
			names[params.title] = names[params.title] || 0;
			names[params.title] += 1;
			if (names[params.title] > 1) {
				this.xml._attrs.title += " #" + names[params.title];
			}
			
			
			//this.xml._attrs.iconlibrary     = "Inject.dll";
			this.xml._attrs.width           = params.width;
			this.xml._attrs.height          = params.height;
			this.xml._attrs.icon            = params.icon || 128;
		}
		
		View.prototype.toXML            = toXML;
		View.prototype.addChild         = addChild;
		View.prototype.removeChild      = removeChild;
		View.prototype.getTitle         = getTitle;
		View.prototype.setTitle         = setTitle;
		View.prototype.setWidth         = setWidth;
		View.prototype.getWidth         = getWidth;
		View.prototype.setHeight        = setHeight;
		View.prototype.getHeight        = getHeight;
		
		View.prototype.getIcon          = getIcon;
		View.prototype.setIcon          = setIcon;
		
		View.prototype.getPanel         = function getPanel() {
			return this;
		};
		
		View.prototype.showControls     = function showControls(fActivate) {
			if (this.isShown) {
				// If view is already shown, throw a error. Parent script is being stupid.
				throw new core.Error("Panel already shown");
			}

			registerControls(this);
			var xml = this.toXML();
			//core.console(core.MAJOR, "xml: " + xml);
			
			skapi.ShowControls(xml, fActivate);
			setControlsShown(this, true);
			
			emitShown(this);
			
			//core.setImmediate(setControlsShown, this, true);
			this.isShown = true;
			return this;
		};
		
		View.prototype.removeControls   = function removeControls() {
			unregisterControls(this);
			skapi.RemoveControls(this.xml._attrs.title);
			setControlsShown(this, false);
			
			emitHidden(this);

			//core.setImmediate(setControlsShown, this, false);
			this.isShown = false;
			return this;
		};

		function CDATA(text, language) {
			this.text = text;
			this.language = language || "JavaScript";
		}
		CDATA.prototype.toXML            = function() {
			var xml = "<script language='" + this.language + "'><![CDATA[";
			xml += this.text;
			xml += "]]></script>";
			return xml;
		};

		View.prototype.addCDATA     = function addCDATA(text, language) {
			this.addChild(new CDATA(text, language));
		};

		View.prototype.callPanelFunction     = function callPanelFunction(szFunction, arg1, arg2, arg3, arg4, arg5) {
			if (typeof arg1 == "object") {
				throw new core.Error("Panel function arg1 must be a string or number due to SkunkWorks serialization limitations.");
			} else if (typeof arg2 == "object") {
				throw new core.Error("Panel function arg2 must be a string or number due to SkunkWorks serialization limitations.");
			} else if (typeof arg3 == "object") {
				throw new core.Error("Panel function arg3 must be a string or number due to SkunkWorks serialization limitations.");
			} else if (typeof arg4 == "object") {
				throw new core.Error("Panel function arg4 must be a string or number due to SkunkWorks serialization limitations.");
			} else if (typeof arg5 == "object") {
				throw new core.Error("Panel function arg5 must be a string or number due to SkunkWorks serialization limitations.");
			}

			if (typeof this.xml === "undefined") {
				throw new core.Error("callPanelFunction called without a this object.");
			}

			// We can't call apply on skapi functions. 
			skapi.CallPanelFunction(this.xml._attrs.title, szFunction, arg1, arg2, arg3, arg4, arg5);
		};

		return View;
	}();
	
	function getProgID() {
		return this.xml._attrs.progid;
	}
	
	function getLeft() {
		return this.xml._attrs.left;
	}
	
	function getTop() {
		return this.xml._attrs.top;
	}
	
	function getPoint(point) {
		/*
			anchorPoints
		CENTER
		BOTTOM
		TOP
		LEFT
		RIGHT
		BOTTOMLEFT
		BOTTOMRIGHT
		TOPLEFT
		TOPRIGHT
		*/
		if (!point) {
			point = "TOPLEFT";
		}
		
		var left = 0;
		var top = 0;
		
		var getLeft = this.getLeft() || 0;
		var getTop = this.getTop() || 0;
		
		var getWidth = this.getWidth() || 0;
		var getHeight = this.getHeight() || 0;
		
		
		core.debug(core.MAJOR, "<getPoint> getName: " + this.getName() + ", getLeft: " + getLeft + ", getWidth: " + getWidth + ", getTop: " + getTop + ", getHeight: " + getHeight);
		
		if (point.match("LEFT")) {
			left = getLeft;
		} else if (point.match("RIGHT")) {
			left = getLeft + getWidth;
		} else {
			left = getLeft + (getWidth / 2);
		}
		
		if (point.match("TOP")) {
			top = getTop;
		} else if (point.match("BOTTOM")) {
			top = getTop + getHeight;
		} else {
			top = getTop + (getHeight / 2);
		}

		core.debug(core.MAJOR, "<getPoint> " +  point + ", left: " + left + ", top: " + top);
		return {left: left, top: top};
	}
	
	function getName() {
		return this.xml._attrs.name;
	}
	
	function setName(value) {
		this.xml._attrs.name = value;
		this.xml._attrs.text = this.xml._attrs.text || value;
		return this;
	}
	
	function setParent(value) {
		if (this.parent) {
			this.parent.removeChild(this);
		}
		
		this.parent = value;
		value.addChild(this);
		
		if (this.parent && this.parent.xml && this.parent.xml._attrs.progid == "DecalControls.FixedLayout") {
			this.xml._attrs.width   = this.xml._attrs.width || core.defaultWidth;
			this.xml._attrs.height  = this.xml._attrs.height || core.defaultHeight;
		}
			
		return this;
	}
	
	function getParent() {
		return this.parent;
	}
	
	core.FixedLayout = function factory() {
		
		function FixedLayout(params) {
			if (!(this instanceof FixedLayout)) {
				return new FixedLayout(params);
			}

			EventEmitter.call(this);

			if (params && params.parent) {
				this.parent = params.parent;
				params.parent.addChild(this);
			}	
		
			this.xml = {};
			this.xml._name = 'control';
			this.xml._attrs = {};
			this.xml._attrs.progid	= "DecalControls.FixedLayout";
			this.xml._attrs.clipped   = params && params.clipped || 0;
		}
		
		FixedLayout.prototype = new EventEmitter();
		FixedLayout.prototype.constructor      = FixedLayout;
		
		FixedLayout.prototype.getWidth = function getWidth() {
			return this.parent && this.parent.getWidth();
		};
		
		FixedLayout.prototype.getHeight = function getHeight() {
			core.debug(core.MAJOR, "FixedLayout.getHeight");
			
			//core.debug("FixedLayout parent progid: " + this.parent.xml._attrs.progid); //this.xml._attrs.progid
			//core.debug("FixedLayout parent title: " + this.parent.xml._attrs.title);
			//core.debug("FixedLayout parent label: " + this.parent.xml._attrs.label);

			return this.parent && this.parent.getHeight();
		};
		
		FixedLayout.prototype.toXML             = toXML;
		FixedLayout.prototype.addChild          = addChild;
		FixedLayout.prototype.removeChild       = removeChild;
		FixedLayout.prototype.getProgID         = getProgID;
		FixedLayout.prototype.getPoint          = getPoint;
		FixedLayout.prototype.getLeft           = getLeft;
		FixedLayout.prototype.getTop            = getTop;
		FixedLayout.prototype.getName           = getName;
		FixedLayout.prototype.setParent         = setParent;
		FixedLayout.prototype.getParent         = getParent;
		FixedLayout.prototype.getPanel          = getPanel;

		FixedLayout.prototype.setClipped = function setClipped(value) {
			this.xml._attrs.clipped = value;
			return this;
		};
		FixedLayout.prototype.getClipped = function getClipped() {
			return this.xml._attrs.clipped;
		};
		
		return FixedLayout;
	}();
	
	function setProperty(property,  value) { // szProperty, vValue
		core.debug(core.MAJOR, "<setProperty>", property, value, isControlShown(this));
		if (isControlShown(this)) {
			skapi.SetControlProperty(this.getPanel().getTitle(), this.getName(), property, value);
		}
		return this;
	}
	
	//function getNewUUID() {
	//	return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	//};
	
	function getPropertyAsync(szProperty, callback, thisArg) {
		return getControlPropertyAsync(this.getPanel().getTitle(), this.getName(), szProperty, callback, thisArg);
	}

	/*
	 * getControlPropertyAsync() retrieves a control's property value and feeds it to a callback or promise.
	 *
	 * @param {string} szPanel: Name of the view panel.
	 * @param {string} szControl: Name of the control.
	 * @param {string} szProperty: Name of the property.
	 * @param {function} callback:  Callback function to be called.
	 * @return {promise} If no callback is provided, a promise will be returned.
	 */
	function getControlPropertyAsync(szPanel, szControl, szProperty, callback, thisArg) {
		if (typeof callback !== "function" && typeof Promise !== "undefined") {
			// If no callback provided, return a promise (requires ES6-shim).
			// http://loige.co/to-promise-or-to-callback-that-is-the-question/
			return new Promise(function(resolve, reject) {
				getControlPropertyAsync(szPanel, szControl, szProperty, function promise_callback(err, results) {
					if (err) return reject(err);
					resolve(results);
				});
			});
		}

		var handler = {};
		
		// Event handler when skapi tells us the value.
		handler.OnControlProperty = function OnControlProperty(szPanel2, szControl2, szProperty2, vValue) {
			//fDebug("<fOnControlProperty> " + szPanel2 + ", " + szControl + ", " + szProperty2 + ", " + vValue);
			if (szPanel2 == szPanel && szControl2 == szControl && (szProperty2 == szProperty || szProperty2 == "data")) {
				skapi.RemoveHandler(evidNil, handler);
				callback.call(thisArg, undefined, vValue);
			}
		};
		skapi.AddHandler(evidOnControlProperty,	handler);
		skapi.GetControlProperty(szPanel, szControl, szProperty);
	}
	
	/*
	 * getControlProperty: Return the value of a control's property.
	 */
	function getControlProperty(szPanel, szControl, szProperty) {

		var _result;
		getControlPropertyAsync(szPanel, szControl, szProperty, function gotAsyncProperty(result) {
			_result = result;
		});

		var starting = (new Date()).getTime();
		var remaining = 1000;
		do {
			skapi.WaitEvent(100, wemSpecific, evidOnControlProperty);
			remaining = 1000 - ((new Date()).getTime() - starting);
		} while (typeof _result === "undefined" && remaining > 0);

		return _result;
	}
	
	function getProperty(szProperty) {
		return getControlProperty(this.getPanel().getTitle(), this.getName(), szProperty);
	}
	
	function setTop(value) {
		this.xml._attrs.top = value;
		return this;
	}
	
	function setOffsetX(value) {
		this.offsetX = value;
		return this;
	}
	
	function setOffsetY(value) {
		this.offsetY = value;
		return this;
	}
	
	function getOffsetX() {
		return this.offsetX;
	}
	
	function getOffsetY() {
		return this.offsetY;
	}
	
	function setAnchor(relativeTo, relativePoint, point) {
		core.debug(core.MAJOR, "<setAnchor> " + relativeTo + ", " + relativePoint + ", " + point);
		this.anchorRelativeTo = relativeTo;
		this.anchorRelativePoint = relativePoint;
		this.anchorPoint = point;
		return this;
	}
	
	function getPoints() {
		return {
			CENTER     : getPoint.apply(this, ["CENTER"]),
			BOTTOM     : getPoint.apply(this, ["BOTTOM"]),
			TOP        : getPoint.apply(this, ["TOP"]),
			LEFT       : getPoint.apply(this, ["LEFT"]),
			RIGHT      : getPoint.apply(this, ["RIGHT"]),
			BOTTOMLEFT : getPoint.apply(this, ["BOTTOMLEFT"]),
			BOTTOMRIGHT: getPoint.apply(this, ["BOTTOMRIGHT"]),
			TOPLEFT    : getPoint.apply(this, ["TOPLEFT"]),
			TOPPRIGHT  : getPoint.apply(this, ["TOPPRIGHT"])
		};
	}
	
	/*
	function setControlEvent(method, thisArg) {
		var self = this;
		return this;
	};
	*/	
	
	function getValue() {
		return getControlValue(this);
	}
	
	function getPanel() {
		if (this.parent) {
			return this.parent.getPanel();
		} else if (this.getTitle) {
			return this;
		}
	}

	core.Control = function factory() {
		function Control(params) {
			if (!(this instanceof Control)) {
				return new Control(params);
			}
			
			EventEmitter.call(this);
			
			if (params && params.parent) {
				this.parent = params.parent;
				params.parent.addChild(this);
			}	

			this.xml = {};
			this.xml._name = 'control';
			this.xml._attrs = {};
			this.xml._attrs.progid	= params && params.progid;
			this.xml._attrs.name    = params && params.name; // note, if xml._attrs.name never gets set the xml won't display.

			if (this.parent && this.parent.xml && this.parent.xml._attrs.progid == "DecalControls.FixedLayout") {
				this.xml._attrs.width   = core.defaultWidth;
				this.xml._attrs.height  = core.defaultHeight;
			}
		}
		
		Control.prototype = new EventEmitter();
		Control.prototype.constructor      = Control;
		
		Control.prototype.toXML             = toXML;
		Control.prototype.setLeft           = setLeft;
		Control.prototype.getLeft           = getLeft;
		Control.prototype.setTop            = setTop;
		Control.prototype.getTop            = getTop;
		Control.prototype.getName           = getName;
		Control.prototype.setName           = setName;
		Control.prototype.getProgID         = getProgID;
		Control.prototype.setOffsetX        = setOffsetX;
		Control.prototype.setOffsetY        = setOffsetY;
		Control.prototype.getOffsetX        = getOffsetX;
		Control.prototype.getOffsetY        = getOffsetY;
		Control.prototype.setWidth          = setWidth;
		Control.prototype.getWidth          = getWidth;
		Control.prototype.setHeight         = setHeight;
		Control.prototype.getHeight         = getHeight;
		Control.prototype.setAnchor         = setAnchor;
		Control.prototype.getPointOffset    = getPointOffset;
		Control.prototype.getPoints         = getPoints;
		Control.prototype.getPoint          = getPoint;
		Control.prototype.setProperty       = setProperty;
		Control.prototype.getProperty       = getProperty;
		Control.prototype.getPropertyAsync  = getPropertyAsync;
		Control.prototype.getValue          = getValue;
		Control.prototype.getPanel          = getPanel;
		Control.prototype.setParent         = setParent;
		Control.prototype.getParent         = getParent;

		return Control;
	}();
	
	function setText(value) {
		this.xml._attrs.text = value;
		this.setProperty("text", value);
		return this;
	}
	
	function getText() {
		return this.xml._attrs.text;
	}
	
	function setTextColor(value) {
		this.xml._attrs.textcolor = value;
		this.setProperty("textcolor", value);
		return this;
	}
	
	function getTextColor() {
		return this.xml._attrs.textcolor;
	}
	
	function setFaceColor(value) {
		this.xml._attrs.facecolor = value;
		this.setProperty("facecolor", value);
		return this;
	}
	
	function getFaceColor() {
		return this.xml._attrs.facecolor;
	}
	
	core.PushButton = function factory() {
		function PushButton(params) {
			if (!(this instanceof PushButton)) {
				return new PushButton(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.PushButton", name: params && params.name}); // parent, "DecalControls.PushButton", params.name
			this.xml._attrs.text    = params && params.text || this.xml._attrs.name;
		}
		PushButton.prototype = new core.Control();
		PushButton.prototype.constructor       = PushButton;
		PushButton.prototype.setText           = setText;
		PushButton.prototype.getText           = getText;
		PushButton.prototype.setTextColor	   = setTextColor;
		PushButton.prototype.getTextColor      = getTextColor;
		PushButton.prototype.setFaceColor      = setFaceColor;
		PushButton.prototype.getFaceColor      = getFaceColor;
		
		return PushButton;
	}();
	
	//<control progid="DecalControls.Checkbox" name="chkBuyMMDsEarly" left="16" top="32" width="220" height="20" fontface="Times New Roman" fontsize="16" fontstyle="" text="Buy MMDs as soon as possible"/>
	core.Checkbox = function factory() {
		function Checkbox(params) {
			if (!(this instanceof Checkbox)) {
				return new Checkbox(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.Checkbox", name: params && params.name}); // parent, "DecalControls.PushButton", params.name
			this.xml._attrs.text  = params && params.text || this.xml._attrs.name;
			this.xml._attrs.checked = params && params.checked && "True" || "False"; // Decal wants a string.
			
			var panel = this.getPanel();
			if (panel) {
				setControlValue(this, this.xml._attrs.checked);
			}
		}

		Checkbox.prototype = new core.Control();
		Checkbox.prototype.constructor      = Checkbox;
		Checkbox.prototype.setText          = setText;
		Checkbox.prototype.getText          = getText;
		Checkbox.prototype.setTextColor     = setTextColor;
		Checkbox.prototype.getTextColor     = getTextColor;
		Checkbox.prototype.setChecked       = function setChecked(value) {
			this.xml._attrs.checked = value;
			this.setProperty("checked", value);
			setControlValue(this, value);
			return this;
		};
		
		Checkbox.prototype.getChecked       = function getChecked() {
			return this.getValue() == "True" || this.getValue() == true;
		};

		return Checkbox;
	}();

	core.Option = function factory() {
		function Option(params) { // parent, szText, szData
			if (params.parent) {
				this.parent = params.parent;
				this.parent.addChild(this);
			}
			
			this.xml = {};
			this.xml._name = 'option';
			this.xml._attrs = {};
			this.xml._attrs.text	= params.text;

			if (typeof params.data !== "undefined") {
				this.xml._attrs.data	= params.data;
			}
		}
		
		Option.prototype.toXML          = toXML;
		return Option;
	}();
		
	core.Row = function factory() {
		function Row(params) { // parent, szText, szData
			if (params.parent) {
				this.parent = params.parent;
				this.parent.addChild(this);
			}
			
			var columns = params.columns;
			
			this.xml = {};
			this.xml._name = 'row';
			this.xml._attrs = {};
			this.xml._content = []

			if (columns) {
				var self = this;
				columns.forEach(function(column) {
					self.xml._content.push({
						_name: 'data',
						_content: column
					});
				});
			}
		}
		
		Row.prototype.toXML          = toXML;
		return Row;
	}();
		
	core.Choice = function factory() {
		function Choice(params) {
			if (!(this instanceof Choice)) {
				return new Choice(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.Choice", name: params && params.name});
		}
		
		Choice.prototype = new core.Control();
		Choice.prototype.constructor        = Choice;
		
		Choice.prototype.addOption = function addOption(params) { // text, data
			//params.parent = params.parent || this;
			new core.Option({parent: this, text: params.text, data: params.data}); // this, text, data
			if (typeof this.xml._attrs.selected === "undefined") {
				this.xml._attrs.selected = 0;
			}

			// If the panel isn't up it'll error. 
			if (typeof params.data !== "undefined") {
				skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "AddChoice('" + params.text + "','" + params.data + "')");
			} else {
				skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "AddChoice('" + params.text + "')");
			}
			
			return this;
		};
		
		Choice.prototype.removeOption = function removeOption(params) { // text, data
			var c;
			for (var i = this.children.length - 1; i >= 0; i--) {
				c = this.children[i];
				if (c.xml._name == "option" && (params.data && c.xml._attrs.data == params.data || c.xml._attrs.text == params.text)) {
					this.children.splice(i, 1);
					break;
				}
			}
			if (this.children.length == 0) {
				delete this.xml._attrs.selected;
			}
			return this;
		};
		
		Choice.prototype.addChild           = addChild;
		Choice.prototype.removeChild        = removeChild;
		/*
		Choice.prototype.toXML              = function _toXML() {
			var first = this.children.length && this.children[0];
			if (first) {
				setControlValue(this, first.xml._attrs.data || first.xml._attrs.text);
			}
			return toXML.apply(this, arguments);
		};
		*/
		
		/*
		Choice.prototype.setValue           = function setValue(value) {
			this.xml._attrs.selected = value;
			this.setProperty("selected", value);
			setControlValue(this, value);
			return this;
		};
		*/
		
		Choice.prototype.setSelected           = function setSelected(value) {
			this.xml._attrs.selected = value;
			this.setProperty("selected", value);
			
			// Update current value.
			var optionValue = this.children[value].xml._attrs.data || this.children[value].xml._attrs.text;
			setControlValue(this, optionValue);
			return this;
		};
		
		Choice.prototype.getSelected           = function getSelected() {
			return this.xml._attrs.selected;
		};
		
		//Choice.prototype.setValue           = function setValue(value) {};

		Choice.prototype.clear              = function clear() {
			this.children = [];
			skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "Clear()");
			return this;
		};

		return Choice;
	}();
	
	core.Notebook = function factory() {
		function Notebook(params) {
			if (!(this instanceof Notebook)) {
				return new Notebook(params);
			}
			
			EventEmitter.call(this);
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.Notebook", name: params && params.name});
		}
		
		Notebook.prototype = new core.Control();
		Notebook.prototype.constructor      = Notebook;
		Notebook.prototype.addChild         = addChild;
		Notebook.prototype.removeChild      = removeChild;

		Notebook.prototype.setValue = function setValue(value) {
			this.setProperty("ActiveTab", value);
			return this;
		};
		
		Notebook.prototype.getWidth         = function getWidth() {
			return this.parent && this.parent.getWidth();
		};
		
		Notebook.prototype.getHeight = getHeight;
		return Notebook;
	}();
	
	core.Page = function factory() {
		function Page(params) {
			if (!(this instanceof Page)) {
				return new Page(params);
			}
		
			EventEmitter.call(this);
		
			if (params.parent) {
				this.parent = params.parent;
				core.debug(core.MAJOR, "Adding page to parent... " + params.parent);
				params.parent.addChild(this);
			}	
			this.xml = {};
			this.xml._name = 'page';
			this.xml._attrs = {};
			this.xml._attrs.label	= params.label || params.text; 
		}

		Page.prototype = new EventEmitter();
		Page.prototype.constructor      = Page;

		Page.prototype.toXML            = toXML;
		Page.prototype.addChild         = addChild;
		Page.prototype.removeChild      = removeChild;
		Page.prototype.getProgID        = getProgID; // Will return nothing. =/
		Page.prototype.getWidth         = function getWidth() {
			return this.parent.getWidth();
		};
		Page.prototype.getHeight        = function getHeight() {
			core.debug(core.MAJOR, this.xml._attrs.label + " Page parent: " + this.parent.xml._name);
			core.debug(core.MAJOR, this.xml._attrs.label + " Page parent progid: " + this.parent.xml._attrs.progid);
			return this.parent.getHeight();
		};
		Page.prototype.getPanel          = getPanel;
		
		return Page;
	}();
	
	core.Edit = function factory() {
		function Edit(params) {
			if (!(this instanceof Edit)) {
				return new Edit(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.Edit", name: params && params.name});
			
			if (typeof params.text !== "undefined") {
				this.xml._attrs.text                = params.text;
				var panel = this.getPanel();
				if (panel) {
					setControlValue(this, params.text);
				}
			}

			this.xml._attrs.imageportalsrc      = 4726;
			this.xml._attrs.marginx             = 5;
			this.xml._attrs.marginy             = 2;
		}
		
		Edit.prototype = new core.Control();
		Edit.prototype.constructor          = Edit;
		Edit.prototype.setMarginX           = function setMarginX(value) {
			this.xml._attrs.marginx = value;
			return this;
		};
		
		Edit.prototype.getMarginX           = function getMarginX() {
			return this.xml._attrs.marginx;
		};
		Edit.prototype.setMarginY           = function setMarginY(value) {
			this.xml._attrs.marginy = value;
			return this;
		};
		Edit.prototype.getMarginY           = function getMarginY() {
			return this.xml._attrs.marginy;
		};
		Edit.prototype.setImagePortalSrc    = function setImagePortalSrc(value) {
			this.xml._attrs.imageportalsrc = value;
			return this;
		};
		Edit.prototype.getImagePortalSrc    = function getImagePortalSrc() {
			return this.xml._attrs.imageportalsrc;
		};
		Edit.prototype.setTextColor	        = setTextColor;
		Edit.prototype.setText              = function setText(value) {
			this.xml._attrs.text = value;
			this.setProperty("text", value);
			setControlValue(this, value);
			return this;
		};
		Edit.prototype.getTextColor         = getTextColor;
		Edit.prototype.getText              = getText;
		

		return Edit;
	}();
	
	function setIcon(value) {
		this.xml._attrs.icon = value;
		return this;
	}
	
	function getIcon() {
		return this.xml._attrs.icon;
	}
	
	core.Button = function factory() {
		function Button(params) {
			if (!(this instanceof Button)) {
				return new Button(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.Button", name: params && params.name});
			this.xml._attrs.icon = params.icon || (0x06000000 + 4600);
			this.xml._attrs.pressedicon = params.pressedicon || (0x06000000 + 4601);
			this.setWidth(this.getHeight()); // Icons are usually square.
		}
		
		Button.prototype = new core.Control();
		Button.prototype.constructor         = Button;
		Button.prototype.setPressedIcon      = function setPressedIcon(value) {
			this.xml._attrs.pressedicon = value;
			return this;
		};
		Button.prototype.getPressedIcon      = function getPressedIcon() {
			return this.xml._attrs.pressedicon;
		};
		Button.prototype.getIcon             = getIcon;
		Button.prototype.setIcon             = setIcon;

		return Button;
	}();
	
	core.StaticText = function factory() {
		function StaticText(params) {
			if (!(this instanceof StaticText)) {
				return new StaticText(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.StaticText", name: params && params.name});
			params.text = params.text || "";// We need to set text to something else client will crash.
			this.xml._attrs.text    = params.text;
		}
		
		StaticText.prototype = new core.Control();
		StaticText.prototype.constructor       = StaticText;
		StaticText.prototype.setText           = setText;
		StaticText.prototype.getText           = getText;
		StaticText.prototype.setTextColor	   = setTextColor;
		StaticText.prototype.getTextColor      = getTextColor;

		return StaticText;
	}();
		
	function getMinimum() {
		return this.xml._attrs.minimum;
	}
	
	function getMaximum() {
		return this.xml._attrs.maximum;
	}
	
	function setMinimum(value) {
		this.xml._attrs.minimum = value;
		return this;
	}
	
	function setMaximum(value) {
		this.xml._attrs.maximum = value;
		return this;
	}
	
	
	
	core.Progress = function factory() {
		function Progress(params) {
			if (!(this instanceof Progress)) {
				return new Progress(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.Progress", name: params && params.name});
			params.value = params.value || 0;
			this.xml._attrs.value = params.value;// We need a default else xml won't load.
		}
		
		Progress.prototype = new core.Control();
		Progress.prototype.constructor       = Progress;

		Progress.prototype.getValue            = getValue;
		Progress.prototype.getMinimum          = getMinimum;
		Progress.prototype.getMaximum          = getMaximum;
		Progress.prototype.setMinimum          = setMinimum;
		Progress.prototype.setMaximum          = setMaximum;
		
		Progress.prototype.setFaceColor = setFaceColor;
		Progress.prototype.getFaceColor = getFaceColor;
		Progress.prototype.setFillColor = function setFillColor(value) {
			this.xml._attrs.fillcolor = value;
			return this;
		};
		Progress.prototype.getFillColor = function getFillColor() {
			return this.xml._attrs.fillcolor;
		};
		Progress.prototype.setBorderColor = function setBorderColor(value) {
			this.xml._attrs.bordercolor = value;
			return this;
		};
		Progress.prototype.getBorderColor = function getBorderColor() {
			return this.xml._attrs.bordercolor;
		};
		Progress.prototype.setBorder = function setBorder(value) {
			this.xml._attrs.border = value;
			return this;
		};
		Progress.prototype.getBorder = function getBorder() {
			return this.xml._attrs.border;
		};
		Progress.prototype.setAlign = function setAlign(value) {
			this.xml._attrs.align = value;
			return this;
		};
		Progress.prototype.getAlign = function getAlign() {
			return this.xml._attrs.align;
		};
		Progress.prototype.setPreText = function setPreText(value) {
			this.xml._attrs.pretext = value;
			return this;
		};
		Progress.prototype.getPreText = function getPreText() {
			return this.xml._attrs.pretext;
		};
		Progress.prototype.setPostText = function setPostText(value) {
			this.xml._attrs.posttext = value;
			return this;
		};
		Progress.prototype.getPostText = function getPostText() {
			return this.xml._attrs.posttext;
		};
		Progress.prototype.setDrawText = function setDrawText(value) {
			this.xml._attrs.drawtext = value;
			return this;
		};
		Progress.prototype.getDrawText = function getDrawText() {
			return this.xml._attrs.drawtext;
		};
		Progress.prototype.setValue = function setValue(value) {
			this.xml._attrs.value = value;
			this.setProperty("value", value);
			return this;
		};
		
		return Progress;
	}();
	
	core.Slider = function factory() {
		function Slider(params) {
			if (!(this instanceof Slider)) {
				return new Slider(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.Slider", name: params && params.name});
		}
		
		Slider.prototype = new core.Control();
		Slider.prototype.constructor       = Slider;
		
		Slider.prototype.setVertical = function setVertical(value) {
			this.xml._attrs.vertical = value;
			return this;
		};
		Slider.prototype.getVertical = function getVertical() {
			return this.xml._attrs.vertical;
		};

		Slider.prototype.getValue            = getValue;
		Slider.prototype.getMinimum          = getMinimum;
		Slider.prototype.getMaximum          = getMaximum;
		Slider.prototype.setMinimum          = setMinimum;
		Slider.prototype.setMaximum          = setMaximum;
		
		Slider.prototype.setValue = function setValue(value) {
			this.xml._attrs["default"] = value;
			this.setProperty("sliderposition", value);
			return this;
		};
		
		return Slider;
	}();
	
	core.Column = function factory() {
		function Column(params) { // parent, progid, fixedwidth, textcolor, align
			if (params.parent) {
				this.parent = params.parent;
				params.parent.addChild(this);
			}
			this.xml = {};
			this.xml._name = 'column';
			this.xml._attrs = {};
			if (params.progid) {
				this.xml._attrs.progid	= params.progid;
			}
			if (typeof params.fixedwidth !== "undefined") {
				this.xml._attrs.fixedwidth	= params.fixedwidth;
			}
			if (params.textcolor) {
				this.xml._attrs.textcolor	= params.textcolor;
			}
			if (params.align) {
				this.xml._attrs.align	= params.align;
			}
		}
		Column.prototype.toXML          = toXML;
		return Column;
	}();
	
	core.List = function factory() {
		function List(params) {
			if (!(this instanceof List)) {
				return new List(params);
			}
			
			core.Control.call(this, {parent: params && params.parent, progid: "DecalControls.List", name: params && params.name});
		
			this.rowCount = 0;
			this.rows = [];
			
			if (params && typeof params.width !== "undefined") {
				this.xml._attrs.width	= params.width;
			}
			if (params && typeof params.height !== "undefined") {
				this.xml._attrs.height	= params.height;
			}
		}
		
		List.prototype = new core.Control();
		List.prototype.constructor       = List;

		List.prototype.addChild         = addChild;
		List.prototype.removeChild      = removeChild;

		List.prototype.addColumn = function addColumn(params) { // progid, fixedwidth, textcolor
			new core.Column({parent: this, progid: params.progid, fixedwidth: params.fixedwidth, textcolor: params.textcolor}); // this, progid, fixedwidth, textcolor
			return this;
		};
		
		List.prototype.removeColumn = function removeColumn(i) {
			if (this.children) {
				this.children.splice(i, 1);
			}
			return this;
		};
		
		/*
		 * getListProperty: Return a list control value for a specific column and row.
		 */
		List.prototype.getListProperty = function getListProperty(intCol, intRow) {
			return this.getProperty("data(" + intCol + "," + intRow + ")");
		};

		/*
		 * getListPropertyAsync: Request a property's value from skapi and call a callback with the value.
		 */
		List.prototype.getListPropertyAsync = function getListPropertyAsync(intCol, intRow, callback, thisArg) {
			return this.getPropertyAsync("data(" + intCol + "," + intRow + ")", callback, thisArg);
		};
		
		List.prototype.getColumnProgID = function getColumnProgID(intCol) {
			if (this.children && this.children.length > intCol) {
				return this.children[intCol].xml._attrs.progid;
			}
		};
		
		List.prototype.getColumnCount = function getColumnCount() {
			return this.children && this.children.length || 0;
		};

		/*
		 * setListProperty() sets a property on the list. 
		 *
		 * @param {intCol} column number.
		 * @param {intRow} row number.
		 * @param {vValue} Data to be input. If vValue is a object the value and color values will be used.
		 * @return {List} returns itself.
		 */
		List.prototype.setListProperty = function setListProperty(intCol, intRow, vValue) {
			if (typeof vValue === "undefined" || vValue == null) {
				core.debug(core.MAJOR, vValue + " is a invalid column value.");
				return;
			}
			
			var columnCount = this.getColumnCount();
			if (columnCount <= intCol) {
				core.console(core.MAJOR, this.getName() + " does not have a column #" + intCol + " for " + vValue);
				return;
			}

			var property = "Data(" + intCol + ", " + intRow + ")";
			
			var prog = this.getColumnProgID(intCol);
			if (prog && prog == "DecalControls.IconColumn") {
				property = "Data(" + intCol + ", " + intRow + ",1)";
			}

			var value = vValue;
			var color;
			if (typeof vValue === "object") {
				value = vValue.value;
				color = vValue.color;
			}
			
			this.setProperty(property,  value);
			if (this.rows[intRow]) {
				this.rows[intRow][intCol] = vValue;
			}
			
			if (color) {
				this.setProperty("Color(" + intCol + ", " + intRow + ")",  color);
			}

			return this;
		};

		/*
		 * setListColorProperty() sets a cell's color in the list. 
		 *
		 * @param {intCol} column number.
		 * @param {intRow} row number.
		 * @param {vValue} String of the BGR hex color. ("&H0000FF" for red)
		 * @return {List} returns itself.
		 */
		List.prototype.setListColorProperty = function setListColorProperty(intCol, intRow, vValue) {
			if (typeof vValue === "undefined" || vValue == null) {
				core.debug(core.MAJOR, vValue + " is a invalid column value.");
				return;
			}
			
			var columnCount = this.getColumnCount();
			if (columnCount <= intCol) {
				core.console(core.MAJOR, this.getName() + " does not have a column #" + intCol + " for " + vValue);
				return;
			}
			
			this.setProperty("Color(" + intCol + ", " + intRow + ")",  vValue);
			
			return this;
		};
		
		List.prototype.addRow = function addRow() {
			core.debug(core.MAJOR, "<addRow>");
			var args = [].slice.call(arguments);

			var view = this.getPanel();
			var szPanel = view && view.getTitle();
			var szControl = this.getName();
			var shown = szPanel && controlRegistry[szPanel] && controlRegistry[szPanel][szControl] && controlRegistry[szPanel][szControl].shown;
			core.debug("szPanel: " + szPanel, "szControl: " + szControl, "shown: " + shown);
			
			if (shown == true) {
				// Add a row.
				skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "AddRow()");
				
				// Insert data.
				for (var i = 0; i < args.length; i++) {
					core.debug(core.MAJOR, i + "," +  this.rowCount + " = " + args[i]);
					this.setListProperty(i, this.rowCount, args[i]);
				}
			} else {
				new core.Row({parent: this, columns: args});
			}

			this.rows.push(args);
			this.rowCount = this.rows.length;
			
			return this;
		};

		List.prototype.setRow = function setRow() {
			core.debug(core.MAJOR, "<setRow>");
			
			var args = [].slice.call(arguments);
			var index = args[0];
			for (var i = 1; i < args.length; i++) {
			//	core.debug(i + "," + this.rowCount + " = " + args[i]);
				this.setListProperty(i - 1, index, args[i]);
			}

			this.rows[index] = args;

			return this;
		};
		
		// Return the array arguments for a given row.
		List.prototype.getRow = function getRow(index) {
			return this.rows.length > index && this.rows[index];
		};
		
		List.prototype.deleteRow = function fDeleteRow(i) {
			skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "DeleteRow(" + i + ")");
			this.rows.splice(i, 1);
			this.rowCount = this.rows.length;
			return this;
		};
		
		List.prototype.insertRow = function fInsertRow(ri) {
			skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "InsertRow(" + ri + ")");
			
			var args = [].slice.call(arguments);
			for (var i = 1; i < args.length; i++) {
			//	core.debug(i + "," + this.rowCount + " = " + args[i]);
				this.setListProperty(i - 1, ri, args[i]);
			}
			
			this.rows.splice(ri, 0, args);
			
			this.rowCount++;
			return this;
		};
		
		List.prototype.getRowCount = function getRowCount() {
			return this.rowCount;
		};
		
		List.prototype.jumpToPosition = function fJumpToPosition(p) {
			if (typeof p !== "undefined") {
				skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "JumpToPosition(" + p + ")");
			}
			return this;
		};

		List.prototype.getScrollPosition = function getScrollPosition() {
			return this.getProperty("ScrollPosition");
		};
		
		List.prototype.getScrollPositionAsync = function getScrollPosition(callback, thisArg) {
			return this.getPropertyAsync("ScrollPosition", callback, thisArg);
		};
		
		List.prototype.clear = function clear() {
			core.debug(core.MAJOR, "<clear>", this.getName());
			skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "Clear()");
			this.rows = [];
			this.rowCount = 0;
			return this;
		};

		List.prototype.getRowCount         = function getRowCount() {
			if (typeof this.rowCount !== "undefined") {
				return this.rowCount;
			}
			return 0;
		};
		
		// Set all the rows at once, lib will only update the cells that have changed. Should be faster than a .clear() and .addrow().
		// If a row is removed in the middle, all rows after will be updated instead of just removing the affected row.
		/*
		// Example
		var rows = [];
		rows.push(["first column", "second column"]);
		rows.push([{value:"red column", color:"&H0000FF"}, {value:"green column", color:"&H00FF00"}]);
		list.setRows(rows);
		*/
		List.prototype.setRows = function setRows(rows) {
			core.debug(core.MAJOR, "<setRows>", rows.length, this.rows.length);
			var nr; // new row
			var column;
			var value, oldValue;
			var color, oldColor;
			for (var ri = 0; ri < rows.length; ri++) {
				nr = rows[ ri ];
				
				if (ri >= this.rows.length) {
					core.debug(core.MAJOR, "addrow...");
					skapi.GetControlProperty(this.getPanel().getTitle(), this.getName(), "AddRow()");
				}
				
				for (var ci = 0; ci < nr.length; ci++) { // column index
					column = nr[ ci ];
					if (typeof column === "object") {
						value = column.value;
						color = column.color;
					} else {
						value = column;
						color = undefined;
					}
					
					// If value is a function, call function to get a value.
					if (typeof value === "function") {
						value = value() || "";
					}

					// if color is a function, call function to get a color.
					if (typeof color === "function") {
						color = color();
					}

					if (this.rows[ri]) {
						
						// Check if row are objects, which have value and color properties.
						if (typeof this.rows[ri][ci] === "object" && typeof column === "object") {
							var oldValue = this.rows[ri][ci].value;
							var oldColor = this.rows[ri][ci].color;
							if (typeof oldValue === "function") {
								oldValue = oldValue();
							}

							if (typeof oldColor === "function") {
								oldColor = oldColor();
							}

							if (oldValue != value) {
								core.console(core.MAJOR, " " + ri + " " + ci + " objects have different values.");
								this.setProperty("Data(" + ci + ", " + ri + ")", value);
							}
							
							if (oldColor != color) {
								core.debug(core.MAJOR, " " + ri + " " + ci + " objects have different colors.");
								this.setProperty("Color(" + ci + ", " + ri + ")", color);
							}
							continue;
						}
						
						// rows are strings.
						if (this.rows[ri][ci] == value) {
							core.debug(core.MAJOR, "Skipping " + ri + " " + ci + " (matching values)");
							continue;
						}
					}

					core.debug(core.MAJOR, "Setting " + ri + " " + ci + " to " + value);
					//this.setProperty("Data(" + ci + ", " + ri + ")", value);
					this.setListProperty(ci, ri, value);
					if (typeof color !== "undefined") {
						this.setProperty("Color(" + ci + ", " + ri + ")", color);
					}
				}
			}
			
			//core.console("rows.length: " + rows.length + ", this.rows.length: " + this.rows.length);
			
			for (var ri = this.rows.length - 1; ri >= rows.length; ri--) {
				core.debug(core.MAJOR, "Removing " + ri);
				this.deleteRow(ri);
			}
				
			this.rows = rows;
			this.rowCount = this.rows.length;
			return this;
		};
		
		/*
		List.prototype.refresh = function refresh() {
			this.setRows(this.rows);
			return this;
		};
		*/
		
		return List;
	}();
	
	//////////////////////////
	// Event Handling       //
	//////////////////////////
	// Here we catch event values so scripts can use control.getValue() to get values synchronously. May not work for lists.
	var controlRegistry = {};
	
	function setControlValue(control, value) {
		var panel = control.getPanel();
		if (panel && panel.getTitle) {
			var szPanel = panel.getTitle();
			var szControl = control.getName();
			core.debug(core.MAJOR, "<setControlValue> szPanel: " + szPanel + ", szControl: " + szControl + ", value: " + value);
			controlRegistry[szPanel] = controlRegistry[szPanel] || {};
			controlRegistry[szPanel][szControl] = controlRegistry[szPanel][szControl] || {control: control};
			controlRegistry[szPanel][szControl].value = value;
		}
	}
	
	function getControlValue(control) {
		var panel = control.getPanel();
		if (panel && panel.getTitle) {
			var szPanel = control.getPanel().getTitle();
			var szControl = control.getName();
			var value = controlRegistry[szPanel] && controlRegistry[szPanel][szControl] && controlRegistry[szPanel][szControl].value;
			core.debug(core.MAJOR, "<getControlValue> szPanel: " + szPanel + ", szControl: " + szControl + ", value: " + value);
			return value;
		}
	}

	function setControlsShown(control, value) {
		var szPanel, szControl;
		if (control.getPanel && control.getPanel()) {
			szPanel = control.getPanel().getTitle();

			if (control.getName) {
				szControl = control.getName();
				core.debug(core.MAJOR, "<setControlsShown>", szPanel, szControl);
				
				if (controlRegistry[szPanel] && controlRegistry[szPanel][szControl]) {
					controlRegistry[szPanel][szControl].shown = value;
				}
			}
		}

		if (control.children) {
			var c;
			for (var i = control.children.length - 1; i >= 0; i--) {
				c = control.children[i];
				setControlsShown(c, value);
				
				// Reset any list rows so lst.setRows() works on next show & render.
				if (value == false && c.rows) {
					c.rows = [];
				}
			}
		}
	}
	
	
	function isControlShown(control) {
		var panel = control.getPanel();
		if (panel && panel.getTitle) {
			var szPanel = control.getPanel().getTitle();
			var szControl = control.getName();
			return controlRegistry[szPanel] && controlRegistry[szPanel][szControl] && controlRegistry[szPanel][szControl].shown;
		}
		return false;
	}
	
	function registerControls(control) {
		core.debug(core.MAJOR, "<registerControls>");
		var szPanel, szControl;
		if (control.getPanel && control.getPanel()) {
			szPanel = control.getPanel().getTitle();
			core.debug(core.MAJOR, "", szPanel, control.xml._attrs.progid);
			if (control.getName) {
				szControl = control.getName();
				core.debug(core.MAJOR, "", szPanel, control.xml._attrs.progid, szControl);

				controlRegistry[szPanel] = controlRegistry[szPanel] || {};
				controlRegistry[szPanel][szControl] = controlRegistry[szPanel][szControl] || {};
				controlRegistry[szPanel][szControl].control = control;
			}
		}
	
		if (control.children) {
			core.debug(core.MAJOR, "children: " + control.children.length);
			var c;
			for (var i = 0; i < control.children.length; i++) {
				c = control.children[i];
				core.debug(core.MAJOR, i, c.xml && c.xml._attrs && c.xml._attrs.progid);
				registerControls(c);
			}
		}
		
		if (control.emit) {
			control.emit("onRegistered");
		}
	}
	
	function unregisterControls(control) {
		var szPanel, szControl;
		if (control.getPanel) {
			szPanel = control.getPanel().getTitle();
			
			//core.debug("<unregisterControls>", szPanel);
			if (control.getName) {
				szControl = control.getName();
				core.debug(core.MAJOR, "<unregisterControls>", szPanel, szControl);
				
				if (controlRegistry[szPanel]) {
					delete controlRegistry[szPanel][szControl];
				}
			}
		}
	
		if (control.children) {
			var c;
			for (var i = control.children.length - 1; i >= 0; i--) {
				c = control.children[i];
				unregisterControls(c);
			}
		}
		
		if (control.emit) {
			control.emit("onUnregistered");
		}
	}
	
	var handler = {};
	handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
		//var key = szPanel + "-" + szControl;
		core.debug(core.MAJOR, "<OnControlEvent>", szPanel, szControl, (controlRegistry[szPanel] && controlRegistry[szPanel][szControl]));

		if (controlRegistry[szPanel] && controlRegistry[szPanel][szControl]) {
			var control = controlRegistry[szPanel][szControl].control;

			if (control.xml._attrs.progid == "DecalControls.List") {
				var intCol = parseInt(dictSzValue(szControl).split(",")[0]);
				var intRow = parseInt(dictSzValue(szControl).split(",")[1]);
				
				
				var column = control.children[intCol];

				// If column is a check column, get the value first.
				if (column && column.xml._attrs.progid == "DecalControls.CheckColumn") {
					return control.getListPropertyAsync(intCol, intRow, function onListProperty(value) {
						// Save it to our control.rows array.
						control.rows[intRow] = control.rows[intRow] || [];
						control.rows[intRow][intCol] = value;
						
						// Fire control event.
						control.emit("onControlEvent", control, szPanel, szControl, dictSzValue(szControl));
					});
				}
			} else if (control.xml._attrs.progid == "DecalControls.Choice") {
				var value = dictSzValue(szControl);
				var selected = control.children.findIndex(function(child) {
					return child.xml._attrs.text == value || child.xml._attrs.data == value;
				});
				control.xml._attrs.selected = selected || 0;
			}

			controlRegistry[szPanel][szControl].value = dictSzValue(szControl);
			
			control.emit("onControlEvent", control, szPanel, szControl, dictSzValue(szControl));

			if (control.xml._attrs.progid == "DecalControls.Notebook") {
				var parent = control.parent;
				emitShown(control.parent);
			}
		}
	};
	skapi.AddHandler(evidOnControlEvent,  handler);
	
	function emitRecursively() {
		var args = [].slice.call(arguments);
		var control = args.splice(0, 1)[0];
		control.emit.apply(control, args);
		if (control.children) {
			control.children.forEach(function(child) {
				if (typeof child.emit === "function") {
					emitRecursively(child, args);
				}
			});
		}
	}

	function emitShown(control) {
		if (typeof control.emit === "function") {
			control.emit("onShow");
		}
		if (control.children) {
			control.children.forEach(function(child, index) {
				if (child.xml && child.xml._name == "page") {
					var value = control.getValue() || 0;
					if (value == index) {
						emitShown(child);
					} else {
						emitHidden(child);
					}
				} else {
					emitShown(child);
				}
			});
		}
	}

	function emitHidden(control) {
		if (typeof control.emit === "function") {
			control.emit("onHide");
		}
		if (control.children) {
			control.children.forEach(function(child) {
				emitHidden(child);
			});
		}
	}
	

	// User prompts.
	
	/*
	 * applyMethod() Apply a function with a thisArg object and argument. 
	 *
	 * @param {object} thisArg: Optional object to be the 'this' object in the function.
	 * @param {function} method: Function to be called.
	 * @param {array} args: List of arguments for the function.
	 * @return {variable} Whatever the method returns.
	 */
	core.applyMethod = function applyMethod() {
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};
	
	core.showDropdownMenuPrompt = function factory() {
		return function showDropdownMenuPrompt(params, callback) {
			core.debug("<showDropdownMenuPrompt>");
			var options = params.options;
			var title = (params.title === undefined ? "Dropdown Prompt" : params.title);
			var width = (params.width === undefined ? 200 : params.width);
			var icon = (params.icon === undefined ? 0 : params.icon);
			var selected = (params.selected === undefined ? 0 : params.selected);
			
			//this.setProperty("selected", value);
			
			function finish() {
				view.removeControls();
				view = undefined;
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			}

			var view = new core.View({
				title : title, 
				width : width, 
				height: 70, 
				icon  : icon
			});
			var layout = new core.FixedLayout({parent: view});
			
			var choMenu = new core.Choice({
				parent: layout, 
				name  : "choMenu"
			})
				.setWidth(layout.getWidth())
				.addOption({text: "Select..."});
				// eslint-disable-next-line no-unused-vars
				//.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {});
			
			options.forEach(function(value) {
				choMenu.addOption(value);
			});
			if (typeof selected !== "undefined") {
				choMenu.setSelected(selected + 1);
			}

			// Footer;
			var btnCancel = new core.PushButton({
				parent: layout, 
				name  : "btnCancel", 
				text  : "Cancel"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return finish(new core.Error("CANCELED"));
				});
			
			var btnDone = new core.PushButton({
				parent: layout, 
				name  : "btnDone", 
				text  : "Done"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var value = choMenu.getValue();

					if (value == "Select...") {
						return finish(new core.Error("NOTHING_SELECTED"));
					}

					return finish(undefined, options.find(function(option) {
						return option.data == value || option.text == value;
					}));
				});

			view.showControls(true);
			
			
		};
	}();
	
	/*
	// Not done.
	core.showListPrompt = function factory() {
		return function showListPrompt(params, callback) {
			core.console("<showListPrompt>");
			var rows = params.rows;
			var columns = params.columns;
			var title = (params.title === undefined ? "List Menu" : params.title);
			var width = (params.width === undefined ? 200 : params.width);
			var height = (params.height === undefined ? 200 : params.height);
			var icon = (params.icon === undefined ? 0 : params.icon);

			function finish() {
				view.removeControls();
				view = undefined;
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			}

			var view = new core.View({
				title : title, 
				width : width, 
				height: height, 
				icon  : icon
			});
			var layout = new core.FixedLayout({parent: view});
			
			var lstList = new core.List({
				parent: layout, 
				name  : "lstList"
			})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					core.console("clicked");
				});
			columns.forEach(function(column) {
				lstList.addColumn(column);
			});
			
			rows.forEach(function(row) {
				lstList.addRow.apply(lstList, row);
			});

			// Footer;
			var btnCancel = new core.PushButton({
				parent: layout, 
				name  : "btnCancel", 
				text  : "Cancel"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return finish(new core.Error("CANCELED"));
				});
			
			var btnDone = new core.PushButton({
				parent: layout, 
				name  : "btnDone", 
				text  : "Done"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var value = choMenu.getValue();

					if (value == "Select...") {
						return finish(new core.Error("NOTHING_SELECTED"));
					}

					return finish(undefined, options.find(function(option) {
						return option.data == value || option.text == value;
					}));
				});

			view.showControls(true);
		};
	}();
	*/
	
	core.showEditPrompt = function factory() {
		return function showEditPrompt(params, callback) {
			core.debug("<showEditPrompt>");
			var title = (params.title === undefined ? "Edit Prompt" : params.title);
			var width = (params.width === undefined ? 200 : params.width);
			var icon = (params.icon === undefined ? 0 : params.icon);
			var value = (params.value === undefined ? "" : params.value);

			function finish() {
				view.removeControls();
				view = undefined;
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}

			var view = new core.View({
				title : title, 
				width : width, 
				height: 70, 
				icon  : icon
			});
			var layout = new core.FixedLayout({parent: view});

			var edText = new core.Edit({
				parent: layout, 
				name: "edText", 
				text: value
			})
				.setWidth(layout.getWidth());

			// Footer;
			var btnCancel = new core.PushButton({
				parent: layout, 
				name  : "btnCancel", 
				text  : "Cancel"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return finish(new core.Error("CANCELED"));
				});
			
			var btnDone = new core.PushButton({
				parent: layout, 
				name  : "btnDone", 
				text  : "Done"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					edText.getPropertyAsync("text", finish);
				});

			view.showControls(true);
		};
	}();
	
	core.showConfirmationPrompt = function factory() {
		return function showConfirmationPrompt(params, callback) {	
			var title = (params.title === undefined ? "Confirmation Prompt" : params.title);
			var width = (params.width === undefined ? 200 : params.width);
			var icon = (params.icon === undefined ? 0 : params.icon);
			
			function finish() {
				view.removeControls();
				view = undefined;
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}
			
			var view = new core.View({
				title : title, 
				width : width, 
				height: 50, 
				icon  : icon
			});
			var layout = new core.FixedLayout({parent: view});
			
			// Footer;
			var btnCancel = new core.PushButton({
				parent: layout, 
				name  : "btnCancel", 
				text  : "Cancel"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return finish(new core.Error("CANCELED"));
				});
			
			var btnConfirm = new core.PushButton({
				parent: layout, 
				name  : "btnConfirm", 
				text  : "Confirm"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					finish(undefined, true);
				});

			view.showControls(true);
		};
	}();
	
	core.showBooleanPrompt = function factory() {
		return function showConfirmationPrompt(params, callback) {	
			var title = (params.title === undefined ? "Confirmation Prompt" : params.title);
			var width = (params.width === undefined ? 200 : params.width);
			var icon = (params.icon === undefined ? 0 : params.icon);
			
			function finish() {
				view.removeControls();
				view = undefined;
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}
			
			var view = new core.View({
				title : title, 
				width : width, 
				height: 50, 
				icon  : icon
			});
			var layout = new core.FixedLayout({parent: view});
			
			// Footer;
			var btnCancel = new core.PushButton({
				parent: layout, 
				name  : "btnCancel", 
				text  : "Cancel"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return finish(new core.Error("CANCELED"));
				});

			
			var btnTrue = new core.PushButton({
				parent: layout, 
				name  : "btnTrue", 
				text  : "True"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					finish(undefined, true);
				});

			
			var btnFalse = new core.PushButton({
				parent: layout, 
				name  : "btnFalse", 
				text  : "False"
			})
				.setAnchor(btnTrue, "TOPLEFT", "TOPRIGHT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					finish(undefined, false);
				});

			view.showControls(true);
		};
	}();
	
	core.showMultipleChoicePrompt = function factory() {
		function _getValue(value) {
			return value;
		}
		return function showMultipleChoicePrompt(params, callback) {	
			var title = (params.title === undefined ? "Multiple Choice Prompt" : params.title);
			var width = (params.width === undefined ? 200 : params.width);
			var height = (params.height === undefined ? (55 + (5*20)) : params.height);
			var icon = (params.icon === undefined ? 0 : params.icon);
			//var choices = params.choices.map(_getValue);
			var choices = core.clone(params.choices);

			function finish() {
				view.removeControls();
				view = undefined;
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}
			
			var view = new core.View({
				title : title, 
				width : width, 
				height: height, 
				icon  : icon
			});
			var layout = new core.FixedLayout({parent: view});
			
			var lstChoices = new core.List({parent: layout, name: "lstChoices"})
				.setWidth(layout.getWidth())
				.setHeight(layout.getHeight() - 20)
				.addColumn({progid: "DecalControls.CheckColumn"})	// enabled
				.addColumn({progid: "DecalControls.TextColumn", fixedwidth: layout.getWidth() - 20}) // Text
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					var intCol = parseInt(value.split(",")[0], 10);
					var intRow = parseInt(value.split(",")[1], 10);
					if (intCol == 0) {
						var choice = choices[intRow];
						return control.getListPropertyAsync(intCol, intRow, function(err, value) {
							if (err) return;
							choice.enabled = value;
						});
					}
				});
				
			choices.forEach(function(choice) {
				lstChoices.addRow(choice.enabled, choice.text);
			});
			
			// Footer;
			var btnCancel = new core.PushButton({
				parent: layout, 
				name  : "btnCancel", 
				text  : "Cancel"
			})
				.setAnchor(layout, "BOTTOMLEFT", "BOTTOMLEFT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					return finish(new core.Error("CANCELED"));
				});

			var btnConfirm = new core.PushButton({
				parent: layout, 
				name  : "btnConfirm", 
				text  : "Confirm"
			})
				.setAnchor(layout, "BOTTOMRIGHT", "BOTTOMRIGHT")
				.setWidth(50)
				// eslint-disable-next-line
				.on("onControlEvent", function onControlEvent(control, szPanel, szControl, value) {
					finish(undefined, choices);
				});
				
			view.showControls(true);
		};
	}();
	
	core.clone = function clone(obj) {
		// Clone a object.
		var copy;

		// Handle the 3 simple types, and null or undefined
		if (null == obj || "object" != typeof obj) return obj;

		// Handle Date
		if (obj instanceof Date) {
			copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}

		// Handle Array
		if (obj instanceof Array) {
			copy = [];
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0, len = obj.length; i < len; i++) {
				copy[i] = core.clone(obj[i]);
			}
			return copy;
		}

		// Handle Object
		if (obj instanceof Object) {
			copy = {};
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = core.clone(obj[attr]);
			}
			return copy;
		}

		throw new Error("Unable to copy obj! Its type isn't supported.");
	};
	
	//////////////////////////
	// SkunkSuite functions //
	//////////////////////////
	
	core.embeds = core.embeds || {};
	
	/**
	 * createView() Returns a view object and will unload it when the parent script is disabled.
	 */
	function createView(params) {
		var script = this;

		var view = new core.View(params);
		
		if (typeof script.on === "function") {
			script.on("onDisable", function onDisable() {
				core.debug(core.MAJOR, "Script disabling, unloading view...");
				view.removeControls();
				script.removeListener("onDisable", onDisable);
			});
		}

		return view;
	}
	
	function embedMixins(script) {
		script.createView = createView;
		return script;
	}
	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};
	
	return core;
}));