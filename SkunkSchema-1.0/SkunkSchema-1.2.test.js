const events = require('events');
EventEmitter =  events.EventEmitter; // Global

const SkunkSchema = require("./SkunkSchema-1.2");

function SKAPI() {}
SKAPI.prototype.AddHandler = function AddHandler(evid, handler) {};
SKAPI.prototype.ShowControls = function ShowControls(schema, visable) {};
SKAPI.prototype.SetControlProperty = function SetControlProperty(schema, visable) {};
SKAPI.prototype.GetControlProperty = function GetControlProperty(schema, visable) {};

beforeEach(() => {
	skapi = new SKAPI();
});

// Globals
evidOnControlEvent = null;

describe('SkunkSchema-1.1', () => {  
	
	it("showDropdownMenuPrompt()", (done) => {
		jest.spyOn(skapi, 'ShowControls').mockImplementationOnce(function(schema, visable) {
			expect(schema).toBe(
`<view title='Dropdown Prompt' icon='0' width='200' height='52'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.PushButton' name='btnCancel' width='100' height='20' top='0' left='0' text='Cancel' />
    <control progid='DecalControls.PushButton' name='btnConfirm' width='100' height='20' top='0' left='100' text='Confirm' />
  </control>
</view>`);
			done();
		});
		SkunkSchema.showDropdownMenuPrompt({options:[{text:"hello"}], selected: 1}, function(err, results) {});
	});
	
	it("showEditPrompt()", (done) => {
		jest.spyOn(skapi, 'ShowControls').mockImplementationOnce(function(schema, visable) {
			
			
			
			expect(schema).toBe(
`<view title='demo' icon='0' width='200' height='72'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Edit' name='edText' width='200' height='20' top='0' left='0' text='foobar' imageportalsrc='4726' />
    <control progid='DecalControls.PushButton' name='btnCancel' width='100' height='20' top='20' left='0' text='Cancel' />
    <control progid='DecalControls.PushButton' name='btnConfirm' width='100' height='20' top='20' left='100' text='Confirm' />
  </control>
</view>`);
			done();
		});
		SkunkSchema.showEditPrompt({title: "demo", value:"foobar"}, function(err, results) {})
	});
	
	it("showConfirmationPrompt()", (done) => {
		jest.spyOn(skapi, 'ShowControls').mockImplementationOnce(function(schema, visable) {
			//console.log("schema: " + schema);
			
			expect(schema).toBe(
`<view title='demo' icon='0' width='200' height='52'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.PushButton' name='btnCancel' width='100' height='20' top='0' left='0' text='Cancel' />
    <control progid='DecalControls.PushButton' name='btnConfirm' width='100' height='20' top='0' left='100' text='Confirm' />
  </control>
</view>`);
			done();
		});
		SkunkSchema.showConfirmationPrompt({title: "demo"}, function(err, results) {})
	});
	
	it("Element tag", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			SkunkSchema.Element("control", {progid:'DecalControls.FixedLayout'}, [
				SkunkSchema.Element("control", {progid:'DecalControls.PushButton', name: "btnButton", text:"Button", left:0, top:0, width:50, height:20})
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.PushButton' name='btnButton' text='Button' left='0' top='0' width='50' height='20' />
  </control>
</view>`);
	});
	
	it("Element tag with children using parent's size", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, function(parent) {
			return [
				SkunkSchema.Element(parent, "control", {progid:'DecalControls.FixedLayout'}, function(parent) {
					return [
						SkunkSchema.Element(parent, "control", {progid:'DecalControls.PushButton', name: "btnButton2", text:"Button2", left:0, top:0, width:parent.getWidth(), height:parent.getHeight()})
					];
				})
			];
		});
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.PushButton' name='btnButton2' text='Button2' left='0' top='0' width='400' height='168' />
  </control>
</view>`);
	});
	
	it("Element tag with notebook and child", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			SkunkSchema.Element("control", {progid:'DecalControls.FixedLayout'}, [
				SkunkSchema.Element("control", {progid:'DecalControls.Notebook', name: "notebookView", left:0, top:0, width: 400, height: 168}, [
					SkunkSchema.Element("page", {label:'Page 1'}, [
						SkunkSchema.Element("control", {progid:'DecalControls.FixedLayout'}, [
							SkunkSchema.Element("control", {progid:'DecalControls.PushButton', name: "btnButton2", text:"Button2", left:0, top:0, width:100, height:20})
						])
					])
				])
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Notebook' name='notebookView' left='0' top='0' width='400' height='168'>
      <page label='Page 1'>
        <control progid='DecalControls.FixedLayout'>
          <control progid='DecalControls.PushButton' name='btnButton2' text='Button2' left='0' top='0' width='100' height='20' />
        </control>
      </page>
    </control>
  </control>
</view>`);
	});
	
	it("Element tag with notebook and children using parent size", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, function(parent) {
			return [
				SkunkSchema.Element(parent, "control", {progid:'DecalControls.FixedLayout'}, function(parent) {
					return [
						SkunkSchema.Element(parent, "control", {progid:'DecalControls.Notebook', name: "notebookView", left:0, top:0, width: parent.getWidth(), height: parent.getHeight()}, function(parent) {
							return [
								SkunkSchema.Element(parent, "page", {label:'Page 1'}, function(parent) {
									return [
										SkunkSchema.Element(parent, "control", {progid:'DecalControls.FixedLayout'}, function(parent) {
											return [
												SkunkSchema.Element(parent, "control", {progid:'DecalControls.PushButton', name: "btnButton2", text:"Button2", left:0, top:0, width:parent.getWidth(), height:parent.getHeight()})
											];
										})
									];
								})
							];
						})
					];
				})
			];
		});
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Notebook' name='notebookView' left='0' top='0' width='400' height='168'>
      <page label='Page 1'>
        <control progid='DecalControls.FixedLayout'>
          <control progid='DecalControls.PushButton' name='btnButton2' text='Button2' left='0' top='0' width='400' height='152' />
        </control>
      </page>
    </control>
  </control>
</view>`);
	});

	it("Element, string as second argument", () => {
		var schema = SkunkSchema.Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			SkunkSchema.Element("control", {progid: 'DecalControls.list', name: "lstConfigProfileGeneral"}, [
				SkunkSchema.Element("column", {progid: "DecalControls.TextColumn", fixedwidth: 400}),
				SkunkSchema.Element("row", "test")
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.list' name='lstConfigProfileGeneral'>
    <column progid='DecalControls.TextColumn' fixedwidth='400' />
    <row>test</row>
  </control>
</view>`);
	});
	
	it("Element with events example", () => {
		var Element = SkunkSchema.Element;
		var schema = Element("view", {title: "a title", icon: 1234, width: 400, height: 200}, [
			Element("control", {progid:'DecalControls.FixedLayout'}, [
				Element("control", {progid:'DecalControls.Notebook', name: "notebookView", left:0, top:0, width: 400, height: 168, onControlEvent: function(szPanel, szControl, dictSzValue) {
					core.info("Tab clicked!", szPanel, szControl);
				}}, [
					Element("page", {label:'Page 1'}, [
						Element("control", {progid:'DecalControls.FixedLayout'}, [
							Element("control", {progid: 'DecalControls.Edit',       name: "edCopperScarab", left: 0, top: 0,  width: 100, height: 20, text: 0,  imageportalsrc: 4726}),
							Element("control", {progid:'DecalControls.PushButton',  name: "btnButton2",	    left: 0, top: 20, width: 100, height: 20, text: "Button2", onControlEvent: function(szPanel, szControl, dictSzValue) {
								var edCopperScarab = this.getElement("edCopperScarab");
								edCopperScarab.getControlProperty("text", function(err, results) {
									if (err) return core.warn("Error getting property", err);
									core.info("edCopperScarab's value is " + results);
								});
							}}),
						])
					])
				])
			])
		]);
		expect(SkunkSchema.prettifyXml(schema.toXML())).toBe(
`<view title='a title' icon='1234' width='400' height='200'>
  <control progid='DecalControls.FixedLayout'>
    <control progid='DecalControls.Notebook' name='notebookView' left='0' top='0' width='400' height='168'>
      <page label='Page 1'>
        <control progid='DecalControls.FixedLayout'>
          <control progid='DecalControls.Edit' name='edCopperScarab' left='0' top='0' width='100' height='20' text='0' imageportalsrc='4726' />
          <control progid='DecalControls.PushButton' name='btnButton2' left='0' top='20' width='100' height='20' text='Button2' />
        </control>
      </page>
    </control>
  </control>
</view>`);
	});
	
	
});