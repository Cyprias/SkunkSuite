/********************************************************************************************\
	File:           SkunkSchema-1.1.js
	Purpose:        Objected oriented GUI schema creation for Skunkworks.
	Creator:        Cyprias
	Date:           02/20/2018
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkSchema-1.2";
var MINOR = 211231;

(function (factory) {
	var dependencies = {};
	dependencies.EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof dependencies.EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
	}
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkSchema12 = factory(dependencies);
	}
}(function (dependencies) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	var EventEmitter = dependencies.EventEmitter;
	
	core.debugging = false;

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	core.setInterval = setInterval;
	core.setTimeout = setTimeout;
	core.setImmediate = setImmediate;
	core.clearTimeout = clearTimeout;
	
	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(core.MAJOR + " " + args.join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (core.debugging != true) return;
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(core.MAJOR + " " + args.join('\t'), opmConsole);
	};
	if (console.log) core.console = function noop() {};

	core.stringTemplate = function stringTemplate(string, data) {
		var proxyRegEx = /\{\{([^\}]+)?\}\}/g;
		return string.replace(proxyRegEx, function(_, key) {
			var keyParts = key.split('.');
			var value = data;
			keyParts.forEach(function(v) {
				value = value[v];
			});
			if (typeof value !== "undefined") {
				return value;
			}
			return '';
		});
	};

	core.stringTimesN = function stringTimesN(n, _char) {
		return Array(n + 1).join(_char);
	};
	
	var EOL = "\n";
	
	core.prettifyXml = function prettifyXml(xmlInput) {
		var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
		var _options$indent = options.indent,
			indentOption = _options$indent === void 0 ? 2 : _options$indent,
			_options$newline = options.newline,
			newlineOption = _options$newline === void 0 ? EOL : _options$newline;
		var indentString = core.stringTimesN(indentOption, ' ');
		var formatted = '';
		var regex = /(>)(<)(\/*)/g;
		var xml = xmlInput.replace(regex, "$1".concat(newlineOption, "$2$3"));
		var pad = 0;
		xml.split(/\r?\n/).forEach(function (l) {
			var line = l.trim();
			var indent = 0;

			if (line.match(/.+<\/\w[^>]*>$/)) {
				indent = 0;
			} else if (line.match(/^<\/\w/)) {
				// Somehow istanbul doesn't see the else case as covered, although it is. Skip it.

				/* istanbul ignore else  */
				if (pad !== 0) {
					pad -= 1;
				}
			} else if (line.match(/^<\w([^>]*[^\/])?>.*$/)) {
				indent = 1;
			} else {
				indent = 0;
			}

			var padding = core.stringTimesN(pad, indentString);
			formatted += padding + line + newlineOption; // eslint-disable-line prefer-template

			pad += indent;
		});
		return formatted.trim();
	};

	core.timedOut = function timedOut(callback, name) {
		callback(new core.Error(name || "TIMED_OUT"));
	};

	core.applyMethod = function applyMethod() {
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};

	core.getControlPropertyAsync = function getControlPropertyAsync(params, callback) {
		var panelName = params.panelName;
		var controlName = params.controlName;
		var property = params.property;
		
		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
	
		var handler = {};
		handler.OnControlProperty = function OnControlProperty(szPanel2, szControl2, szProperty2, vValue) {
			if (szPanel2 == panelName && szControl2 == controlName && (szProperty2 == property || szProperty2 == "data")) {
				finish(undefined, vValue);
			}
		};
		skapi.AddHandler(evidOnControlProperty,	handler);
		skapi.GetControlProperty(panelName, controlName, property);

		var tid = core.setTimeout(core.timedOut, 1000, finish, "GET_PROPERTY_TIMED_OUT");
	};

	core.showDropdownMenuPrompt = function showDropdownMenuPrompt(params, callback) {
		core.debug("<showDropdownMenuPrompt>");
		var options = params.options || [];
		var title = (params.title === undefined ? "Dropdown Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
		var selected = (params.selected === undefined ? 0 : params.selected);
		function finish() {
			schema.removeControls();
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var schema = Element("view", {title: title, icon: icon, width: width, height: 72}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.Choice', name: "choMenu", top: 0, left: 0, width: width, height: 20,  selected:selected}, function() {
					var children = [];
					children.push(Element("option", {text: "Select..."}));
					options.forEach(function(str) {
						children.push(Element("option", str));
					});
					return children;
				}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnCancel",  width: width/2, height: 20, top: 20, left: 0,            text: "Cancel", onControlEvent: function(szPanel, szControl, dictSzValue) {
					return finish(new core.Error("CANCELED"));
				}}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnConfirm",    width: width/2, height: 20, top: 20, left: width / 2, text: "Confirm", onControlEvent: function(szPanel, szControl, dictSzValue) {
					return core.getControlPropertyAsync({
						panelName: title,
						controlName: "choMenu",
						property: "selected"
					}, function onValue(err, results) {
						if (err) return core.warn("Error getting menu selected", err);
						finish(undefined, options[results - 1]);
					});
				}})
			])
		]);

		schema.showControls(true);
	};


	core.showEditPrompt = function showEditPrompt(params, callback) {
		var title = (params.title === undefined ? "Edit Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
		var value = (params.value === undefined ? "" : params.value);
		var imageportalsrc = (params.imageportalsrc === undefined ? 4726 : params.imageportalsrc);

		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			skapi.RemoveControls(title);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var schema = Element("view", {title: title, icon: icon, width: width, height: 72}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.Edit', name: "edText",  width: width, height: 20, top: 0, left: 0,             text: value, imageportalsrc: imageportalsrc}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnCancel",  width: width/2, height: 20, top: 20, left: 0,             text: "Cancel"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnConfirm",    width: width/2, height: 20, top: 20, left: width / 2,    text: "Confirm"})
			])
		]);
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			if (szControl == "btnCancel") {
				return finish(new core.Error("CANCELED"));
			} else if (szControl == "btnConfirm") {
				return core.getControlPropertyAsync({
					panelName: title,
					controlName: "edText",
					property: "text"
				}, finish);
			}
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		var string = schema.toXML();
		skapi.ShowControls(core.prettifyXml(string), true);
	};

	core.showConfirmationPrompt = function showConfirmationPrompt(params, callback) {
		core.debug("<showConfirmationPrompt>");
		var title = (params.title === undefined ? "Edit Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
		
		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			skapi.RemoveControls(title);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var schema = Element("view", {title: title, icon: icon, width: width, height: 52}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.PushButton', name: "btnCancel",  width: width/2, height: 20, top: 0, left: 0,             text: "Cancel"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnConfirm",    width: width/2, height: 20, top: 0, left: width/2,    text: "Confirm"})
			])
		]);
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			if (szControl == "btnCancel") {
				return finish(new core.Error("CANCELED"));
			} else if (szControl == "btnConfirm") {
				return finish(undefined, true);
			}
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		var string = schema.toXML();
		skapi.ShowControls(core.prettifyXml(string), true);
	};

	core.showBooleanPrompt = function showBooleanPrompt(params, callback) {
		core.debug("<showBooleanPrompt>");
		var title = (params.title === undefined ? "Boolean Prompt" : params.title);
		var width = (params.width === undefined ? 200 : params.width);
		var icon = (params.icon === undefined ? 0 : params.icon);
	
		function finish() {
			skapi.RemoveHandler(evidNil, handler);
			skapi.RemoveControls(title);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var schema = Element("view", {title: title, icon: icon, width: width, height: 52}, [
			Element("control", {progid: 'DecalControls.FixedLayout'}, [
				Element("control", {progid: 'DecalControls.PushButton', name: "btnCancel",  width: 50, height: 20, top: 0, left: 0,             text: "Cancel"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnFalse",   width: 50, height: 20, top: 0, left: width - 100,   text: "False"}),
				Element("control", {progid: 'DecalControls.PushButton', name: "btnTrue",    width: 50, height: 20, top: 0, left: width - 50,    text: "True"})
			])
		]);
		
		var handler = {};
		handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			if (szControl == "btnCancel") {
				return finish(new core.Error("CANCELED"));
			} else if (szControl == "btnFalse") {
				return finish(undefined, false);
			} else if (szControl == "btnTrue") {
				return finish(undefined, true);
			}
		};
		skapi.AddHandler(evidOnControlEvent, handler);
		
		var string = schema.toXML();
		skapi.ShowControls(string, true);
	};

	var Element = core.Element = function Element(parent, selector, attrs, children) {
		if (!(this instanceof Element)) {
			return new Element(parent, selector, attrs, children);
		}
		EventEmitter.apply(this);
		
		if (parent instanceof Element) {
			this.parent = parent;
		} else {
			children = attrs;
			attrs = selector;
			selector = parent;
		}

		if (typeof attrs === "string" || Array.isArray(attrs)) {
			children = attrs;
			attrs = undefined;
		}

		//core.console("children: " + typeof children);
		if (typeof children == "boolean" || typeof children == "number") {
			children = String(children);
		}
		
		this.selector = selector;
		this.attrs = attrs;
		this.children = children;
	};
	Element.prototype = new EventEmitter();
	Element.prototype.constructor = Element;
	
	
	Element.prototype.getChildren = function getChildren() {
		if (typeof this.children === "function") {
			return this.children(this);
		} else if (Array.isArray(this.children)) {
			return this.children;
		}
	};
	
	Element.prototype.toString = function toString() {
		var parts = [this.selector];
		if (this.attrs.title) parts.push(this.attrs.title);
		if (this.attrs.progid) parts.push(this.attrs.progid);
		if (this.attrs.name) parts.push(this.attrs.name);
		return "[" + parts.join(" ") + "]";
	};
	
	Element.prototype.toXML = function toXML() {
		var selector = this.selector;
		var attrs = this.attrs || {};
		var children = this.children;
		
		var self = this;

		var strAttrs = [];
		if (typeof children === "undefined") {
			Object.keys(attrs).forEach(function(attr) {
				var value = attrs[attr];
				if (typeof value === "string" || typeof value === "number") {
					strAttrs.push(attr + "='" + attrs[attr] + "'");
				}
			});
			return core.stringTemplate("<{{selector}} {{attrs}} />", {selector: selector, attrs: strAttrs.join(" ")});
		}
		
		var template = "<{{selector}} {{attrs}}>{{children}}</{{selector}}>";
		
		if (Object.keys(attrs).length == 0) {
			template = "<{{selector}}>{{children}}</{{selector}}>";
		}
		
		var strChildren;
		if (typeof children === "string") {
			strChildren = children;
		} else if (typeof children === "function") {
			strChildren = children(this).map(function(child) {
				return child.toXML();
			})
				.join("");
		} else {
			strChildren = children.map(function(child) {
				return child.toXML();
			}).join("");
		}
		
		Object.keys(attrs).forEach(function(attr) {
			var value = attrs[attr];
			if (typeof value === "string" || typeof value === "number") {
				strAttrs.push(attr + "='" + attrs[attr] + "'");
			}
		});
		return core.stringTemplate(template, {selector: selector, attrs: strAttrs.join(" "), children: strChildren});
	};
	
	Element.prototype.getWidth = function getWidth() {
		return this.attrs.width || this.parent && this.parent.getWidth() || 0;
	};
	
	Element.prototype.getHeight = function getHeight() {
		if (this.attrs.height) {
			return this.attrs.height;
		}
		if (this.parent) {
			if (this.parent.selector == "view") {
				return this.parent.getHeight() - 32;
			} else if (this.parent.selector == "page") {
				return this.parent.getHeight() - 16;
			}
			return this.parent.getHeight();
		}
		return 0;
	};

	function registerChildren(registry, element, root) {
		root = root || element;
		if (element.attrs && element.attrs.name) {
			registry[element.attrs.name] = element;
		}
		if (element.children) {
			var children;
			if (Array.isArray(element.children)) {
				children = element.children;
			} else if (typeof element.children === "function") {
				children = element.children(element);
			} else if (typeof element.children === "string") {
				return;
			} else {
				core.console("Unknown children type", typeof element.children);
			}
			children.forEach(function(child) {
				child.parent = element;
				child.root = root;
				registerChildren(registry, child, root);
			});
		}
	}

	Element.prototype.showControls = function showControls(visible) {
		visible = (!visible === undefined ? false : visible);
		var string = this.toXML();
		var string = core.prettifyXml(string);
		skapi.ShowControls(string, visible);
		
		var registry = this.registry = {};
		var title = this.attrs.title;
		registerChildren(registry, this);
		
		var handler = this.handler = {};
		var self = this;
		handler.OnControlEvent = handler.OnControlEvent || function OnControlEvent(szPanel, szControl, dictSzValue) {
			if (szPanel != title) return;
			var element = registry[szControl];
			var value = dictSzValue(szControl);
			element.value = value;

			

			if (element && element.attrs && element.attrs.onControlEvent) {
				element.attrs.onControlEvent.apply(element, arguments);
			} 
			
			if (element && element.emit) {
				element.emit("onControlEvent", szPanel, szControl, dictSzValue, value);
			}

			if (element.attrs.progid == "DecalControls.Notebook") {
				if (element.parent) {
					emitShown(element.parent);
				}
			}
			
		};
		skapi.AddHandler(evidOnControlEvent,  handler);
		
		emitShown(this);
	};

	Element.prototype.removeControls   = function removeControls() {
		var title = this.attrs.title;
		skapi.RemoveControls(title);
		skapi.RemoveHandler(evidNil, this.handler);
	};

/*
	Element.prototype.getElement = function getElement(name) {
		var root = this.root || this;
		return root.registry[name];
	};
*/

	function getChildren(element) {
		if (element.children) {
			var children;
			if (Array.isArray(element.children)) {
				return element.children;
			} else if (typeof element.children === "function") {
				return element.children(element);
			}
		}
	}

	function findNestedName(elements, name) {
		return elements.reduce(function (found, element) {
			if(found) return found;
			if (element.attrs && element.attrs.name == name) {
				return element;
			}
			var children = getChildren(element);
			if (children && children.length > 0) {
				return findNestedName(element.children, name)
			}
		}, null)
	}

	Element.prototype.getElement = function getElement(name) {
		var root = this.root || this;
		return findNestedName(root.children, name);
	};

	Element.prototype.getControlProperty = function getControlProperty(property, callback) {
		var root = this.root || this;
		var title = root.attrs.title;
		return core.getControlPropertyAsync({
			panelName  : title,
			controlName: this.attrs.name,
			property   : property
		}, callback);
	};

	Element.prototype.setControlProperty = function setControlProperty(property, value) {
		var root = this.root || this;
		var title = root.attrs.title;
		this.attrs[property] = value;
		return skapi.SetControlProperty(title, this.attrs.name, property, value);
	};
	
	Element.prototype.setText = function setText(value) {
		return this.setControlProperty("text", value);
	};


	function emitShown(element) {
		if (typeof element.emit === "function") {
			element.emit("onShow");
		}
		var children = element.getChildren();
		if (children) {
			children.forEach(function(child, index) {
				if (child.selector == "page") {
					var value = element.value || 0;
					if (value == index) {
						emitShown(child);
					} else {
						emitHidden(child);
					}
				} else {
					emitShown(child);
				}
			});
		}
	}

	function emitHidden(element) {
		if (typeof element.emit === "function") {
			element.emit("onHide");
		}
		var children = element.getChildren();
		if (children) {
			children.forEach(function(child) {
				emitHidden(child);
			});
		}
	}

	core.embeds = core.embeds || {};
	
	/**
	 * createView() Returns a view object and will unload it when the parent script is disabled.
	 */
	function createView(params) {
		var script = this;
		var view = new core.View(params);
		if (typeof script.on === "function") {
			script.on("onDisable", function onDisable() {
				core.debug("Script disabling, unloading view...");
				view.removeControls();
			});
		}
		return view;
	}
	
	function embedMixins(script) {
		script.createView = createView;
		return script;
	}
	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	return core;
}));