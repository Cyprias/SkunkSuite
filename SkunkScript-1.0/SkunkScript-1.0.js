/********************************************************************************************\
	File:           SkunkScript-1.0.js
	Purpose:        Simple script framework for Skunkworks.
	Creator:        Cyprias
	Date:           09/17/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkScript-1.0";
var MINOR = 190816;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkScript10 = factory();
	}
}(function () {
	var core;

	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	var debugging = false;

	core.scripts            = core.scripts || {};
	core.statuses           = core.statuses || {};
	core.embeds             = core.embeds || {};
	core.initializeQueue    = core.initializeQueue || [];
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;

	core.consoleLog = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(args.join('\t'), opmConsole);
	};

	core.debug = function debug() {
		if (debugging != true) return;
		skapi.OutputLine(Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};

	function getName() {
		return this._script.moduleName || this._script.name;
	}
	
	function getShortName() {
		return this._script.shortName || this.getName();
	}
	
	function setShortName(value) {
		this._script.shortName = value;
		return this;
	}
	
	
	function enable() {
		this._script.enabledState = true;
		if (!queuedForInitialization(this)) {
			var args = Array.prototype.slice.call(arguments);
			return core.enableScript(this, args);
		}
	}
	
	function disable() {
		this._script.enabledState = false;
		var args = Array.prototype.slice.call(arguments);
		return core.disableScript(this, args);
	}

	function isEnabled() {
		return this._script.enabledState;
	}

	function initialize() {
		this._script.initialized = true;
		
		for (var i = core.initializeQueue.length - 1; i >= 0; i--) {
			if (core.initializeQueue[i] == this) {
				core.initializeQueue.splice(i, 1);
				break;
			}
		}
		
		var args = Array.prototype.slice.call(arguments);
		return core.initializeScript(this, args);
	}
	
	function isInitialized() {
		return this._script.initialized;
	}
	
	function tick() {
		this._script.lastTick = new Date().getTime();
		var args = Array.prototype.slice.call(arguments);
		return core.tickScript(this, args);
	}
	
	function newModule(/*name, libNames*/) {
		var args = [].slice.call(arguments);
		var moduleName = args.splice(0, 1)[0];
		
		var base;
		if (typeof moduleName !== "string") {
			base = moduleName;
			moduleName = args.splice(0, 1)[0];
		}
		
		var scriptName = this.getName();
		
		// Modules are scripts. 
		var module = core.newScript(base, scriptName + "_" + moduleName);
		
		module._script.isModule = true;
		module._script.moduleName = moduleName;
		
		var libNames = args.splice(0, args.length);
		core.embedLibraries(module, libNames);
		
		if (typeof this.emit === "function") {
			this.emit("onModuleCreated", module);
		} else if (this.emitter) {
			this.emitter.emit("onModuleCreated", module);
		} else if (typeof this.onModuleCreated === "function") {
			this.onModuleCreated(module);
		}
		
		this._script.modules[moduleName] = module;
		this._script.orderedModules.push(module);
		
		//consoleLog("created module " + module.getName() + ", enabled: " + module._script.enabledState);
		
		return module;
	}
	
	function getModule(moduleName, silent) {
		if (!this._script.modules[moduleName] && !silent) {
			throw new core.Error("Cannot find module " + moduleName);
		}
		return this._script.modules[moduleName];
	}
	
	function enableModule(moduleName) {
		var module = this.getModule(moduleName);
		return module.enable();
	}
	
	function disableModule(moduleName) {
		var module = this.getModule(moduleName);
		return module.disable();
	}
	
	function toString() {
		return "[Script " + this.getName() + "]";
	}
	
	function set(name, value) {
		this._script.settings[name] = value;
		return this;
	}
	
	function get(name) {
		return this._script.settings[name];
	}
	
	// We don't do inheritance so the script can optionally provide their own base object, we embed our stuff onto it. 
	function Script(/*[object,] <name>, [libNames...]*/) {
		var args = [].slice.call(arguments);
		var scriptName = args.splice(0, 1)[0];
		var script;

		if (typeof scriptName !== "string") {
			script = scriptName;
			scriptName = args.splice(0, 1)[0];
		} else {
			script = {};
		}

		script._script                  = {};           // Store our variables in _script. Functions will go right on the script object.
		script._script.name             = scriptName;
		script._script.enabledState     = false;        // Default is off, script should call .initialize() and .enable() in thier main() function.
		script._script.initialized      = false;
		script._script.lastTick         = 0;
		script._script.shortName        = null;         // Short name
		script._script.settings         = {};

		// functions
		script.disable          = disable;
		script.disableModule    = disableModule;
		script.enable           = enable;
		script.enableModule     = enableModule;
		script.getModule        = getModule;
		script.getName          = getName;
		script.initialize       = initialize;
		script.isEnabled        = isEnabled;
		script.isInitialized    = isInitialized;
		script.newModule        = newModule;
		script.tick             = tick;
		script.getShortName     = getShortName;
		script.setShortName     = setShortName;
		script.toString         = toString;
		script.set              = set;
		script.get              = get;

		return script;
	}

	core.newScript = function newScript(/*[object,] scriptName, libNames...*/) {
		var script = Script.apply(undefined, arguments);
		var scriptName = script.getName();
		
		this.scripts[scriptName] = script;
		script._script.modules = {};
		script._script.orderedModules = [];
		
		script._script.isModule = false;

		var args = [].slice.call(arguments);
		var i = (typeof args[0] === "object") && 2 || 1;
		var libNames = args.splice(i, args.length);
		this.embedLibraries(script, libNames);

		this.initializeQueue.push(script);
		return script;
	};
	
	function queuedForInitialization(script) {
		for (var i = 0; i < core.initializeQueue.length; i++) {
			if (core.initializeQueue[i] == script) {
				return true;
			}
		}
		return false;
	}
	
	function getVariable(name) {
		return new Function('return ' + name + ';').call();
	}
	
	function getLibrary(libname/*, silent*/) {
		return ((typeof LibStub !== "undefined") && LibStub.getLibrary(libname, true)) || getVariable(libname) || (typeof require === "function" && require(libname));
	}
	
	core.getScript = function getScript(name, silent) {
		if (!this.scripts[name] && !silent) {
			throw new core.Error("Cannot find script " + name);
		}
		return this.scripts[name];
	};
	
	core.embedLibrary = function embedLibrary(script, libname, silent) {
		var lib = getLibrary(libname, silent);
		
		//var lib = getVariable(libname) || (typeof require === "function" && require(libname));

		var scriptName = script.getName();
		if (typeof lib === "undefined" && !silent) {
			throw new core.Error("Cannot find library: " + libname);
		} else if (lib && typeof lib.embed === "function") {
			lib.embed(script);
			this.embeds[scriptName] = this.embeds[scriptName] || [];
			this.embeds[scriptName].push(libname);
			return true;
		} else if (lib) {
			throw new core.Error("Library " + libname + " is not embed capable.");
		}
	};

	core.embedLibraries = function embedLibraries(script, libs) { // *script, libnames...*/
		//var args = [].slice.call(arguments);
		//var script = args.splice(0, 1)[0];
		
		var libname;
		for (var i = 0; i < libs.length; i++) {
			libname = libs[i];
			this.embedLibrary(script, libname, false);
		}
	};

	core.enableScript = function enableScript(script, args) {
		//consoleLog("<enableScript>", script.getName());
		var name = script.getName();

		if (this.statuses[name]) {
			core.consoleLog(core.MAJOR, "not enabling A", this.statuses[name]);
			return false;
		}

		this.statuses[name] = true;
		
		var clone = args.slice(0);
		clone.splice(0, 0, "onEnable");

		if (typeof script.emit === "function") {
			script.emit.apply(script, clone);
		} else if (script.emitter) {
			script.emitter.emit.apply(script, clone);
		} else if (typeof script.onEnable === "function") {
			script.onEnable.apply(script, args);			
		}

		if (this.statuses[name]) {
		//	consoleLog("enabling embeds and modules...");
			
			// Enable embeds.
			var scriptName = script.getName();
			var embeds = this.embeds[scriptName];
			if (embeds) {
				var libname;
				var lib;
				for (var i = 0; i < embeds.length; i++) {
					libname = embeds[i];
					lib = getLibrary(libname, true);
					if (lib) {
						if (typeof lib.emit === "function") {
							lib.emit("onEmbedEnable", {script: script, args: args});
						} else if (lib.emitter) {
							lib.emitter.emit("onEmbedEnable", {script: script, args: args});
						}
					}
				}
			}
			
			// Enable modules.
			var modules = script._script.orderedModules;
			for (var i = 0; i < modules.length; i++) {
			//	consoleLog(" enabling module", modules[i].getName());
				this.enableScript(modules[i], args);
			}
		}

		return this.statuses[name];
	};

	core.disableScript = function disableScript(script, args) {
		var name = script.getName();
		
		if (!this.statuses[name]) return false;

		this.statuses[name] = false;
		
		var clone = args.slice(0);
		clone.splice(0, 0, "onDisable");
		if (typeof script.emit === "function") {
			script.emit.apply(script, clone);
		} else if (script.emitter) {
			script.emitter.emit.apply(script, clone);
		} else if (typeof script.onDisable === "function") {
			script.onDisable.apply(script, args);	
		}
		
		if (!this.statuses[name]) {
			var scriptName = script.getName();
			var embeds = this.embeds[scriptName];
			if (embeds) {
				var libname;
				var lib;
				for (var i = 0; i < embeds.length; i++) {
					libname = embeds[i];
					lib = getLibrary(libname, true);
					if (lib) {
						if (typeof lib.emit === "function") {
							lib.emit("onEmbedDisable", {script: script, args: args});
						} else if (lib.emitter) {
							lib.emitter.emit("onEmbedDisable", {script: script, args: args});
						}
					}
				}
			}
			
			// Disable modules.
			var modules = script._script.orderedModules;
			for (var i = 0; i < modules.length; i++) {
				this.disableScript(modules[i], args);
			}
		}
		
		return !this.statuses[name];
	};
	
	core.initializeScript = function initializeScript(script, args) {
		
		var clone = args.slice(0);
		clone.splice(0, 0, "onInitialize");
		if (typeof script.emit === "function") {
			script.emit.apply(script, clone);
		} else if (script.emitter) {
			script.emitter.emit.apply(script, clone);
		} else if (typeof script.onInitialize === "function") {
			script.onInitialize.apply(script, args);	
		}
		
		// Init embeds.
		var scriptName = script.getName();
		var embeds = this.embeds[scriptName];
		if (embeds) {
			var libname;
			var lib;
			for (var i = 0; i < embeds.length; i++) {
				libname = embeds[i];
				lib = getLibrary(libname, true);
				if (lib) {
					if (typeof lib.emit === "function") {
						lib.emit("onEmbedInitialize", {script: script, args: args});
					} else if (lib.emitter) {
						lib.emitter.emit("onEmbedInitialize", {script: script, args: args});
					}
				}
			}
		}
		
		
		// Initialize modules.
		var modules = script._script.orderedModules;
		for (var i = 0; i < modules.length; i++) {
		//	consoleLog(" enabling module", modules[i].getName());
			this.initializeScript(modules[i], args);
		}
		
		// Tell other scripts that a script/module has been initalized.
		var s;
		for (var scriptName in this.scripts) {
			if (!this.scripts.hasOwnProperty(scriptName)) continue;
			s = this.scripts[scriptName];
			
			if (typeof s.emit === "function") {
				s.emit("onScriptInitialized", {script: script, args: args});
			} else if (s.emitter) {
				s.emitter.emit("onScriptInitialized", {script: script, args: args});
			}
		}
	};

	core.tickScript = function tickScript(script, args) {
		var name = script.getName();
		
		// Check if the script is disabled.
		if (!this.statuses[name]) return false;
			
		var clone = args.slice(0);
		clone.splice(0, 0, "onTick");
		if (typeof script.emit === "function") {
			script.emit.apply(script, clone);
		} else if (script.emitter) {
			script.emitter.emit.apply(script, clone);
		} else if (typeof script.onTick === "function") {
			script.onTick.apply(script, args);
		}
		
		// Tick modules.
		var modules = script._script.orderedModules;
		for (var i = 0; i < modules.length; i++) {
			this.tickScript(modules[i], args);
		}
	};
	
	return core;
}));

/*
	// Basic example.
	var core = LibStub("SkunkScript-1.0").newScript("MyScript");
	
	core.onInitialize = function onInitialize() {}; // Do init tasks here.
	core.onEnable = function onEnable() {}; // More initialization of stuff that enables the use of your script.
	core.onDisable = function onDisable() {}; // Disable your script so that all frames, events, timers are gone.
	core.onTick = function onTick() {}; // Your main loop logic.
	
	function main() {
		core.initialize();
		core.enable();
		while (core._script.enabledState == true) {
			core.tick();
			skapi.WaitEvent(1000, wemFullTimeout);
		}
		console.StopScript(); // Prevent Skunkworks from sometimes crashing.
	}
*/