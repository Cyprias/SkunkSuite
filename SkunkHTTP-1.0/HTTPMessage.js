/********************************************************************************************\
	File:           SkunkHTTP-1.0\HTTPMessage.js
	Creator:        Cyprias
	Date:           04/07/17
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Dependencies:   SkunkHTTP-1.0\Headers.js, Stream.js
\*********************************************************************************************/

(function (factory) {
	var dependencies = {};
	
	dependencies.Stream = (typeof Stream !== "undefined" && Stream) || require("SkunkSuite\\Stream"); 
	if (typeof dependencies.Stream === "undefined") {
		throw new Error("Stream.js isn't loaded.");
	}
	
	dependencies.Headers = (typeof Headers !== "undefined" && Headers) || require("Headers"); 
	if (typeof dependencies.Headers === "undefined") {
		throw new Error("Headers.js isn't loaded.");
	}
	
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		// eslint-disable-next-line no-undef
		HTTPMessage = factory(dependencies);
	}
}(function (dependencies) {
	var debugging = false;
	
	var Stream = dependencies.Stream;
	var Headers = dependencies.Headers;
	
	
	function debug() {
		if (debugging == true) {
			skapi.OutputLine("[HTTPMessage.js] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	}

	function HTTPMessage() {
		Stream.call(this);
		debug("<HTTPMessage>");
		this.headers            = new Headers();
		this.method             = null;
		this.rawHeaders         = []; // The raw header list exactly as they were received.
		this.statusCode         = null;
		this.statusMessage      = null;
		this.url                = null;
	}
	HTTPMessage.Headers = Headers;

	HTTPMessage.prototype = new Stream();
	HTTPMessage.prototype.constructor      = HTTPMessage;

	// Make the toString() more informative.
	HTTPMessage.prototype.toString = function () {
		return "[HTTPMessage " + this.url + "]";
	};
	
	HTTPMessage.prototype.toJSON = function () {
		var self = this;
		var copy = {};

		for (var key in self) {
			if (!self.hasOwnProperty(key)) continue;
			
			// Prevent looping.
			if (self[key] instanceof HTTPMessage) {
				copy[key] = self[key].toString();
				continue;
			}
			
			copy[key] = self[key];
		}

		return copy;
	};
	
	debug("loaded.");
	return HTTPMessage;
}));