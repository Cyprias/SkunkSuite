/********************************************************************************************\
	File:           SkunkHTTP-1.0\Headers.js
	Creator:        Cyprias
	Date:           04/07/17
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

(function (factory) {
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// eslint-disable-next-line no-undef
		Headers = factory();
	}
}(function () {
	var debugging = false;
	
	function debug() {
		if (debugging == true) {
			skapi.OutputLine("[Headers.js] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	}

	function Headers(headers) {
		debug("<Headers>");
		for (var header in headers) {
			if (!headers.hasOwnProperty(header)) continue;
			this.append(header, headers[header]);
		}
	}

	var normalizeValue = Headers.normalizeValue = function normalizeValue(value) {
		if (typeof value !== 'string') {
			value = String(value);
		}
		return value;
	};
	
	var normalizeName = Headers.normalizeName = function normalizeName(name) {
		if (typeof name !== 'string') {
			name = String(name);
		}
		if (/[^a-z0-9\-#$%&'*+.\^_`|~]/i.test(name)) {
			throw new Error('Invalid character in header field name');
		}
		return name.toLowerCase();
	};

	Headers.prototype.append = function append(name, value) {
		name = normalizeName(name);
		value = normalizeValue(value);
		var oldValue = this[name];
		this[name] = oldValue ? oldValue + ',' + value : value;
	};

	Headers.prototype.del = function del(name) {
		delete this[normalizeName(name)];
	};
	
	Headers.prototype.has = function(name) {
		return this.hasOwnProperty(normalizeName(name));
	};
	
	Headers.prototype.get = function(name) {
		name = normalizeName(name);
		return this.has(name) ? this[name] : undefined;
	};

	Headers.prototype.set = function(name, value) {
		this[normalizeName(name)] = normalizeValue(value);
	};
	
	debug("loaded.");
	return Headers;
}));