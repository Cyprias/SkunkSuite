/********************************************************************************************\
	File:           SkunkHTTP-1.0\Request.js
	Creator:        Cyprias
	Date:           04/07/17
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Dependencies:   SkunkHTTP-1.0\HTTPMessage.js, SkunkHTTP-1.0\Response.js
\*********************************************************************************************/

(function (factory) {
	var dependencies = {};
	dependencies.HTTPMessage = (typeof HTTPMessage !== "undefined" && HTTPMessage) || require("HTTPMessage");
	if (typeof dependencies.HTTPMessage === "undefined") {
		throw new Error("HTTPMessage.js isn't loaded.");
	}
	
	dependencies.Response = (typeof Response !== "undefined" && Response) || require("Response"); // Response requires HTTPMessage
	if (typeof dependencies.Response === "undefined") {
		throw new Error("Response.js isn't loaded.");
	}

	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		Request = factory(dependencies);
	}
}(function (dependencies) {
	var debugging = false;

	var HTTPMessage = dependencies.HTTPMessage;
	var Response = dependencies.Response;

	/* eslint-disable */
	var SXH_SERVER_CERT_IGNORE_UNKNOWN_CA           = 0x0100;
	var SXH_SERVER_CERT_IGNORE_WRONG_USAGE          = 0x0200;
	var SXH_SERVER_CERT_IGNORE_CERT_CN_INVALID      = 0x1000;
	var SXH_SERVER_CERT_IGNORE_CERT_DATE_INVALID    = 0x2000;
	var SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS    = 0x3300;
	
	var XHR_UNINITIALIZED   = 0;
	var XHR_LOADING         = 1;
	var XHR_LOADED          = 2;
	var XHR_INTERACTIVE     = 3;
	var XHR_COMPLETED       = 4;
	/* eslint-enable */
	
	// eslint-disable-next-line no-unused-vars
	function consoleLog() {
		skapi.OutputLine("[Request.js] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	}
	
	var debug = function debug() {
		if (debugging == true) {
			skapi.OutputLine("[Request.js] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	};

	function Request(options, callback) {
		debug("<Request>");
		HTTPMessage.call(this);
		
		var self = this;
		if (typeof options === "string") {
			var url = options;
			options = {};
			options.url = url;
		}
		
		self.method = options.method || 'GET';
		self.url = options.url;
		self.headers = options.headers || {};

		self.strictSSL = true; // false to allow SSL errors (like self-signed certs).
		if (typeof options.strictSSL !== "undefined") {
			self.strictSSL = options.strictSSL;
		}
		
		self.auth = options.auth || {};
		
		// With Microsoft's ServerXMLHTTP class, the programmer can't specify a preferred authentication mechanism. The workaround is to specify a custom header manually.
		// https://zanstra.home.xs4all.nl/inTec/ServerXMLHTTP.htm
		if (self.auth.user && self.auth.pass) {
			self.headers["Authorization"] = "Basic " + Base64.encode(self.auth.user + ":" + self.auth.pass);
		}
		
		self.time = options.time || false; // Include timings in the responce object.
		self.body = options.body;// for PATCH, POST and PUT
		
		self.jsonReviver = options.jsonReviver;
		self.jsonReplacer = options.jsonReplacer;

		self.timeout = options.timeout || 30 * 1000;
		
		self._callback = callback;
		self._thisArg = options.thisArg;
		self._json = false;
		
		var xmlhttp;
		self.getXMLHTTP = function() {
			return xmlhttp; 
		};
		self.setXMLHTTP = function(value) {
			xmlhttp = value;
		};

		// JSON.stringify won't stringify objects when assigned directly to a array (noStringify.foo = "bar");
		// Our XMLHTTP object is set here which JSON throws an error when trying to stringify.
		//self.noStringify = []; 
		
		self.init(options);
	}

	Request.create = function create(options, callback) {
		return new Request(options, callback);
	};
	
	Request.HTTPMessage = HTTPMessage;
	Request.Response = Response;
	
	// Prototype stuff
	Request.prototype = new HTTPMessage();
	Request.prototype.constructor      = Request;

	Request.prototype.init = function init(options) {
		debug("<init>");
		var self = this;
		
		// Handle the body stuff.
		if (options.json) {
		//	debug("json! " + this.method + ", " + this.url);
			self.json(options.json);
		} else if (options.form) {
			self.form(options.form);
		}
		
		if (options.query) {
			self.query(options.query);
		}

		// Call send on the next tick, this allows chaining of other functions like form()/json()
		if (typeof setImmediate === "function") {
			setImmediate(function requestSend() {
				self.send();
			});
		} else {
			throw new Error("Can't find setImmediate()!");
		}

		return this;
	};

	function escapeParams(params) {
		var param = "";
		for (var key in params) {
			if (param.length > 0) {
				param += "&";
			}
			
			// If key points to a object, throw an error. 
			if (typeof params[key] === "object") {
				throw new Error("Can't escape param key '" + key + "', try JSON.stringify().");
			} else if (typeof params[key] === "undefined") {
				continue;
			}
			
			// eslint-disable-next-line newline-per-chained-call
			param += encodeURIComponent(key).split("%20").join("+") + "=" + encodeURIComponent(params[key]).split("%20").join("+");
		}
		return param;
	}
	
	Request.prototype.query = function query(params) {
		this.url += "?" + escapeParams(params);
		return this;
	};
	
	Request.prototype.form = function form(form) {
		var self = this;
		
		try {
			self.body = escapeParams(form);
		} catch (e) {
			throw new Error("Cannot convert object to form string.");
		}
		self.headers["Content-Type"] = "application/x-www-form-urlencoded";
		
		return self;
	};
	
	Request.prototype.json = function json(value) {
		debug("<json> value: " + value + " (" + (typeof value) + ")");
		var self = this;
		
		self.headers["Content-Type"] = "application/json";
		self.headers["Accept"] = "application/json,text/html";

		if (typeof value === 'boolean') {
			if (typeof self.body !== "undefined") {
				if (typeof JSON === "undefined") {
					throw new Error("Cannot stringify json, JSON isn't loaded.");
				}
				self.body = JSON.stringify(self.body);
			}
		} else {
			self.body = JSON.stringify(value);
		}
		self._json = true;
		return self;
	};

	Request.prototype.recycle = function () {
		debug("<recycle>");
		var self = this;

		if (typeof self.getXMLHTTP !== "undefined") {
			var xmlhttp = self.getXMLHTTP();
			if (xmlhttp) {
				xmlhttp.abort(); // causes onreadystatechange to fire with a readyState of 0.

				pool[self.strictSSL] = pool[self.strictSSL] || [];
				pool[self.strictSSL].push(xmlhttp);
				self.setXMLHTTP(undefined);
			}
		}

		return self;
	};
	
	Request.prototype.abort = function () {
		debug("<abort>");
		var self = this;
		self._aborted = true;
		
		self.recycle();
		
		if (typeof self.emit !== "undefined") {
			self.emit("abort");
		}
		return self;
	};

	var pool = {};
	Request.prototype.send = function send() {
		debug("<send>");
		var self = this;

		if (self._aborted) { 
			//throw new Error("Request has already aborted.");
			return;
		}
		
		var xmlhttp;
		if (pool[self.strictSSL] && pool[self.strictSSL].length > 0) {
			debug("Using pooled xmlhttp. (" + pool[self.strictSSL].length + ")");
			xmlhttp = pool[self.strictSSL].splice(0, 1)[0];
		} else {
			debug("Creating new xmlhttp.");
			if (self.strictSSL == false) {
				// MSXML2.ServerXMLHTTP allows self signed certs but fails to work on some domains, unsure why.
				xmlhttp = new ActiveXObject("MSXML2.ServerXMLHTTP");
			} else {
				xmlhttp = new ActiveXObject("MSXML2.XMLHTTP.6.0");
			}
		}

		self.setXMLHTTP(xmlhttp);
		
		if (self.strictSSL == false) {
			xmlhttp.setOption(2, SXH_SERVER_CERT_IGNORE_ALL_SERVER_ERRORS);
		}
		
		var timingStart = (new Date()).getTime();
		var lastChange = timingStart;

		var response = new Response(xmlhttp);
		response.req = this;
		this.res = response;
		
		if (self.time == true) {
			response.timingStart = timingStart;
			response.timings = {};
			response.timingPhases = {};
		}
		
		xmlhttp.onreadystatechange = function onreadystatechange() {
			var readyState = xmlhttp.readyState;

			// Save timings.
			var now = (new Date()).getTime();
			if (self.time == true) {
				response.timings[readyState] = now;
				response.timingPhases[readyState] = now - lastChange;
				lastChange = now;
			}
			
			// Refresh the response object.
			response.refresh(xmlhttp);
			
			// Debug message of the current state.
			var msg = "<onreadystatechange> readyState: " + readyState;
			if (readyState >= XHR_LOADING) {
				try {
					msg += "\n status: " + xmlhttp.status;
				} catch (e) {}
				
				try {
					msg += "\n statusText: " + xmlhttp.statusText;
				} catch (e) {}
				
				try {
					msg += "\n headers: " + xmlhttp.getAllResponseHeaders();
				} catch (e) {}
			}

			if (readyState >= XHR_COMPLETED) {
				try {
					msg += "\n responseText: " + xmlhttp.responseText;
				} catch (e) {}
			}
			
			var elapsed = (new Date()).getTime() - timingStart;
			msg += "\n elapsed: " + elapsed;
			debug(msg); // Logging to console actually causes a waitevent within skapi which can allow multiple onreadystatechange to fire, so we only want to call debug once with all the lines.

			// Finalize the reponse and fire callbacks.
			if (xmlhttp && readyState == XHR_COMPLETED) {
				
				response.method = self.method;
				response.url = self.url;

				if (self.time == true) {
					response.timingEnd = now;
					response.timingElapsed = now - timingStart;
				}
				
				//var body = xmlhttp.responseText;
				//debug("body: " + body);
				
				// Cancel timeout timer.
				if (self.timeout && typeof timeoutTimer !== "undefined") {
				//	consoleLog("canceling timer.");
					if (typeof clearTimeout === "function") {
						clearTimeout(timeoutTimer);
					}
					timeoutTimer = undefined;
				}
				
				if (self._json == true) {

					try {
						response.body = JSON.parse(response.body, this.jsonReviver, this.jsonReplacer);
					} catch (e) {
						if (self._callback) {
							self._callback.call(self._thisArg, e, response, response.body);
						}
						if (typeof self.emit !== "undefined") {
							self.emit("error", e, response, response.body);
							self.end();
						}
						self.abort();
						return;
					}
				}
				
				self.recycle();
				
				if (self._callback) {
					self._callback.call(self._thisArg, undefined, response, response.body);
				}
				
				if (typeof self.emit !== "undefined") {
					self.emit("response", response, response.body);
					self.write(response);
					self.end();
				}
			}
		};

		debug("user: " + self.auth.user + ", pass: " + self.auth.pass);
		xmlhttp.open(this.method, this.url, true, self.auth.user, self.auth.pass);
		//xmlhttp.setTimeouts(5 * 1000, 5 * 1000, 15 * 1000, self.timeout); // Used for MSXML2.ServerXMLHTTP.

		for (var header in self.headers) {
			if (!self.headers.hasOwnProperty(header)) continue;
			xmlhttp.setRequestHeader(header, self.headers[header]);
			debug("set header '" + header + "' to " + self.headers[header]);
		}

		debug("Sending request... self.body: " + self.body);
		xmlhttp.send(self.body);

		var timeoutTimer;
		if (typeof setTimeout === "function") {
			timeoutTimer = setTimeout(function requestTimeout() {
				
				self.recycle();
				
				var e = new Error('ETIMEDOUT');
				e.code = 'ETIMEDOUT';
				if (self._callback) {
					self._callback.call(self._thisArg, e, response);
				}
				if (typeof self.emit !== "undefined") {
					self.emit("error", e, response);
					self.end();
				}
				self.abort();
			}, self.timeout);
		}

		return self;
	};

	// Make the toString() more informative.
	Request.prototype.toString = function () {
		return "[Request " + this.url + "]";
	};

	// https://ctrlq.org/code/19920-encode-decode-base64-javascript
	var Base64 = {
		_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
		encode : function(e) {
			var t = "";
			var n, r, i, s, o, u, a;
			var f = 0;
			e = Base64._utf8_encode(e);
			while (f < e.length) {
				n = e.charCodeAt(f++);
				r = e.charCodeAt(f++);
				i = e.charCodeAt(f++);
				s = n >> 2;
				o = (n & 3) << 4 | r >> 4;
				u = (r & 15) << 2 | i >> 6;
				a = i & 63;
				if (isNaN(r)) {
					u = a = 64;
				} else if (isNaN(i)) {
					a = 64;
				}
				t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) +
					this._keyStr.charAt(u) + this._keyStr.charAt(a);
			}
			return t;
		},
		decode: function(e) {
			var t = "";
			var n, r, i;
			var s, o, u, a;
			var f = 0;
			e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
			while (f < e.length) {
				s = this._keyStr.indexOf(e.charAt(f++));
				o = this._keyStr.indexOf(e.charAt(f++));
				u = this._keyStr.indexOf(e.charAt(f++));
				a = this._keyStr.indexOf(e.charAt(f++));
				n = s << 2 | o >> 4;
				r = (o & 15) << 4 | u >> 2;
				i = (u & 3) << 6 | a;
				t = t + String.fromCharCode(n);
				if (u != 64) {
					t = t + String.fromCharCode(r);
				}
				if (a != 64) {
					t = t + String.fromCharCode(i);
				}
			}
			t = Base64._utf8_decode(t);
			return t;
		},
		_utf8_encode: function(e) {
			e = e.replace(/\r\n/g, "\n");
			var t = "";
			for (var n = 0; n < e.length; n++) {
				var r = e.charCodeAt(n);
				if (r < 128) {
					t += String.fromCharCode(r);
				} else if (r > 127 && r < 2048) {
					t += String.fromCharCode(r >> 6 | 192);
					t += String.fromCharCode(r & 63 | 128);
				} else {
					t += String.fromCharCode(r >> 12 | 224);
					t += String.fromCharCode(r >> 6 & 63 | 128);
					t += String.fromCharCode(r & 63 | 128);
				}
			}
			return t;
		},
		_utf8_decode: function(e) {
			var t = "";
			var n = 0;
			var r, c2, c3 = 0;
			while (n < e.length) {
				r = e.charCodeAt(n);
				if (r < 128) {
					t += String.fromCharCode(r);
					n++;
				} else if (r > 191 && r < 224) {
					c2 = e.charCodeAt(n + 1);
					t += String.fromCharCode((r & 31) << 6 | c2 & 63);
					n += 2;
				} else {
					c2 = e.charCodeAt(n + 1);
					c3 = e.charCodeAt(n + 2);
					t += String.fromCharCode((r & 15) << 12 | (c2 & 63) <<
						6 | c3 & 63);
					n += 3;
				}
			}
			return t;
		}
	};
	
	debug("loaded.");
	return Request;
}));