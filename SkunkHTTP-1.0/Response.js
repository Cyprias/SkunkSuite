/********************************************************************************************\
	File:           SkunkHTTP-1.0\Response.js
	Creator:        Cyprias
	Date:           04/07/17
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Dependencies:   SkunkHTTP-1.0\HTTPMessage.js
\*********************************************************************************************/

(function (factory) {
	var dependencies = {};

	dependencies.HTTPMessage = (typeof HTTPMessage !== "undefined" && HTTPMessage) || require("HTTPMessage");
	if (typeof dependencies.HTTPMessage === "undefined") {
		throw new Error("HTTPMessage.js isn't loaded.");
	}
	
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		Response = factory(dependencies);
	}
}(function (dependencies) {
	var debugging = false; // Warning! xmlhttp's values can change while OutputLine() is executing, this can cause errors. So keep this off.
	
	var HTTPMessage = dependencies.HTTPMessage;
		
	function debug() {
		if (debugging == true) {
			skapi.OutputLine("[Response.js] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	}

	function Response(xmlhttp) {
	//	debug("<Response>");
		HTTPMessage.call(this);
		
		if (xmlhttp && xmlhttp.readyState > 1) { // Sometimes gives 'The data necessary to complete this operation is not yet available.' when checking > 0.
			this.statusCode         = xmlhttp.status;
			this.statusMessage      = xmlhttp.statusText;
		}

		if (xmlhttp && xmlhttp.readyState > 1) { // Sometimes gives 'The data necessary to complete this operation is not yet available.' when checking > 0.
			this.parseResponseHeaders(xmlhttp.getAllResponseHeaders());
		}
	}

	Response.HTTPMessage = HTTPMessage;
	
	Response.prototype = new HTTPMessage();
	Response.prototype.constructor      = Response;

	function trimStr(strInput) {
		var objRegex = new RegExp("(^\\s+)|(\\s+$)");
		return strInput.replace(objRegex, "");
	}
	
	Response.prototype.parseResponseHeaders = function parseResponseHeaders(rawHeaders) {
		//debug("<parseResponseHeaders>");
		
		var headerPairs = rawHeaders.split("\u000d\u000a");
		var parts;
		var header;
		var value;
		for (var i = 0; i < headerPairs.length; i++) {
			parts = headerPairs[i].split(':');
			if (parts[1]) {
				header = parts[0];
				value = trimStr(parts[1]);
				if (this.headers.get(header) != value) {
					//this.headers[header] = value;
					this.headers.set(header, value);
					this.rawHeaders.push(header);
					this.rawHeaders.push(value);
				}
			}
		}
		return this;
	};
	
	Response.prototype.refresh = function refresh(xmlhttp) {
	//	debug("<refresh>");
		this.readyState         = xmlhttp.readyState;
		if (xmlhttp.readyState >= 1) { // XHR_LOADING
			try {
				this.statusCode = xmlhttp.status;
			} catch (e) {
				this.statusCode = null;
			}

			try {
				this.statusMessage      = xmlhttp.statusText;
			} catch (e) {
				this.statusMessage      = null;
			}
				
				
			try {
				this.parseResponseHeaders(xmlhttp.getAllResponseHeaders());
			} catch (e) {
				this.headers = undefined;
				this.rawHeaders = undefined;
			}
		}
		if (xmlhttp.readyState >= 4) { // XHR_COMPLETED
			this.body = xmlhttp.responseText;
		}
		return this;
	};

	// Make the toString() more informative.
	Response.prototype.toString = function () {
		return "[Response " + this.url + "]";
	};

	Response.prototype.json = function json() { // resolves the string to a object.
		var obj;
		try {
			obj = JSON.parse(this.body, this.jsonReviver, this.jsonReplacer);
		} catch (e) {
			//
		}
		return obj;
	};
	
	debug("loaded.");
	return Response;
}));