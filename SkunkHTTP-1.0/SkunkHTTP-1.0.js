/********************************************************************************************\
	File:           SkunkHTTP-1.0\SkunkHTTP-1.0.js
	Purpose:        HTTP functions for Skunkworks.
	Creator:        Cyprias
	Date:           04/07/17
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Dependencies:   SkunkHTTP-1.0\Request.js
	Optional:       SkunkTimer
\*********************************************************************************************/

var MAJOR = "SkunkHTTP-1.0";
var MINOR = 200101;

(function (factory) {
	var dependencies = {};
	dependencies.Request = (typeof Request !== "undefined" && Request) || require("Request"); // Request requires HTTPMessage & Response.
	if (typeof dependencies.Request === "undefined") {
		throw new Error("Request.js isn't loaded.");
	}
	
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		// eslint-disable-next-line no-undef
		SkunkHTTP = factory(dependencies);
	}
}(function (dependencies) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	var Request = dependencies.Request;
	
	var debugging = false;

	// eslint-disable-next-line no-unused-vars
	core.debug = function debug() {
		if (debugging != true) return;
		skapi.OutputLine(Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};

	var HTTP = function HTTP(url, callback) {
		// Direct call to HTTP(), treat as a GET request.
		return HTTP.get(url, callback);
	};

	function verbFunc(verb) {
		var method = verb.toUpperCase();
		return function methodFunction(url, options, callback) {
			if (typeof callback === "undefined" && typeof options === "function") {
				callback = options;
			}
			if (typeof url === "object") {
				options = url;
			} else if (typeof url === "string") {
				options = options || {};
				options.url = url;
			}
			options.method = method;

			if (typeof callback !== "function" && typeof Promise !== "undefined") {
				// If no callback provided, return a promise (requires ES6-shim).
				// http://loige.co/to-promise-or-to-callback-that-is-the-question/
				var args = Array.prototype.slice.call(arguments);
				var self = this;
				return new Promise(function(resolve, reject) {
					args.push(function callbackStub(err, res) {
						if (err) return reject(err);
						resolve(res);
					});
					methodFunction.apply(self, args);
				});
			}

			return Request.create(options, callback);
		};
	}
	
	HTTP.get         = verbFunc('get');
	HTTP.head        = verbFunc('head');
	HTTP.post        = verbFunc('post');
	HTTP.put         = verbFunc('put');
	HTTP.patch       = verbFunc('patch');
	HTTP.del         = verbFunc('delete');
	HTTP.destroy     = verbFunc('delete');
	HTTP['delete']   = verbFunc('delete');

	core.get         = verbFunc('get');
	core.head        = verbFunc('head');
	core.post        = verbFunc('post');
	core.put         = verbFunc('put');
	core.patch       = verbFunc('patch');
	core.del         = verbFunc('delete');
	core.destroy     = verbFunc('delete');
	core['delete']   = verbFunc('delete');
	
	// In case caller script wants access to these.
	core.HTTP           = HTTP;
	core.Request        = Request;
	core.HTTPMessage    = Request.HTTPMessage;
	core.Response       = Request.Response;
	core.Headers        = Request.HTTPMessage.Headers;

	return core;
}));

/*
	// Loading via swx file.
	<script src="modules\\SkunkSuite\\LibStub.js"/> <!-- LibStub is optional. A global 'SkunkHTTP' object will be created when loaded via swx. -->
	<script src="modules\\SkunkSuite\\EventEmitter.js"/>
	<script src="modules\\SkunkSuite\\Stream.js"/>
	<script src="modules\\SkunkSuite\\SkunkHTTP-1.0\\Headers.js"/>
	<script src="modules\\SkunkSuite\\SkunkHTTP-1.0\\HTTPMessage.js"/>
	<script src="modules\\SkunkSuite\\SkunkHTTP-1.0\\Response.js"/> 
	<script src="modules\\SkunkSuite\\SkunkHTTP-1.0\\Request.js"/>
	<script src="modules\\SkunkSuite\\SkunkHTTP-1.0\\SkunkHTTP-1.0.js"/>
	
	// Loading via require().
	var SkunkHTTP = require("SkunkSuite\\SkunkHTTP-1.0");
	
	// Optional retrieving via LibStub (files must already be loaded via swx or require).
	var SkunkHTTP = LibStub("SkunkHTTP-1.0");
	
	// Usage
	SkunkHTTP.get("http://pokeapi.co/api/v2/pokemon/pikachu/", function getCallback(err, res) {
		if (err) {
			consoleLog("Error: " + err.message);
			return;
		}

		var data = res.json();
		consoleLog("name: " + (data && data.name));
	});
	
	// Usage as a stream (experimental)
	// The Request object is a readable stream that writes the 'response' object to the stream.
	var Request = LibStub("SkunkHTTP-1.0").Request;
	var streamFromHTTP = new Request({url:"http://pokeapi.co/api/v2/pokemon/pikachu/", method:"GET"});
	
	// Create a through stream that that'll convert the response object into a pretty json string.
	var streamResponseToPrettyJson = new Stream();
	
	// Replace the write() function with our own to convert the data before passing it on.
	streamResponseToPrettyJson.write = function write(res) {
		if (this.paused) {
			this.buffer.push(res)
			return false
		}

		// Turn the response body into a object.
		var obj = res.json();
		
		// Turn it back into json with tab spaces.
		var json = JSON.stringify(obj, undefined, "\t");
		
		// Pass the json on.
		this.emit('data', json)
		return !this.paused;
	};

	// Create a writable stream to pass our pretty stream to.
	var streamToFile = fs.createWriteStream("pikachu.txt");
	
	// Finally pipe it through.
	streamFromHTTP.pipe(streamResponseToPrettyJson).pipe(streamToFile).on("end", function() {
		chat("All done!");
	});
	
*/