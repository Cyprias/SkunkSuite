/********************************************************************************************\
	File:           SkunkLogger-1.0
	Purpose:        Log messages to chat, console and emitter.
	Creator:        Cyprias
	Date:           10/25/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR   = "SkunkLogger-1.0";
var MINOR   = 210709; // Year Month Day.

(function (factory) {
	
	var _EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof _EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
	}
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(_EventEmitter);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkLogger10 = factory(_EventEmitter);
	}
}(function (EventEmitter) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	core.defaultDepth = 1;
	core.defaultArgumentsDepth = 1;
	core.defaultTraceDepth = 10;
	core.defaultTraceArgumentsDepth = 1;

	core.emitter = core.emitter || new EventEmitter();
	core.embeds = core.embeds || {};

	//var noop = function() {}; // Blank function.

	function embedMixins(script) {
		for (var methodName in logMethods) {
			if (!logMethods.hasOwnProperty(methodName)) continue;
			script[methodName] = methodFactory({methodName: methodName, opm: logMethods[methodName].opm, cmc: logMethods[methodName].cmc, trace: logMethods[methodName].trace, script: script});
		}

		script._script.logTimers = script._script.logTimers || {};
		script['time'] = core.time;
		script['timeEnd'] = core.timeEnd;

		return script;
	}

	core.embed = function embed(script) {
		this.embeds[script] = true;
		return embedMixins(script);
	};
	
	var logMethods = {};
	logMethods["error"]     = {opm: opmConsole | opmChatWnd, cmc: cmcRed};
	logMethods["warn"]      = {opm: opmConsole | opmChatWnd, cmc: cmcPaleRose}; // cmcCoral
	logMethods["info"]      = {opm: opmConsole | opmChatWnd, cmc: cmcDimWhite};
	logMethods["console"]   = {opm: opmConsole};
	logMethods["log"]       = {opm: opmConsole};
	logMethods["debug"]     = {opm: 0};
	logMethods["trace"]     = {opm: 0, trace: true};

	function methodFactory(params) {
		var script      = params.script;
		var methodName  = params.methodName; // Function name.
		var opm         = params.opm;
		var trace       = params.trace;
		var cmc         = params.cmc || cmcPaleBlue;

		// Handle chat messages slightly differently.
		var chatWnd = false;
		if (opm & opmChatWnd) {
			chatWnd = true;
			opm &= ~opmChatWnd;
		}
		
		return function (/*messages...*/) {	
			var messages = Array.prototype.slice.call(arguments);

			if (opm > 0) {
				var szName = methodName;
				if (script) {
					szName = script.getShortName() + "|" + methodName;
				}
				var str = "[" + szName + "] " + messages.join('\t'); // \t
					
				// Include seconds.
				var now = new Date();

				var hour = now.getHours();
				if (hour < 10) {
					hour = "0" + hour;
				}
				
				var min = now.getMinutes();
				if (min < 10) {
					min = "0" + min;
				}
				
				var sec = now.getSeconds();
				if (sec < 10) {
					sec = "0" + sec;
				}
				var ms = "" + now.getMilliseconds();
				while (ms.length < 3) {
					ms = "0" + ms;
				}
				sec += "." + ms;
					
				var nowString = hour + ":" + min + ":" + sec;
				
				if (opm & opmConsole && skapi.acoChar && skapi.acoChar.szName) {
					nowString = skapi.acoChar.szName + " " + nowString;
				}
				
				skapi.OutputLine(nowString + " " + str, opm, cmc);
			}
			
			if (chatWnd == true) {
				//var str = "// " + messages.join(', ');
				var chatPrefix = script.chatPrefix || "";
				skapi.OutputLine(chatPrefix + messages.join(', '), opmChatWnd, cmc);
			}

			var stacktrace;
			if (trace == true) {
				stacktrace = core.getStackTrace();
			} else {
				var callerName = core.getMethodName(arguments.callee.caller);
				stacktrace = [callerName];
			}

			core.emitter.emit(methodName, {messages: messages, stacktrace: stacktrace, methodName: methodName, script: script});
			return this;
		};
	}
	
	for (var methodName in logMethods) {
		if (!logMethods.hasOwnProperty(methodName)) continue;
		core[methodName] = methodFactory({methodName: methodName, opm: logMethods[methodName].opm, cmc: logMethods[methodName].cmc, trace: logMethods[methodName].trace});
	}
	
	core.setLogMethod = function setLogMethod(params) {
		core[params.methodName] = methodFactory(params);
	};

	core.stringifyArguments = function stringifyArguments(args) {
		var result = [];
		var slice = Array.prototype.slice;
		for (var i = 0; i < args.length; ++i) {
			var arg = args[i];
			if (arg === undefined) {
				result[i] = 'undefined';
			} else if (arg === null) {
				result[i] = 'null';
			} else if (arg.constructor) {
				if (arg.constructor === Array) {
					if (arg.length < 3) {
						result[i] = '[' + stringifyArguments(arg) + ']';
					} else {
						result[i] = '[' + stringifyArguments(slice.call(arg, 0, 1)) + '...' + stringifyArguments(slice.call(arg, -1)) + ']';
					}
				} else if (arg.constructor === Object) {
					result[i] = '#object ' + arg.toString();
				} else if (arg.constructor === Function) {
					result[i] = '#function ' + core.getMethodName(arg);
				} else if (arg.constructor === String) {
					result[i] = '"' + arg + '"';
				} else if (arg.constructor === Number) {
					result[i] = arg;
				} else {
					result[i] = arg.toString();// '?'
				}
			} else {
				if (arg.szName) { // aco?
					result[i] = "#aco " + arg.szName;
				} else if (arg.sz) { // maploc
					result[i] = "#maploc " + arg.sz(1);	
				} else {
					result[i] = '?';
				}
			}
		}
		return result; //.join(',');
	};

	core.getMethodName = function factory() {
		return function getMethodName(method) {
			if (method.name) {
				return method.name;
			}

			if (method.toString().match(/function ([^\(]+)/)) {
				method.name = RegExp.$1;
			} else {
				method.name = "{anonymous}";
			}

			return method.name;
		};
	}();
		
	//var methodArgs = {};
	core.getMethodArgumentNames = function factory() {
		return function getMethodArgumentNames(method) {
			//if (methodArgs[method]) {
			//	return methodArgs[method];
			//}
			if (method.toString().match(/function(.*)\((.*)\)/)) {
				var args = RegExp.$2;
				//methodArgs[method] = args.replace(/^\s+|\s+$/g, "").split(/\s*,\s*/);
				var str = args.replace(/^\s+|\s+$/g, "").split(/\s*,\s*/);
				return str; //methodArgs[method];
			}
		};
	}();

	core.ignoreFunctions = {};
	core.isIgnoreFunction = function isIgnoreFunction(szMethod) {
		return core.ignoreFunctions[szMethod] || false;
	};
	
	core.addIgnoreFunction = function addIgnoreFunction(method) {
		//var szMethod = core.getMethodName(method);
		core.ignoreFunctions[ method ] = true;
		return this;
	};

	core.removeIgnoreFunction = function removeIgnoreFunction(method) {
		//var szMethod = core.getMethodName(method);
		delete core.ignoreFunctions[ method ];
		return this;
	};

	
	function makeid() {
		var text = "";
		var possible = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		for (var i = 0; i < 2; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}

	core.getStackTrace = function getStackTrace(params) {
		var caller = (!params || params.caller === undefined ? arguments.callee.caller : params.caller);
		var maxDepth = (!params || params.maxDepth === undefined ? core.defaultTraceDepth : params.maxDepth);
		var ignoreFunctions = (!params || params.ignoreFunctions === undefined ? {} : params.ignoreFunctions);
		var includedArgumentsDepth = (!params || params.includedArgumentsDepth === undefined ? core.defaultTraceArgumentsDepth : params.includedArgumentsDepth);
		var includeUUID = (!params || params.includeUUID === undefined ? true : params.includeUUID);
		
		var callstack = [];
		var szMethod;
		var args;
		do {
			if (ignoreFunctions[caller]) continue;
			if (core.isIgnoreFunction(caller)) continue; // global ignore functions.
			szMethod = core.getMethodName(caller);
			if (szMethod) {
				if (callstack.length < includedArgumentsDepth) {
					args = core.stringifyArguments(caller.arguments);
					for (var i = args.length - 1; i >= 0; i--) {
						if (args[i] == "undefined") {
							args.splice(i, 1);
						}
					}
					szMethod += "(" + args.join(', ') + ")";
				}
				if (includeUUID == true) {
					// Assign each caller's arguments its own uuid for the log, to help differentiate specific functions being called. 
					caller.arguments.__uuid = caller.arguments.__uuid || makeid(); 
					// <> brackets so they don't get confused with array[] and object{}.
					szMethod += "<" + caller.arguments.__uuid + ">"; 
				}

				//callstack.splice(0, 0, szMethod); 
				callstack.push(szMethod);
			}
			caller = caller.caller;
		} while (caller && callstack.length < maxDepth);
		return callstack;
	};

	core.logTimers = core.logTimers || {};
	core.time = function time(label) {
		label = label || 'default';
		var timers = this._script && this._script.logTimers || this.logTimers || core.logTimers;
		timers[label] = new Date().getTime();
	};
	
	core.timeEnd = function timeEnd(/*label*/) {
		var args = [].slice.call(arguments);
		var label = 'default';
		
		if (args.length > 0) {
			label = args.splice(0, 1)[0];
		}
		
		var timers = this._script && this._script.logTimers || this.logTimers || core.logTimers;
		
		if (typeof timers[label] === "undefined") {
			this.warn("No such label '" + label + "' for script.timeEnd()");
			return;
		}
		
		var elapsed = new Date().getTime() - timers[label];
		args.splice(0, 0, label + ": " + elapsed + "ms"); 

		var stacktrace = core.getStackTrace({depth: 1, includedArgumentsDepth: 0});
		core.emitter.emit("debug", {messages: args, methodName: "timeEnd", stacktrace: stacktrace});
	};

	// Flush cache timer.
	core.timer = core.timer || skapi.TimerNew();
	core.timer.tag = MAJOR;
	core.timer.cmsec = -1000 * 60;// 1 min
	var handler = {};
	handler.OnTimer = function OnTimer(t) {
		if (t.tag != core.timer.tag) return;
		methodNames = {};
		methodArgs = {};
		t.cmsec = -1000 * 60;// 1 min
	};
	skapi.AddHandler(evidOnTimer,	         handler);
		
	return core;
}));