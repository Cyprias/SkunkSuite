/********************************************************************************************\
	File:           SkunkEnum.js
	Purpose:        Names for magic numbers.
	Creator:        Cyprias
	Date:           10/28/2020
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/), SkunkTimer, AnimationFilter.
\*********************************************************************************************/

var MAJOR = "SkunkEnum-1.0";
var MINOR = 201028;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkEnum10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	core.debugging = false;
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	
	core.embeds = core.embeds || {};
	
	function embedMixins(script) {
		script.EquipMask = core.EquipMask;
		
		return script;
	}
	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};
	
	// https://github.com/ACEmulator/ACE/blob/master/Source/ACE.Entity/Enum/EquipMask.cs
	var EquipMask = core.EquipMask = {};
	EquipMask.None                  = 0x00000000;
	EquipMask.HeadWear              = 0x00000001;
	EquipMask.ChestWear             = 0x00000002;
	EquipMask.AbdomenWear           = 0x00000004;
	EquipMask.UpperArmWear          = 0x00000008;
	EquipMask.LowerArmWear          = 0x00000010;
	EquipMask.HandWear              = 0x00000020;
	EquipMask.UpperLegWear          = 0x00000040;
	EquipMask.LowerLegWear          = 0x00000080;
	EquipMask.FootWear              = 0x00000100;
	EquipMask.ChestArmor            = 0x00000200;
	EquipMask.AbdomenArmor          = 0x00000400;
	EquipMask.UpperArmArmor         = 0x00000800;
	EquipMask.LowerArmArmor         = 0x00001000;
	EquipMask.UpperLegArmor         = 0x00002000;
	EquipMask.LowerLegArmor         = 0x00004000;
	EquipMask.NeckWear              = 0x00008000;
	EquipMask.WristWearLeft         = 0x00010000;
	EquipMask.WristWearRight        = 0x00020000;
	EquipMask.FingerWearLeft        = 0x00040000;
	EquipMask.FingerWearRight       = 0x00080000;
	EquipMask.MeleeWeapon           = 0x00100000;
	EquipMask.Shield                = 0x00200000;
	EquipMask.MissileWeapon         = 0x00400000;
	EquipMask.MissileAmmo           = 0x00800000;
	EquipMask.Held                  = 0x01000000;
	EquipMask.TwoHanded             = 0x02000000;
	EquipMask.TrinketOne            = 0x04000000;
	EquipMask.Cloak                 = 0x08000000;
	EquipMask.SigilOne              = 0x10000000;
	EquipMask.SigilTwo              = 0x20000000;
	EquipMask.SigilThree            = 0x40000000;
	EquipMask.Clothing              = EquipMask.ChestWear | EquipMask.AbdomenWear | EquipMask.UpperArmWear | EquipMask.LowerArmWear | EquipMask.UpperLegWear | EquipMask.LowerLegWear;
	EquipMask.Armor                 = EquipMask.ChestArmor | EquipMask.AbdomenArmor | EquipMask.UpperArmArmor | EquipMask.LowerArmArmor | EquipMask.UpperLegArmor | EquipMask.LowerLegArmor | EquipMask.FootWear;
	EquipMask.ArmorExclusive	    = EquipMask.ChestArmor | EquipMask.AbdomenArmor | EquipMask.UpperArmArmor | EquipMask.LowerArmArmor | EquipMask.UpperLegArmor | EquipMask.LowerLegArmor;
	EquipMask.Extremity             = EquipMask.HeadWear | EquipMask.HandWear | EquipMask.FootWear;
	EquipMask.Jewelry               = EquipMask.NeckWear | EquipMask.WristWearLeft | EquipMask.WristWearRight | EquipMask.FingerWearLeft | EquipMask.FingerWearRight | EquipMask.TrinketOne | EquipMask.Cloak | EquipMask.SigilOne | EquipMask.SigilTwo | EquipMask.SigilThree;
	EquipMask.WristWear             = EquipMask.WristWearLeft | EquipMask.WristWearRight;
	EquipMask.FingerWear            = EquipMask.FingerWearLeft | EquipMask.FingerWearRight;
	EquipMask.Sigil                 = EquipMask.SigilOne | EquipMask.SigilTwo | EquipMask.SigilThree;
	EquipMask.ReadySlot             = EquipMask.Held | EquipMask.TwoHanded | EquipMask.TrinketOne | EquipMask.Cloak | EquipMask.SigilOne | EquipMask.SigilTwo;
	EquipMask.Weapon                = EquipMask.SigilTwo | EquipMask.TrinketOne | EquipMask.Held;
	EquipMask.WeaponReadySlot       = EquipMask.SigilOne | EquipMask.SigilTwo | EquipMask.TrinketOne | EquipMask.Held;
	EquipMask.Selectable            = EquipMask.MeleeWeapon | EquipMask.Shield | EquipMask.MissileWeapon | EquipMask.Held | EquipMask.TwoHanded;
	EquipMask.SelectablePlusAmmo    = EquipMask.Selectable | EquipMask.MissileAmmo;
	EquipMask.All                   = 0x7FFFFFFF;
	EquipMask.CanGoInReadySlot      = 0x7FFFFFFF;

	EquipMask.UnderwearPants        = EquipMask.UpperLegWear | EquipMask.LowerLegWear;
	EquipMask.UnderwearShirt        = EquipMask.ChestWear | EquipMask.UpperArmWear | EquipMask.LowerArmWear;
	
	
	return core;
}));