/******************************************************************************\
	File:           LibStub.js
	Purpose:        Simple library tracking script for Skunkworks.
	Author:         Cyprias
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\******************************************************************************/

/* eslint-disable */
var MAJOR = "LibStub";
var MINOR = 180423;
/* eslint-enable */

if (typeof LibStub === "undefined" || LibStub.minor < MINOR) {

	// Create the global LibStub object if it's missing.
	if (typeof LibStub === "undefined") {
		LibStub = function LibStub_() {
			return LibStub.getLibrary.apply(LibStub, arguments);
		};
		LibStub.libs = {};
		LibStub.minors = {};
	}
	
	// Assign the current version to it.
	LibStub.minor = MINOR;
	
	// Function to register a new library.
	LibStub.newLibrary = function newLibrary(major, minor, base) {
		if (typeof major !== "string") {
			throw new Error("major needs to be a string.");
		} else if (typeof minor !== "number") {
			throw new Error("minor needs to be a number.");
		}

		var oldMinor = this.minors[major];
		if (typeof oldMinor !== "undefined" && oldMinor >= minor) {
			return;
		}

		this.minors[major] = minor;
		this.libs[major] = this.libs[major] || base || {};
		return this.libs[major];
	};

	// Function to get a library.
	LibStub.getLibrary = function getLibrary(major, silent) {
		if (typeof this.libs[major] === "undefined" && !silent) {
			throw new Error("Cannot find library: " + major);
		}
		
		//return [this.libs[major], this.minors[major]];
		return this.libs[major];
	};
	
	// Function to get the older versions loaded.
	LibStub.getMinors = function getMinors(major) {
		return this.minors[major];
	};
}