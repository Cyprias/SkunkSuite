/********************************************************************************************\
	File:           SkunkProcess-1.0
	Purpose:        Functions relating to our current process.
	Creator:        Cyprias
	Date:           10/04/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkProcess-1.0";
var MINOR = 180708;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkProcess10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	var debugging = false;
	
	core.embeds = core.embeds || {};

	//var noop = function() {}; // Blank function.
	
	core.consoleLog = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine("[SkunkProcess] " + args.join('\t'), opmConsole);
	};
	
	// eslint-disable-next-line no-undefined-vars
	core.debug = function debug() {
		if (debugging != true) return;
		core.consoleLog.apply(core, arguments);
	};

	function embedMixins(script) {
		return script;
	}

	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	/*
	 * findOurProcessId: Gets our Skunkworks process ID by spawning a blank command prompt and getting its parent process id.
	 *
	 * @return {number}: Our process ID.
	 */
	function findOurProcessId() {
		//https://stackoverflow.com/a/15435873
		
		var objWshShell = new ActiveXObject("WScript.Shell");
		var locator = new ActiveXObject ("WbemScripting.SWbemLocator");  
		var service = locator.ConnectServer(".");
		
		var pid;
		
		// Spawn a empty command prompt to act as a child process.
		var objChildProcess = objWshShell.Exec("%ComSpec% /C pause");
		
		// Request a list of processes matching our child's processID.
		var colPIDs = service.ExecQuery("Select * From Win32_Process Where ProcessId=" + objChildProcess.ProcessID);
		
		// Loop through them and return the first (Should only be one).
		var e = new Enumerator(colPIDs);
		for (; !e.atEnd(); e.moveNext ()) {
			pid = e.item().ParentProcessId;
			break;
		}
		objChildProcess.Terminate();
		return pid;
	}
	
	var myPid;// = findOurProcessId();
	
	/*
	 * getMyProcessId: Returns our Skunkworks process ID.
	 *
	 * @param {string} path The path of the file.
	 * @return {number}: Our process ID.
	 */
	core.getMyProcessId = function getMyProcessId() {
		myPid = myPid || findOurProcessId();
		return myPid;
	};
	
	/*
	 * getProcess: Get the process object for a process id.
	 *
	 * @param {number} param.pid The process ID.
	 * @return {object}: Process object. (see https://msdn.microsoft.com/en-us/library/aa394372%28v=vs.85%29.aspx)
	 */
	core.getProcess = function getProcess(params) {
		params = params || {};
		params.pid = params.pid || core.getMyProcessId();

		var locator = new ActiveXObject ("WbemScripting.SWbemLocator");  
		var service = locator.ConnectServer(".");
		
		try  {
			var coll = service.ExecQuery("select * from Win32_Process Where ProcessId=" + params.pid);
			var items = new Enumerator(coll);
			while (!items.atEnd()) {
				return items.item();
			}
		} catch (e) {
			skapi.Outputsz("Error in getProcess().\n");
			skapi.Outputsz("Actual error message: " + e + "\n");
		}
	};
	
	/*
	 * getProcessTimes: Get CPU times for a process. See getProcessUsage() for usage.
	 *
	 * @param {number} param.pid The process ID.
	 * @return {object}: Contains PercentProcessorTime & TimeStamp_Sys100NS.
	 */
	core.getProcessTimes = function getProcessTimes(params) {
		params = params || {};
		params.pid = params.pid || core.getMyProcessId();
		
		var locator = new ActiveXObject ("WbemScripting.SWbemLocator");  
		var service = locator.ConnectServer(".");
		
		var coll = service.ExecQuery("Select * from Win32_PerfRawData_PerfProc_Process where IDProcess=" + params.pid);	
		
		var items = new Enumerator(coll);
		var PercentProcessorTime, TimeStamp_Sys100NS;
		var objInstance1;
		while (!items.atEnd()) {
			objInstance1 = items.item();
			PercentProcessorTime = objInstance1.PercentProcessorTime;
			TimeStamp_Sys100NS = objInstance1.TimeStamp_Sys100NS;
			break;
		}
		
		return {PercentProcessorTime: PercentProcessorTime, TimeStamp_Sys100NS: TimeStamp_Sys100NS};
	};
	
	/*
	 * getProcessUsage: Returns the CPU usage percentage from 0 to 1 of a process' times. ( getProcessTimes() )
	 *
	 * @param {object} params: Collection of arguments.
	 * @param {number} param.pid The process ID.
	 * @return {object}: Contains PercentProcessorTime & TimeStamp_Sys100NS.
	 */
	core.getProcessUsage = function getProcessUsage(params) { // N1, D1, N2, D2
		// Input values from getProcessTimes to get a single core CPU usage. 
		var N1 = params.before.PercentProcessorTime;
		var D1 = params.before.TimeStamp_Sys100NS;
		var N2 = params.after.PercentProcessorTime;
		var D2 = params.after.TimeStamp_Sys100NS;
		
		var Nd = (N2 - N1);
		var Dd = (D2 - D1);
		return (Nd / Dd);
	};
	
	/*
	 * getProcessMemory: Returns a process' memory usage in bytes.
	 *
	 * @param {number} param.pid The process ID.
	 * @return {object}: Contains PercentProcessorTime & TimeStamp_Sys100NS.
	 */
	core.getProcessMemory = function getProcessMemory(params) {
		params = params || {};
		params.pid = params.pid || core.getMyProcessId();
		
		var process = core.getProcess(params);
		return process && process.WorkingSetSize;
	};
	
	return core;
}));