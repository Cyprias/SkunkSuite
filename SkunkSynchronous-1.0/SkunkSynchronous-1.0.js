/********************************************************************************************\
	File:           SkunkSynchronous-1.0
	Purpose:        A wrapper for asynchronous functions so scripts can execute them synchronously.
	Creator:        Cyprias
	Date:           10/04/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/), SkunkTimer-1.0.
\*********************************************************************************************/

var MAJOR = "SkunkSynchronous-1.0";
var MINOR = 200312;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkSynchronous10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	var debugging = false;
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	
	core.Error = Error; // Default to built in error object. This allows other scripts to replace it if need be.
	
	core.embeds = core.embeds || {};

	var noop = function() {}; // Blank function.
	
	core.consoleLog = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine("[SkunkSynchronous] " + args.join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (debugging != true) return;
		core.consoleLog.apply(core, arguments);
	};

	function scriptSynchronous(params) {
		var script = this;
		
		params.duration = params.duration || script.synchronousDefaults.duration;
		params.wem = params.wem || script.synchronousDefaults.wem;
		params.waitRejectMethod = params.waitRejectMethod || script.synchronousDefaults.waitRejectMethod;
		params.waitEventMethod = params.waitEventMethod || script.synchronousDefaults.waitEventMethod;

		return core.synchronous.apply(core, arguments);
	}
	
	function embedMixins(script) {
		script.synchronous       = scriptSynchronous;
		script.synchronousGenerator       = core.synchronousGenerator;



		script.synchronousDefaults = {};
		script.synchronousDefaults.duration         = 100;
		script.synchronousDefaults.wem              = wemFullTimeout;

		return script;
	}

	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	/*
	if (typeof core.emitter !== "undefined") {
		core.emitter.on("onEmbedDisable", function(script) {});
	}
	*/

	function makeid() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for (var i = 0; i < 4; i++) {
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}
	
	core.inWaitEvent = false;
	core.WaitEvent = function WaitEvent(duration, wem, evid) {
		core.inWaitEvent = true;
		var result = skapi.WaitEvent(duration, wem, evid);
		core.inWaitEvent = false;
		return result;
	};

	core.setWaitEvent = function setWaitEvent(value) {
		core.WaitEvent = value;
	};
	
	function applyMethod(operation, params) {
		var elasped = new Date().getTime() - operation.attemptStartTime;
		core.debug(core.MAJOR + " <applyMethod> Calling method id: " + params.id + ", attempt #" + operation.attempt + ", elasped: " + elasped);
		var start = new Date().getTime();
		params.method.apply(params.thisArg, params.args);
		var elasped = new Date().getTime() - start;
		core.debug(core.MAJOR + " </applyMethod> elasped: " + elasped);
	}
	
	function callRetryMethod(method, thisArg, operation) {
		core.debug(core.MAJOR + " <callRetryMethod>");
		method.call(thisArg, operation); // 
		core.debug(core.MAJOR + " </callRetryMethod>");
	}
	
	function Operation() {}
	Operation.prototype.toJSON = function () {
		var self = this;
		var copy = {};
		for (var key in self) {
			if (!self.hasOwnProperty(key)) continue;
			copy[key] = self[key];
		}
		
		// Removing _operation from copy to prevent infinite loop.
		delete copy.syncParams.thisArg._operation;
		return copy;
	};
	
	var deprecatedWarning = false;
	/**
	* synchronous: Call a asynchronous function synchronously.
	*
	* @param {string}   params.id                   : A ID for debug logs.
	* @param {function} params.method               : Async function to be called.
	* @param {array}    params.args                 : Optional arguments to apply to the async function.
	* @param {function} params.waitRejectMethod     : Optional function to call after each skapi.WaitEvent(), if waitRejectMethod() returns true synchronous() stops executing.
	* @param {object}   params.waitRejectThisArg    : Optional 'this' object to given to params.waitRejectMethod.
	* @param {int}      params.duration             : Optional duration for skapi.WaitEvent().
	* @param {int}      params.wem                  : Optional wem for skapi.WaitEvent().
	* @param {object}   params.thisArg              : Optional 'this' object to be given to params.method.
	* @param {function} params.retryMethod          : Optional function to call after the async function calls our callback, retryMethod determins whether we should retry the async function again.
	* @param {object}   params.retryThisArg         : Optional 'this' object to be given to params.retryMethod.
	* @param {number}   params.timeout              : Optional timeout in miliseconds. Default 60 seconds.
	* @return {object}  Operation object containing the results and other info involved with this function call.
	*/
	core.synchronous = (function factory() {
		function resolve() {
			this._resolved = true;
		}
		
		function retry(value) {
			this._retry = typeof value !== "undefined" && value || true;
			this.resolve();
		}
		
		function rejected() {
			//throw new core.Error("Synchronous opperation has already rejected!");
			core.consoleLog("Synchronous opperation has already rejected!");
			if (core.trace) {
				core.trace("Synchronous opperation has already rejected!");
			}
		}
		
		return function synchronous(params) {
			arguments.__uuid = arguments.__uuid || makeid();
			params.id = arguments.__uuid;
			core.debug(core.MAJOR + " <synchronous>", params.id);
			
			if (!deprecatedWarning) {
				skapi.OutputLine("SkunkSynchronous.synchronous() is deprecated and may be removed at some point.", opmConsole, cmcRed);
				deprecatedWarning = true;
			}
			
			if (typeof params.method === "undefined") {
				var e = new core.Error("params.method is missing");
				core.debug(core.MAJOR + " Error: " + e);
				throw e;
			}

			params.args                 = params.args || []; // arguments applied to the method().
			params.waitRejectMethod     = params.waitRejectMethod || params.waitBailMethod || noop;
			params.thisArg              = params.thisArg || {};
			params.waitRejectThisArg    = params.waitRejectThisArg || params.waitBailThisArg;
			params.waitEventMethod      = params.waitEventMethod || core.WaitEvent;
			
			// WaitEvent() arguments.
			params.duration         = params.duration || 100;
			params.wem              = params.wem || wemFullTimeout; //wemSingle
			params.timeout          = params.timeout || 1000 * 10;

			var operation = new Operation();                                 // State of this sync execution. This is given to the retryMethod to determin if the script wants to retry or not.
			operation.syncParams        = params;
			operation.attempt           = 1;                    // We start at one cause it makes more sense in debugging logs.
			operation._retry            = false;                // Whether we should retry the async method. This gets set by params.retryMethod(operation).
			operation.startTime         = new Date().getTime(); // In case retryMethod wants to know this.
			operation._resolved         = false;
			operation._rejected         = false;                // Set when waitRejectMethod returns true;

			// Add operaton to the async method's this object, so it can tell if the sync operation was rejected.
			params.thisArg._operation   = params.thisArg._operation || operation;

			// 'arguments' from a function call lacks .push function.
			if (params.args && typeof params.args.length === "number" && typeof params.args.push == "undefined") {
				params.args = Array.prototype.slice.call(params.args);
			}
			
			// Add a callback to the args that we'll apply to the params.method. This assumes the async method uses callback as its last argument.
			params.args.push(function syncCallback(err, results) {
				core.debug(core.MAJOR + " <syncCallback>", params.id, err, results);
				operation.results = [].slice.call(arguments);
			});

			// Called by the script's retryMethod to stop futher retry attempts.
			operation.resolve = resolve;

			// Called by the script's retryMethod to attempt another retry of the async method.
			operation.retry = retry;

			// Our retry loop.
			var elapsed;
			do { 
				operation._retry = false; // reset
					
				if (params.timeout) {
					elapsed = new Date().getTime() - operation.attemptStartTime;
					if (elapsed > params.timeout) {
						var methodName = core.getMethodName(params.method);
						core.consoleLog(methodName + " <" + params.id + "> has timed out! (a)");
						core.debug(methodName + " <" + params.id + "> has timed out! (a)");
						return operation;
					}
				}
					
					
				// Flush the current event queue, so when we register our event handlers they don't catch pending events.
				core.debug(core.MAJOR + " Flushing event queue...");
				params.waitEventMethod();
				
				// Event queue was flushed, call their waitRejectMethod to see if execution should return to the callee function.
				core.debug(core.MAJOR + " Calling waitRejectMethod()...");
				if (params.waitRejectMethod.call(params.waitRejectThisArg)) {
					core.debug(core.MAJOR + " Sync REJECTED prior to apply.");
					operation._rejected = true;
					
					operation.retry = rejected;
					operation.resolve = rejected;
					
					return operation;
				}
								
				// Call the method in a timer so if it happens to call the callback syncly (bad) our wait loop will catch it.
				core.debug(core.MAJOR + " Queuing applyMethod()...");
				operation.attemptStartTime = new Date().getTime();
				setImmediate(applyMethod, operation, params);
				
				// Enter into loop waiting for a result in our syncCallback.
				while (typeof operation.results === "undefined") {
					// Wait a little bit of time.
					params.waitEventMethod(params.duration, params.wem);
					
					// We just did a wait event, ask calling script if we should bail so the callee function can finish executing.
					if (params.waitRejectMethod.call(params.waitRejectThisArg)) {
						core.debug(core.MAJOR + " Sync REJECTED after apply.");
						operation._rejected = true;
						return operation;
					}
					
					if (params.timeout) {
						elapsed = new Date().getTime() - operation.attemptStartTime;
						if (elapsed > params.timeout) {
							var methodName = core.getMethodName(params.method);
							core.consoleLog(methodName + " <" + params.id + "> has timed out! (b)");
							core.debug(methodName + " <" + params.id + "> has timed out! (b)");
							return operation;
						}
					}
					
					// If operation.results is defined, check if we should retry.
					if (typeof operation.results !== "undefined") {
						core.debug(core.MAJOR + " operation.results set while we waited: " + operation.results);
						if (typeof params.retryMethod === "function") {
							operation._resolved = false; // reset from last attempt's retry()

							// Call params.retryMethod() and wait for either resolve() or retry() to be called.
							core.debug(core.MAJOR + " Queuing retry method...");
							setImmediate(callRetryMethod, params.retryMethod, params.retryThisArg, operation);

							// Wait for retryHandler() to give us a operation._resolved asynchronously. This can result in a endless wait if it fails to do so.
							core.debug(core.MAJOR + " Waiting for async retryMethod to fire... _resolved: " + operation._resolved + ", _rejected: " + operation._rejected);
							while (operation._resolved != true && operation._rejected != true) {
								params.waitEventMethod(params.duration, params.wem);
								if (params.waitRejectMethod.call(params.waitRejectThisArg)) {
									core.debug(core.MAJOR + " Sync REJECTED waiting for retry.");
									operation._rejected = true;
									return operation;
								}
					
								if (params.timeout) {
									elapsed = new Date().getTime() - operation.attemptStartTime;
									if (elapsed > params.timeout) {
										core.consoleLog(methodName + " <" + params.id + "> has timed out! (c)");
										core.debug(methodName + " <" + params.id + "> has timed out! (c)");
										return operation;
									}
								}
							} 
						}
						break;
					}
				}
				
				// Increase the attempt count by one.
				operation.attempt++;
				
				// Reset the results object that we watch for after calling the async method.
				if (operation._retry == true) {
					delete operation.results;
				}

			} while (operation._retry == true);
			
			var elapsed = new Date().getTime() - operation.startTime;
			core.debug(core.MAJOR + " </synchronous>", params.id, elapsed);
			return operation;
		};
	})();

	core.getMethodName = function factory() {
		return function getMethodName(method) {
			var szMethod = "{anonymous}";// anon function
			if (method.toString().match(/function ([^\(]+)/)) {
				szMethod = RegExp.$1;
			} else if (method.name) {
				szMethod = method.name;
			}
			return szMethod;
		};
	}();

	core.synchronousGenerator = function factory() {
		return function synchronousGenerator(asyncFunction) {
			core.debug("<synchronousGenerator>");
			if (typeof asyncFunction !== "function") throw new core.Error("asyncFunction not provided");
			return function asyncWrapper(params) {
				core.debug("<asyncWrapper>");
				skapi.WaitEvent(); // flush
				var results;
				asyncFunction(params, function onResult(err, result) {
					core.debug("<asyncWrapper|onResult>", err, result);
					results = Array.prototype.slice.call(arguments);
				});
				do {
					skapi.WaitEvent(100, wemSingle);
				} while (typeof results === "undefined");
				var err = results && results.length > 0 && results[0];
				if (err) throw err;
				return results && results.length > 1 && results[1];
			};
		};
	}();
	
	return core;
}));