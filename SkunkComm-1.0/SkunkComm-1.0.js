/********************************************************************************************\
	File:           SkunkComm-1.0.js
	Purpose:        Communication handler for Skunkworks.
	Creator:        Cyprias
	Date:           09/17/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkComm-1.0";
var MINOR = 210731;

(function (factory) {
	
	var params = {};
	params.EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof params.EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
	}
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(params);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkComm10 = factory(params);
	}
}(function (params) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	var debugging = false;
	
	var _maxtextlen	= 255;	// Max length AC allows messages. Our chunks will be 254 to fit the special prefix.
	//var _maxtextlen	= 5;	// testing
	
	// ASCII characters attached to the beginning of a message to indicate what part of the message it is.
	var MSG_MULTI_FIRST = String.fromCharCode(1); // TODO: Figure out which work on the emulator server, I don't think 1-3 do.
	var MSG_MULTI_NEXT  = String.fromCharCode(2);
	var MSG_MULTI_LAST  = String.fromCharCode(3); 
	
	core.embeds = core.embeds || {};

	var EventEmitter = params.EventEmitter;
	core.emitter = core.emitter || new EventEmitter();

	var noop = function() {}; // Blank function.
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	
	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(args.join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (debugging != true) return;
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(args.join('\t'), opmConsole);
	};
	
	function scriptTell() {
		return core.tell.apply(core, arguments);
	}
	
	function scriptChat() {
		return core.chat.apply(core, arguments);
	}
	
	var handler = {};
	handler.OnTell = function OnTell(acoSender, acoReceiver, szMsg) {
		core.debug(core.MAJOR, "<OnTell>", Array.prototype.slice.call(arguments).join(", "));
		
		szMsg = prepIncomingSpaces(szMsg);
		if (szMsg.match(/^([\001|\002|\003|\004|\005])(.*)/)) {// 4 and 5 aren't used yet.
			var control = RegExp.$1;
			var rest = RegExp.$2;
			switch (control) {
			case MSG_MULTI_FIRST:
				onReceiveMultipartFirst({
					evid       : evidOnTell,
					acoSender  : acoSender,
					acoReceiver: acoReceiver,
					szMsg      : rest
				});
				break;
			case MSG_MULTI_NEXT:
				onReceiveMultipartNext({
					evid       : evidOnTell,
					acoSender  : acoSender,
					acoReceiver: acoReceiver,
					szMsg      : rest
				});
				break;
			case MSG_MULTI_LAST:
				onReceiveMultipartLast({
					evid       : evidOnTell,
					acoSender  : acoSender,
					acoReceiver: acoReceiver,
					szMsg      : rest
				});
				break;
			}
		} else {
			processReceivedMessage({
				evid       : evidOnTell,
				acoSender  : acoSender,
				acoReceiver: acoReceiver,
				szMsg      : szMsg
			});
		}
	};
	skapi.AddHandler(evidOnTell,	handler);
	
	handler.OnTellFellowship = function OnTellFellowship(szSender, szMsg) {
		core.debug(core.MAJOR, "<OnTellFellowship>", Array.prototype.slice.call(arguments).join(", "));
		
		if (szSender == "") {
			szSender = skapi.acoChar.szName;
		}
		
		szMsg = prepIncomingSpaces(szMsg);
		if (szMsg.match(/^([\001|\002|\003|\004|\005])(.*)/)) {// 4 and 5 aren't used yet.
			var control = RegExp.$1;
			var rest = RegExp.$2;
			switch (control) {
			case MSG_MULTI_FIRST:
				onReceiveMultipartFirst({
					evid    : evidOnTellFellowship,
					szSender: szSender,
					szMsg   : rest
				});
				break;
			case MSG_MULTI_NEXT:
				onReceiveMultipartNext({
					evid    : evidOnTellFellowship,
					szSender: szSender,
					szMsg   : rest
				});
				break;
			case MSG_MULTI_LAST:
				onReceiveMultipartLast({
					evid    : evidOnTellFellowship,
					szSender: szSender,
					szMsg   : rest
				});
				break;
			}
		} else {
			processReceivedMessage({
				evid    : evidOnTellFellowship,
				szSender: szSender,
				szMsg   : szMsg
			});
		}
	};
	skapi.AddHandler(evidOnTellFellowship,	handler);
	
	handler.OnTellAllegiance = function OnTellAllegiance(szSender, szMsg) {
		core.debug(core.MAJOR, "<OnTellAllegiance>", Array.prototype.slice.call(arguments).join(", "));
		
		szMsg = prepIncomingSpaces(szMsg);
		if (szMsg.match(/^([\001|\002|\003|\004|\005])(.*)/)) {// 4 and 5 aren't used yet.
			var control = RegExp.$1;
			var rest = RegExp.$2;
			switch (control) {
			case MSG_MULTI_FIRST:
				onReceiveMultipartFirst({
					evid    : evidOnTellAllegiance,
					szSender: szSender,
					szMsg   : rest
				});
				break;
			case MSG_MULTI_NEXT:
				onReceiveMultipartNext({
					evid    : evidOnTellAllegiance,
					szSender: szSender,
					szMsg   : rest
				});
				break;
			case MSG_MULTI_LAST:
				onReceiveMultipartLast({
					evid    : evidOnTellAllegiance,
					szSender: szSender,
					szMsg   : rest
				});
				break;
			}
		} else {
			processReceivedMessage({
				evid    : evidOnTellAllegiance,
				szSender: szSender,
				szMsg   : szMsg
			});
		}
	};
	
	handler.OnTellChannel = function OnTellChannel(szChannel, szSender, szMsg) {
		core.debug(core.MAJOR, "<OnTellChannel>", Array.prototype.slice.call(arguments).join(", "));
		
		processReceivedMessage({
			szChannel: szChannel,
			szSender : szSender,
			szMsg    : szMsg
		});
	};

	core.escapeRegExp = function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	};
	
	// PhatAC not firing evidOnTellFellowship upon other player chat? Using OnChatBoxMessage instead.
	handler.OnChatBoxMessage = function OnChatBoxMessage(szRawMsg, cmc) {
		core.debug(core.MAJOR, "<OnChatBoxMessage>", Array.prototype.slice.call(arguments).join(", "));
		
		if (szRawMsg.match(/^\[Allegiance\] <Tell:IIDString:(\d*):(.*)>(.*)\<\\Tell> says, "(.*)"/i)) {
			var szSender = RegExp.$2;
			var szMsg = RegExp.$4;
			handler.OnTellAllegiance(szSender, szMsg);
		} else if (szRawMsg.match(/^You say to your fellowship, "(.*)"/)) {
			var szMsg = RegExp.$1;
			this.OnTellFellowship(skapi.acoChar.szName, szMsg);
		//} else if (cmc == 19 && szRawMsg.match(/^\[Fellowship\] You say, "(.*)"/i)) { // Emulator fellowship message: [Fellowship] You say, "xxx"
		//	var szMsg = RegExp.$1;
		//	this.OnTellFellowship(skapi.acoChar.szName, szMsg);
		} else if (szRawMsg.match(/^(.*) says to your fellowship, "(.*)"/)) { // PhatAC not firing evidOnTellFellowship upon other player chat?
			var szWho = RegExp.$1;
			var szMsg = RegExp.$2;
			this.OnTellFellowship(szWho, szMsg);
		} else if (szRawMsg.match(/^\[(.*)\] <Tell:IIDString:(\d*):(.*)>(.*)\<\\Tell> says, "(.*)"/i)) { // Global, Trade, LFG, ect.
			var szChannel = RegExp.$1;
			var szSender = RegExp.$3;
			var szMsg = RegExp.$5;
			this.OnTellChannel(szChannel, szSender, szMsg);
		} else if (szRawMsg.match(/^<Tell:IIDString:(\d*):(.*)>(.*)\<\\Tell> says, "(.*)"/i)) { // local chat with clickable name, don't do anything.
		} else if (szRawMsg.match(/^(.*) says, "(.*)"/i)) {// Emu fellowship chat without clickable name.
			var szWho = RegExp.$1;
			var szMsg = RegExp.$2;
			this.OnTellFellowship(szWho, szMsg);
		}
	};
	skapi.AddHandler(evidOnChatBoxMessage,	handler);
	
	handler.OnChatServer = function OnChatServer(szRawMsg, cmc) {
		core.debug(core.MAJOR, "<OnChatServer>", Array.prototype.slice.call(arguments).join(", "));
		
		if (cmc == 3 && szRawMsg.match(/^You think, "(.*)"/i)) { // Treat as a tell from ourselves. 
			var szMsg = RegExp.$1;
			this.OnTell(skapi.acoChar, skapi.acoChar, szMsg);
		}
	};
	skapi.AddHandler(evidOnChatServer,	handler);
	
	handler.OnChatLocal = function OnChatLocal(acoSender, szMsg) {
		core.debug(core.MAJOR, "<OnChatLocal>", Array.prototype.slice.call(arguments).join(", "));

		szMsg = prepIncomingSpaces(szMsg);
		if (szMsg.match(/^([\001|\002|\003|\004|\005])(.*)/)) {// 4 and 5 aren't used yet.
			var control = RegExp.$1;
			var rest = RegExp.$2;
			switch (control) {
			case MSG_MULTI_FIRST:
				onReceiveMultipartFirst({
					evid     : evidOnChatLocal,
					acoSender: acoSender,
					szMsg    : rest
				});
				break;
			case MSG_MULTI_NEXT:
				onReceiveMultipartNext({
					evid     : evidOnChatLocal,
					acoSender: acoSender,
					szMsg    : rest
				});
				break;
			case MSG_MULTI_LAST:
				onReceiveMultipartLast({
					evid     : evidOnChatLocal,
					acoSender: acoSender,
					szMsg    : rest
				});
				break;
			}
		} else {
			processReceivedMessage({
				evid     : evidOnChatLocal,
				acoSender: acoSender,
				szMsg    : szMsg
			});
		}
	};
	skapi.AddHandler(evidOnChatLocal,	handler);
	
	handler.OnChatEmoteCustom = function OnChatEmoteCustom(acoSender, szMsg) {
		core.debug(core.MAJOR, "<OnChatEmoteCustom>", Array.prototype.slice.call(arguments).join(", "));
		
		szMsg = prepIncomingSpaces(szMsg);
		if (szMsg.match(/^([\001|\002|\003|\004|\005])(.*)/)) {// 4 and 5 aren't used yet.
			var control = RegExp.$1;
			var rest = RegExp.$2;
			switch (control) {
			case MSG_MULTI_FIRST:
				onReceiveMultipartFirst({
					evid     : evidOnChatEmoteCustom,
					acoSender: acoSender,
					szMsg    : rest
				});
				break;
			case MSG_MULTI_NEXT:
				onReceiveMultipartNext({
					evid     : evidOnChatEmoteCustom,
					acoSender: acoSender,
					szMsg    : rest
				});
				break;
			case MSG_MULTI_LAST:
				onReceiveMultipartLast({
					evid     : evidOnChatEmoteCustom,
					acoSender: acoSender,
					szMsg    : rest
				});
				break;
			}
		} else {
			processReceivedMessage({
				evid     : evidOnChatEmoteCustom,
				acoSender: acoSender,
				szMsg    : szMsg
			});
		}
	};
	skapi.AddHandler(evidOnChatEmoteCustom,	handler);

	function embedMixins(script) {
		script.tell = scriptTell;
		script.chat = scriptChat;
		
		// legacy script embeds.
		script.registerCommEvents = noop;
		script.unregisterCommEvents = noop;

		return script;
	}

	core.embed = function embed(script) {
		var name = script.getName();
		this.embeds[name ] = script;
		embedMixins(script);
		return script;
	};

	var msgSpool = {};

	/**
	 * onReceiveMultipartFirst: Process the first chunk of a multipart message.
	 */
	function onReceiveMultipartFirst(params) {
		var evid = params.evid;
		var acoSender = params.acoSender;
		var szSender = params.szSender || acoSender && acoSender.szName;
		var key = evid + "\t" + szSender;
		msgSpool[key] = [params.szMsg];
	}

	/**
	 * onReceiveMultipartNext: Process a middle chunk of a multipart message.
	 */
	function onReceiveMultipartNext(params) { // script, evid, acoSender, acoReceiver, szMsg
		var evid = params.evid;
		var acoSender = params.acoSender;
		var szSender = params.szSender || acoSender && acoSender.szName;
		var key = evid + "\t" + szSender;
		if (!msgSpool[key]) {
			return;
		}
		msgSpool[key].push(params.szMsg);
	}
	
	/**
	 * onReceiveMultipartLast: Process the last chunk of a multipart message.
	 */
	function onReceiveMultipartLast(params) {
		var evid = params.evid;
		var acoSender = params.acoSender;
		var szSender = params.szSender || acoSender && acoSender.szName;
		var acoReceiver = params.acoReceiver || skapi.acoChar;
		var szReceiver = params.szReceiver || acoReceiver && acoReceiver.szName;
		var szMsg = params.szMsg;
		
		core.debug(core.MAJOR, "<onReceiveMultipartLast> szSender: " + szSender + ", szMsg: " + szMsg.length);
		var key = evid + "\t" + szSender;
		
		if (!msgSpool[key]) {
			return;
		}
		
		msgSpool[key].push(szMsg);
		
		var szFullMsg = msgSpool[key][0];
		
		// eslint-disable-next-line no-restricted-syntax
		for (var i = 1; i < msgSpool[key].length; i++) {
			szFullMsg += msgSpool[key][i];
		}
		delete msgSpool[key];
		
		processReceivedMessage({
			evid       : evid,
			acoSender  : acoSender,
			szSender   : szSender,
			szMsg      : szFullMsg,
			acoReceiver: acoReceiver,
			szReceiver : szReceiver
		});
	}
	
	/**
	 * prepOutgoingSpaces: Prepare a message for going out. In this case, we replace the beginning and ending space with a special character so the server doesn't trim it.
	 */
	function prepOutgoingSpaces(szString) {
		// Replace the beginning space.
		if (szString.substr(0, 1) == " ") {
			szString = szString.substr(1);
			szString = _fakeSpace + szString;
		}
		
		// Replace the ending space.
		if (szString.substr(szString.length - 1) == " ") {
			szString = szString.substr(0, szString.length - 1);
			szString = szString + _fakeSpace;
		}
		
		return szString;
	}
	
	/**
	 * prepIncomingSpaces: Replace our special space with a normal space.
	 */
	var _fakeSpace = String.fromCharCode(177);	// I replace spaces with \177, it looks like a normal space in game but doesn't get trimmed by the server. 
	function prepIncomingSpaces(szString) {
		var itemExp = new RegExp(_fakeSpace, "g");
		szString = szString.replace(itemExp, ' ');	//177	DualWield	/\177/g
		return szString;
	}
	
	function singleTell(params, callback) {// szRecipient, szMsg
		var szRecipient = params.szRecipient;
		var szMsg = params.szMsg;
		var thisArg = params.thisArg;

		var szFormatted = prepOutgoingSpaces(szMsg);
		callback = callback || noop;
	
		var handler = {};
		handler.OnChatServer = function OnChatServer(szRawMsg) {
			if (szRawMsg.match(/You tell (.*), "(.*)"/)) {
				var szWho = RegExp.$1;
				var szMsg = RegExp.$2;

				if (szFormatted.indexOf(szMsg) >= 0) {
					resolve(undefined, true);
				} else {
					var err = new core.Error("CORRUPTION");
					err.szFormatted = szFormatted;
					err.szRawMsg = szRawMsg;
					err.szWho = szWho;
					err.szMsg = szMsg;
					err.event = "OnChatServer";
					resolve(err);
				}
			} else if (szRecipient == skapi.acoChar.szName && szRawMsg.match(/^You think, "(.*)"/i)) {
				var szMsg = RegExp.$1;
				
				if (szFormatted.indexOf(szMsg) >= 0) {
					resolve(undefined, true);
				} else {
					var err = new core.Error("CORRUPTION");
					err.szFormatted = szFormatted;
					err.szRawMsg = szRawMsg;
					err.szWho = szRecipient;
					err.szMsg = szMsg;
					err.event = "OnChatServer";
					resolve(err);
				}
				
			}
			
		};
		skapi.AddHandler(evidOnChatServer,          handler);
		
		handler.OnTell = function OnTell(acoSender, acoReceiver, szMsg) {
			//core.console("<OnTell> " + (szMsg.length == szFormatted.length));
			//core.console(" szMsg: " + szMsg + " (" + szMsg.length + ")");
			//core.console(" szFormatted: " + szFormatted + " (" + szFormatted.length + ")");
			
			if (acoSender.szName.match(skapi.szName)) {
				if (szFormatted.indexOf(szMsg) >= 0) {
					resolve(undefined, true);
				} else {
					var err = new core.Error("CORRUPTION");
					err.szFormatted = szFormatted;
					err.szMsg = szMsg;
					err.event = "OnTell";
					resolve(err);
				}
			}
		};
		skapi.AddHandler(evidOnTell,          handler);
		
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg/*, cmc*/) {
			if (szMsg.match(/^You must wait (\d*)s before communicating again!/)) {
				resolve(new core.Error("GAGGED"));
			}
		};
		skapi.AddHandler(evidOnChatBoxMessage,          handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/That person is not available now./i)) {
				resolve(new core.Error("PERSON_NOT_AVAILABLE"));
			}
		};
		skapi.AddHandler(evidOnTipMessage,          handler);

		var tid;
		if (typeof setTimeout === "function" && !core.inJest()) {
			tid = setTimeout(core.timedOut, 2000, resolve);
		}
		
		function resolve() {
			skapi.RemoveHandler(evidNil, handler);
			if (typeof clearTimeout === "function") {
				clearTimeout(tid);
			}
			callback.apply(thisArg, arguments);
		}

		skapi.Tell(szRecipient, szFormatted);
	}

	function singleChat(params, callback) { // chrm, szMsg
		var chrm = params.chrm;
		var szMsg = params.szMsg;
		var thisArg = params.thisArg;

		var szFormatted = prepOutgoingSpaces(szMsg);
		callback = callback || noop;
			
		var handler = {};
		handler.OnTellFellowship = function OnTellFellowship(szSender, szMsg) {
			if (szSender == "") {// blank for us.

				//if (szMsg.match(szFormatted))
				if (szFormatted.indexOf(szMsg) >= 0) {
				//	core.console("matches indexOf");
					resolve(undefined, true);
				} else {
				//	core.console("doesn't match indexOf");
					var err = new core.Error("CORRUPTION");
					err.szFormatted = szFormatted;
					err.szMsg = szMsg;
					err.event = "OnTellFellowship";
					resolve(err);
				}
			}
		};
		if (chrm == chrmFellowship) {
		//	core.console("Adding OnTellFellowship callback...");
			skapi.AddHandler(evidOnTellFellowship,	handler);
		}
		
		handler.OnTellAllegiance = function OnTellAllegiance(szSender, szMsg) {
			if (szSender.match(skapi.szName)) {
				if (szFormatted.indexOf(szMsg) >= 0) {
					resolve(undefined, true);
				} else {
					var err = new core.Error("CORRUPTION");
					err.szFormatted = szFormatted;
					err.szMsg = szMsg;
					err.event = "OnTellAllegiance";
					resolve(err);
				}
			}
		};
		if (chrm == chrmAllegiance) {
			skapi.AddHandler(evidOnTellAllegiance,	handler);
		}

		handler.OnChatLocal = function OnChatLocal(acoSender, szMsg) {
			if (acoSender.szName.match(skapi.szName)) {
				if (szFormatted.indexOf(szMsg) >= 0) {
					resolve(undefined, true);
				} else {
					var err = new core.Error("CORRUPTION");
					err.szFormatted = szFormatted;
					err.szMsg = szMsg;
					err.event = "OnChatLocal";
					resolve(err);
				}
			}
		};
		if (chrm == chrmLocal) {
			skapi.AddHandler(evidOnChatLocal,	handler);
		}

		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			//core.console("<OnChatBoxMessage> " + szMsg + ", cmc: " + cmc);
			if (szMsg.match(/^You must wait (\d*)s before communicating again!/)) {
				resolve(new core.Error("GAGGED"));
			} else if (szMsg.match(/^\[Allegiance\] <Tell:IIDString:(\d*):(.*)>(.*)\<\\Tell> says, "(.*)"/i)) {
				var szSender = RegExp.$2;
				var szMsg = RegExp.$3;
				handler.OnTellAllegiance(szSender, szMsg);
			} else if (szMsg.match(/^You say to your fellowship, "(.*)"/i)) {
				var szMsg = RegExp.$1;
				handler.OnTellFellowship("", szMsg);
			} else if (cmc == 19 && szMsg.match(/^\[Fellowship\] You say, "(.*)"/i)) { // Emulator fellowship message: [Fellowship] You say, "xxx"
				var szMsg = RegExp.$1;
				handler.OnTellFellowship("", szMsg);
			} else if (szMsg.match(/^You do not have the authority within your allegiance to do that./)) {
				resolve(new core.Error("NO_PERMISSION"));
			}
		};
		skapi.AddHandler(evidOnChatBoxMessage,	handler);
		
		var tid;
		if (typeof setTimeout === "function" && !core.inJest()) {
			tid = setTimeout(core.timedOut, 2000, resolve);
		}
		
		function resolve() {
			skapi.RemoveHandler(evidNil, handler);
			if (typeof clearTimeout === "function") {
				clearTimeout(tid);
			}
			callback.apply(thisArg, arguments);
		}

		core.debug("skapi.Chat(" + chrm + ", " + szFormatted + ");");
		skapi.Chat(chrm, szFormatted);
	}
	
	/**
	 * splitMessage: Split a message into chunks, return a array of the chunks. Does not include the splitter prefixes.
	 */
	function splitMessage(szMsg) {
		var chunks = [];
		var textlen = szMsg.length;
		var maxtextlen = _maxtextlen - 1;
		var chunk = szMsg.substr(0, maxtextlen);//strsub(text, 1, maxtextlen)
		chunks.push(chunk);
		
		var pos = maxtextlen;
		while (pos + maxtextlen <= textlen) {
			chunk = szMsg.substr(pos, maxtextlen);
			pos = pos + maxtextlen;
			chunks.push(chunk);
		}
		
		chunk = szMsg.substr(pos);
		chunks.push(chunk);
		return chunks;
	}

	function doLimit(fn, limit) {
		return function (iterable, iteratee, callback) {
			return fn(iterable, limit, iteratee, callback);
		};
	}

	function _withoutIndex(iteratee) {
		return function (value, index, callback) {
			return iteratee(value, callback);
		};
	}
	
	function once(fn) {
		return function () {
			if (fn === null) return;
			var callFn = fn;
			fn = null;
			callFn.apply(this, arguments);
		};
	}
	
	function createArrayIterator(coll) {
		var i = -1;
		var len = coll.length;
		return function next() {
			return ++i < len ? {value: coll[i], key: i} : null;
		};
	}

	function onlyOnce(fn) {
		return function() {
			if (fn === null) throw new core.Error("Callback was already called.");
			var callFn = fn;
			fn = null;
			callFn.apply(this, arguments);
		};
	}
	
	var breakLoop = {};
	
	function _eachOfLimit(limit) {
		return function (obj, iteratee, callback) {
			callback = once(callback || noop);
			if (limit <= 0 || !obj) {
				return callback(null);
			}
			var nextElem = createArrayIterator(obj);
			var done = false;
			var running = 0;

			function iterateeCallback(err, value) {
				running -= 1;
				if (err) {
					done = true;
					return callback(err);
				} else if (value === breakLoop || (done && running <= 0)) {
					done = true;
					return callback(null);
				} else {
					replenish();
				}
			}

			function replenish () {
				while (running < limit && !done) {
					var elem = nextElem();
					if (elem === null) {
						done = true;
						if (running <= 0) {
							return callback(null);
						}
						return;
					}
					running += 1;
					iteratee(elem.value, elem.key, onlyOnce(iterateeCallback));
				}
			}

			replenish();
		};
	}
	
	function eachLimit$1(coll, limit, iteratee, callback) {
		_eachOfLimit(limit)(coll, _withoutIndex(iteratee), callback);
	}

	var eachSeries = doLimit(eachLimit$1, 1);
	
	/*
	 * tell() sends a tell message and calls a callback when done.
	 *
	 * @param {Object} params - Function parameters
	 * @param {string} params.szRecipient: Player name.
	 * @param {string} params.szMsg: Message
	 * @param {function} callback
	 * @return {undefined}
	 */
	core.tell = function tell(params, callback) {
		// Traditional skapi.Tell(szRecipient, szMsg) support.
		var szRecipient, szMsg;
		if (arguments.length >= 1 && typeof arguments[0] == "string") szRecipient = arguments[0];
		if (arguments.length >= 2 && typeof arguments[1] == "string") szMsg = arguments[1];
		if (typeof params !== "object") {
			params = {
				szRecipient: szRecipient,
				szMsg      : szMsg
			};
		}

		if (typeof arguments[arguments.length - 1] === "function") {
			callback = arguments[arguments.length - 1];
		}

		/*
		// Check if there's a callback, if not try to promisify (requires es6-shim.js).
		if (typeof callback !== "function" && typeof Promise !== "undefined") {
			return new Promise(function(resolve, reject) {
				core.tell(params, function promise_callback(err, results) {
					if (err) return reject(err);
					resolve(results);
				});
			});
		}
*/
		//callback = callback;// || noop;

		function resolve() {
			if (typeof callback === "function") {
				callback.apply(params.thisArg, arguments);
			}
		}

		// szMsg array support.
		if (isArray(params.szMsg)) {
			return eachSeries(params.szMsg, function iteratee(szMsg, cb) {
				tell({
					szRecipient: params.szRecipient, 
					szMsg      : szMsg
				}, cb);
			}, resolve);
		}
		
		if (typeof params.szRecipient !== "string") {
			var err = new core.Error("szRecipient not a string.");
			if (typeof callback === "function") {
				return callback(err);
			} else {
				throw err;
			}
		} else if (typeof params.szMsg !== "string") {
			var err = new core.Error("szMsg not a string.");
			if (typeof callback === "function") {
				return callback(err);
			} else {
				throw err;
			}
		}
		
		params.callback = params.callback || callback;
		params.thisArg = params.thisArg || params.env;
		
		if (params.szMsg.length <= _maxtextlen) {
			singleTell(params, params.callback);
			return;
		}

		// Split the message into chunks.
		var chunks = splitMessage(params.szMsg);

		// Add prefixes.
		chunks[0] = MSG_MULTI_FIRST + chunks[0];
		
		// eslint-disable-next-line no-restricted-syntax
		for (var c = 1; c < chunks.length - 1; c++) {
			chunks[c] = MSG_MULTI_NEXT + chunks[c];
		}
		chunks[chunks.length - 1] = MSG_MULTI_LAST + chunks[chunks.length - 1];

		// Start sending chunks...
		eachSeries(chunks, function iteratee(szChunk, cb) {
			singleTell({szRecipient: params.szRecipient, szMsg: szChunk, thisArg: params.thisArg}, cb);
		}, resolve);
	};
	
	core.chat = function chat(params, callback) {
		// Traditional skapi.Chat(chrm, szMsg) support.
		var chrm, szMsg;
		if (arguments.length >= 1 && typeof arguments[0] == "number") chrm = arguments[0];
		if (arguments.length >= 2 && typeof arguments[1] == "string") szMsg = arguments[1];
		if (typeof params !== "object") {
			params = {
				chrm : chrm,
				szMsg: szMsg
			};
		}
		
		if (typeof arguments[arguments.length - 1] === "function") {
			callback = arguments[arguments.length - 1];
		}

		function resolve() {
			if (typeof callback === "function") {
				callback.apply(params.thisArg, arguments);
			}
		}

		// szMsg array support.
		if (isArray(params.szMsg)) {
			return eachSeries(params.szMsg, function iteratee(szMsg, cb) {
				chat({
					chrm : params.chrm, 
					szMsg: szMsg
				}, cb);
			}, resolve);
		}
		
		if (typeof params.chrm !== "number") {
			var err = new core.Error("chrm not a number.");
			if (typeof callback === "function") {
				return callback(err);
			} else {
				throw err;
			}
		} else if (typeof params.szMsg !== "string") {
			var err = new core.Error("szMsg not a string.");
			if (typeof callback === "function") {
				return callback(err);
			} else {
				throw err;
			}
		}
		
		params.callback = params.callback || callback;
		params.thisArg = params.thisArg || params.env;
		
		if (params.szMsg.length <= _maxtextlen) {
			singleChat(params, params.callback);
			return;
		}
		
		// Split the message into chunks.
		var chunks = splitMessage(params.szMsg);

		// Add prefixes.
		chunks[0] = MSG_MULTI_FIRST + chunks[0];

		// eslint-disable-next-line no-restricted-syntax
		for (var c = 1; c < chunks.length - 1; c++) {
			chunks[c] = MSG_MULTI_NEXT + chunks[c];
		}
		chunks[chunks.length - 1] = MSG_MULTI_LAST + chunks[chunks.length - 1];

		// Start sending chunks...
		eachSeries(chunks, function iteratee(szChunk, cb) {
			core.debug("Sending chunk " + szChunk + "...");
			singleChat({chrm: params.chrm, szMsg: szChunk}, cb);
		}, resolve);
	};
	
	var middlewares = [];
	core.use = function use(fn) {
		if (fn instanceof Array) {
			// eslint-disable-next-line no-cond-assign, no-restricted-syntax
			for (var i = 0, f; f = fn[i++];) {
				this.use(f);
			}
			return this;
		}

		middlewares.push(fn);
		return this;
	};

	var run = (function factory() {
		var slice = Array.prototype.slice;
		function fail(err) {
			throw err;
		}
		return function run() {
			var self = this;
			var i = 0;
			var last = arguments[arguments.length - 1];
			var done = 'function' == typeof last && last;
			var args = done
				? slice.call(arguments, 0, arguments.length - 1)
				: slice.call(arguments);

			// next step
			function next(err) {
				if (err) return (done || fail)(err);
				var fn = middlewares[i++];
				var arr = slice.call(args);

				if (!fn) {
					return done && done.apply(null, [null].concat(args));
				}

				arr.push(next);
				fn.apply(self, arr);
			}

			next();

			return this;
		};
	})();

	var callMiddleware = (function factory() {
		
		function middleware_tell(params, callback) {
			var szSender = this._szSender;
			return core.tell({
				szRecipient: params.szRecipient || szSender,
				szMsg      : params.szMsg
			}, callback);
		}
		
		function middleware_chat(params, callback) {
			var chrm = params.chrm || this._chrm;
			return core.chat({
				chrm : chrm,
				szMsg: params.szMsg
			}, callback);
		}
		
		function middleware_reply(params, callback) {
			var evid = this._evid;
			if (evid == evidOnTell) {
				return this.tell({
					szMsg: params.szMsg
				}, callback);
			} else {
				return this.chat({
					szMsg: params.szMsg
				}, callback);
			}
		}
		
		return function callMiddleware(params) {
			//core.console("<callMiddleware>");
			var req         = {};
			var evid        = req.evid = params.evid;
			req.acoSender = params.acoSender;
			var szSender    = req.szSender = params.szSender;
			req.szMsg = params.szMsg;
			req.szChannel = params.szChannel;
			
			var res         = {};
			res.tell        = middleware_tell;
			res.chat        = middleware_chat;
			res.reply       = middleware_reply;
			res._szSender   = szSender;
			res._evid       = evid;
			
			if (evid == evidOnChatLocal) {
				res._chrm = chrmLocal;
			} else if (evid == evidOnChatEmoteCustom) {
				res._chrm = chrmEmote;
			} else if (evid == evidOnTellFellowship) {
				res._chrm = chrmFellowship;
			} else if (evid == evidOnTellAllegiance) {
				res._chrm = chrmAllegiance;
			}
			
			run(req, res);
		};
	})();

	function processReceivedMessage(params) {
		var evid = params.evid;
		var acoSender = params.acoSender;
		var szSender = params.szSender = params.szSender || acoSender && acoSender.szName;
		var szMsg = params.szMsg;
		var acoReceiver = params.acoReceiver;
		params.szReceiver = params.szReceiver || acoReceiver && acoReceiver.szName;
		var szChannel = params.szChannel;
		core.debug(core.MAJOR, "<processReceivedMessage>", szMsg);
		
		// Fire emitter. Scripts can listen via SkunkComm.emitter.on("onMessage", function(payload) {});
		core.emitter.emit("onMessage", params);
		
		// Call embeded script.onMSG() functions.
		var script;
		for (var scriptName in core.embeds)  {
			if (!Object.prototype.hasOwnProperty.call(core.embeds, scriptName)) continue;
			
			script = core.embeds[scriptName];
			if (!script.isEnabled()) continue;
			
			if (evid == evidOnTell && typeof script.OnTell === "function") {
				script.OnTell(acoSender, acoReceiver, szMsg);
			} else if (evid == evidOnTellFellowship && typeof script.OnTellFellowship === "function") {
				script.OnTellFellowship(szSender, szMsg);
			} else if (evid == evidOnChatEmoteCustom && typeof script.OnChatEmoteCustom === "function") {
				script.OnTellFellowship(acoSender, szMsg);
			} else if (evid == evidOnTellAllegiance && typeof script.OnTellAllegiance === "function") {
				script.OnTellAllegiance(szSender, szMsg);
			} else if (evid == evidOnChatLocal && typeof script.OnChatLocal === "function") {
				script.OnChatLocal(acoSender, szMsg);
			} else if (typeof szChannel !== "undefined" && typeof script.OnTellChannel === "function") {
				script.OnTellChannel(szChannel, szSender, szMsg);
			}
		}

		// Fire middleware. SkunkComm.use(function(req, res, next));
		callMiddleware({
			evid     : evid,
			acoSender: acoSender,
			szSender : szSender,
			szMsg    : szMsg,
			szChannel: szChannel
		});
	}
	
	var isArray = core.isArray = function isArray(arr) {
		return Object.prototype.toString.call(arr) == '[object Array]';
	};
	
	core.timedOut = function timedOut(callback) {
		callback(new core.Error("TIMED_OUT"));
	};
	
	core.inJest = function inJest() {
		return typeof process === "object" && process.env && typeof process.env.JEST_WORKER_ID !== "undefined";
	};
	
	return core;
}));