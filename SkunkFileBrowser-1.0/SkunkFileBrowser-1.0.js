/********************************************************************************************\
	File:           SkunkFileBrowser-1.0.js
	Purpose:        File browser GUI for Skunkworks.
	Creator:        Cyprias
	Date:           06/08/2018
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkFileBrowser-1.0";
var MINOR = 190729;
	
(function (factory) {
	if (typeof module === 'object' && module.exports) {
		// Script was loaded via require(), return as module exports.
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkFileBrowser10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	var debugging = false;
	
	var _dir = typeof __dirname !== "undefined" && __dirname + "\\" || ".\\";

	core.Error = Error;
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;

	var iconFolder      = 0x60012D5;
	var iconFile        = 0x6001310;
	
	core.debug = function debug() {
		if (debugging != true) return;
		skapi.OutputLine(Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};

	core.info = function info() {
		skapi.OutputLine(Array.prototype.slice.call(arguments).join('\t'), opmChatWnd);
	};
	
	core._designUI = function _designUI() {
		var SkunkSchema             = LibStub("SkunkSchema-1.0");
		
		var view = new SkunkSchema.View({title: "File Browser", width: 300, height: 90 + (20 * 10), icon: 0});
		var fixedLayout = core.controls.fixedLayout = new SkunkSchema.FixedLayout({parent:view});
		var fullPath = core.getFullPath({path: _dir});
		core.debug(core.MAJOR, "fullPath: " + fullPath);
		
		var edPath = new SkunkSchema.Edit({parent: fixedLayout, name: "edPath", text: fullPath})
			.setWidth(fixedLayout.getWidth() - 60);
			

		var btnUp = new SkunkSchema.PushButton({parent: fixedLayout, name: "btnUp", text: "Up"})
			.setAnchor(edPath, "TOPRIGHT", "TOPLEFT")
			.setWidth(20);

		// eslint-disable-next-line no-unused-vars
		var btnRefresh = new SkunkSchema.PushButton({parent: fixedLayout, name: "btnRefresh", text: "Refresh"})
			.setAnchor(btnUp, "TOPRIGHT", "TOPLEFT")
			.setWidth(40);

		var lstContents = new SkunkSchema.List({parent: fixedLayout, name: "lstContents"})
			.setAnchor(edPath, "BOTTOMLEFT")
			.setWidth(fixedLayout.getWidth())
			.setHeight(fixedLayout.getHeight() - 60)
			.addColumn({progid: "DecalControls.IconColumn"})
			.addColumn({progid: "DecalControls.TextColumn", fixedwidth: fixedLayout.getWidth()}); // fixedLayout.getWidth()
			//.addColumn({progid:"DecalControls.TextColumn", fixedwidth:100}); // fixedLayout.getWidth()
			
		core.debug(core.MAJOR, "lstContents.children: " + lstContents.children);
			
		var stFileName = new SkunkSchema.StaticText({parent: fixedLayout, name: "stFileName", text: "File name: "})
			.setAnchor(lstContents, "BOTTOMLEFT", "TOPLEFT")
			.setWidth(50);

		// eslint-disable-next-line no-unused-vars
		var edFileName = new SkunkSchema.Edit({parent: fixedLayout, name: "edFileName", text: ""})
			.setAnchor(stFileName, "TOPRIGHT")
			.setWidth(fixedLayout.getWidth() - 100);

		var btnOpenSave = new SkunkSchema.PushButton({parent: fixedLayout, name: "btnOpenSave", text: "Open"})
			.setAnchor(lstContents, "BOTTOMRIGHT", "TOPRIGHT")
			.setWidth(50);

		// eslint-disable-next-line no-unused-vars
		var btnCancel = new SkunkSchema.PushButton({parent: fixedLayout, name: "btnCancel", text: "Cancel"})
			.setAnchor(btnOpenSave, "BOTTOMLEFT", "TOPLEFT")
			.setWidth(50);

		// eslint-disable-next-line no-unused-vars
		var btnHelp = new SkunkSchema.PushButton({parent: fixedLayout, name: "btnHelp", text: "Help"})
			.setAnchor(fixedLayout, "BOTTOMLEFT", "BOTTOMLEFT")
			.setWidth(30);
		
		view.showControls(true);

		var xml = view.toXML();
		core.debug(core.MAJOR, "" + xml);
	};

	core.getFolderFiles = function getFolderFiles(params) {
		var path = params.path;
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var folder   =   fso.GetFolder(path);
		var files = new Enumerator(folder.files);
		var list = [];
		for (; !files.atEnd();   files.moveNext()) {
			list.push(files.item());
		}
		return list;
	};
	
	core.getFolderSubfolders = function getFolderSubfolders(params) {
		var path = params.path;
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var folder   =   fso.GetFolder(path);
		var f = new Enumerator(folder.SubFolders);
		var list = [];
		for (; !f.atEnd();   f.moveNext()) {
			list.push(f.item());
		}
		return list;
	};
	
	core.getFullPath = function getFullPath(params) {
		var path = params.path;
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var folder   =   fso.GetFolder(path);
		return folder.Path;
	};

	core.getParentFolder = function getParentFolder(params) {
		var path = params.path;
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var folder   =   fso.GetFolder(path);
		return folder.ParentFolder;
	};
	
	core.simpleUUID = function simpleUUID() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16)
			.substring(1);
	};
	
	core.escapeRegExp = function escapeRegExp(str) {
		return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	};
	
	core.showBrowser = (function() {

		function refreshContents(params) {
			var panel = params.panel;
			var control = params.control;
			var path = params.path;

			skapi.GetControlProperty(panel, control, "Clear()");

			var folders = core.getFolderSubfolders({path: path});
			var files = core.getFolderFiles({path: path});

			var contents = [];
			var f;
			var intRow = 0;
			for (var i = 0; i < folders.length; i++) {
				f = folders[i];
				contents.push(f);
				
				skapi.GetControlProperty(panel, control, "AddRow()");
				skapi.SetControlProperty(panel, control, "data(0," + intRow + ", 1)", iconFolder);
				skapi.SetControlProperty(panel, control, "data(1," + intRow + ")", f.Name);
				
				intRow++;
			}
			
			for (var i = 0; i < files.length; i++) {
				f = files[i];
				contents.push(f);
				
				skapi.GetControlProperty(panel, control, "AddRow()");
				skapi.SetControlProperty(panel, control, "data(0," + intRow + ", 1)", iconFile);
				skapi.SetControlProperty(panel, control, "data(1," + intRow + ")", f.Name);

				intRow++;
			}
			
			return contents;
		}
		
		return function showBrowser(params, callback) {
			if (typeof callback !== "function" && typeof Promise !== "undefined") {
				return new Promise(function(resolve, reject) {
					showBrowser(params, function promise_callback(err, results) {
						if (err) return reject(err);
						resolve(results);
					});
				});
			}

			//var thisArg     = params.thisArg;
			var title       = params.title || "File Browser: " + core.simpleUUID();
			var path        = params.path || _dir;
			path            = core.getFullPath({path: path});
			var mode        = params.mode || "open"; // open, save or create.
			var help        = params.help;// string printed if help button is clicked, button only show if help message provided.

			var selectedFilePath;
			var selectedFileName;
			
			var handler = {};
			handler.OnControlEvent = function OnControlEvent(szPanel, szControl, dictSzValue) {
				if (szPanel != title) return;
				var value = dictSzValue(szControl);
				core.debug(core.MAJOR, "<OnControlEvent>", szPanel, szControl, value);
				if (szControl == "btnCancel") {
					resolve(undefined, {result: "CANCELED"});
				} else if (szControl == "btnHelp") {
					core.info(help);
				} else if (szControl == "btnRefresh") {
					contents = refreshContents({
						path   : path,
						panel  : title,
						control: "lstContents"
					});
				} else if (szControl == "btnUp") {
					var parent = core.getParentFolder({path: path});
					if (!parent) {
						core.info("Can't go any higher.");
						return;
					}
					path = parent.Path;
					
					skapi.SetControlProperty(title, "edPath", "text", path.replace(/\\/g, "\\"));
					
					contents = refreshContents({
						path   : path,
						panel  : title,
						control: "lstContents"
					});
					skapi.SetControlProperty(title, "edFileName", "text", "");
					selectedFilePath = null;
					selectedFileName = null;
				} else if (szControl == "lstContents") {
					//var intCol = parseInt(value.split(",")[0]);
					var intRow = parseInt(value.split(",")[1]);
					var content = contents[intRow];
					core.debug(core.MAJOR, "Type: " + content.Type + ", name: " + content.Name);
					if (content.Type == "File folder") {
						path = content.Path;
						skapi.SetControlProperty(title, "edPath", "text", path.replace(/\\/g, "\\"));
						contents = refreshContents({
							path   : path,
							panel  : title,
							control: "lstContents"
						});
						skapi.SetControlProperty(title, "edFileName", "text", "");
						selectedFilePath = null;
						selectedFileName = null;
					} else {
						skapi.SetControlProperty(title, "edFileName", "text", content.Name);
						selectedFilePath = content.Path;
						selectedFileName = content.Name;
					}
				} else if (szControl == "btnOpenSave") {
					
					getControlPropertyAsync(szPanel, "edFileName", "text", function onValue(err, value) {
						if (err) return resolve(err);
						//core.info("onValue", err, results);
						if (value == "") {
							selectedFilePath = content; // reset back to before
							return;
						}
						var filePath = path + "\\" + value;
						core.debug(core.MAJOR, "filePath: " + filePath);
						
						selectedFilePath = filePath;
						selectedFileName = value;
						
						if (mode == "open") {
							if (selectedFilePath) {
								resolve(null, {
									result: "OPEN_PATH",
									path  : selectedFilePath,
									name  : selectedFileName
								});
							} else {
								core.info("No file selected.");
							}
						} else if (mode == "save") {
							if (selectedFilePath) {
								resolve(null, {
									result: "SAVE_PATH",
									path  : selectedFilePath,
									name  : selectedFileName
								});
							} else {
								core.info("No file name given.");
							}
						} else if (mode == "create") {
							if (selectedFilePath) {
								resolve(null, {
									result: "CREATE_PATH",
									path  : selectedFilePath,
									name  : selectedFileName
								});
							} else {
								core.info("No file name given.");
							}
						} else {
							core.info("Unknown mode type: " + mode);
						}
						
						
					});
				} else if (szControl == "edFileName") {
					if (value == "") {
						selectedFilePath = content; // reset back to before
						return;
					}
					var filePath = path + "\\" + value;
					core.debug(core.MAJOR, "filePath: " + filePath);
					
					selectedFilePath = filePath;
					selectedFileName = value;
				} else if (szControl == "edPath") {
					//
					var fso = new ActiveXObject("Scripting.FileSystemObject");
					if (!fso.FolderExists(value)) {
						core.info("Folder '" + value + "' does not exist.");
						skapi.SetControlProperty(title, "edPath", "text", path.replace(/\\/g, "\\"));
						return;
					}
					var folder  =   fso.GetFolder(value);
					path = folder.Path;
					contents = refreshContents({
						path   : path,
						panel  : title,
						control: "lstContents"
					});
				}
			};
			skapi.AddHandler(evidOnControlEvent, handler);

			function resolve() {
				skapi.RemoveHandler(evidNil, handler);
				skapi.RemoveControls(title);
				callback.apply(params.thisArg, arguments);
			}

			var listHeight = 20 * 6; // 6 rows
			var xml = '<view title="' + title + '" width="300" height="' + (90 + listHeight) + '" icon="4821">\
				<control progid="DecalControls.FixedLayout" clipped="0">\
					<control progid="DecalControls.Edit" name="edPath" width="240" height="20" text="' + path.replace(/\\/g, "\\") + '" imageportalsrc="4726" marginx="5" marginy="2"/>\
					<control progid="DecalControls.PushButton" name="btnUp" width="20" height="20" text="Up" left="240" top="0"/>\
					<control progid="DecalControls.PushButton" name="btnRefresh" width="40" height="20" text="Refresh" left="260" top="0"/>\
					<control progid="DecalControls.List" name="lstContents" width="300" height="' + listHeight + '" left="0" top="20">\
						<column progid="DecalControls.IconColumn"/>\
						<column progid="DecalControls.TextColumn" fixedwidth="280"/>\
					</control>\
					<control progid="DecalControls.StaticText" name="stFileName" width="50" height="20" text="File name: " left="0" top="' + (20 + listHeight) + '"/>\
					<control progid="DecalControls.Edit" name="edFileName" width="200" height="20" text="" imageportalsrc="4726" marginx="5" marginy="2" left="50" top="' + (20 + listHeight) + '"/>';
			if (mode == "open") {
				xml += '<control progid="DecalControls.PushButton" name="btnOpenSave" width="50" height="20" text="Open" left="250" top="' + (20 + listHeight) + '"/>';
			} else if (mode == "save") {
				xml += '<control progid="DecalControls.PushButton" name="btnOpenSave" width="50" height="20" text="Save" left="250" top="' + (20 + listHeight) + '"/>';
			} else if (mode == "create") {
				xml += '<control progid="DecalControls.PushButton" name="btnOpenSave" width="50" height="20" text="Create" left="250" top="' + (20 + listHeight) + '"/>';
			}
					
			xml += '<control progid="DecalControls.PushButton" name="btnCancel" width="50" height="20" text="Cancel" left="250" top="' + (40 + listHeight) + '"/>';
			if (help) {// Show help button if script gave a help message.
				xml += '<control progid="DecalControls.PushButton" name="btnHelp" width="50" height="20" text="Help" left="0" top="' + (40 + listHeight) + '"/>';
			}
			xml += '	</control>\
			</view>';
			skapi.ShowControls(xml, true);
			
			var contents = refreshContents({
				path   : path,
				panel  : title,
				control: "lstContents"
			});
		};
	})();

	core.timedOut = function timedOut(resolve) {
		resolve(new core.Error("TIMED_OUT"));
	};

	function getControlPropertyAsync(szPanel, szControl, szProperty, callback, thisArg) {
		function resolve() {
			skapi.RemoveHandler(evidNil, handler);
			clearTimeout(tid);
			callback.apply(thisArg, arguments);
		}
		
		var tid = setTimeout(core.timedOut, 5000, resolve);
		
		// Event handler when skapi tells us the value.
		var handler = {};
		handler.OnControlProperty = function OnControlProperty(szPanel2, szControl2, szProperty2, vValue) {
			//fDebug("<fOnControlProperty> " + szPanel2 + ", " + szControl + ", " + szProperty2 + ", " + vValue);
			if (szPanel2 == szPanel && szControl2 == szControl && (szProperty2 == szProperty || szProperty2 == "data")) {
				skapi.RemoveHandler(evidNil, handler);
				resolve(undefined, vValue);
			}
		};
		skapi.AddHandler(evidOnControlProperty,	handler);
		skapi.GetControlProperty(szPanel, szControl, szProperty);
	}

	return core;
}));