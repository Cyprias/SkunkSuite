# SkunkActions

Collection of asynchronous functions to perform actions in game and wait for results.

## Getting Started

Add to your script's swx file
```
<script src="modules\\SkunkSuite\\SkunkActions\\AnimationFilter.js"/>
<script src="modules\\SkunkSuite\\SkunkActions\\EffectsFilter.js"/>
<script src="modules\\SkunkSuite\\SkunkActions\\SkunkActions.js"/>
```

Your script can use the global object `SkunkActions10` or `LibStub("SkunkActions-1.0")` to access the functions.

## Available Functions
Action functions accept 2 arguments. `params` and `callback`. `params` can include a `logger` function, `thisArg` for the callback and a `timeout` (miliseconds) to change the timeout.

Example
```
SkunkActions.learnScroll({
	aco: skapi.acoSelected,
	logger: function() {
		skapi.OutputLine(Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	},
	thisArg: {aco: skapi.acoSelected},
	timeout: 10 * 1000
}, function onResult(err, result) {
	if (err) return skapi.OutputLine("Error: " + err, opmChatWndAll, cmcRed);
	skapi.OutputLine("Learned " + this.aco);
});
```

### approachVendor({aco}, callback);

### beginRecallCommand({command}, callback);

### castSpell({spellid, aco, onlyWindupAnimation}, callback);

### dropItem({aco}, callback);

### equipItem({aco, offhand}, callback);

### giveToAco({acoItem, acoTarget}, callback);

### learnScroll({aco}, callback);

### lootItem({aco, allowStacking}, callback);

### mergeItems({aco, acoPartner}, callback);

### moveItem({aco, acoContainer}, callback);

### moveToContainer({aco, acoContainer, iitem, fAllowStacking}, callback);

### moveToPack({aco, ipack, iitem, fAllowStacking}, callback);

### openContainer({aco}, callback);

### pickupItem({aco}, callback);

### raiseAttr({attr, dexp}, callback);

### raiseSkill({skid, dexp}, callback);

### raiseVital({vital, dexp}, callback);

### salvageItem({aco}, callback);

### salvageItemsTogether({items}, callback);

### setCombatMode({mode, force}, callback);

### splitAco({aco, quantity}, callback);

### strikeAttackKey({aco, cmid}, callback);

### unequipItem({aco}, callback);

### useOnAco({aco, acoTarget, onChance}, callback);

### vendorBuyAll({}, callback);

### vendorSellAll({}, callback);

### waitToEnterPortalSpace({}, callback);

### waitToExitPortalSpace({}, callback);