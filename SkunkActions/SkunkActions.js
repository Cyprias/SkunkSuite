/********************************************************************************************\
	File:           SkunkActions-1.0.js
	Purpose:        Async functions to preform actions in game and wait for results.
	Creator:        Cyprias
	Date:           10/20/2020
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/), SkunkTimer, AnimationFilter.
\*********************************************************************************************/

var MAJOR = "SkunkActions-1.0";
var MINOR = 230309;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkActions10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	core.debugging = false;
	
	//var AnimationFilter         = LibStub("AnimationFilter-1.0", true);
	var AnimationFilter     = (typeof LibStub !== "undefined" && LibStub.getLibrary("AnimationFilter-1.0", true)) || (typeof require === "function" && require("SkunkSuite\\SkunkActions\\AnimationFilter"));
	var EffectsFilter     = (typeof LibStub !== "undefined" && LibStub.getLibrary("EffectsFilter-1.0", true)) || (typeof require === "function" && require("SkunkSuite\\SkunkActions\\EffectsFilter"));
	
	core.embeds = core.embeds || {};
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	core.setTimeout = setTimeout;
	core.setInterval = setInterval;
	core.setImmediate = setImmediate;
	core.clearTimeout = clearTimeout;
	core.addHandler = function addHandler(evid, handler) {
		// For some reason just doing 'core.addHandler = skapi.AddHandler' causes skunkworks to throw an error. So we wrap it.
		skapi.AddHandler(evid, handler);
	};
	core.removeHandler = function removeHandler(evid, handler) {
		// For some reason just doing 'core.removeHandler = skapi.RemoveHandler' causes skunkworks to throw an error. So we wrap it.
		skapi.RemoveHandler(evid, handler);
	};
	
	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		skapi.OutputLine(core.MAJOR + " " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (core.debugging == true) {
			skapi.OutputLine(core.MAJOR + " [D] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	};

	function embedMixins(script) {
		script.approachVendor = core.approachVendor;
		script.autoWieldEx = core.autoWieldEx;
		script.beginRecallCommand = core.beginRecallCommand;
		script.castSpell = core.castSpell;
		script.dropItem = core.dropItem;
		script.equipItem = core.equipItem;
		script.giveToAco = core.giveToAco;
		script.learnScroll = core.learnScroll;
		script.lootItem = core.lootItem;
		script.mergeItems = core.mergeItems;
		script.moveItem = core.moveItem;
		script.moveToContainer = core.moveToContainer;
		script.moveToPack = core.moveToPack;
		script.openContainer = core.openContainer;
		script.pickupItem = core.pickupItem;
		script.raiseAttr = core.raiseAttr;
		script.raiseSkill = core.raiseSkill;
		script.raiseVital = core.raiseVital;
		script.salvageItem = core.salvageItem;
		script.salvageItemsTogether = core.salvageItemsTogether;
		script.setCombatMode = core.setCombatMode;
		script.splitAco = core.splitAco;
		script.strikeAttackKey = core.strikeAttackKey;
		script.unequipItem = core.unequipItem;
		script.useOnAco = core.useOnAco;
		script.vendorBuyAll = core.vendorBuyAll;
		script.vendorSellAll = core.vendorSellAll;
		script.waitToEnterPortalSpace = core.waitToEnterPortalSpace;
		script.waitToExitPortalSpace = core.waitToExitPortalSpace;
		script.acceptTrade = core.acceptTrade;
		
		return script;
	}
	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	core.timedOut = function timedOut(callback, name) {
		core.debug("Timeout! " + name);
		callback(new core.Error(name || "TIMED_OUT"));
	};
	
	core.applyMethod = function applyMethod() {
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};
	
	///////////////////////////

	core.moveToContainer = function moveToContainer(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		var acoContainer = (!params || params.acoContainer === undefined ? 0 : params.acoContainer);
		var iitem = (!params || params.iitem === undefined ? 0 : params.iitem);
		var fAllowStacking = (!params || params.fAllowStacking === undefined ? true : params.fAllowStacking);
		logger(core.MAJOR + " <moveToContainer>", "aco: " + aco, "acoContainer: " + acoContainer, "iitem: " + iitem, "fAllowStacking: " + fAllowStacking);

		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory(aco2) {
			logger(core.MAJOR + " <moveToContainer|OnAddToInventory>", aco2);
			if (aco2.oid == aco.oid) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory, handler);

		handler.OnMoveItem = function OnMoveItem(aco2, acoContainer2, iitem) {
			logger(core.MAJOR + " <moveToContainer|OnMoveItem>", aco2, acoContainer2, iitem);
			if (aco2.oid == aco.oid) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnMoveItem, handler);

		handler.OnAdjustStack = function OnAdjustStack(aco2, citemPrev) {
			logger(core.MAJOR + " <moveToContainer|OnAdjustStack>", aco2, citemPrev);
			if (aco2.oid == aco.oid) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAdjustStack, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/The Pack is completely full!/)) {
				return finish(new core.Error("CONTAINER_FULL"));
			} else 	if (szMsg.match(/You can only move or use one item at a time/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/That item is not valid!/)) {
				return finish(new core.Error("ITEM_INVALID"));
			}
			logger(core.MAJOR + " <moveToContainer|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		/*
		var acoPack = skapi.AcoFromIpackIitem(ipack, iitemNil);
		core.info("acoPack: " + acoPack, "citemContents: " + acoPack.citemContents, iitem);
		if (acoPack && acoPack.citemContents <= iitem) {
			var acoLast = skapi.AcoFromIpackIitem(ipack, acoPack.citemContents - 1);
			core.info("aco: " + aco, "acoLast: " + acoLast);
			if (acoLast && acoLast.oid == aco.oid) {
				return finish(undefined, true);
			}
		}
		*/
		

		aco.MoveToContainer(acoContainer, iitem, fAllowStacking);
		var tid = core.setTimeout(core.timedOut, 2000, finish, "MOVE_TO_CONTAINER_TIMED_OUT");
	};
	
	/*
	 * splitAco - Split a stack of items and get the new stack.
	 *
	 * @param {Object} params - Function parameters
	 * @param {object} params.aco - Item stack to be split.
	 * @param {number} params.quantity: Amount to be split off.
	 * @param {number} params.timeout: (Optional) How long in miliseconds to wait before calling callback with a timedout result. (default 5000ms)
	 * @param {object} params.thisArg: (Optional) Object to be given to callback that will accessible via 'this'.
	 * @param {function} callback -  Function to be called with a result and acoItem if successful.
	 * @returns {object} aco of the new object.
	 */
	core.splitAco = function splitAco(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		var quantity = params.quantity;
		logger(core.MAJOR + " <splitAco>", "aco: " + aco, "quantity: " + quantity);

		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		if (quantity > aco.citemStack) {
			return finish(new core.Error("QUANITY_MORE_THAN_STACK"));
		}

		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory(acoItem) {
			if (aco.oid == acoItem.oid) return;
			if (acoItem.szPlural == aco.szPlural && acoItem.citemStack == quantity) {
				return finish(undefined, acoItem);
			}
			logger(core.MAJOR + " <splitAco|OnAddToInventory> UNCAUGHT", acoItem, acoItem.citemStack);
			logger(core.MAJOR + " ", aco, aco.citemStack);
		};
		core.addHandler(evidOnAddToInventory,            handler);
		
		handler.OnAdjustStack = function OnAdjustStack(acoItem) { // citemPrev
			if (aco.oid == acoItem.oid) return;
			if (aco.szName == acoItem.szName && acoItem.citemStack == quantity) {// check name instead of oid since the adjusting stack is our previous stack.
				finish(undefined, acoItem);
			}
		};
		core.addHandler(evidOnAdjustStack, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy!/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You can only move or use one item at a time/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) can't be split - you're too busy/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/Split amount not valid!/)) {
				return finish(new core.Error("INVALID_SPLIT_AMOUNT"));
			} else if (szMsg.match(/The (.*) can't be split/)) {
				return finish(new core.Error("CANT_BE_SPLIT"));
			}
			logger(core.MAJOR + " <splitAco|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,            handler);

		// Select the item.
		skapi.SelectAco(aco);
		
		// Set and split the stack.
		skapi.SetStackCount(quantity);
		
		// Move to a new spot.
		aco.MoveToPack(0, 0, false);
		
		var tid = core.setTimeout(core.timedOut, params.timeout || 2000, finish, "SPLIT_ACO_TIMED_OUT");
	};
	
	core.giveToAco = function giveToAco(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var acoItem = params.acoItem;
		var acoTarget = params.acoTarget;
		logger(core.MAJOR + " <giveToAco>", "acoItem: " + acoItem, "acoTarget: " + acoTarget);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnChatServer = function OnChatServer(szMsg) { // cmc
			if (szMsg.match(/You give (.*)/)) {
				return finish(undefined, true);
			} else if (szMsg.match(/You hand over (.*)./)) {
				return finish(undefined, true);
			} else if (szMsg.match(/You allow (.*) to examine your (.*)./)) {
				return finish(undefined, true);
			}
		};
		core.addHandler(evidOnChatServer, handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/The (.*) can't be given/)) {
				//return finish(new core.Error("CANT_BE_GIVEN"));
				core.setTimeout(finish, 250, new core.Error("CANT_BE_GIVEN"));
			}
			logger(core.MAJOR + " <giveToAco|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);

		acoItem.GiveToAco(acoTarget);
		var tid = core.setTimeout(core.timedOut, 3000, finish, "GIVE_TO_ACO_TIMED_OUT");
	};
	
	core.salvageItem = function salvageItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		logger(core.MAJOR + " <salvageItem>", "aco: " + aco);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory(acoItem) {
			logger(core.MAJOR + " <salvageItem|OnAddToInventory>", acoItem);
			if (acoItem.mcm & mcmSalvageBag) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			logger(core.MAJOR + " <salvageItem|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,      handler);
		
		var tid = core.setTimeout(core.timedOut, params.timeout || 1000, finish, "SALVAGE_ITEM_TIMED_OUT");

		var acoUst = skapi.AcoFindInInv("Ust");
		if (!acoUst) return finish(new core.Error("NO_UST"));
		acoUst.Use(); // Reset
		aco.AddToUst(); // Add item to ust window/
		skapi.Salvage(); // Hit salvage and wait for bag to added to inventory.
	};
	
	core.salvageItemsTogether = function salvageItemsTogether(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var items = params.items;
		logger(core.MAJOR + " <salvageItemsTogether>", "items: " + items.length);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory(acoItem) {
			logger(core.MAJOR + " <salvageItem|OnAddToInventory>", acoItem);
			if (acoItem.mcm & mcmSalvageBag) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory, handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			logger(core.MAJOR + " <salvageItemsTogether|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,      handler);

		var acoUst = skapi.AcoFindInInv("Ust");
		if (!acoUst) return finish(new core.Error("UST_NOT_FOUND"));
		acoUst.Use(); // reset
		items.forEach(function(aco) {
			aco.AddToUst();
		});
		skapi.Salvage(); // Hit salvage and wait for bag to added to inventory.
		
		var tid = core.setTimeout(core.timedOut, 3000, finish, "SALVAGE_ITEMS_TOGETHER_TIMED_OUT");
	};
	
	core.lootItem = function lootItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var acoItem = params.aco;
		var allowStacking = (params.allowStacking === undefined ? true : params.allowStacking);
		logger(core.MAJOR + " <lootItem>", "acoItem: " + acoItem, "allowStacking: " + allowStacking);
		
		function finish() {
			clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
		}
		
		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory(aco) {
			logger(core.MAJOR + " <lootItem|OnAddToInventory> " + aco.szName);
			if (aco.oid == acoItem.oid) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory, handler);		

		handler.OnAdjustStack = function OnAdjustStack(aco) { // citemPrev
			if (aco.szName == acoItem.szName) {// check name instead of oid since the adjusting stack is our previous stack.
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAdjustStack, handler);

		handler.OnCloseContainer = function OnCloseContainer() { // aco
			finish(new core.Error("CONTAINER_CLOSED"));
		};
		core.addHandler(evidOnCloseContainer, handler);

		handler.OnMoveItem = function OnMoveItem(aco) { // , acoContainer, iitem
			if (aco.szName == acoItem.szName || aco.oid == acoItem.oid) {// check name instead of oid since the adjusting stack is our previous stack.
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnMoveItem, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/The (.*) can't be put in the container - the item is under someone else's control/)) {
				return finish(new core.Error("SOMEONE_ELSES_CONTROL"));
			} else if (szMsg.match(/The item is under someone else's control!/)) {
				return finish(new core.Error("SOMEONE_ELSES_CONTROL"));

			//} else if (szMsg.match(/You can only move or use one item at a time/)){
			} else if (szMsg.match(/The (.*) can't be merged - you're too busy/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You're too busy!/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) can't be put in the container - you're too busy/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) can't be put in the container/)) {
				return finish(new core.Error("UNABLE_TO_MOVE_OBJECT"));
			} else if (szMsg.match(/You cannot move or use an item while attacking/)) {
				return finish(new core.Error("CANT_WHILE_ATTACKING"));
			} else if (szMsg.match(/Unable to move to object!/)) {
				return finish(new core.Error("UNABLE_TO_MOVE_OBJECT"));
			} else if (szMsg.match(/Backpack is completely full!/)) {
				return finish(new core.Error("BACKPACK_FULL"));
			}
			logger(core.MAJOR + " <lootItem|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);

		var tid = core.setTimeout(core.timedOut, params.timeout || 5000, finish, "LOOT_ITEM_TIMED_OUT");

		acoItem.MoveToPack(0, 0, allowStacking);
	};
	
	core.learnScroll = function learnScroll(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco; // Scroll to learn.
		logger(core.MAJOR + " <learnScroll>", "aco: " + aco);
		
		function finish() { // err, result
			clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
			callback = undefined;
		}

		var handler = {};
		handler.OnChatServer = function OnChatServer(szMsg, cmc) {
			if (szMsg.match(/^You learn the (.*) spell./)) {
				return finish(undefined, true);
			} else if (szMsg.match(/^You already know that spell!/)) {
				return finish(new core.Error("ALREADY_KNOWN"));
			} else if (szMsg.match(/^You are not skilled enough in (.*) to learn this spell./)) {
				return finish(new core.Error("SKILL_TOO_LOW"));
			} else if (szMsg.match(/^You are not trained in (.*)!/)) {
				return finish(new core.Error("NOT_TRAINED"));
			}
			logger(core.MAJOR + " <learnScroll|OnChatServer> UNCAUGHT", szMsg, cmc);
		};
		core.addHandler(evidOnChatServer, handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy!/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/^Source item not found!/)) {
				return finish(new core.Error("SOURCE_ITEM_NOT_FOUND"));
			} else if (szMsg.match(/^Using the (.*)/)) {
				return;
			}
			logger(core.MAJOR + " <learnScroll|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);

		var tid = core.setTimeout(core.timedOut, 3 * 1000, finish, "LEARN_SCROLL_TIMED_OUT");
		aco.Use();
	};
	
	core.beginRecallCommand = function beginRecallCommand(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var command = params.command;
		logger(core.MAJOR + " <beginRecallCommand>", "command: " + command);
	
		function finish() {
			AnimationFilter && AnimationFilter.removeCallback(onAnimation);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		if (typeof AnimationFilter === "undefined") {
			return finish(new core.Error("beginRecallCommand needs AnimationFilter"));
		}

		function onAnimation(payload) {
			if (payload.object != skapi.acoChar.oid) return;
			if (payload.animation == 339) { // /lifestone 
				return finish(undefined, true);
			} else if (payload.animation == 358) { // /marketplace
				return finish(undefined, true);
			}
		}
		AnimationFilter.addCallback(onAnimation);

		skapi.InvokeChatParser(command);
		var tid = core.setTimeout(core.timedOut, 1000 * 5, finish, "BEGIN_RECAL_COMMAND_TIMED_OUT");
	};
	
	core.waitToEnterPortalSpace = function waitToEnterPortalSpace(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		logger(core.MAJOR + " <waitToEnterPortalSpace>");
		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnStartPortalSelf = function OnStartPortalSelf() {
			logger(core.MAJOR + " <waitToExitPortalSpace|OnStartPortalSelf>");
			finish(undefined, true);
		};

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/^You have moved too far/)) {
				logger(core.MAJOR + " <waitToEnterPortalSpace|OnTipMessage> We moved too far");
				return finish(new core.Error("MOVED_TOO_FAR"));
			}
			logger(core.MAJOR + " <waitToEnterPortalSpace|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		core.addHandler(evidOnStartPortalSelf, handler);
		var tid = core.setTimeout(core.timedOut, 1000 * 20, finish, "ENTER_PORTAL_TIMED_OUT");
	};
	
	core.waitToExitPortalSpace = function waitToExitPortalSpace(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		logger(core.MAJOR + " <waitToExitPortalSpace>");

		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		var handler = {};
		handler.OnEndPortalSelf = function OnEndPortalSelf() {
			logger(core.MAJOR + " <waitToExitPortalSpace|OnEndPortalSelf>");
			finish(undefined, true);
		};
		core.addHandler(evidOnEndPortalSelf, handler);
		var tid = core.setTimeout(core.timedOut, 1000 * 15, finish, "EXIT_PORTAL_TIMED_OUT");
	};
	
	core.mergeItems = function mergeItems(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		var acoPartner = params.acoPartner;
		logger(core.MAJOR + " <mergeItems>", "aco: " + aco, "acoPartner: " + acoPartner);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnAdjustStack = function OnAdjustStack(aco2, citemPrev) {
			if (aco2.szName == aco.szName) {
				return finish(undefined, true);
			}
			logger(core.MAJOR + " <mergeItems|OnAdjustStack> UNCAUGHT", aco2.szName, aco2.oid, "citemPrev: " + citemPrev, "citemStack: " + aco2.citemStack);
		};
		core.addHandler(evidOnAdjustStack, handler);
		
		//handler.OnMoveItem = function OnMoveItem(aco2, acoContainer) { // iitem
		//	logger(core.MAJOR + " <mergeItems|OnMoveItem>", aco2.szName, aco2.oid, "acoContainer: " + acoContainer, "citemStack: " + aco2.citemStack);
		//};
		//core.addHandler(evidOnMoveItem, handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You can only move or use one item at a time/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/Source stack not found!/)) {
				return finish(new core.Error("SOURCE_STACK_NOT_FOUND"));
			} else if (szMsg.match(/The (.*) can't be merged/)) {
				return finish(new core.Error("CANT_MERGE"));
			} else if (szMsg.match(/Cannot give (.*) to (.*)/)) {
				return finish(new core.Error("CANT_GIVE_TO_ITEM"));
			} else if (szMsg.match(/The destination stack is already full./)) {
				return finish(new core.Error("DESTINATION_IS_FULL"));
			} else if (szMsg.match(/You must first pick up the (.*)/)) {
				return finish(new core.Error("MUST_PICKUP_FIRST"));
			}
			logger(core.MAJOR + " <mergeItems|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		aco.MoveToStack(acoPartner);
		var tid = core.setTimeout(core.timedOut, 3000, finish, "MERGE_ITEMS_TIMED_OUT");
	};
	
	core.moveItem = function moveItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		var acoContainer = params.acoContainer;
		logger(core.MAJOR + " <moveItem>", "aco: " + aco, "acoContainer: " + acoContainer);

		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		var handler = {};
		handler.OnMoveItem = function OnMoveItem(aco2) {
			if (aco.szName == aco.szName || aco.oid == aco.oid) {// check name instead of oid since the adjusting stack is our previous stack.
				return finish(undefined, true);
			}
			logger(core.MAJOR + " <moveItem|OnMoveItem> UNCAUGHT", aco2);
		};
		core.addHandler(evidOnMoveItem, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy!/)) {
				logger(core.MAJOR + " <moveItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You can only move or use one item at a time/)) {
				logger(core.MAJOR + " <moveItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/Casting (.*)/)) {
				return;
			} else if (szMsg.match(/The Pack can fit no more containers!/)) {
				logger(core.MAJOR + " <moveItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("DESTINATION_IS_FULL"));
			}
			logger(core.MAJOR + " <moveItem|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		
		aco.MoveToContainer(acoContainer, undefined, true);
		var tid = core.setTimeout(core.timedOut, 3000, finish, "MOVE_ITEM_TIMED_OUT");
	};
	
	core.dropItem = function dropItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		logger(core.MAJOR + " <dropItem>", "aco: " + aco);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnRemoveFromInventory = function OnRemoveFromInventory(acoItem) {
			if (acoItem.oid == aco.oid) {
				return finish(undefined, true);
			}
		};
		core.addHandler(evidOnRemoveFromInventory, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You can only move or use one item at a time/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You must first pick up the (.*)/)) {
				return finish(new core.Error("MUST_PICKUP_FIRST"));
			} else if (szMsg.match(/You have dropped too many items recently!/)) {
				return finish(new core.Error("DROPPED_TOO_MANY_ITEMS"));
			} else if (szMsg.match(/The (.*) can't be dropped/)) {
				return finish(new core.Error("CANT_BE_DROPPED"));
			} else if (szMsg.match(/^You're too busy/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/^In Portal Space - Please Wait.../)) {
				return finish(new core.Error("IN_PORTAL_SPACE"));
			}
			logger(core.MAJOR + " <dropItem|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		aco.Drop();
		var tid = core.setTimeout(core.timedOut, 10*1000, finish, "DROP_ITEM_TIMED_OUT");
	};
	
	core.raiseSkill = function raiseSkill(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var skid = params.skid;
		var dexp = params.dexp;
		logger(core.MAJOR + " <raiseSkill>", "skid: " + skid, "dexp: " + dexp);

		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnChatServer = function OnChatServer(szMsg, cmc) {
			if (szMsg.match(/Your base/)) {
				return finish(undefined, true);
			} else if (szMsg.match(/Your attempt to raise (.*) has failed./)) {
				return finish(new core.Error("ATTEMPT_FAILED"));
			}
			logger(core.MAJOR + " <raiseSkill|OnChatServer> UNCAUGHT", szMsg, cmc);
		};
		core.addHandler(evidOnChatServer,  handler);
		
		// Check if dexp was given, skapi.RaiseSkill doesn't accept undefined or null.
		if (dexp) {
			logger(core.MAJOR + " Calling skapi.RaiseSkill(" + skid + ", " + dexp + ")");
			skapi.RaiseSkill(skid, dexp);
		} else {
			logger(core.MAJOR + " Calling skapi.RaiseSkill(" + skid + ")");
			skapi.RaiseSkill(skid);
		}

		var tid = core.setTimeout(core.timedOut, 3000, finish, "RAISE_SKILL_TIMED_OUT");
	};
	
	core.raiseAttr = function raiseAttr(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var attr = params.attr;
		var dexp = params.dexp;
		logger(core.MAJOR + " <raiseAttr>", "attr: " + attr, "dexp: " + dexp);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnChatServer = function OnChatServer(szMsg, cmc) {
			if (szMsg.match(/Your base/)) {
				return finish(undefined, true);
			} else if (szMsg.match(/Your attempt to raise (.*) has failed./)) {
				return finish(new core.Error("ATTEMPT_FAILED"));
			}
			logger(core.MAJOR + " <raiseAttr|OnChatServer> UNCAUGHT", szMsg, cmc);
		};
		core.addHandler(evidOnChatServer,  handler);

		// Check if dexp was given, skapi.RaiseAttr doesn't accept undefined or null.
		if (dexp) {
			logger(core.MAJOR + " Calling skapi.RaiseAttr(" + attr + ", " + dexp + ")");
			skapi.RaiseAttr(attr, dexp);
		} else {
			logger(core.MAJOR + " Calling skapi.RaiseAttr(" + attr + ")");
			skapi.RaiseAttr(attr);
		}

		var tid = core.setTimeout(core.timedOut, 3000, finish, "RAISE_ATTR_TIMED_OUT");
	};
	
	core.raiseVital = function raiseVital(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var vital = params.vital;
		var dexp = params.dexp;
		logger(core.MAJOR + " <raiseVital>", "vital: " + vital, "dexp: " + dexp);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnChatServer = function OnChatServer(szMsg, cmc) {
			if (szMsg.match(/Your base/)) {
				return finish(undefined, true);
			}
			logger(core.MAJOR + " <raiseVital|OnChatServer> UNCAUGHT", szMsg, cmc);
		};
		core.addHandler(evidOnChatServer,  handler);

		// Check if dexp was given, skapi.RaiseVital doesn't accept undefined or null.
		if (dexp) {
			logger(core.MAJOR + " Calling skapi.RaiseVital(" + vital + ", " + dexp + ")");
			skapi.RaiseVital(vital, dexp);
		} else {
			logger(core.MAJOR + " Calling skapi.RaiseVital(" + vital + ")");
			skapi.RaiseVital(vital);
		}
		
		var tid = core.setTimeout(core.timedOut, 3000, finish, "RAISE_VITAL_TIMED_OUT");
	};
	
	core.approachVendor = function approachVendor(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		logger(core.MAJOR + " <approachVendor>", "aco: " + aco);

		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnApproachVendor = function OnApproachVendor(acoMerchant, mcmBuy, cpyMaxBuy, fractBuy, fractSell, coaco) {
			if (aco.oid == acoMerchant.oid) {
				//return finish(undefined, true);
				return finish(undefined, {
					acoMerchant: acoMerchant,
					mcmBuy     : mcmBuy,
					cpyMaxBuy  : cpyMaxBuy,
					fractBuy   : fractBuy,
					fractSell  : fractSell,
					coaco      : coaco
				});
			}
		};
		core.addHandler(evidOnApproachVendor,            handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			logger(core.MAJOR + " <approachVendor|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,      handler);
		
		logger(core.MAJOR + " Using " + aco.szName + "...");
		aco.Use();
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 10000, finish, "APPROACH_VENDOR_TIMED_OUT");
	};
	
	core.vendorSellAll = function vendorSellAll(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		logger(core.MAJOR + " <vendorSellAll>");
		
		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnStatTotalPyreals = function OnStatTotalPyreals() { // cpy
			core.setTimeout(finish, 500, undefined, true);
		};
		core.addHandler(evidOnStatTotalPyreals, handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy!/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You do not have enough free pack space to sell that!/)) {
				return finish(new core.Error("NOT_ENOUGH_SPACE"));
			}
			logger(core.MAJOR + " <vendorSellAll|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,            handler);



		skapi.VendorSellAll();
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 5000, finish, "VENDER_SELL_ALL_TIMED_OUT");
	};
	
	core.vendorBuyAll = function vendorBuyAll(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		logger(core.MAJOR + " <vendorBuyAll>");
		
		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory() { // aco
			logger("<OnAddToInventory>");
			core.setTimeout(finish, 250, undefined, true);
		};
		core.addHandler(evidOnAddToInventory, handler);
		
		handler.OnAdjustStack = function OnAdjustStack() { // aco, citemPrev
			logger("<OnAdjustStack>");
			core.setTimeout(finish, 250, undefined, true);
		};
		core.addHandler(evidOnAdjustStack, handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy!/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You must empty some slots in your backpack first/)) {
				return finish(new core.Error("BACKPACK_FULL"));
			}
			logger(core.MAJOR + " <vendorBuyAll|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,            handler);

		skapi.VendorBuyAll();
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 5000, finish, "VENDER_BUY_ALL_TIMED_OUT");
	};

	core.pickupItem = function pickupItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		logger(core.MAJOR + " <pickupItem>", "aco: " + aco);
		
		function finish() {
			clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			
			// Call callback in a timer so ensure asynchronicity.
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);	
		}
		
		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory(aco2) {
			if (aco2.oid == aco.oid) {
				return finish(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory,      handler);

		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/The (.*) can't be merged/)) {
				return finish(new core.Error("CANT_MERGE"));
			} else if (szMsg.match(/Action cancelled!/)) {
				return finish(new core.Error("ACTION_CANCELLED"));
			} else if (szMsg.match(/The (.*) can't be picked up - action cancelled/)) {
				return finish(new core.Error("ACTION_CANCELLED"));
			} else if (szMsg.match(/That item is not valid!/)) {
				return finish(new core.Error("ITEM_INVALID"));
			}
			logger(core.MAJOR + " <pickupItem|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,      handler);

		aco.MoveToPack(0, 0, true);
		var tid = core.setTimeout(finish, 10 * 1000, new Error("PICKUP_ITEM_TIMED_OUT"));
	};

	core.moveToPack = function moveToPack(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		var ipack = (!params || params.ipack === undefined ? 0 : params.ipack);
		var iitem = (!params || params.iitem === undefined ? 0 : params.iitem);
		var fAllowStacking = (!params || params.fAllowStacking === undefined ? true : params.fAllowStacking);
		logger(core.MAJOR + " <moveToPack>", "aco: " + aco, "ipack: " + ipack, "iitem: " + iitem, "fAllowStacking: " + fAllowStacking);

		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		var handler = {};
		handler.OnAddToInventory = function OnAddToInventory(aco2) {
			logger(core.MAJOR + " <moveToPack|OnAddToInventory>", aco2);
			if (aco2.oid == aco.oid) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory, handler);

		handler.OnMoveItem = function OnMoveItem(aco2, acoContainer, iitem) {
			logger(core.MAJOR + " <moveToPack|OnMoveItem>", aco2, acoContainer, iitem);
			if (aco2.oid == aco.oid) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnMoveItem, handler);

		handler.OnAdjustStack = function OnAdjustStack(aco2, citemPrev) {
			logger(core.MAJOR + " <moveToPack|OnAdjustStack>", aco2, citemPrev);
			if (aco2.oid == aco.oid) {
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAdjustStack, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/That item is not valid!/)) {
				return finish(new core.Error("ITEM_INVALID"));
			}
			logger(core.MAJOR + " <moveToPack|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,      handler);
		
		/*
		var acoPack = skapi.AcoFromIpackIitem(ipack, iitemNil);
		core.info("acoPack: " + acoPack, "citemContents: " + acoPack.citemContents, iitem);
		if (acoPack && acoPack.citemContents <= iitem) {
			var acoLast = skapi.AcoFromIpackIitem(ipack, acoPack.citemContents - 1);
			core.info("aco: " + aco, "acoLast: " + acoLast);
			if (acoLast && acoLast.oid == aco.oid) {
				return finish(undefined, true);
			}
		}
		*/
		

		aco.MoveToPack(ipack, iitem, fAllowStacking);
		var tid = core.setTimeout(core.timedOut, 2000, finish, "MOVE_TO_PACK_TIMED_OUT");
	};

	core.strikeAttackKey = function strikeAttackKey(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = (!params || params.aco === undefined ? skapi.acoSelected : params.aco);
		var cmid = (!params || params.cmid === undefined ? cmidCombatLowAttack : params.cmid);
		logger(core.MAJOR + " <strikeAttackKey>", "aco: " + aco, "cmid: " + cmid);
		
		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			AnimationFilter && AnimationFilter.removeCallback(onAnimation);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		if (typeof AnimationFilter === "undefined") {
			return finish(new core.Error("strikeAttackKey() needs AnimationFilter"));
		}
		
		var handler = {};
		handler.OnMeleeDamageOther = function OnMeleeDamageOther(szName, dhealth, dmty) {
			logger(core.MAJOR + " <strikeAttackKey|OnMeleeDamageOther>", szName, dhealth, dmty);
			finish(undefined, true);
		};
		core.addHandler(evidOnMeleeDamageOther, handler);
		
		handler.OnMeleeEvadeOther = function OnMeleeEvadeOther(szName) {
			logger(core.MAJOR + " <strikeAttackKey|OnMeleeEvadeOther>", szName);
			finish(undefined, true);
		};
		core.addHandler(evidOnMeleeEvadeOther, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy!/)) {
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You charged too far!/)) {
				return finish(new core.Error("CHARGED_TOO_FAR"));
			} else if (szMsg.match(/Action cancelled!/)) {
				return finish(new core.Error("ACTION_CANCELLED"));
			} else if (szMsg.match(/You must select a valid combat target before attacking/)) {
				return finish(new core.Error("MUST_SELECT_TARGET"));
			}
			logger(core.MAJOR + " <strikeAttackKey|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage,            handler);
		
		function onAnimation(payload) {
			if (payload.object != aco.oid) return;
			if (payload.animation_1 == 17) { // Target died
				logger(core.MAJOR + " <strikeAttackKey|onAnimation> Target died animation");
				finish(undefined, true);
			}
		}
		AnimationFilter.addCallback(onAnimation);

		skapi.Keys(cmid);
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 3000, finish, "STRIKE_ATTACK_TIMED_OUT");
	};

	core.castSpell = function castSpell(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var spellid = params.spellid;
		var aco = (!params || params.aco === undefined ? skapi.acoChar : params.aco);
		var onlyWindupAnimation =  (!params || params.onlyWindupAnimation === undefined ? false : params.onlyWindupAnimation);

		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			AnimationFilter && AnimationFilter.removeCallback(onAnimation);
			EffectsFilter && EffectsFilter.removeCallback(onEffect);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		var spell = spellid && skapi.SpellInfoFromSpellid(spellid);
		if (!spell) return finish(new core.Error("INVALID_SPELL"));
		
		var handler = {};
		handler.OnChatServer = function OnChatServer(szMsg) { // cmc
			if (szMsg.match(/You cast (.*) on (.*), refreshing (.*)/i)) {
				var spellName = RegExp.$1;
				if (spellName == spell.szName) {
					logger(core.MAJOR + " <castSpell|OnChatServer> Success: " + szMsg);
					return finish(null, true);
				}
			} else if (szMsg.match(/You cast (.*) on (.*), but it is surpassed by (.*)/i)) {
				var spellName = RegExp.$1;
				if (spellName == spell.szName) {
					logger(core.MAJOR + " <castSpell|OnChatServer> Success: " + szMsg);
					return finish(null, true);
				}
			} else if (szMsg.match(/^You cast (.*) on/)) {
				var spellName = RegExp.$1;
				if (spellName == spell.szName) {
					logger(core.MAJOR + " <castSpell|OnChatServer> Success: " + szMsg);
					return finish(null, true);
				}
			} else if (szMsg.match(/(.*) resists your spell/)) {
				// Only check for resists on non-projectile type spells. Otherwise if we launch a bolt and start casting another this will catch the previous bolt's resist message.
				if (!spell || (spell.skid != skidWarMagic && spell.skid != skidVoidMagic)) {
					logger(core.MAJOR + " <castSpell|OnChatServer> Failure: " + szMsg);
					return finish(new core.Error("SPELL_RESISTED"));
				}
			} else if (szMsg.match(/The spell consumed the following components: (.*)/)) {
				return;
			} else if (szMsg.match(/(.*) has defeated (.*)!/)) {
				return;
			} else if (szMsg.match(/With (.*) you restore (\d*) points of (\w*) to (.*)./)) {
				logger(core.MAJOR + " <castSpell|OnChatServer> Success: " + szMsg);
				return finish(null, true);
			} else if (szMsg.match(/Target is out of range!/)) {
				logger(core.MAJOR + " <castSpell|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("SPELL_OUT_OF_RANGE"));
			} else if (szMsg.match(/You lose (\d*) points of (\w*) due to casting (.*) on (.*)/)) {
				logger(core.MAJOR + " <castSpell|OnChatServer> Success: " + szMsg);
				return finish(null, true);
			} else if (szMsg.match(/Your movement disrupted spell casting!/)) {
				logger(core.MAJOR + " <castSpell|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("MOVEMENT_DISRUPTED"));
			} else if (szMsg.match(/You fail to affect (.*) with (.*)/)) {
				logger(core.MAJOR + " <castSpell|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("FAILED_TO_AFFECT"));
			}
			logger(core.MAJOR + " <castSpell|OnChatServer> UNCAUGHT szMsg: '" + szMsg + "'");
		};
		core.addHandler(evidOnChatServer, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You're too busy/)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You do not have all of this spell's components/)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("MISSING_SPELL_COMPONENTS"));
			} else if (szMsg.match(/You don't know that spell!/)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("DONT_KNOW_SPELL"));
			} else if (szMsg.match(/You don't have enough Mana to cast this spell./)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("NOT_ENOUGH_MANA"));
			} else if (szMsg.match(/That item doesn't have enough Mana./)) {
				return;
			} else if (szMsg.match(/Casting (.*)/)) {
				return;
			} else if (szMsg.match(/You are unprepared to cast a spell/)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("UNPREPARED"));
			} else if (szMsg.match(/In Portal Space - Please Wait.../))  {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("IN_PORTAL_SPACE"));
			} else if (szMsg.match(/Your projectile spell mislaunched!/)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("PROJECTILE_MISLAUNCHED"));
			} else if (szMsg.match(/Unable to move to object!/)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("UNABLE_MOVE_OBJECT"));
			} else if (szMsg.match(/Out of Range!/)) { // Emulator
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("SPELL_OUT_OF_RANGE"));
			} else if (szMsg.match(/You cannot cast this spell upon yourself/)) {
				logger(core.MAJOR + " <castSpell|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANT_CAST_ON_SELF"));
			} else if (szMsg.match(/You don't have all the components for this spell./)) {
				logger(core.MAJOR + " <castSpell|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("INSUFFICIENT_SPELL_COMPONENTS"));
			} 
			logger(core.MAJOR + " <castSpell|OnTipMessage> UNCAUGHT szMsg: '" + szMsg + "'");
		};
		core.addHandler(evidOnTipMessage, handler);
		
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			if (szMsg.match(/^You have been involved in a player killer battle too recently to do that!/)) {
				logger(core.MAJOR + " <castSpell|OnChatBoxMessage> Failure: " + szMsg);
				return finish(new core.Error("PK_RESTRICTED"));
			} else if (cmc == 7 && szMsg.match(/Your spell fizzled./)) { // Sometimes doesn't fire on PhatAC 
				logger(core.MAJOR + " <castSpell|OnChatBoxMessage> Failure: " + szMsg);
				return finish(new core.Error("SPELL_FIZZLED"));
			} else if (cmc == 7 && szMsg.match(/You fail to affect (.*) because you are acting across a house boundary!/)) {
				logger(core.MAJOR + " <castSpell|OnChatBoxMessage> Failure: " + szMsg);
				return finish(new core.Error("HOUSE_BOUNDARY"));
			} else if (szMsg.match(/You fail to summon the portal!/)) {
				logger(core.MAJOR + " <castSpell|OnChatBoxMessage> Failure: " + szMsg);
				return finish(new core.Error("FAILED_SUMMON_PORTAL"));
			} else if (szMsg.match(/You must have linked with a portal in order to recall to it!/)) {
				logger(core.MAJOR + " <castSpell|OnChatBoxMessage> Failure: " + szMsg);
				return finish(new core.Error("NO_LINKED_PORTAL"));
			} else if (szMsg.match(/You say, "(.*)"/)) {
				return;
			} else if (szMsg.match(/(.*) says, "(.*)"/)) {
				return;
			} else if (szMsg.match(/You cast (.*) on (.*), refreshing (.*)/i)) {
				return; // Caught in OnChatServer
			} else if (szMsg.match(/You fail to affect (.*) with (.*)/)) { return;
			}
			logger(core.MAJOR + " <castSpell|OnChatBoxMessage> UNCAUGHT szMsg: '" + szMsg + "' cmc: " + cmc);
		};
		core.addHandler(evidOnChatBoxMessage, handler); 
		
		handler.OnObjectDestroy = function OnObjectDestroy(acoDestroyed) {
			if (acoDestroyed.oid == aco.oid) {
				logger(core.MAJOR + " <castSpell|OnObjectDestroy> Failure: " + acoDestroyed.oid);
				return finish(new core.Error("INVALID_TARGET"));
			}
		};
		core.addHandler(evidOnObjectDestroy, handler); 

		function onEffect(payload) {
			if (!skapi.acoChar || payload.object != skapi.acoChar.oid) return;
			if (payload.effect == 16) { // purple bubbles around us, happens during summoning spell.
				logger(core.MAJOR + " <castSpell|onEffect> Success: effect == " + payload.effect);
				return finish(null, true);
			} else if (payload.effect == 81) {
				logger(core.MAJOR + " <castSpell|onEffect> Success: effect == " + payload.effect);
				return finish(new core.Error("SPELL_FIZZLED"));
			} else if (payload.effect == 116) { // Lifestone recall bubbles
				logger(core.MAJOR + " <castSpell|onEffect> Success: effect == " + payload.effect);
				return finish(undefined, true);
			}
			logger(core.MAJOR + " <castSpell|onEffect> UNCAUGHT " + JSON.stringify(payload, null, "\t"));
		}
		EffectsFilter && EffectsFilter.addCallback(onEffect);

		function onAnimation(payload) {
			if (!skapi.acoChar || payload.object != skapi.acoChar.oid) return;

			if (onlyWindupAnimation == true) {
				if (payload.animation_1 == 306) {// lvl 8, 7
					logger(core.MAJOR + " <castSpell|onAnimation> Success: animation_1 == " + payload.animation_1);
					return finish(undefined, true);
				} else if (payload.animation_1 == 120) {// lvl 6
					logger(core.MAJOR + " <castSpell|onAnimation> Success: animation_1 == " + payload.animation_1);
					return finish(undefined, true);
				} else if (payload.animation_1 == 118) {// lvl 5
					logger(core.MAJOR + " <castSpell|onAnimation> Success: animation_1 == " + payload.animation_1);
					return finish(undefined, true);
				} else if (payload.animation_1 == 116) {// lvl 4
					logger(core.MAJOR + " <castSpell|onAnimation> Success: animation_1 == " + payload.animation_1);
					return finish(undefined, true);
				} else if (payload.animation_1 == 114) {// lvl 3
					logger(core.MAJOR + " <castSpell|onAnimation> Success: animation_1 == " + payload.animation_1);
					return finish(undefined, true);
				} else if (payload.animation_1 == 112) {// lvl 2
					logger(core.MAJOR + " <castSpell|onAnimation> Success: animation_1 == " + payload.animation_1);
					return finish(undefined, true);
				}
			}

			if (payload.animation_type == 8) {
				// Turning to object.
				return;
			}

			if (payload.animation_1 == 47) {
				// Protection other
				return;
			} else if (payload.animation_1 == 57) {
				// Protection self
				return;
			}

			if (payload.animation_1 == 51) {
				// Launch bolt animation.
				logger(core.MAJOR + " <castSpell|onAnimation> Success: animation_1 == " + payload.animation_1);
				return finish(undefined, true);
			}
		}
		AnimationFilter && AnimationFilter.addCallback(onAnimation);
		
		logger(core.MAJOR + " Calling skapi.CastSpell(" + spell.szName + ", " + aco + ")...");
		skapi.CastSpell(spellid, aco);	
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 5000, finish, "CAST_SPELL_TIMED_OUT");
	};

	var noop = core.noop = function noop() {}; // no operation.
	core.useOnAco = function useOnAco(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var onChance = (!params || params.onChance === undefined ? noop : params.onChance);
		
		var aco = params.aco;
		var acoTarget = params.acoTarget;
		logger(core.MAJOR + " <useOnAco>", "aco: " + aco, "acoTarget: " + acoTarget);

		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		if (!aco || !aco.fExists) {
			return finish(new core.Error("ACO_DOESNT_EXIST"));
		} else if (!acoTarget || !acoTarget.fExists) {
			return finish(new core.Error("ACO_TARGET_DOESNT_EXIST"));
		}
		
		var handler = {};
		handler.OnObjectDestroy = function OnObjectDestroy(acoDestroyed) {
			if (acoDestroyed.oid == acoTarget.oid) {
				return;
			}
			logger(core.MAJOR + " <useOnAco|OnObjectDestroy> UNCAUGHT: " + acoDestroyed.szName);
		};
		core.addHandler(evidOnObjectDestroy, handler); 
		
		handler.OnChatServer = function OnChatServer(szMsg, cmc) { // cmc
			if (cmc == 0 && szMsg.match(/The (.*) drains (.*) points of mana from the (.*)./gm)) {
				var szItem = RegExp.$1;
				var szTarget = RegExp.$3;
				logger(core.MAJOR + " szItem: " + szItem, "szTarget: " + szTarget);
				if (szItem == aco.szName) {
					logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
					return finish(undefined, true);
				}
				return;
			} else if (cmc == 0 && szMsg.match(/The (.*) is already full of mana./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("ALREADY_FULL"));
			} else if (cmc == 24 && szMsg.match(/(.*) fails to apply the (.*) \(workmanship (.*)\) to the (.*)\. The target is destroyed\./)) {
				var who = RegExp.$1;
				var salvage = RegExp.$2;
				var workmanship = Number(RegExp.$3);
				var item = RegExp.$4;
				logger(core.MAJOR + " who: " + who, "salvage: " + salvage, "workmanship: " + workmanship, "item: " + item);
				if (who == skapi.acoChar.szName) {
					logger(core.MAJOR + " <useOnAco|OnChatServer> Failure: " + szMsg);
					return finish(new core.Error("TINK_FAILED"));
				}
			} else if (cmc == 24 && szMsg.match(/(.*) successfully applies the (.*) \(workmanship (.*)\) to the (.*)\./)) {
				var who = RegExp.$1;
				var salvage = RegExp.$2;
				var workmanship = Number(RegExp.$3);
				var item = RegExp.$4;
				logger(core.MAJOR + " who: " + who, "salvage: " + salvage, "workmanship: " + workmanship, "item: " + item);
				if (who == skapi.acoChar.szName) {
					logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
					return finish(undefined, true);
				}
			} else if (cmc == 24 && szMsg.match(/The target item has been imbued already!/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("ALREADY_IMBUED"));
			} else if (szMsg.match(/You combine/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You've combined/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You place/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You've fashioned/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You make/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You attach/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You add/)) { //You add the spirit to the essence
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/This essence is already full/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("ALREADY_FULL"));
			} else if (szMsg.match(/You fail to make any (.*)./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("MAKE_FAILED"));
			} else if (szMsg.match(/You fail to make the (.*) into (.*)./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("MAKE_FAILED"));
			} else if (szMsg.match(/The mana scarab infuses the quill with its magical power./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You dip the quill into the magical ink, further empowering it./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You trace the rune with your enchanted quill and the pattern of the spell forms before you as a scroll./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/The (.*) has been unlocked./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You carefully adjust the teeth on the mangled key returning it to a state of usability./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You effortlessly realign the twisted key careful not to snap the rigid marble./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You cast (.*) on yourself/)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/A sigil rises to the surface as you bathe the aetheria in mana. /)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You have already bathed this aetheria in mana./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/The (.*) gives (.*) mana to the (.*)./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/The (.*) Mana Charge is destroyed./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Success: " + szMsg);
				return finish(undefined, true);
			} else if (cmc == 0 && szMsg.match(/You have no items equipped that need mana./)) {
				logger(core.MAJOR + " <useOnAco|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("NO_MANA_NEEDED"));
			}

			logger(core.MAJOR + " <useOnAco|OnChatServer> UNCAUGHT szMsg: '" + szMsg + "'");
		};
		core.addHandler(evidOnChatServer, handler);
		
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/Using the (.*) with the (.*)/)) {
				return;
			} else if (szMsg.match(/Source item not found!/)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("SOURCE_ITEM_NOT_FOUND"));
			} else if (szMsg.match(/You're too busy!/)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) cannot be used/)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANNOT_BE_USED"));
			} else if (szMsg.match(/You cannot use (.*) on (.*)./)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANNOT_BE_USED"));
			} else if (szMsg.match(/You can't lock or unlock what is open!/)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("ALREADY_OPEN"));
			} else if (szMsg.match(/The lock is already unlocked./)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("ALREADY_UNLOCKED"));
			} else if (szMsg.match(/Using the (.*) on (.*)/)) {
				return;
			} else if (szMsg.match(/Action cancelled!/)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("ACTION_CANCELLED"));
			} else if (szMsg.match(/The (.*) drains (.\d) points of mana from the (.*)./)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Success: " + szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/You must be at rest in peace mode to do trade skills./)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("MUST_BE_IN_PEACE"));
			} else if (szMsg.match(/You must be at rest in peace mode to do trade skills/)) {
				logger(core.MAJOR + " <useOnAco|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANT_IN_COMBAT"));
			}

			logger(core.MAJOR + " <useOnAco|OnTipMessage> UNCAUGHT szMsg: '" + szMsg + "'");
		};
		core.addHandler(evidOnTipMessage, handler);

		handler.OnCraftConfirm = function OnCraftConfirm(szMsg) {
			if (szMsg.match(/You determine that you have a (.*)% chance to succeed./)) {
				// Caller script can decide to click the yes/no confirmation box. 
				var chance = Number(RegExp.$1);
				return onChance(chance);
			}

			logger(core.MAJOR + " <useOnAco|OnCraftConfirm> UNCAUGHT szMsg: '" + szMsg + "'");
		};
		core.addHandler(evidOnCraftConfirm, handler);
		
		/*
		handler.OnChatBoxMessage = function OnChatBoxMessage(szMsg, cmc) {
			logger(core.MAJOR + " <useOnAco|OnChatBoxMessage> UNCAUGHT szMsg: '" + szMsg + "' cmc: " + cmc);
		};
		core.addHandler(evidOnChatBoxMessage, handler); 
		*/
		
		logger(core.MAJOR + " Using " + aco + " on " + acoTarget + "...");
		aco.UseOnAco(acoTarget);
		var tid = core.setTimeout(core.timedOut, params && params.timeout || 10*1000, finish, "USE_ON_ACO_TIMED_OUT");
	};

	core.equipItem = function equipItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var offhand = (!params || params.offhand === undefined ? false : params.offhand);
		var aco = params.aco;

		function finish() { // err, result
			tid && core.clearTimeout(tid);
			tid2 && core.clearTimeout(tid2);
			handler && core.removeHandler(evidNil, handler);

			// Call callback in a timer so ensure asynchronicity.
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);	
			callback = undefined;
		}

		if (!aco) {
			return finish(new core.Error("MISSING_ACO"));
		}

		if (aco.eqmWearer & eqmAll) {
			logger(core.MAJOR + " Item already equipped");
			finish(undefined, true);
			return;
		}

		var handler = {};
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You can only move or use one item at a time/)) {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/In Portal Space - Please Wait.../)) {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("IN_PORTAL_SPACE"));
			} else if (szMsg.match(/The (.*) can't be put in the container/)) {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/You're too busy!/))   {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/A shield may not be worn with the (.*)/))   {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("SHILED_WITH_WEAPON")); //(_e.results.cantEquip);
			} else if (szMsg.match(/You cannot move or use an item while attacking/)) {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANT_WHILE_ATTACKING"));
			} else if (szMsg.match(/The (.*) can't be wielded/)) {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANT_BE_WIELDED"));
			} else if (szMsg.match(/You're already wearing/)) {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("SLOT_IN_USE"));
			} else if (szMsg.match(/You must remove your (.*) to wear that/)) {
				var itemName = RegExp.$1;
				var err = new core.Error("SLOT_IN_USE");
				err.itemName = itemName
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(err);
			} else if (szMsg.match(/Moving (.*) to your backpack/)) {
				return;
			} else if (szMsg.match(/Backpack is completely full!/)) {
				logger(core.MAJOR + " <equipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("BACKPACK_IS_FULL"));
			}
			logger(core.MAJOR + " <equipItem>", "Uncaught OnTipMessage", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		var tid = core.setTimeout(finish, 4000, new core.Error("EQUIP_ITEM_TIMED_OUT"));
		
		// Continously check if we've equipped the item. 
		var tid2 = core.setInterval(function checkEquipped() {
			if (aco.eqmWearer & eqmAll) {
				logger(core.MAJOR + " <equipItem|checkEquipped> Success: aco.eqmWearer & eqmAll");
				finish(undefined, true);
			}
		}, 250);

		if (offhand == true) {
			core.autoWieldEx({oid: aco.oid, offhand: offhand});
		} else {
			aco.Use();
		}
	};

	core.autoWieldEx = function autoWieldEx(params) {
		var oid = params.oid;
		var slotID = (params.offhand == true ? 1 : 2);
		var schema = "<view title='ACHooks'><control progid='DecalControls'/><script language='JavaScript'>";
		schema += "<![CDATA[";
		schema += "function autoWieldEx(lObjectID, SlotID, Explicit, NotExplicit){hooks.AutoWieldEx(lObjectID, SlotID, Explicit, NotExplicit);}";
		schema += "]]>";
		schema += "</script></view>";
		skapi.ShowControls(schema, false);
		skapi.CallPanelFunction("ACHooks", 'autoWieldEx', oid, slotID, 0, 1);
		skapi.RemoveControls("ACHooks");
	};

	core.unequipItem = function unequipItem(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var aco = params.aco;
		
		function finish() {
			core.clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			
			// Call callback in a timer so ensure asynchronicity.
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);	
			callback = undefined;
		}

		if (aco.eqmWearer == 0) {
			return finish();
		}

		var handler = {};
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/You can only move or use one item at a time/)) {
				logger(core.MAJOR + " <unequipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/In Portal Space - Please Wait.../)) {
				logger(core.MAJOR + " <unequipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("IN_PORTAL_SPACE"));
			} else if (szMsg.match(/The (.*) can't be put in the container/)) {
				logger(core.MAJOR + " <unequipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("UNABLE_TO_MOVE_OBJECT"));
			} else if (szMsg.match(/You're too busy!/))   {
				logger(core.MAJOR + " <unequipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/A shield may not be worn with the (.*)/))   {
				logger(core.MAJOR + " <unequipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("SHILED_WITH_WEAPON"));
			} else if (szMsg.match(/You cannot move or use an item while attacking/)) {
				logger(core.MAJOR + " <unequipItem|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANT_WHILE_ATTACKING"));
			} else if (szMsg.match(/Backpack is completely full!/)) {
				return finish(new core.Error("BACKPACK_FULL"));
			}
			logger(core.MAJOR + " <unequipItem|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		handler.OnAddToInventory = function OnAddToInventory(aco2) {
			if (aco2.oid == aco.oid) {
				logger(core.MAJOR + " <unequipItem|OnAddToInventory> Success");
				finish(undefined, true);
			}
		};
		core.addHandler(evidOnAddToInventory, handler);

		var tid = core.setTimeout(finish, 4000, new core.Error("UNEQUIP_ITEM_TIMED_OUT"));

		aco.MoveToPack(0, 0, false);
	};

	core.openContainer = function openContainer(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var acoContainer = params.aco;
		logger(core.MAJOR + " <openContainer>", acoContainer.szName);
		
		function finish(err, results) { // err, results
			logger(core.MAJOR + " <openContainer|finish>", err, results);
			tid && core.clearTimeout(tid);
			handler && core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}
		
		if (!acoContainer || !acoContainer.fExists) {
			return finish(new core.Error("NO_EXISTS"));
		}
		
		var handler = {};
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg == "") { // empty message
			} else if (szMsg.match(/Using the (.*)/)) {
				return;
			} else if (szMsg.match(/You can only move or use one item at a time/)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) is already in use by someone else!/)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CONTAINER_IN_USE"));
			} else if (szMsg.match(/Unable to move to object!/)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("UNABLE_TO_MOVE_OBJECT"));
			} else if (szMsg.match(/AutoRun OFF/)) {
				return;
			} else if (szMsg.match(/The (.*) is already being viewed./)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CONTAINER_IN_USE"));
			} else if (szMsg.match(/Action cancelled!/)) {
				return;
			} else if (szMsg.match(/You do not yet have the right to loot the (.*)./)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("NOT_PERMITTED"));
			} else if (szMsg.match(/You may not loot the (.*) because the (.*) has generated a rare item./))  {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("NOT_PERMITTED"));
			} else if (szMsg.match(/You cannot move or use an item while attacking/)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CANT_WHILE_ATTACKING"));
			} else if (szMsg.match(/The (.*) can't be put in the container - you're too busy/))  {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The Chest is locked!/)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("CHEST_LOCKED"));
			} else if (szMsg.match(/You do not have permission to loot that corpse!/)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("NOT_PERMITTED"));
			} else if (szMsg.match(/You're too busy!/)) {
				logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
				return finish(new core.Error("TOO_BUSY"));
			} else if (szMsg.match(/The (.*) is already in use by (.*)!/)) {
				var container = RegExp.$1;
				//var chatacter = RegExp.$2;
				if (container == acoContainer.szName) {
					logger(core.MAJOR + " <openContainer|OnTipMessage> Failure: " + szMsg);
					return finish(new core.Error("CONTAINER_IN_USE"));
				}
				logger("Uncaught ", szMsg);
				return;
			}
			logger(core.MAJOR + " <openContainer|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);
		
		handler.OnCloseContainer = function OnCloseContainer(aco) {
			// In case we close the container.
			if (aco.oid == acoContainer.oid) {
				logger(core.MAJOR + " <openContainer|OnCloseContainer> Failure");
				return finish(new core.Error("CONTAINER_CLOSED"));
			}
		};
		core.addHandler(evidOnCloseContainer, handler);

		handler.OnOpenContainerPanel = function OnOpenContainerPanel(aco) {
			if (aco.oid == acoContainer.oid) {
				logger(core.MAJOR + " <openContainer|OnOpenContainerPanel> Success");
				core.setTimeout(finish, 500, undefined, true);
			}
		};
		core.addHandler(evidOnOpenContainerPanel, handler);
		
		handler.OnChatServer = function OnChatServer(szMsg, cmc) {
			if (szMsg.match(/^You don't have permission to loot the (.*)/)) {
				logger(core.MAJOR + " <openContainer|OnChatServer> Failure: " + szMsg);
				return finish(new core.Error("NO_PERMISSION"));
			}
			logger(core.MAJOR + " <openContainer|OnChatServer> UNCAUGHT", szMsg, cmc);
		};
		core.addHandler(evidOnChatServer, handler);

		logger(core.MAJOR + " calling acoContainer.Use()...");
		acoContainer.Use();
		var tid = core.setTimeout(core.timedOut, 10 * 1000, finish, "OPEN_CONTAINER_TIMED_OUT");
	};

	core.setCombatMode = function setCombatMode(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		var mode = params.mode;

		//var force = (!params || params.force === undefined ? false : params.force);
		
		function finish() {
			core.removeHandler(evidNil, handler);
			core.clearTimeout(tid);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
			callback = undefined;
		}

		//if (mode == skapi.fCombatMode && force != true) {
		//	logger(core.MAJOR + " Skapi says we're already in the proper combat mode.");
		//	return finish(undefined, true);
		//}

		// Register our event handlers.
		var handler = {};
		handler.OnCombatMode = function OnCombatMode(fOn) {
			if (fOn == mode) {
				logger(core.MAJOR + " <setCombatMode|OnCombatMode> Success: " + fOn);
				finish(undefined, true);
			} else {
				logger(core.MAJOR + " <setCombatMode|OnCombatMode> Failure: " + fOn);
				finish(new core.Error("EXITED_COMBAT"));
			}
		};
		core.addHandler(evidOnCombatMode, handler);

		// Tell skapi to set combat mode.
		logger(core.MAJOR + " Calling skapi.SetCombatMode(" + mode + ")...");
		
		// Strike key instead of skapi.SetCombatMode(mode); so that OnCombatMode will fire with a failure or success.
		skapi.keys(cmidCombatToggleCombat);
		
		// Start a timeout timer.
		var tid = core.setTimeout(core.timedOut, 3000, finish, "SET_COMBAT_TIMED_OUT");
	};

	core.acceptTrade = function acceptTrade(params, callback) {
		var logger = (!params || params.logger === undefined ? core.debug : params.logger);
		logger("<acceptTrade>");
		var timeout = params && params.timeout || 15*1000;

		function finish() {
			clearTimeout(tid);
			core.removeHandler(evidNil, handler);
			core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments); // Call callback in a timer so other event handlers can process the event.
		}
		var tid = core.setTimeout(core.timedOut, timeout, finish);
		
		var handler = {};
		handler.OnTipMessage = function OnTipMessage(szMsg) {
			if (szMsg.match(/Trade Complete!/)) {
				logger("<acceptTrade|OnTipMessage> SUCCESS", szMsg);
				return finish(undefined, true);
			} else if (szMsg.match(/Your trading partner does not have enough free slots to complete the trade!/)) {
				logger("<acceptTrade|OnTipMessage> FAILURE", szMsg);
				return finish(new core.Error("PARTNER_NOT_ENOUGH_SPACE"));
			} else if (szMsg.match(/Your trading partner is too encumbered to complete the trade!/)) {
				logger("<acceptTrade|OnTipMessage> FAILURE", szMsg);
				return finish(new core.Error("PARTNER_TOO_ENCUMBERED"));
			} else if (szMsg.match(/(.*) put invalid items in the trade!/)) {
				logger("<acceptTrade|OnTipMessage> FAILURE", szMsg);
				return finish(new core.Error("PARTNER_INVALID_ITEM"));
			} else if (szMsg.match(/The trade has been cancelled./)) {
				logger("<acceptTrade|OnTipMessage> FAILURE", szMsg);
				return finish(new core.Error("TRADE_CANCELLED"));
			} else if (szMsg.match(/That item is not valid!/)) {
				logger("<acceptTrade|OnTipMessage> FAILURE", szMsg);
				return finish(new core.Error("INVALID_ITEM"));
			} else if (szMsg.match(/Trade confirmation failed.../)) {
				logger("<acceptTrade|OnTipMessage> FAILURE", szMsg);
				return finish(new core.Error("CONFIRMATION_FAILED"));
			}
			logger(core.MAJOR + " <acceptTrade|OnTipMessage> UNCAUGHT", szMsg);
		};
		core.addHandler(evidOnTipMessage, handler);

		handler.OnTradeReset = function OnTradeReset(aco) {
			logger("<acceptTrade|OnTradeReset>", aco);
			if (aco != skapi.acoChar) {
				// reset seems to fire before Trade Complete message when the trade was accepted. So wait 100ms before resolving as a canceled trade.
				clearTimeout(tid);
				tid = core.setTimeout(finish, 500, new core.Error("OTHER_CANCELED_TRADE"));
			}
		};
		core.addHandler(evidOnTradeReset, handler);
		
		handler.OnTradeEnd = function OnTradeEnd(arc) {
			logger("<acceptTrade|OnTradeEnd>", arc);
		};
		core.addHandler(evidOnTradeEnd, handler);

		skapi.TradeAccept();
	};

	return core;
}));