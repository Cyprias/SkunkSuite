/********************************************************************************************\
	File Name:      EffectsFilter-1.0.js
	Purpose:        Watch raw server messages for visual & sound effects on entities.
	Creator:        Cyprias
	Date:           03/31/2015
	License:        MIT License	(http://opensource.org/licenses/MIT)
\*********************************************************************************************/

var MAJOR = "EffectsFilter-1.0";
var MINOR = 181020;

(function (factory) {
    if (typeof module === 'object' && module.exports) {
        module.exports = factory();
	} else {
        EffectsFilter = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	var debugging = false;
	
	var mtyVisualSoundEffect                = 0xF7550000 - 0x100000000;

	/******************************************************************************\
		debug: Print a message to chat.
	\******************************************************************************/
	core.debug = function debug(szMsg) {
		if (debugging == true) {
			skapi.OutputSz(szMsg + "\n", opmConsole);
		}
	};

	var handler = {};

	/******************************************************************************\
		fOnRawServerMessage: Process raw server messages.
	\******************************************************************************/
	handler.OnRawServerMessage = function fOnRawServerMessage(mty, mbuf) {
		switch(mty) { // Handle the types we define
		case mtyVisualSoundEffect:
			mbuf.SkipCb(4);
			
			var payload = {};
			payload.object = mbuf.Get_DWORD();
			payload.effect = mbuf.Get_DWORD();
			payload.speed = mbuf.Get_float();

			fireCallback(payload);
			break;
		}
	};

	var _callbacks = [];
	/******************************************************************************\
		fAddCallback: Add a evid callback.
	\******************************************************************************/
	core.addCallback = function addCallback(method, thisArg) {
		_callbacks.push({method:method, thisArg:thisArg});
		if (_callbacks.length == 1) {
			skapi.AddHandler(mtyVisualSoundEffect,  handler);
		}
	};
	
	/******************************************************************************\
		fRemoveCallback: Remove a callback function.
	\******************************************************************************/
	core.removeCallback = function removeCallback(method, thisArg) {
		for (var i=_callbacks.length-1; i>=0; i--) {
			if (_callbacks[i].method == method) {
				if (thisArg && thisArg != _callbacks[i].thisArg) continue;
				_callbacks.splice(i,1);
			}
		}
		if (_callbacks.length == 0) {
			skapi.RemoveHandler(mtyVisualSoundEffect,  handler);
		}
	};
	
	/******************************************************************************\
		fireCallback: Fire a registered callback.
	\******************************************************************************/
	function fireCallback() {
	//	core.debug(_callbacks.length + " callbacks to fire.");
		for ( var i=_callbacks.length-1; i >= 0; i-- ) {
			_callbacks[i].method.apply(_callbacks[i].thisArg, arguments);
		}
	};

	return core;
}));