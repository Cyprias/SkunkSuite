/********************************************************************************************\
	File:           SkunkDB-1.0.js
	Purpose:        Flat file database for Skunkworks
	Creator:        Cyprias
	Date:           11/18/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/
// I recommend flattening objects before saving then.
// File format is similar to Newline Delimited JSON (ndjson.org)

var MAJOR = "SkunkDB-1.0";
var MINOR = 190507;

(function (factory) {

	var dependencies = {};
	dependencies.EventEmitter   = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter"); 
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(dependencies);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkDB10 = factory(dependencies);
	}
}(function (dependencies) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	var EventEmitter = dependencies.EventEmitter;
	
	// OpenAsTextStream Modes
	var ForReading              = 1;  // Open a file for reading. You cannot write to this file
	var ForWriting              = 2;  // Open a file for writing
	var ForAppending            = 8;  // Open a file and write to the end of the file
	
	// OpenAsTextStream Formats
	/* eslint-disable */
	var TristateFalse           = 0;  // Open the file as ASCII
	var Tristate                = -1; // Open the file as Unicode
	var TristateUseDefault      = -2; // Open the file using the system default 
	/* eslint-enable */
	
	var consoleLog = core.consoleLog = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine("[SkunkDB] " + args.join('\t'), opmConsole);
	};
	
	if (typeof JSON === "undefined") {
		if (typeof require === "function") {
			consoleLog("Loading json2...");
			require("SkunkSuite\\json2"); // json2 creates a global object.
			require("SkunkSuite\\iso8601"); // iso8601 creates a global object.
		} else {
			consoleLog("JSON is undefined! You need to load json2.js & iso8601.js prior to SkunkDB-1.0.js in your swx file.");
			skapi.WaitEvent(1000, wemFullTimeout);
		}
	}

	function evaluateValues(params) {
		var rowValue = params.rowValue;
		var operator = params.operator;
		var targetValue = params.targetValue;

		if (operator == "==") {
			return rowValue == targetValue;
		} else if (operator == ">" || operator == "gt") {
			return rowValue > targetValue;
		} else if (operator == "<" || operator == "lt") {
			return rowValue < targetValue;
		} else if (operator == "<=" || operator == "lte") {
			return rowValue <= targetValue;
		} else if (operator == ">=" || operator == "gte") {
			return rowValue >= targetValue;
		} else if (operator == "!=") {
			return rowValue != targetValue;
		}
	}
	
	function evaluateRow(params) {
		var wheres = params.wheres;
		var row = params.row;

		var w;
		var operator;
		var rowValue;
		var targetValue;
		for (var i = 0; i < wheres.length; i++) {
			w = wheres[i];
			if (typeof row[ w.key ] === "undefined") {
				//consoleLog("Row doesn't have " + w.key + " row.");
				return false;
			}
			rowValue = row[ w.key ];
			operator = w.operator;
			targetValue = w.value;

			if (!evaluateValues({rowValue: rowValue, operator: operator, targetValue: targetValue})) {
				return false;
			}
		}
		return true;
	}
	
	function mkdirSync(path) {
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		if (!fso.FolderExists(path)) {
			var parentFolder = fso.GetParentFolderName(path);
			if (parentFolder && !fso.FolderExists(parentFolder)) {
				mkdirSync(parentFolder);
			}
			fso.CreateFolder(path);
		}
	}
	
	function checkFileExists(filePath) {
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		if (!fso.FileExists(filePath)) {
			var folder = fso.GetParentFolderName(filePath);
			if (folder && !fso.FolderExists(folder)) {
				mkdirSync(folder);
			}
			
			var file = fso.CreateTextFile(filePath, true);
			file.Close();
		}
	}
	
	function Query(params) {
		EventEmitter.call(this);
		
		//this.filePath = params && params.filePath; // Can also be set using .into() or .from().
		this.format = params && params.format || TristateUseDefault;

		this._wheres = [];
		this._updates = [];
	}
	Query.prototype = new EventEmitter();
	Query.prototype.constructor      = Query;
	
	
	core.query = function query(params) {
		return new Query(params);
	};
	
	core.select = function core_select(keys) {
		var q = new Query();
		return q.select(keys);
	};

	core.insert = function core_insert() {
		var q = new Query();
		return q.insert();
	};
	
	core.update = function core_update() {
		var q = new Query();
		return q.update();
	};
	
	core.del = function core_del() {
		var q = new Query();
		return q.del();
	};
	
	Query.prototype.into = function into(filePath) {
		this.filePath = filePath;
		checkFileExists(filePath);
		return this;
	};

	Query.prototype.from = function from(filePath) {
		this.filePath = filePath;
		checkFileExists(filePath);
		return this;
	};
	
	Query.prototype.select = function select(keys) {
		this._type = "SELECT";
		this._keys = keys;
		return this;
	};
	
	Query.prototype.update = function update() {
		this._type = "UPDATE";
		return this;
	};
	
	Query.prototype.del = function del() { // ES3 can't use 'delete' keyword.
		this._type = "DELETE";
		return this;
	};
	
	Query.prototype.set = function set(params) {
		if (params.key && params.value) {
			this._updates.push({key: params.key, value: params.value});
		} else {
			var value;
			for (var key in params) {
				if (!params.hasOwnProperty(key)) continue;
				value = params[key];
				this._updates.push({
					key  : key, 
					value: value
				});
			}
		}
		return this;
	};
	
	Query.prototype.where = function where(params) {
		if (typeof params.operator !== "undefined") {
			this._wheres.push({key: params.key, operator: params.operator, value: params.value});
		} else {
			
			// No operator given, treat each key and value as a ==.
			for (var key in params) {
				if (!params.hasOwnProperty(key)) continue;
				this._wheres.push({key: key, operator: "==", value: params[key]});
			}
		}

		return this;
	};
	
	Query.prototype.insert = function insert() {
		this._type = "INSERT";
		return this;
	};
	
	Query.prototype.values = function insert(values) {
		this._values = values;
		return this;
	};
	
	Query.prototype.limit = function limit(value) {
		this._limit = value;
		return this;
	};
		
	Query.prototype.offset = function offset(value) {
		this._offset = value;
		return this;
	};

	function isArray(arr) {
		return Object.prototype.toString.call(arr) == '[object Array]';
	}
	
	Query.prototype.execute = function execute() {
		var format = this.format;
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		
		if (this._type == "SELECT") {
			//var file = fso.getFile(this.filePath);
			//core.consoleLog("Size: " + file.Size);

			var textStream = fso.OpenTextFile(this.filePath, ForReading, false, format);
			var matches = 0;
			var rows;
			if (typeof this.events["onResult"] === "undefined" || this.events["onResult"].length == 0) {
				rows = [];
			}

			var row;
			var skipped = 0;
			var tempRow;
			while (!textStream.AtEndOfStream) {
				line = textStream.ReadLine();
				
				try {
					row = JSON.parse(line);
				} catch (e) {
					core.consoleLog("Error while JSON.parse: " + e);
					core.consoleLog("file: " + this.filePath);
					core.consoleLog("line: " + line);
					core.consoleLog("Query type: " + this._type);
					continue;
				}

				if (!evaluateRow({wheres: this._wheres, row: row})) {
					continue;
				}

				if (this._offset) {
					if (skipped < this._offset) {
						skipped += 1;
						continue;
					}
				}
				
				if (this._keys && isArray(this._keys)) {
					tempRow = row;
					row = {};
					var k;
					for (var i = 0; i < this._keys.length; i++) {
						k = this._keys[i];
						row[ k ] = tempRow[ k ];
					}
				}
				
				
				matches += 1;
				if (rows) {
					rows.push(row);
				} else {
					this.emit("onResult", row);
				}
				
				if (this._limit && matches >= this._limit) {
					break;
				}
			}

			textStream.Close();
			
			this.emit("onResults", {rows: rows});
			return {rows: rows};
		} else if (this._type == "INSERT") {
			var textStream = fso.OpenTextFile(this.filePath, ForAppending, true, format);

			var v;
			var json;
			for (var i = 0; i < this._values.length; i++) {
				v = this._values[i];
				json = JSON.stringify(v);
				textStream.WriteLine(json);
			}

			textStream.Close();
			
			this.emit("onResults", {rowsModified: this._values.length});
			return {rowsModified: this._values.length};
		} else if (this._type == "UPDATE") {
			var format = this.format;
			
			// Rename the file as a temp file, then create a new file to write the lines to.
			var filePath = this.filePath;
			var tempPath = this.filePath + "_" + new Date().getTime();

			// Rename the old file.
			fso.MoveFile(filePath, tempPath);

			// Get streams for both.
			var oldStream = fso.OpenTextFile(tempPath, ForReading, true, format);
			var newStream = fso.OpenTextFile(filePath, ForWriting, true, format);
			
			var row;
			var json;
			var skipped = 0;
			var rowsModified = 0;
			var u;
			var line;
			while (!oldStream.AtEndOfStream) {
				line = oldStream.ReadLine();
				try {
					row = JSON.parse(line);
				} catch (e) {
					core.consoleLog("Error while JSON.parse: " + e);
					core.consoleLog("file: " + this.filePath);
					core.consoleLog("line: " + line);
					core.consoleLog("Query type: " + this._type);
					continue;
				}
			
			
				if (evaluateRow({wheres: this._wheres, row: row})) {
					if (!this._limit || rowsModified < this._limit) {
						
						if (this._offset) {
							if (skipped < this._offset) {
								skipped += 1;
								
								json = JSON.stringify(row);
								newStream.WriteLine(json);
								
								continue;
							}
						}
						
						
						for (var i = 0; i < this._updates.length; i++) {
							u = this._updates[i];
							row[ u.key ] = u.value;
						}
						rowsModified += 1;
					}
				}
				
				json = JSON.stringify(row);
				newStream.WriteLine(json);
			}
			
			oldStream.Close();
			newStream.Close();
			
			fso.DeleteFile(tempPath);
			
			this.emit("onResults", {rowsModified: rowsModified});
			return {rowsModified: rowsModified};
		} else if (this._type == "DELETE") {
			var format = this.format;
			
			// Rename the file as a temp file, then create a new file to write the lines to.
			var filePath = this.filePath;
			var tempPath = this.filePath + "_" + new Date().getTime();
			
			// Rename the old file.
			fso.MoveFile(filePath, tempPath);

			// Get streams for both.
			var oldStream = fso.OpenTextFile(tempPath, ForReading, true, format);
			var newStream = fso.OpenTextFile(filePath, ForWriting, true, format);
			
			var row;
			var json;
			var rowsSkipped = 0;
			var rowsModified = 0;
			var u;
			var line;
			while (!oldStream.AtEndOfStream) {
				line = oldStream.ReadLine();
				try {
					row = JSON.parse(line);
				} catch (e) {
					core.consoleLog("Error while JSON.parse: " + e);
					core.consoleLog("file: " + this.filePath);
					core.consoleLog("line: " + line);
					core.consoleLog("Query type: " + this._type);
					continue;
				}
			
			
				if (evaluateRow({wheres: this._wheres, row: row})) {
					if (this._offset && rowsSkipped < this._offset) {
						rowsSkipped += 1;
						json = JSON.stringify(row);
						newStream.WriteLine(json);
						continue;
					}
					
					if (!this._limit || rowsModified < this._limit) {
						rowsModified += 1;
						continue;
					}
				}
				
				json = JSON.stringify(row);
				newStream.WriteLine(json);
			}
			
			oldStream.Close();
			newStream.Close();
			
			fso.DeleteFile(tempPath);
			
			this.emit("onResults", {rowsModified: rowsModified});
			return {rowsModified: rowsModified};
		}
	};
	Query.prototype.exec = Query.prototype.execute;

	// Clear a file of its existing values.
	core.truncate = function core_truncate(filePath) {
		var fso = new ActiveXObject("Scripting.FileSystemObject");
		var textStream = fso.OpenTextFile(filePath, ForWriting, true, TristateUseDefault);
		textStream.Close();
	};
	
	return core;
}));