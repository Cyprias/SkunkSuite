# SkunkDB-1.0

## Synopsis

A flatfile based database which writes objects as json encoded strings. Each line in the file is a seperate row. 

## Motivation

I wanted to have a SQL like interface for reading and writing infomation to files that would be shared amongst multiple game clients running on a single machine. 

## Example usage
```
var SkunkDB = require("SkunkSuite\\SkunkDB-1.0");
var data = [];
data.push({name:"Bob", age:30});
data.push({name:"Sam", age:50});
data.push({name:"Tom", age:20});
SkunkDB.insert().into("someFile.txt").values(data).execute();

var results = SkunkDB.select().from("someFile.txt").where({name:"Tom"}).execute();
debug("Tom is " + results.rows[0].age + " years old.");
```


## Contributors
- Cyprias

## License
MIT License	(http://opensource.org/licenses/MIT)