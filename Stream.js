(function (factory) {
	// Dependencies
	var _EventEmitter = (typeof EventEmitter !== "undefined" && EventEmitter) || require("SkunkSuite\\EventEmitter");
	if (typeof _EventEmitter === "undefined") {
		throw new Error("EventEmitter.js isn't loaded.");
	}
	
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(_EventEmitter);
	} else {
		// eslint-disable-next-line no-undef
		Stream = factory(_EventEmitter);
	}
}(function (EventEmitter) {
	var debugging = false;

	// eslint-disable-next-line no-unused-vars
	var debug = function debug() {
		if (debugging == true) {
			skapi.OutputLine("[Stream.js] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	};

	function Stream() {
		EventEmitter.call(this);
		this.paused = false;
		this.buffer = [];
	}

	Stream.prototype = new EventEmitter();
	Stream.prototype.constructor      = Stream;

	Stream.prototype.pipe = function pipe(dest) {
		var source = this;

		function ondata(chunk) {
			dest.write(chunk);
		}
		
		source.on('data', ondata);

		function onend() {
			dest.end();
		}
		
		source.on('end', onend);

		// remove all the event listeners that were added.
		function cleanup() {
			source.removeListener('data', ondata);
			source.removeListener('end', onend);
			source.removeListener('end', cleanup);
		}

		source.on('end', cleanup);

		// tell the dest that it's being piped to
		dest.emit('pipe', source);
		
		// Allow for unix-like usage: A.pipe(B).pipe(C)
		return dest; 
	};

	Stream.prototype.write = function write(data) {
		if (this.paused) {
			this.buffer.push(data);
			return false;
		}

		this.emit('data', data);
		return !this.paused;
	};

	Stream.prototype.end = function end(data) {
		if (data) {
			this.write(data);
		}
		this.emit("end");
		return this;
	};
	
	Stream.prototype.pause = function pause() {
		this.paused = true;
		return this;
	};
	
	Stream.prototype.resume = function resume() {
		if (this.paused) {
			this.paused = false;
			this.emit('resume');
		}
		this.drain();
		if (!this.paused) {
			this.emit('drain');
		}
		return this;
	};
	
	Stream.prototype.drain = function drain() {
		while (this.buffer.length && !this.paused) {
			var data = this.buffer.splice(0, 1)[0];
			this.write(data);
		}
	};
	
	return Stream;
}));