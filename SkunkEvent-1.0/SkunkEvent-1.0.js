/********************************************************************************************\
	File:           SkunkEvent-1.0.js
	Purpose:        Event handler for Skunkworks.
	Creator:        Cyprias
	Date:           09/17/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkEvent-1.0";
var MINOR = 190730;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkEvent10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;

	core.embeds = core.embeds || {};
	core.handlers = core.handlers || {};

	core.debugging = false;
	core.debug = function debug() {
		if (core.debugging != true) return;
		skapi.OutputLine(Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};
	
	var fnRE = /function(?:\s+([\w$]+))?\s*\(/;
	function getFunctionName(method) {
		return fnRE.test(method.toString()) ? RegExp.$1 : undefined;
	}
	
	function getVariable(name) {
		return new Function('return ' + name + ';').call();
	}
	
	function addHandler(evid, handler) {
		if (typeof evid === "function") {
			var func = evid; // for santiy sake.
			var functionName = getFunctionName(func);
			var evidName = "evid" + functionName;
			evid = getVariable(evidName);

			handler = handler || {};
			handler[functionName] = func;
			
			core.handlers[this] = core.handlers[this] || {};
			core.handlers[this][func] = handler;

		} else {
			if (typeof handler === "undefined") {
				handler = this;
			}
		}
		skapi.AddHandler(evid, handler);
	}
	
	function removeHandler(evid, handler) {
		if (typeof evid === "function") {
			var func = evid; // for santiy sake.
			if (typeof handler === "undefined") {
				if (typeof core.handlers[this] === "undefined") {
					return;
				}
				handler = core.handlers[this][func];
				delete core.handlers[this][func];
			}
		}
		if (typeof handler === "undefined") {
			// Clearing both will wipe all event handlers. If you need to do that, just call skapi.RemoveHandler() directly.
			core.debug(core.MAJOR, " Something is trying to wipe all event handlers!");
			throw new core.Error(core.MAJOR + " Something is trying to wipe all event handlers!");
		}
		skapi.RemoveHandler(evid, handler);
	}
	
	function unregisterAllEvents() {
		skapi.RemoveHandler(evidNil, this);
		
		if (core.handlers[this]) {
			var handler;
			for (var func in core.handlers[this]) {
				if (!core.handlers[this].hasOwnProperty(func)) continue;
				handler = core.handlers[this][func];
				skapi.RemoveHandler(evidNil, handler);
			}
		}
	}
	
	function embedMixins(script) {
		script.addHandler = addHandler;
		script.removeHandler = removeHandler;
		script.unregisterAllEvents = unregisterAllEvents;
		
		if (typeof script.on === "function") {
			script.on("onDisable", function() {
				script.unregisterAllEvents();
			});
		}
		
		return script;
	}

	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	if (typeof core.emitter !== "undefined") {
		core.emitter.on("onEmbedDisable", function(script) {
			script.unregisterAllEvents();
		});
	}
	
	return core;
}));

/*
	// Basic example.
	var core = LibStub("SkunkScript-1.0").newScript("MyScript", "SkunkEvent-1.0");
	
	core.OnAnimationSelf = function OnAnimationSelf(dwA, dwB, dwC, dwD, dwE) {
		// We did a animation.
	};
	
	core.onInitialize = function onInitialize() {};
	core.onEnable = function onEnable() {
		// Example handler.
		var handler = {};
		handler.OnTell = function OnTell(acoSender, acoReceiver, szMsg) {
			// We received a tell.
		};
		
		// Register our handler's OnTell event.
		core.addHandler(evidOnTell, handler); // Uses the 'core' object if no handler is provided.
		
		// Register OnAnimationSelf event in our 'core' object.
		core.addHandler(evidOnAnimationSelf);
		
		// Register OnLocChangeSelf right here. Note this requires the function's name to be the event name to work.
		core.addHandler(function OnLocChangeSelf(maploc) {
			// We're moving! Disable script.
			core.disable();
		});
	};
	core.onDisable = function onDisable() {
		// Any events registered via core.addHandler will auto unregister when core.disable() is called.
	};

	function main() {
		core.initialize();
		core.enable();
		while (core._enabledState == true) {
			skapi.WaitEvent(1000, wemFullTimeout);
		}
		console.StopScript(); // Prevent Skunkworks from sometimes crashing.
	}
*/