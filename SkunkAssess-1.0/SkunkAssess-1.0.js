/********************************************************************************************\
	File:           SkunkAssess-1.0.js
	Purpose:        Queued assess object helper for Skunkwork scripts.
	Creator:        Cyprias
	Date:           09/17/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkAssess-1.1";
var MINOR = 200413;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkAssess11 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	core.debugging = false;
	
	core.embeds = core.embeds || {};
	core.handlers = core.handlers || {};

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	core.setTimeout = setTimeout;
	core.setInterval = setInterval;
	core.setImmediate = setImmediate;
	core.clearTimeout = clearTimeout;

	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		skapi.OutputLine(core.MAJOR + " " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (core.debugging == true) {
			skapi.OutputLine(core.MAJOR + " [D] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	};

	function embedMixins(script) {
		script.assessAco = core.assessAco;
		script.inAssessRadius = core.inAssessRadius;
		script.queueAssessAco = core.queueAssessAco;

		return script;
	}

	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	core.inAssessRadius = function inAssessRadius(maploc) {
		// max ourdoor distance on emulator is 90m
		var maxDist = 90 / 240; //90 to attempt, success distance varries.
		if (skapi.maplocCur && skapi.maplocCur.fIndoors) {
			maxDist = 45 / 240;
		}
		return skapi.maplocCur && skapi.maplocCur.Dist3DToMaploc(maploc) < maxDist;
	};

	core.timedOut = function timedOut(callback, name) {
		callback(new core.Error(name || "TIMED_OUT"));
	};
	
	core.applyMethod = function applyMethod() {
		var args = [].slice.call(arguments);
		var thisArg;
		if (typeof args[0] !== "function") {
			thisArg = args.splice(0, 1)[0];
		}
		var method = args.splice(0, 1)[0];
		var methodArgs = args.splice(0, 1)[0];
		return method && method.apply(thisArg, methodArgs);
	};

	
	core.assessAco = function factory() {
		return function assessAco(params, callback) {
			core.debug(core.MAJOR + "<assessAco>");
			var aco = params.aco;
			var fLowPriority = params.fLowPriority === undefined ? true : params.fLowPriority;

			function finish(err, results) {
				core.debug(core.MAJOR + "<assessAco|finish>", err, results);
				handler && skapi.RemoveHandler(evidNil, handler);
				core.clearTimeout(tid);
				core.setImmediate(core.applyMethod, params && params.thisArg, callback, arguments);
				callback = undefined;
			}
			
			var handler = {};
			
			if (!aco.fExists) {
				return finish(new core.Error("NO_EXIST"));
			}
			
			
			handler.OnAssessItem = function OnAssessItem(aco2) {
				if (aco.oid == aco2.oid) {
					return finish(undefined, true);
				}
			};
			skapi.AddHandler(evidOnAssessItem,      handler);
			
			//handler.OnAssessCreature = function OnAssessCreature(aco, fSuccess, cbi, cai, chi) {};
			handler.OnAssessCreature = handler.OnAssessItem;
			skapi.AddHandler(evidOnAssessCreature,  handler);
			
			core.debug(core.MAJOR + "skapi.AssessAco(" + aco.szName + ", " + fLowPriority + ")");
			skapi.AssessAco(aco, fLowPriority);
			var tid = core.setTimeout(core.timedOut, params.timeout || 2000, finish, "ASSESS_TIMED_OUT");
		};
	}();
	
	core.assessQueue = [];
	var gtid;
	var increments = 0;
	core.queueAssessAco = function factory() {
		return function queueAssessAco(params, callback) {
			core.debug(core.MAJOR + "<queueAssessAco>", params.aco.szName);
			var fLowPriority = params.fLowPriority === undefined ? true : params.fLowPriority;
			var priority = params.priority === undefined ? 0 : params.priority;
			core.assessQueue.push({
				id          : increments,
				aco         : params.aco,
				fLowPriority: fLowPriority,
				priority    : priority,
				time        : new Date().getTime(),
				callback    : callback
			});
			increments += 1;
			gtid = gtid || core.setImmediate(core.processQueue);
		};
	}();
		
	
	core.padString = function factory() {
		return function padString(str, maxSize) {
			if (str.length < maxSize) {
				str += Array(1 + (maxSize - str.length)).join(" ");
			}
			return str;
		};
	}();
	
	core.round = function round(rnum, rlength) { // Arguments: number to round, number of decimal places
		if (rlength == null) rlength = 0;
		return Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
	};
	
	core.processQueue = function factory() {
		function _sortQueue(a, b) {

			if (a.fLowPriority != b.fLowPriority) { 
				return a.fLowPriority - b.fLowPriority;
			}
			
			if (a.priority != b.priority) { 
				return b.priority - a.priority;
			}
			
			// Sort by Object category masks
			if (a.aco.ocm != b.aco.ocm) { 
				if (a.aco.ocm & ocmPK && !(b.aco.ocm & ocmPK)) return -1;
				if (b.aco.ocm & ocmPK && !(a.aco.ocm & ocmPK)) return 1;
				
				if (a.aco.ocm & ocmPlayer && !(b.aco.ocm & ocmPlayer)) return -1;
				if (b.aco.ocm & ocmPlayer && !(a.aco.ocm & ocmPlayer)) return 1;
				
				if (a.aco.ocm & ocmMonster && !(b.aco.ocm & ocmMonster)) return -1;
				if (b.aco.ocm & ocmMonster && !(a.aco.ocm & ocmMonster)) return 1;
				
				if (a.aco.ocm & ocmPlayerCorpse && !(b.aco.ocm & ocmPlayerCorpse)) return -1;
				if (b.aco.ocm & ocmPlayerCorpse && !(a.aco.ocm & ocmPlayerCorpse)) return 1;
				
				if (a.aco.ocm & ocmMonsterCorpse && !(b.aco.ocm & ocmMonsterCorpse)) return -1;
				if (b.aco.ocm & ocmMonsterCorpse && !(a.aco.ocm & ocmMonsterCorpse)) return 1;
				
				if (a.aco.ocm & ocmPortal && !(b.aco.ocm & ocmPortal)) return -1;
				if (b.aco.ocm & ocmPortal && !(a.aco.ocm & ocmPortal)) return 1;
				
				if (a.aco.ocm & ocmEquipment && !(b.aco.ocm & ocmEquipment)) return -1;
				if (b.aco.ocm & ocmEquipment && !(a.aco.ocm & ocmEquipment)) return 1;
			}

			// Sort by Object type masks
			if (a.aco.oty != b.aco.oty) { 
				if (a.aco.oty & otyPK && !(b.aco.oty & otyPK)) return -1;
				if (b.aco.oty & otyPK && !(a.aco.oty & otyPK)) return 1;
				
				if (a.aco.oty & otyPKL && !(b.aco.oty & otyPKL)) return -1;
				if (b.aco.oty & otyPKL && !(a.aco.oty & otyPKL)) return 1;
				
				if (a.aco.oty & otyPlayer && !(b.aco.oty & otyPlayer)) return -1;
				if (b.aco.oty & otyPlayer && !(a.aco.oty & otyPlayer)) return 1;

				if (a.aco.oty & otyCorpse && !(b.aco.oty & otyCorpse)) return -1;
				if (b.aco.oty & otyCorpse && !(a.aco.oty & otyCorpse)) return 1;

				if (a.aco.oty & otyPortal && !(b.aco.oty & otyPortal)) return -1;
				if (b.aco.oty & otyPortal && !(a.aco.oty & otyPortal)) return 1;

				if (a.aco.oty & otySelectable && !(b.aco.oty & otySelectable)) return -1;
				if (b.aco.oty & otySelectable && !(a.aco.oty & otySelectable)) return 1;

				if (a.aco.oty & otyInscribable && !(b.aco.oty & otyInscribable)) return -1;
				if (b.aco.oty & otyInscribable && !(a.aco.oty & otyInscribable)) return 1;
			}

			// Sort by object location catagory.
			if (a.aco.olc != b.aco.olc) { 
				//return a.aco.olc - b.aco.olc;
				if (a.aco.olc & olcEquipped && !(b.aco.olc & olcEquipped)) return -1;
				if (b.aco.olc & olcEquipped && !(a.aco.olc & olcEquipped)) return 1;
				
				if (a.aco.olc & olcOnGround && !(b.aco.olc & olcOnGround)) return -1;
				if (b.aco.olc & olcOnGround && !(a.aco.olc & olcOnGround)) return 1;
				
				if (a.aco.olc & olcInventory && !(b.aco.olc & olcInventory)) return -1;
				if (b.aco.olc & olcInventory && !(a.aco.olc & olcInventory)) return 1;
				
				if (a.aco.olc & olcContained && !(b.aco.olc & olcContained)) return -1;
				if (b.aco.olc & olcContained && !(a.aco.olc & olcContained)) return 1;
			}

			if (a.aco.mcm != b.aco.mcm ) {
				var mcmWeapons = mcmWeaponsMelee | mcmWeaponsMissile | mcmMagicItems;
				if (a.aco.mcm & mcmWeapons && !(b.aco.mcm & mcmWeapons)) return -1;
				if (b.aco.mcm & mcmWeapons && !(a.aco.mcm & mcmWeapons)) return 1;
				
				if (a.aco.mcm & mcmArmor && !(b.aco.mcm & mcmArmor)) return -1;
				if (b.aco.mcm & mcmArmor && !(a.aco.mcm & mcmArmor)) return 1;
			}

			// Sort by distance.
			var aDist = a.aco.olc & olcOnGround && skapi.maplocCur.Dist3DToMaploc(a.aco.maploc) || 0;
			var bDist = b.aco.olc & olcOnGround && skapi.maplocCur.Dist3DToMaploc(b.aco.maploc) || 0;
			if (aDist != bDist) {
				return aDist - bDist;
			}
			
			// Newest first.
			return a.time - b.time;
		}
		return function processQueue() {
			core.debug(core.MAJOR + "<processQueue>");

			core.assessQueue = core.assessQueue.filter(function(queue) {
				if (!queue.aco.fExists) {
					if (queue.callback) {
						queue.callback(new core.Error("NO_EXIST"));
					}
				}
				return queue.aco.fExists;
			});

			if (core.assessQueue.length == 0) {
				gtid = undefined;
				return;
			}
			
			core.assessQueue.sort(_sortQueue);
			
			/*
			var nameSize = core.assessQueue.reduce(function(tally, info) {
				return Math.max(tally, info.aco.szName.length);
			}, 0);
			
			core.assessQueue.forEach(function(info, i) {
				var dist = info.aco.olc & olcOnGround && skapi.maplocCur.Dist3DToMaploc(info.aco.maploc) || 0;
				var elapsed = new Date().getTime() - info.time;
				// "fLP: " + info.fLowPriority, "priority: " + info.priority, 
				core.console(i, info.id, core.padString(info.aco.szName, nameSize),"olc: " + info.aco.olc, "ocm: " + info.aco.ocm, "oty: " + info.aco.oty, "elapsed: " + core.round(elapsed), "dist: " + core.round(dist * 240));
			});
			*/

			var info = core.assessQueue[0];
			var dist = info.aco.olc & olcOnGround && skapi.maplocCur.Dist3DToMaploc(info.aco.maploc) || 0;
			var elapsed = new Date().getTime() - info.time;
			core.debug(core.MAJOR + "Assessing #" + info.id + "/" + increments + " (" + core.assessQueue.length + ")", info.aco.szName, "olc: " + info.aco.olc, "ocm: " + info.aco.ocm, "oty: " + info.aco.oty, "elapsed: " + core.round(elapsed), "dist: " + core.round(dist * 240));
			
			return core.assessAco({
				aco         : info.aco,
				fLowPriority: info.fLowPriority
			}, function onAssess(err, results) {
				core.debug(core.MAJOR + "<processQueue|onAssess>", err, results);
				core.assessQueue = core.assessQueue.filter(function(queue) {
					if (info.aco.oid != queue.aco.oid) {
						return true;
					}
					if (queue.callback) {
						queue.callback(err, results);
					}
					return false;
				});
				core.setImmediate(processQueue);
			});
		};
	}();

	return core;
}));