/********************************************************************************************\
	File:           SkunkLayout-1.0.js
	Purpose:        UI mouse position for Skunkworks.
	Creator:        Cyprias
	Date:           09/17/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       - Skunkworks (http://skunkworks.sourceforge.net/)
	                - SkunkFS-1.0.js
\*********************************************************************************************/

var MAJOR = "SkunkLayout-1.0";
var MINOR = 230317;

(function (factory) {
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// eslint-disable-next-line no-undef
		SkunkLayout10 = factory();
	}
}(function () {
	var debugging = false;
	var fs = (typeof LibStub !== "undefined" && LibStub("SkunkFS-1.0", true)) || require("SkunkSuite\\SkunkFS-1.0");

	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;

	core.debug = function debug() {
		if (debugging != true) return;
		skapi.OutputLine(Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};

	
	var layout;

	core.loadUI = function loadUI(filename) {
		if (!core.exists(filename)) {
			throw new core.Error(filename + " does not exist.");
		}
		layout = core.getParsedUI(filename);
		skapi.InvokeChatParser("/loadui " + filename);
	};
	
	core.getCurrentLayout = function getCurrentLayout() {
		return layout;
	};
	
	core.getUIPath = function getUIPath(filename) {
		var fShell = new ActiveXObject("WScript.Shell");
		var userDocuments = fShell.SpecialFolders("MyDocuments");
		var userAcDir = userDocuments + "/Asheron's Call";
		return userAcDir + "/" + filename + ".txt";
	};
	
	core.exists = function exists(filename) {
		var fullPath = core.getUIPath(filename);
		return fs.existsSync(fullPath);
	};
	
	core.getRawUI = function getRawUI(filename) {
		var fullPath = core.getUIPath(filename);
		if (!fs.existsSync(fullPath)) {
			throw new core.Error(fullPath + " does not exist.");
		}
		return fs.readFileSync(fullPath);
	};
	
	core.parseUIContent = function parseUIContent(content) {
		// parse turbine's inconsistent save format.
		var fixSpacing = content.replace(/:\s/g, ":");
		
		var rawSettings = fixSpacing.split("\n");
		if (rawSettings.length > 0) {
			var results = {};
			for (var i = 0, l = rawSettings.length; i < l; i++) {
				var panelSettingsStr = rawSettings[i];
				if (panelSettingsStr.length > 0) {
					var name = panelSettingsStr.substring(panelSettingsStr.lastIndexOf("<") + 1, panelSettingsStr.lastIndexOf(">"));
					var settings = panelSettingsStr.split(" ");
					var xStr = settings[1];
					var yStr = settings[2];
					var wStr = settings[3];
					var hStr = settings[4];
					results[name] = {
						x: new Number(xStr.substr(xStr.indexOf(":") + 1)),
						y: new Number(yStr.substr(yStr.indexOf(":") + 1)),
						w: new Number(wStr.substr(wStr.indexOf(":") + 1)),
						h: new Number(hStr.substr(hStr.indexOf(":") + 1))
					};
				}
			}
			return results;
		}
	};
	
	core.getParsedUI = function getParsedUI(filename) {
		var content = core.getRawUI(filename);
		return core.parseUIContent(content);
	};
	
	core.deleteUI = function deleteUI(filename) {
		var path = core.getUIPath(filename);
		fs.unlinkSync(path);
	};
	
	/*
	CHAT: Chat box.
	COMB: Combat bar?
	ENVP: Merchant window.
	EXAM: Examine/assess window.
	FCH1: ?
	FCH2: ?
	FCH3: ?
	FCH4: ?
	INDI: ?
	PANS: Multi panel (social, spell, map, inventory)
	PBAR: ?
	RADA: Radar
	SBOX: 3D area
	SVIT: Side by side vitals?
	TBAR: Hotkey bar
	VITS: Stacked vitals	
	*/
	
	core.Position = function Position(data) {
		if (typeof data === "object") {
			for (var key in data) {
				if (!data.hasOwnProperty(key)) continue;
				this[key] = data[key];
			}
		}
	};

	Position.prototype.setPanelName = function (value) {
		this.panelName = value;
		return this;
	};
	Position.prototype.setX = function (value) {
		this.x = value;
		return this;
	};
	Position.prototype.setY = function (value) {
		this.y = value;
		return this;
	};
	Position.prototype.setOffsetX = function (value) {
		this.offsetX = value;
		return this;
	};
	Position.prototype.setOffsetY = function (value) {
		this.offsetY = value;
		return this;
	};
	Position.prototype.setOffsetW = function (value) {
		this.offsetW = value;
		return this;
	};
	Position.prototype.setOffsetH = function (value) {
		this.offsetH = value;
		return this;
	};
	Position.prototype.setOffsetIndexX = function (value) {
		this.offsetIndexX = value;
		return this;
	};
	Position.prototype.setOffsetIndexY = function (value) {
		this.offsetIndexY = value;
		return this;
	};
	Position.prototype.setWindowXModifier = function (value) {
		this.windowXModifier = value;
		return this;
	};
	Position.prototype.setWindowYModifier = function (value) {
		this.windowYModifier = value;
		return this;
	};
		
	Position.prototype.mouseMove = function mouseMove(index) {
		var coords = this.getCoordinates(index);
		skapi.MouseMove(coords.x, coords.y);
		return this;
	};

	Position.prototype.click = function (index) {
		this.mouseMove(index);
		skapi.MouseButton(ibuttonLeft, kmoClick);
		return this;
	};
	
	Position.prototype.getCoordinates = function getCoordinates(index) {
	//	debug("<getCoordinates> " + index);
		var myX = this.x || 0;
		var myY = this.y || 0;
		
		if (this.windowXModifier) {
			myX = skapi.dxpWindow * this.windowXModifier;
		}
		if (this.windowYModifier) {
			myY = skapi.dypWindow * this.windowYModifier;
		}
		
		if (typeof this.panelName !== "undefined") {
			if (typeof layout === "undefined") {
				core.loadUILayout();
			}
			
			if (typeof layout === "undefined") {
				core.debug(core.MAJOR, "A UI layout has not been loaded yet.");
				throw new core.Error("A UI layout has not been loaded yet.");
			}
			
			
			var panel = layout[this.panelName];
			myX = panel.x;
			myY = panel.y;
			if (this.offsetW) {
				myX += (panel.w + this.offsetW);
			}
			if (this.offsetH) {
				myY += (panel.h + this.offsetH);
			}
		}
		if (this.offsetX) {
			myX += this.offsetX;
		}
		if (this.offsetY) {
			myY += this.offsetY;
		}
		if (typeof index !== "undefined") {
			if (typeof this.offsetIndexX !== "undefined") {
				myX += (this.offsetIndexX * index);
			}
			if (typeof this.offsetIndexY !== "undefined") {
				myY += (this.offsetIndexY * index);
			}
		}
		return {x: myX, y: myY};
	};

	// Positions
	
	// Login screen positions.
	core.enterWorld                     = new Position()
		.setX(340)
		.setY(390);
		
	core.characterSlot                  = new Position()
		.setX(100)
		.setY(220)
		.setOffsetY(32);
	
	// Window positions. Positions that relate to window resolution.
	core.messageBoxOK                   = new Position()
		.setWindowXModifier(0.5)
		.setWindowYModifier(0.5)
		.setOffsetY(24);
	
	// Panel positions.
	core.allegianceIgnoreRequests       = new Position()
		.setPanelName("PANS")
		.setOffsetX(20)
		.setOffsetH(-60);
		
	core.fellowshipAutoAcceptRequests   = new Position()
		.setPanelName("PANS")
		.setOffsetX(35)
		.setOffsetY(275);
		
	core.fellowshipCreate               = new Position()
		.setPanelName("PANS")
		.setOffsetX(150)
		.setOffsetY(345);
		
	core.fellowshipDisband              = new Position()
		.setPanelName("PANS")
		.setOffsetX(240)
		.setOffsetH(-20);
		
	core.fellowshipDismiss              = new Position()
		.setPanelName("PANS")
		.setOffsetX(150)
		.setOffsetH(-20);
		
	core.fellowshipIgnoreRequests       = new Position()
		.setPanelName("PANS")
		.setOffsetX(35)
		.setOffsetY(260);
		
	core.fellowshipLeader               = new Position()
		.setPanelName("PANS")
		.setOffsetX(60)
		.setOffsetH(-50);
		
	core.fellowshipName                 = new Position()
		.setPanelName("PANS")
		.setOffsetX(200)
		.setOffsetY(235);
		
	core.fellowshipOpen                 = new Position()
		.setPanelName("PANS")
		.setOffsetX(240)
		.setOffsetH(-50);
		
	core.fellowshipQuit                 = new Position()
		.setPanelName("PANS")
		.setOffsetX(150)
		.setOffsetH(-50);
		
	core.fellowshipRecruit              = new Position()
		.setPanelName("PANS")
		.setOffsetX(60)
		.setOffsetH(-20);
		
	core.fellowshipShareExp             = new Position()
		.setPanelName("PANS")
		.setOffsetX(35)
		.setOffsetY(290);
		
	core.fellowshipShareLoot            = new Position()
		.setPanelName("PANS")
		.setOffsetX(35)
		.setOffsetY(305);
		
	core.fellowshipSlot            = new Position()
		.setPanelName("PANS")
		.setOffsetX(150)
		.setOffsetY(80)
		.setOffsetIndexY(32);
		
		
	core.friendsAppearOffline           = new Position()
		.setPanelName("PANS")
		.setOffsetX(95)
		.setOffsetH(-135);
		
	core.socalTab                       = new Position()
		.setPanelName("PANS")
		.setOffsetX(42)
		.setOffsetY(17)
		.setOffsetIndexX(70);
		
	core.yesButton                      = new Position()
		.setX(320)
		.setY(325);
		
	core.noButton                       = new Position()
		.setX(480)
		.setY(325);

	core.examineCloseButton             = new Position()
		.setPanelName("EXAM")
		.setOffsetW(-20)
		.setOffsetY(15);
	
	core.merchantCloseButton            = new Position()
		.setPanelName("ENVP")
		.setOffsetW(-20)
		.setOffsetY(15);
		
	core.merchantItemsTab               = new Position()
		.setPanelName("ENVP")
		.setOffsetX(50).
		setOffsetY(15);
		
	core.merchantBuyingTab              = new Position()
		.setPanelName("ENVP")
		.setOffsetX(140)
		.setOffsetY(15);
		
	core.merchantSellingTab             = new Position()
		.setPanelName("ENVP")
		.setOffsetX(230)
		.setOffsetY(15);
		
	core.merchantBuySellItem            = new Position()
		.setPanelName("ENVP")
		.setOffsetW(-40)
		.setOffsetY(40);
		
	core.merchantBuySellAll             = new Position()
		.setPanelName("ENVP")
		.setOffsetW(-40)
		.setOffsetY(65);
		
	core.inscriptionBox                 = new Position()
		.setPanelName("EXAM")
		.setOffsetW(-30)
		.setOffsetH(-30);
	
	core.wandSpell                      = new Position()
		.setPanelName("COMB")
		.setOffsetX(25)
		.setOffsetY(50);
	
	core.closeAC                     = new Position()
		.setWindowXModifier(1)
		.setOffsetX(-10)
		.setOffsetY(-10);
		
	/*
		var panelSettings = uiSettings[ACWinConstants.EXAM];
		var myX = (panelSettings.x + panelSettings.w - 20);
		var myY = panelSettings.y + 15;
		fMouseMoveTo(myX, myY);
	*/
	
	// eslint-disable-next-line no-unused-vars
	function round(rnum, rlength) { // Arguments: number to round, number of decimal places
		if (rlength == null) rlength = 0;
		return Math.round(rnum * Math.pow(10, rlength)) / Math.pow(10, rlength);
	}
	
	// Move mouse to the lng lat coordinates on the F10 map.
	core.mapMouseMove = function mapMouseMove(lng, lat) {
		var panel = layout["PANS"];
		var mapYOffset = Math.ceil((panel.h - 372) / 2); // Expanding panel causes map to move to center.
		var startX = panel.x;
		var startY = panel.y + mapYOffset;
		var gotoX = 1.17766541 * lng + 152.37791057;
		var gotoY = -1.2210373 * lat + 202.20010795;
		var fX = startX + gotoX;
		var fY = startY + gotoY;
		skapi.MouseMove(fX, fY);
	};
	
	
	// Automation
	/*
	core.openSocialPanel = function openSocialPanel() {
		skapi.Keys("{F5}"); // Spellbook first, bow dance.
		skapi.Keys("{F3}"); // social panel
	};
	*/
	
	core.openFellowshipPanel = function openFellowshipPanel() {
		skapi.Keys(cmidToggleSpellComponentsPanel); // bow dance.
		skapi.Keys(cmidToggleFellowshipPanel);
	};
	
	core.setFellowshipName = function setFellowshipName(value) {
		core.fellowshipName.click();
		
		core.clearText();
		
		try {
			skapi.PasteSz(value);
		} catch (e) {}
	};
	
	core.clearText = function clearText() {
		skapi.Keys("{home}");
		skapi.Keys("{shift}", kmoUp);	// unshift just in case.
		skapi.Keys("{shift}", kmoDown);
		skapi.Keys("{pgup}");
		skapi.Keys("{shift}", kmoUp);
	};
	
	core.createFellowship = function createFellowship(name) {
		core.openFellowshipPanel();
		core.setFellowshipName(name);
		core.fellowshipCreate.click();
	};

	core.simpleUUID = function simpleUUID() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	};

	core.loadUILayout = function loadUILayout() {
		var uuid = core.simpleUUID();
		skapi.InvokeChatParser("/saveui " + uuid);
		skapi.sleep(100);
		if (core.exists(uuid)) {
			core.loadUI(uuid);
			core.deleteUI(uuid);
		}
	};

	return core;
}));

/*
TODO:
	Hotkey 1-9, 2 bars
	Allegiance, ignore ally requests
	Fellowship Auto accept
	Fellowship share xp
	Fellowship share loot
	Fellowship leader
	Fellowship Quit
	fellowship recruit
	Fellowship Dismiss
	Fellowship Disband
	Combat repeat attacks
	Combat auto target
	Combat keep in view
	
*/