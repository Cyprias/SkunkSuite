/********************************************************************************************\
	File:           SkunkConfig-1.0.js
	Purpose:        Manage saving and loading config files.
	Creator:        Cyprias
	Date:           07/02/2021
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkConfig-1.0";
var MINOR = 211228;

(function (factory) {
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory();
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkConfig10 = factory();
	}
}(function () {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}
	core.debugging = false;


	core.embeds = core.embeds || {};
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;

	/* eslint-disable */
	// OpenAsTextStream Modes
	var ForReading              = 1;  // Open a file for reading. You cannot write to this file
	var ForWriting              = 2;  // Open a file for writing
	var ForAppending            = 8;  // Open a file and write to the end of the file
	
	// OpenAsTextStream Formats
	var TristateFalse           = 0;  // Open the file as ASCII
	var Tristate                = -1; // Open the file as Unicode
	var TristateUseDefault      = -2; // Open the file using the system default 
	/* eslint-enable */

	core.console = function consoleLog() { // 'console' is taken by Skunkworks.
		skapi.OutputLine(core.MAJOR + " " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
	};
	
	core.debug = function debug() {
		if (core.debugging == true) {
			skapi.OutputLine(core.MAJOR + " [D] " + Array.prototype.slice.call(arguments).join('\t'), opmConsole);
		}
	};

	function embedMixins(script) {
		script.Config = core.Config;
		
		return script;
	}
	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	var fso;
	if (typeof jest === "undefined") {
		fso = new ActiveXObject("Scripting.FileSystemObject");
	}

	function existsSync(path) {
		return fso.FileExists(path) || fso.FolderExists(path);
	}

	function readFileSync(path, options) {
		if (!fso.FileExists(path)) {
			throw new core.Error(path + " does not exist.");
		}
		
		var mode = options && options.mode || ForReading;
		var format = options && options.format || TristateUseDefault;
		
		var file = fso.getFile(path);
		var hFile = file.OpenAsTextStream(mode, format);
		var dataString;
		if (!hFile.AtEndOfStream) {
			dataString = hFile.Read(file.Size); //http://www.4guysfromrolla.com/webtech/010401-1.shtml
		}
		hFile.close();
		return dataString;
	}
	
	function writeFileSync(path, data, options) {
		var mode = options && options.mode || ForWriting;
		var format = options && options.format || TristateUseDefault;
		var textStream = fso.OpenTextFile(path, mode, true, format);
		textStream.Write(data);
		textStream.Close();
	}

	function mkdirSync(path) {
		if (!fso.FolderExists(path)) {
			var parentFolder = fso.GetParentFolderName(path);
			if (parentFolder && !fso.FolderExists(parentFolder)) {
				mkdirSync(parentFolder);
			}
			try {
				fso.CreateFolder(path);
			} catch (e) {
				core.consoleLog("Error while creating folder: " + e);
				core.consoleLog("path: " + path);
			}
		}
	}

	function touchSync(path, options) {
		if (!existsSync(path)) {
			mkdirSync(fso.GetParentFolderName(path));
		}
		
		//var mode = options && options.mode || ForWriting;
		var format = options && options.format || TristateUseDefault;
		var textStream = fso.OpenTextFile(path, ForWriting, true, format);
		textStream.Close();
	}

	var Config = core.Config = function Config(folderPath, defaults, defaultProfile) {
		if (!(this instanceof Config)) {
			return new Config(folderPath, defaults, defaultProfile);
		}
		
		defaults = core.clone(defaults);

		//this._defaults = defaults;
		//this._defaultProfile = defaultProfile;
		
		this._folderPath = folderPath;
		
		/* eslint-disable */
		var keyServer       = this._keyServer       = skapi.szWorld;
		var keyCharacter    = this._keyCharacter    = keyServer + " - " + skapi.acoChar.szName;
		var keyAccount      = this._keyAccount      = skapi.szAccount;
		var keyMonarch      = this._keyMonarch      = skapi.acoChar.oidMonarch;
		var keyProfile      = this._keyProfile      = defaultProfile || core.getProfileKey(folderPath, keyCharacter) || "Default";
		/* eslint-enable */
		
		this.global     = defaults.global || {};
		this.server     = defaults.server || {};
		this.character  = defaults.character || {};
		this.profile    = defaults.profile || {};
		this.account    = defaults.account || {};
		this.monarch    = defaults.monarch || {};
	};

	// old function name.
	Config.prototype.reload = function reload() {
		return this.load();
	};

	Config.prototype.load = function load() {
		this.loadGlobal();
		this.loadServer();
		this.loadCharacter();
		this.loadAccount();
		this.loadMonarch();
		this.loadProfile();
		return this;
	};
	
	Config.prototype.loadServer = function loadServer() {
		var self = this;
		var pathServer = this._folderPath + "/server/" + this._keyServer + ".json";
		if (existsSync(pathServer)) {
			var text = readFileSync(pathServer);
			var data;
			try {
				data = JSON.parse(text);
			} catch (e) {
				core.consoleLog("Error reading server file: " + e);
				core.consoleLog("path: " + pathServer);
				return;
			}
			Object.keys(data).forEach(function(key) {
				self.server[key] = data[key];
			});
		}
		return this;
	};
	
	Config.prototype.loadCharacter = function loadCharacter() {
		var self = this;
		var pathCharacter = this._folderPath + "/character/" + this._keyCharacter + ".json";
		if (existsSync(pathCharacter)) {
			var text = readFileSync(pathCharacter);
			var data = JSON.parse(text);
			Object.keys(data).forEach(function(key) {
				self.character[key] = data[key];
			});
		}
		return this;
	};
	
	Config.prototype.loadAccount = function loadAccount() {
		var self = this;
		var pathAccount = this._folderPath + "/account/" + this._keyAccount + ".json";
		if (existsSync(pathAccount)) {
			var text = readFileSync(pathAccount);
			var data = JSON.parse(text);
			Object.keys(data).forEach(function(key) {
				self.account[key] = data[key];
			});
		}
		return this;
	};
	
	Config.prototype.loadMonarch = function loadMonarch() {
		var self = this;
		var pathMonarch = this._folderPath + "/monarch/" + this._keyMonarch + ".json";
		if (existsSync(pathMonarch)) {
			var text = readFileSync(pathMonarch);
			var data = JSON.parse(text);
			Object.keys(data).forEach(function(key) {
				self.monarch[key] = data[key];
			});
		}
		return this;
	};
	
	Config.prototype.setProfileKey = function setProfileKey(name) {
		var pathProfileKeys = this._folderPath + "/profileKeys.json";
		if (existsSync(pathProfileKeys)) {
			var text = readFileSync(pathProfileKeys);
			var profileKeys = JSON.parse(text);
			//this._keyProfile = profileKeys[this._keyCharacter] || this._keyProfile;
			this._keyProfile = profileKeys[this._keyCharacter] = name;
			writeFileSync(pathProfileKeys, JSON.stringify(profileKeys, undefined, "\t"));
		}
		
		return this;
	};

	Config.prototype.loadProfile = function loadProfile() {
		var self = this;
		
		var pathProfileKeys = this._folderPath + "/profileKeys.json";
		if (existsSync(pathProfileKeys)) {
			var text = readFileSync(pathProfileKeys);
			var profileKeys = JSON.parse(text);
			this._keyProfile = profileKeys[this._keyCharacter] || this._keyProfile;
		}

		var pathProfile = this._folderPath + "/profile/" + this._keyProfile + ".json";
		if (existsSync(pathProfile)) {
			var text = readFileSync(pathProfile);
			var data = JSON.parse(text);
			Object.keys(data).forEach(function(key) {
				self.profile[key] = data[key];
			});
		}
		return this;
	};
	
	Config.prototype.loadGlobal = function loadGlobal() {
		var self = this;
		var pathGlobal = this._folderPath + "/global.json";
		if (existsSync(pathGlobal)) {
			var text = readFileSync(pathGlobal);
			var data = JSON.parse(text);
			Object.keys(data).forEach(function(key) {
				self.global[key] = data[key];
			});
		}
		return this;
	};
	
	
	
	Config.prototype.createProfile = function createProfile(name, defaults) {
		var self = this;
		
		//this._keyProfile = name;
		//this.profile = core.clone(defaults);
		
		var pathProfile = this._folderPath + "/profile/" + name + ".json";
		touchSync(pathProfile);
		writeFileSync(pathProfile, JSON.stringify(defaults, undefined, "\t"));
		
		return this;
	};
	
	Config.prototype.deleteProfile = function deleteProfile(name) {
		var pathProfile = this._folderPath + "/profile/" + name + ".json";
		fso.DeleteFile(pathProfile);
		return this;
	};
	
	Config.prototype.save = function save() {
		var self = this;
		this.saveGlobal();
		this.saveServer();
		this.saveCharacter();
		this.saveAccount();
		this.saveMonarch();
		this.saveProfile();
		return this;
	};
	
	Config.prototype.saveProfile = function saveProfile() {
		var self = this;
		var pathProfileKeys = this._folderPath + "/profileKeys.json";
		var profileKeys = {};
		if (existsSync(pathProfileKeys)) {
			var text = readFileSync(pathProfileKeys);
			profileKeys = JSON.parse(text);
		} else {
			touchSync(pathProfileKeys);
		}
		profileKeys[this._keyCharacter] = this._keyProfile;
		writeFileSync(pathProfileKeys, JSON.stringify(profileKeys, undefined, "\t"));

		var pathProfile = this._folderPath + "/profile/" + this._keyProfile + ".json";
		var profile = {};
		if (existsSync(pathProfile)) {
			var text = readFileSync(pathProfile);
			profile = JSON.parse(text);
		} else {
			touchSync(pathProfile);
		}
		Object.keys(this.profile).forEach(function(key) {
			profile[key] = self.profile[key];
		});
		writeFileSync(pathProfile, JSON.stringify(profile, undefined, "\t"));
		return this;
	};
	
	Config.prototype.saveGlobal = function saveGlobal() {
		var self = this;
		if (Object.keys(this.global).length > 0) {
			var pathGlobal = this._folderPath + "/global.json";
			var global = {};
			if (existsSync(pathGlobal)) {
				var text = readFileSync(pathGlobal);
				global = JSON.parse(text);
			} else {
				touchSync(pathGlobal);
			}
			Object.keys(this.global).forEach(function(key) {
				global[key] = self.global[key];
			});
			writeFileSync(pathGlobal, JSON.stringify(global, undefined, "\t"));
		}
		return this;
	};
	
	Config.prototype.saveServer = function saveServer() {
		var self = this;
		if (Object.keys(this.server).length > 0) {
			var pathServer = this._folderPath + "/server/" + this._keyServer + ".json";
			var server = {};
			if (existsSync(pathServer)) {
				var text = readFileSync(pathServer);
				server = JSON.parse(text);
			} else {
				touchSync(pathServer);
			}
			Object.keys(this.server).forEach(function(key) {
				server[key] = self.server[key];
			});
			writeFileSync(pathServer, JSON.stringify(server, undefined, "\t"));
		}
		return this;
	};

	Config.prototype.saveCharacter = function saveCharacter() {
		var self = this;
		if (Object.keys(this.character).length > 0) {
			var pathCharacter = this._folderPath + "/character/" + this._keyCharacter + ".json";
			var character = {};
			if (existsSync(pathCharacter)) {
				var text = readFileSync(pathCharacter);
				character = JSON.parse(text);
			} else {
				touchSync(pathCharacter);
			}
			Object.keys(this.character).forEach(function(key) {
				character[key] = self.character[key];
			});
			writeFileSync(pathCharacter, JSON.stringify(character, undefined, "\t"));
		}
		return this;
	};

	Config.prototype.saveAccount = function saveAccount() {
		var self = this;
		if (Object.keys(this.account).length > 0) {
			var pathAccount = this._folderPath + "/account/" + this._keyAccount + ".json";
			var account = {};
			if (existsSync(pathAccount)) {
				var text = readFileSync(pathAccount);
				account = JSON.parse(text);
			} else {
				touchSync(pathAccount);
			}
			Object.keys(this.account).forEach(function(key) {
				account[key] = self.account[key];
			});
			writeFileSync(pathAccount, JSON.stringify(account, undefined, "\t"));
		}
		return this;
	};
	
	Config.prototype.saveMonarch = function saveMonarch() {
		var self = this;
		if (Object.keys(this.monarch).length > 0) {
			var pathMonarch = this._folderPath + "/monarch/" + this._keyMonarch + ".json";
			var monarch = {};
			if (existsSync(pathMonarch)) {
				var text = readFileSync(pathMonarch);
				monarch = JSON.parse(text);
			} else {
				touchSync(pathMonarch);
			}
			Object.keys(this.monarch).forEach(function(key) {
				monarch[key] = self.monarch[key];
			});
			writeFileSync(pathMonarch, JSON.stringify(monarch, undefined, "\t"));
		}
		return this;
	};

	Config.prototype.getProfiles = function getProfiles() {
		var pathProfile = this._folderPath + "/profile";
		if (!fso.FolderExists(pathProfile)) {
			throw new core.Error(pathProfile + " does not exist.");
		}
		var list = [];
		var f   =   fso.GetFolder(pathProfile);
		var files = new Enumerator(f.files);
		var file;
		// eslint-disable-next-line no-restricted-syntax
		for (; !files.atEnd();   files.moveNext()) {
			file = files.item();
			list.push(fso.GetBaseName(file.Path));
		}
		return list;
	};
	
	Config.prototype.switchProfile = function switchProfile(name) {
		this._keyProfile      = name;
		var pathProfileKeys = this._folderPath + "/profileKeys.json";
		var profileKeys = {};
		if (existsSync(pathProfileKeys)) {
			var text = readFileSync(pathProfileKeys);
			profileKeys = JSON.parse(text);
		} else {
			touchSync(pathProfileKeys);
		}
		profileKeys[this._keyCharacter] = this._keyProfile;
		writeFileSync(pathProfileKeys, JSON.stringify(profileKeys, undefined, "\t"));
		
		var pathProfile = this._folderPath + "/profile/" + this._keyProfile + ".json";
		if (existsSync(pathProfile)) {
			var text = readFileSync(pathProfile);
			this.profile = JSON.parse(text);
		} else {
			this.profile = {};
		}
		
		return this;
	};
	
	Config.prototype.copyProfile = function copyProfile(name) {
		var pathProfile = this._folderPath + "/profile/" + name + ".json";
		if (existsSync(pathProfile)) {
			var text = readFileSync(pathProfile);
			this.profile = JSON.parse(text);
		}
		return this;
	};
	
	core.clone = function clone(obj) {
		// Clone a object.
		var copy;

		// Handle the 3 simple types, and null or undefined
		if (null == obj || "object" != typeof obj) return obj;

		// Handle Date
		if (obj instanceof Date) {
			copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}

		// Handle Array
		if (obj instanceof Array) {
			copy = [];
			// eslint-disable-next-line no-restricted-syntax
			for (var i = 0, len = obj.length; i < len; i++) {
				copy[i] = core.clone(obj[i]);
			}
			return copy;
		}

		// Handle Object
		if (obj instanceof Object) {
			copy = {};
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = core.clone(obj[attr]);
			}
			return copy;
		}

		throw new Error("Unable to copy obj! Its type isn't supported.");
	};

	
	core.getProfileKey = function getProfileKey(folderPath, name) {
		var pathProfileKeys = folderPath + "/profileKeys.json";
		if (existsSync(pathProfileKeys)) {
			var text = readFileSync(pathProfileKeys);
			var profileKeys = JSON.parse(text);
			return profileKeys[name];
		}
	};
	

	return core;
}));

/*
	var now = new Date();
	var config = new SkunkConfig10.Config("./configs/Main").reload();

	core.info("global.now: " + config.global.now);
	config.global.now = now;
	
	core.info("server.now: " + config.server.now);
	config.server.now = now;
	
	core.info("character.now: " + config.character.now);
	config.character.now = now;
	
	core.info("account.now: " + config.account.now);
	config.account.now = now;

	core.info("monarch.now: " + config.monarch.now);
	config.monarch.now = now;
	
	core.info("profile.now: " + config.profile.now);
	config.profile.now = now;

	config.save();
*/