/********************************************************************************************\
	Tests using Jest (jestjs.io). Requires Node.js with Jest installed.
\********************************************************************************************/

function SKAPI() {
	this.handlers = {};
	this.acoChar = {
		szName: "PLAYER"
	};
	this.szWorld = "WORLD";
}
SKAPI.prototype.AddHandler = function AddHandler(evid, handler) {
	var handlers = this.handlers;
	handlers[evid] = handlers[evid] || [];
	handlers[evid].push(handler);
};
SKAPI.prototype.RemoveHandler = function RemoveHandler(evid, handler) {
	var handlers = this.handlers;
	if (!handlers[evid]) return;
	
	if (evid == evidNil) {
		// Remove all events linked to handler.
		
		var h;
		for (var e in handlers) {
			if (!handlers.hasOwnProperty(e)) continue;
			for (var i = handlers[e].length - 1; i >= 0; i--) {
				h = handlers[e][i];
				if (h == handler) {
					handlers[e].splice(i, 1);
					continue;
				}
			}
		}
	} else if (handlers[evid]) {
		for (var i = handlers[evid].length - 1; i >= 0; i--) {
			h = handlers[evid][i];
			if (h == handler) {
				handlers[evid].splice(i, 1);
				continue;
			}
		}
	}
};
SKAPI.prototype.TimerNew = function TimerNew() {
	return {};
};
SKAPI.prototype.OutputLine = function OutputLine() {};
SKAPI.prototype.SelectAco = function SelectAco() {};
SKAPI.prototype.SetStackCount = function SetStackCount() {};
SKAPI.prototype.spoofEvent = function spoofEvent(evid, eventName, args) {
	var handlers = this.handlers;
	if (!handlers[evid]) return;
	var handler;
	for (var i = 0; i < handlers[evid].length; i++) {
		handler = handlers[evid][i];
		if (typeof handler[eventName] === "function") {
			handler[eventName].apply(handler, args);
		}
	}
	
	// Most of our functions call their callback in a timer so that they're called asyncly.
	jest.advanceTimersByTime(1); 
};

skapi = global_skapi = new SKAPI(); // global for modules to see.

// Load skapi variables.
require("../../SkapiDefs.js");

// Load SkunkTrader modules.
EventEmitter = require("../EventEmitter.js");
require("../LibStub.js");
require("../SkunkScript-1.0/SkunkScript-1.0.js");
require("../SkunkTimer-1.0/SkunkTimer-1.0.js");
require("../SkunkEvent-1.0/SkunkEvent-1.0.js");
require("../SkunkAssess-1.0/SkunkAssess-1.0.js");
require("../SkunkComm-1.0/SkunkComm-1.0.js");
require("../SkunkSynchronous-1.0/SkunkSynchronous-1.0.js");
require("../SkunkLogger-1.0/SkunkLogger-1.0.js");
jstoxml = require("../SkunkSchema-1.0/jstoxml.js");
require("../SkunkSchema-1.0/SkunkSchema-1.0.js");
Stream = require("../Stream.js");
require("../SkunkFS-1.0/SkunkFS-1.0.js");

// Use fake timers.
jest.useFakeTimers();

beforeEach(() => {
	skapi = new SKAPI();
});

describe('Test SkunkComm', () => {
	var SkunkComm = LibStub("SkunkComm-1.0");
	
	test('Test receiving OnTell message', function(done) {
		jest.spyOn(SkunkComm.emitter, 'emit').mockImplementationOnce(function(event, params) {
			//console.log("<emit 1>", event, params);
			expect(params.evid).toBe(evidOnTell);
			expect(params.acoSender).toMatchObject(acoSender);
			expect(params.szMsg).toBe(szMsg);
			expect(params.szSender).toBe(acoSender.szName);
			expect(params.szReceiver).toBe(acoReceiver.szName);
			done();
		});
		
		var acoSender = {
			szName: "SENDER"
		};
		var acoReceiver = {
			szName: "RECEIVER"
		};
		var szMsg = "Hello";
		global_skapi.spoofEvent(evidOnTell, "OnTell", [acoSender, acoReceiver, szMsg]);
	});
	
	test('Test receiving OnTell multi message', function(done) {
		jest.spyOn(SkunkComm.emitter, 'emit').mockImplementationOnce(function(event, params) {
			//console.log("<emit 2>", event, params);
			expect(params.evid).toBe(evidOnTell);
			expect(params.acoSender).toMatchObject(acoSender);
			expect(params.szMsg).toBe("ABCDEFGHI");
			expect(params.szSender).toBe(acoSender.szName);
			expect(params.szReceiver).toBe(acoReceiver.szName);
			done();
		});
		
		var acoSender = {
			szName: "SENDER"
		};
		var acoReceiver = {
			szName: "RECEIVER"
		};
		global_skapi.spoofEvent(evidOnTell, "OnTell", [acoSender, acoReceiver, String.fromCharCode(1) + "ABC"]);
		global_skapi.spoofEvent(evidOnTell, "OnTell", [acoSender, acoReceiver, String.fromCharCode(2) + "DEF"]);
		global_skapi.spoofEvent(evidOnTell, "OnTell", [acoSender, acoReceiver, String.fromCharCode(3) + "GHI"]);
	});
});
