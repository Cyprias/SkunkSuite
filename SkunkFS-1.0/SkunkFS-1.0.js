/********************************************************************************************\
	File:           SkunkFS-1.0.js
	Purpose:        File system helper for Skunkworks.
	Creator:        Cyprias
	Date:           09/17/2017
	License:        MIT License	(http://opensource.org/licenses/MIT)
	Requires:       Skunkworks (http://skunkworks.sourceforge.net/)
\*********************************************************************************************/

var MAJOR = "SkunkFS-1.0";
var MINOR = 200509;

(function (factory) {
	var params = {};
	params.Stream = (typeof Stream !== "undefined" && Stream) || require("SkunkSuite\\Stream");
	
	// Check if script was loaded via require().
	if (typeof module === 'object' && module.exports) {
		module.exports = factory(params);
	} else {
		// Script was loaded via swx file, set as a global object.
		// eslint-disable-next-line no-undef
		SkunkFS10 = factory(params);
	}
}(function (params) {
	var core;
	if (typeof LibStub !== "undefined") {
		core = LibStub.newLibrary(MAJOR, MINOR);
		if (!core) return LibStub(MAJOR);
	} else {
		core = {};
	}

	var Stream = params.Stream;

	// OpenAsTextStream Modes
	var ForReading              = 1;  // Open a file for reading. You cannot write to this file
	var ForWriting              = 2;  // Open a file for writing
	var ForAppending            = 8;  // Open a file and write to the end of the file
	
	// OpenAsTextStream Formats
	/* eslint-disable */
	var TristateFalse           = 0;  // Open the file as ASCII
	var Tristate                = -1; // Open the file as Unicode
	var TristateUseDefault      = -2; // Open the file using the system default 
	/* eslint-enable */
	
	core.MAJOR = MAJOR;
	core.MINOR = MINOR;
	core.Error = Error;
	
	core.embeds = core.embeds || {};

	core.consoleLog = function consoleLog() { // 'console' is taken by Skunkworks.
		var args = Array.prototype.slice.call(arguments);
		skapi.OutputLine(args.join('\t'), opmConsole);
	};
	
	var mixinNames = [
		"appendFileSync",
		"existsSync",
		"mkdirSync",
		"readdirSync",
		"readdirSync2",
		"readFileSync",
		"realpathSync",
		"writeFileSync",
		"statSync",
		"watchFile",
		"unwatchFile",
		"unlinkSync",
		"createWriteStream",
		"createReadStream",
		"renameSync",
		"compressFileSync",
		"touchSync",
		"copyFileSync"
	];

	var fso;
	if (typeof jest === "undefined") {
		fso = new ActiveXObject("Scripting.FileSystemObject");
	}

	function embedMixins(script) {
		var name;
		for (var i = 0; i < mixinNames.length; i++) {
			name = mixinNames[i];
			script[name] = core[name];
		}

		/*
		if (typeof script.on === "function") {
			script.on("onDisable", function() {
				core.consoleLog("event onDisable");
				//script.unregisterAllEvents();
			});
		}
		*/
		
		return script;
	}

	
	core.embed = function embed(script) {
		this.embeds[script] = true;
		embedMixins(script);
		return script;
	};

	if (typeof core.emitter !== "undefined") {
		//core.emitter.on("onEmbedDisable", function(/*script*/) {});
	}
	
	/*
	 * Appends a string to a file.
	 *
	 * @param {string} path The path of the file.
	 * @param {string} data The string to be written.
	 * @param {object} options Options related to accessing the file.
	 * @return {undefined}
	 */
	core.appendFileSync = function appendFileSync(path, data, options) {
	//	skapi.OutputLine("<appendFileSync> path: " + path, opmConsole);
		var mode = options && options.mode || ForAppending;
		var format = options && options.format || TristateUseDefault;
		var textStream;
		
		try {
			textStream = fso.OpenTextFile(path, mode, true, format);
			textStream.WriteLine(data);
			textStream.Close();
		} catch (e) {
			core.consoleLog(core.MAJOR, "Error writing to " + path);
			core.consoleLog(core.MAJOR, e);
		}
	};
	
	/*
	 * Returns true/false if a file exists.
	 *
	 * @param {string} path The path of the file.
	 * @return {boolean} whether the file exists.
	 */
	core.existsSync = function existsSync(path) {
		return fso.FileExists(path) || fso.FolderExists(path);
	};
	
	/*
	 * parentFolderExistsSync() returns true/false if the parent folder of a file path exists.
	 *
	 * @param {string} path The path of the file.
	 * @return {boolean} whether the folder exists.
	 */
	core.parentFolderExistsSync = function parentFolderExistsSync(path) {
		var parentFolder = fso.GetParentFolderName(path);
		return fso.FolderExists(parentFolder);
	};
	
	/*
	 * Creates a directory.
	 *
	 * @param {string} path The path of the file.
	 * @return {undefined}
	 */
	core.mkdirSync = function mkdirSync(path) {
		if (!fso.FolderExists(path)) {
			var parentFolder = fso.GetParentFolderName(path);
			if (parentFolder && !fso.FolderExists(parentFolder)) {
				mkdirSync(parentFolder);
			}
			try {
				fso.CreateFolder(path);
			} catch (e) {
				core.consoleLog("Error while creating folder: " + e);
				core.consoleLog("path: " + path);
			}
		}
	};
	
	/*
	 * Returns a array of file names in a directory.
	 *
	 * @param {string} path The path of the file.
	 * @return {array} file names.
	 */
	core.readdirSync = function readdirSync(path) {
		
		if (!fso.FolderExists(path)) {
			throw new core.Error(path + " does not exist.");
		}
		var list = [];
		
		var f   =   fso.GetFolder(path);
		var files = new Enumerator(f.files);
		for (; !files.atEnd();   files.moveNext()) {
			list.push(files.item().Path);
		}
		return list;
	};
	
	core.readdirSync2 = function factory() {
		function _clone(file) {
			var stats = {};
			stats.Attributes            = file.Attributes;
			stats.DateCreated           = file.DateCreated;
			stats.DateLastAccessed      = file.DateLastAccessed;
			stats.DateLastModified      = file.DateLastModified;
			stats.Name                  = file.Name;
			stats.Path                  = file.Path;
			stats.Directory             = file.ParentFolder.Path;
			stats.Size                  = file.Size;
			stats.Type                  = file.Type;
			stats.ExtensionName         = fso.GetExtensionName(file.Path);
			stats.BaseName              = fso.GetBaseName(file.Path);
			return stats;
		}
		return function readdirSync2(path) {
			
			if (!fso.FolderExists(path)) {
				throw new core.Error(path + " does not exist.");
			}
			var list = [];
			
			var f   =   fso.GetFolder(path);
			
			var folders = new Enumerator(f.SubFolders);
			var file;
			for (; !folders.atEnd();   folders.moveNext()) {
				file = folders.item();
				list.push(_clone(file));
			}

			var files = new Enumerator(f.files);
			for (; !files.atEnd();   files.moveNext()) {
				file = files.item();
				list.push(_clone(file));
			}
			
			return list;
		};
	}();

	/*
	 * Returns the contents of a file.
	 *
	 * @param {string} path The path of the file.
	 * @param {object} options Options related to accessing the file.
	 * @return {string} The contents of the file.
	 */
	core.readFileSync = function readFileSync(path, options) {
	//	skapi.OutputLine("<readFileSync> path: " + path, opmConsole);
		if (!fso.FileExists(path)) {
			throw new core.Error(path + " does not exist.");
		}
		
		var mode = options && options.mode || ForReading;
		var format = options && options.format || TristateUseDefault;
		
		var file = fso.getFile(path);
		var hFile = file.OpenAsTextStream(mode, format);
		var dataString;
		if (!hFile.AtEndOfStream) {
			dataString = hFile.Read(file.Size); //http://www.4guysfromrolla.com/webtech/010401-1.shtml
		}
		hFile.close();
		return dataString;
	};
	
	/*
	 * Returns the resolved path of a file.
	 *
	 * @param {string} path The path of the file.
	 * @return {string} The resolved path.
	 */
	core.realpathSync = function realpathSync(path) {
		if (fso.FileExists(path)) {
			return fso.getFile(path).Path;
		}
	};
	
	/*
	 * Writes a string to a file.
	 *
	 * @param {string} path The path of the file.
	 * @param {string} data The string to be written.
	 * @param {object} options Options related to accessing the file.
	 * @return {undefined}
	 */
	core.writeFileSync = function writeFileSync(path, data, options) {
		var mode = options && options.mode || ForWriting;
		var format = options && options.format || TristateUseDefault;
		var textStream = fso.OpenTextFile(path, mode, true, format);
		textStream.Write(data);
		textStream.Close();
	};
	
	/*
	 * Return a object of file stats.
	 *
	 * @param {string} path The path of the file.
	 * @return {object} stats
	 */
	core.statSync = function statSync(path) {
		if (!fso.FileExists(path) && !fso.FolderExists(path)) {
			return;
		}
		
		var f;
		if (fso.FileExists(path)) {
			f = fso.getFile(path);
		} else if (fso.FolderExists(path)) {
			f = fso.getFolder(path);
		}

		
		var stats = {};
		stats.Attributes            = f.Attributes;
		stats.DateCreated           = f.DateCreated;
		stats.DateLastAccessed      = f.DateLastAccessed;
		stats.DateLastModified      = f.DateLastModified;
		stats.Name                  = f.Name;
		stats.Path                  = f.Path;
		stats.Directory             = f.ParentFolder.Path;
		
		//stats.ShortName             = file.ShortName;
		//stats.ShortPath             = file.ShortPath;
		stats.Size                  = f.Size;
		stats.Type                  = f.Type;
		
		stats.ExtensionName         = fso.GetExtensionName(path);
		stats.BaseName              = fso.GetBaseName(path);
		
		//stats.AbsolutePathName      = fso.GetAbsolutePathName(path); // Same as f.Path
		//stats.ParentFolderName      = fso.GetParentFolderName(path); // Same as f.Directory

		return stats;
	};
	
	/*
	 * Watch a file for modifications and fire a callback.
	 *
	 * @param {string} filename The path of the file.
	 * @param {object} options Options for the monitor.
	 * @param {function} listener(err, currStats, prevStats) Function to call upon file modification. 
	 * @return {undefined}
	 */
	core.watchFile = function watchFile(filename, options, listener) {
		if (!fso.FileExists(filename)) {
			throw new core.Error(filename + " does not exist.");
		}

		var persistent = true;
		var interval = 1;// in seconds
		if (typeof options !== "undefined") {
			if (typeof options.persistent !== "undefined") {
				persistent = options.persistent;
			}
			if (typeof options.interval !== "undefined") {
				interval = options.interval;
			}
		}
		
		
		var file = fso.getFile(filename);
		var path = file.Path.replace(/\\/g, "\\\\");

		// Stats to give to the listener as the previous stats.
		var prev = core.statSync(filename);
		
		// Register our events.
		var events = {};
		events.OnObjectReady = function OnObjectReady(/*objWbemObject, objWbemAsyncContext*/) {
			var curr = core.statSync(filename);
			listener(undefined, curr, prev);
			prev = curr;
			
			if (persistent == false) {
				core.unwatchFile(filename, listener);
				
				//oSink.Cancel(); // Stop	
			}
		};
		
		/*events.OnCompleted = function OnCompleted(HResult, oErr, oCtx) {
			// cancled
		};*/
		
		// Create our sink.
		var oSink = new ActiveXObject("WbemScripting.SWbemSink"); 
		
		// Attach our events our events. We use eval due to Skunkworks not having WScript.CreateObject to do it for us.
		eval("function oSink::OnObjectReady(objWbemObject, objWbemAsyncContext) {return events.OnObjectReady(objWbemObject, objWbemAsyncContext);}");
		
		//eval("function oSink::OnCompleted(HResult, oErr, oCtx) {return events.OnCompleted(HResult, oErr, oCtx);}");
		
		// Get service.
		var strComputer = ".";
		var oSvc = GetObject("winmgmts:{impersonationLevel=impersonate}!\\\\" + strComputer + "\\root\\CIMV2");
		
		// Create our query to monitor the file.
		var strQuery = "SELECT * FROM __InstanceModificationEvent";
		strQuery += " WITHIN " + interval;
		strQuery += " WHERE TargetInstance ISA 'CIM_DataFile'";
		strQuery += " AND TargetInstance.Name='" + path + "'";

		// Execute.
		oSvc.ExecNotificationQueryAsync(oSink, strQuery);
		
		core.watchSinks[filename] = core.watchSinks[filename] || [];
		core.watchSinks[filename].push({listener: listener, oSink: oSink});
	};
	
	core.watchSinks = core.watchSinks = {};
	core.unwatchFile = function unwatchFile(filename, listener) {
		if (typeof core.watchSinks[filename] === "undefined") {
			return;
		}
		
		for (var i = core.watchSinks[filename].length - 1; i >= 0; i--) {
			if (typeof listener === "undefined" || listener == core.watchSinks[filename][i].listener) {
				core.watchSinks[filename][i].oSink.Cancel();
				core.watchSinks[filename].splice(i, 1);
				continue;
			}
		}
		
		if (core.watchSinks[filename].length == 0) {
			delete core.watchSinks[filename];
		}
	};
	
	/*
	 * Delete a file.
	 *
	 * @param {string} path The path of the file.
	 * @return {undefined}
	 */
	core.unlinkSync = function unlinkSync(path) {
		if (fso.FileExists(path)) {
			fso.DeleteFile(path);
		} else if (fso.FolderExists(path)) {
			fso.DeleteFolder(path);
		}
	};
	
	/*
	 * Write a file line by line from a stream.
	 *
	 * @param {string} path: The path of the file.
	 * @param {string} options: File options.
	 * @return {Stream}
	 */
	core.createWriteStream = function createWriteStream(path, options) {
		var mode = options && options.mode || ForWriting;
		var format = options && options.format || TristateUseDefault;

		var textStream;

		var opened = false;
		
		var stream = new Stream();
		stream.on("data", function(data) {
			if (opened != true) {
				textStream = fso.OpenTextFile(path, mode, true, format);
				opened = true;
			}
			textStream.WriteLine(data);
		});
		
		stream.on("end", function(/*data*/) {
			textStream.Close();
			opened = false;
		});
		
		return stream;
	};
	
	/*
	 * Read a file line by line to a stream.
	 *
	 * @param {string} path: The path of the file.
	 * @param {string} options: File options.
	 * @return {Stream}
	 */
	core.createReadStream = function createReadStream(path, options) {
		var mode = options && options.mode || ForReading;
		var format = options && options.format || TristateUseDefault;

		var file = fso.getFile(path);
		var hFile = file.OpenAsTextStream(mode, format);
		
		var stream = new Stream();
		var started;
		setImmediate(function readLine() {
			started = new Date().getTime();
			while (!hFile.AtEndOfStream && (new Date().getTime() - started) < 100) { // Limit ourselves to 100ms of CPU time each loop (~600 lines), then repeat again after a waitevent.
				stream.write(hFile.ReadLine());
			}
			
			if (!hFile.AtEndOfStream) {
				setImmediate(readLine);
			} else {
				hFile.close();
				stream.end();
			}
		});

		return stream;
	};

	/*
	 * Rename a file.
	 *
	 * @param {string} oldPath: The path to the file.
	 * @param {string} newPath: The new path for the file.
	 * @return {none}
	 */
	core.renameSync = function renameSync(oldPath, newPath) {
		fso.MoveFile(oldPath, newPath);
	};

	/*
	 * Compress (zip) a file.
	 *
	 * @param {string} oldPath: The path to the file.
	 * @param {string} newPath: The new path for the file with a .zip extention.
	 * @return {none}
	 */
	core.compressFileSync = function compressFileSync(sourcePath, destPath) {
		// Get source file object.
		var sourceFile = fso.getFile(sourcePath);
		
		var zipZile;
		if (!fso.FileExists(destPath))  {
			// Create the zip file on disk.
			zipZile = fso.CreateTextFile(destPath, true);
			
			//zipZile.Write('PK\05\06' + new Array(19).join('\0'));
			
			zipZile.Write('PK' + String.fromCharCode(5) + String.fromCharCode(6) + new Array(19).join('\0'));
			zipZile.Close();
		} else {
			zipZile = fso.OpenTextFile(destPath, 1, true);
		}
		
		// Shell object.
		var shell = new ActiveXObject("Shell.Application");

		// Create zip 'folder' object.
		var zipFolder = shell.NameSpace(fso.getFile(destPath).Path);
		
		// Move source file into zip folder.
		zipFolder.MoveHere(sourceFile.Path, 0);
		
		// Wait for file to be moved...
		var started = new Date().getTime();
		var elapsed;
		do {
			elapsed = new Date().getTime() - started;
			if (elapsed >= 60000) break; // Got stuck once compressing file, just bail after a minute and maybe we'll try again later.
			skapi.Sleep(100);
		} while (fso.FileExists(sourcePath));
	};
	
	/*
	 * Create a empty file.
	 *
	 * @param {string} path: The path to the file.
	 * @return {none}
	 */
	core.touchSync = function touchSync(path, options) {
		if (!core.existsSync(path)) {
			core.mkdirSync(fso.GetParentFolderName(path));
		}
		
		//var mode = options && options.mode || ForWriting;
		var format = options && options.format || TristateUseDefault;
		var textStream = fso.OpenTextFile(path, ForWriting, true, format);
		textStream.Close();
	};
	
	/*
	 * Copy a file from one path to another.
	 *
	 * @param {string} src: File path to copy.
	 * @param {string} dest: Path to copy file to.
	 * @param {boolean} overwrite: Overwrite destination if it already exists.
	 * @return {none}
	 */
	core.copyFileSync = function copyFileSync(src, dest, overwrite) {

		if (fso.FileExists(src)) {
			if (!fso.FileExists(dest) || overwrite == true) {
				return fso.CopyFile(src, dest, overwrite);
			}
		} else if (fso.FolderExists(src)) {
			if (!fso.FolderExists(dest) || overwrite == true) {
				return fso.CopyFolder(src, dest, overwrite);
			}
		}
	};

	return core;
}));